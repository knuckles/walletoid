<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2017, The walletoid authors.
  ~ All rights reserved.
  ~ Use of this source code is governed by a BSD-style license that can be
  ~ found in the LICENSE file.
  -->

<resources>
    <string name="about_app">About application</string>
    <string name="add_new_category">Add new category</string>
    <string name="add_new_currency">Add new currency</string>
    <string name="all_categories">All</string>
    <string name="app_description">A simple personal finance tracker.</string>
    <string name="app_version">version %s</string>
    <string name="are_you_sure">This can\'t be undone.\nAre you sure?</string>
    <string name="autorepeat">Autorepeat</string>
    <string name="backup_created">Backup created.</string>
    <string name="backup_database">Backup database</string>
    <string name="backup_error">Backup error!</string>
    <string name="backup_restored">Backup loaded.</string>
    <string name="backups_require_storage_access">Backups require storage access.</string>
    <string name="cant_remove_category">This category can\'t be removed.</string>
    <string name="cant_remove_currency">Can\'t remove current reference currency.</string>
    <string name="cat_name_duplicate">Category name duplication: %s</string>
    <string name="cat_removal_db_error">Couldn\'t remove category due to database error.</string>
    <string name="cat_removed">Category removed.</string>
    <string name="categories">Categories</string>
    <string name="category_name">Category name</string>
    <string name="color">Color</string>
    <string name="confirm_records_removal">Really remove selected records?</string>
    <string name="confirm_repeated_record">
        This record is set on repeat. To confirm its addition, press the \"%s\" button.
    </string>
    <string name="csv_db_error">database error</string>
    <string name="csv_io_error">IO error</string>
    <string name="csv_parse_error">could not parse input</string>
    <string name="csv_read_progress">Read %d records</string>
    <string name="csv_read_error">Failed reading: %s</string>
    <string name="csv_unexpected_error">unexpected error</string>
    <string name="currencies">Currencies</string>
    <string name="currencies_dlg_hint">Select reference (main) currency.\nUse long tap for context
        actions.
    </string>
    <string name="currency_name">Currency name</string>
    <string name="currency_rate">Rate</string>
    <string name="currency_symbol">Symbol</string>
    <string name="db_import">Import CSV</string>
    <string name="duplicate">Duplicate</string>
    <string name="edit">Edit</string>
    <string name="edit_category">Edit category: %s</string>
    <string name="edit_currency">Edit currency: %s</string>
    <string name="err_viewing">Text viewer is required to read this file.</string>
    <string name="expense_title">Expense</string>
    <string name="expenses">Expenses</string>
    <string name="expenses_breakdown_per">Breakdown of expenses per %s:</string>
    <string name="import_cancelling">Cancelling import. Please wait…</string>
    <string name="import_in_progress">Importing data…</string>
    <string name="import_this_q">Import this?</string>
    <string name="income_title">Income</string>
    <string name="incomes">Incomes</string>
    <string name="invalid_num_format">Invalid number format.</string>
    <string name="manage_categories">Manage categories</string>
    <string name="master_rec_label_text">Parent record:</string>
    <string name="month_dlg_title">Pick a month</string>
    <string name="move_records_to">Move records to:</string>
    <string name="need_file_chooser">File manager application is required to choose files.</string>
    <string name="new_cat_marker">[new]</string>
    <string name="no_data_for_period">No data for selected period.</string>
    <string name="no_scheduled_records_title">No schedule</string>
    <string name="no_scheduled_records_hint">
        Schedule a record from its edit screen by checking the corresponding checkbox.
    </string>
    <string name="no_records_in_month">
        No records conforming the filter.\nMake a record by tapping one of the buttons below.
    </string>
    <string name="num_categories_details">Expense: %1$d; Income: %2$d</string>
    <string name="num_records">Records</string>
    <string name="num_records_details">Total: %1$d; Autorepeated: %2$d</string>
    <string name="each_year">Year</string>
    <string name="each_month">Month</string>
    <string name="each_week">Week</string>
    <string name="each_day">Day</string>
    <string name="per_month">month</string>
    <string name="per_week">week</string>
    <string name="per_day">day</string>
    <string name="pick_category">Pick a category</string>
    <string name="pick_color">Pick a color</string>
    <string name="preferences">Preferences</string>
    <string name="rebuild_rec_dict">Rescan record descriptions</string>
    <string name="rec_category">Category</string>
    <string name="rec_descr_hint">Purpose</string>
    <string name="rec_done">Done</string>
    <string name="rec_sorting_order">Records display order:</string>
    <string name="rec_sorting_old2new">from older to newer</string>
    <string name="rec_sorting_new2old">from newer to older</string>
    <string name="rec_sum_hint">Sum</string>
    <string name="record">Record</string>
    <string name="records_during">\"%1$s\" in %2$s</string>
    <string name="reference_currency_is">Reference currency: %s</string>
    <string name="remove">Remove</string>
    <string name="remove_category">Remove category: %s</string>
    <string name="remove_currency">Remove currency: %s</string>
    <string name="removed_n_records">Removed %d records.</string>
    <string name="repeat_record">Repeat record every</string>
    <string name="repeat_record_yearly">Yearly</string>
    <string name="repeat_record_monthly">Monthly</string>
    <string name="repeat_record_weekly">Weekly</string>
    <string name="repeat_record_daily">Daily</string>
    <string name="repeated_record_pending">Confirm record</string>
    <string name="restore_database">Restore from backup</string>
    <string name="saturation_label">Sat.:</string>
    <string name="save_and_return">Save ⏎</string>
    <string name="select_main_record">Select main record</string>
    <string name="select_record">Select record</string>
    <string name="set_currency">Set currency</string>
    <string name="set_category">Set category</string>
    <string name="setup">Setup</string>
    <string name="show_category">Show</string>
    <string name="show_stat">Statistics</string>
    <string name="slaves_label_text">Child records:</string>
    <string name="specify_default_currency">Specify default currency</string>
    <string name="third_party_notice">This program uses some third-party software and artwork:
    </string>
    <string name="today">Today</string>
    <string name="tomorrow">Tomorrow</string>
    <string name="total">Total</string>
    <string name="yesterday">Yesterday</string>

    <!-- Predefined categories -->
    <string name="cats_clothes">Clothes</string>
    <string name="cats_communications">Communications</string>
    <string name="cats_e8ment">Entertainment</string>
    <string name="cats_education">Education</string>
    <string name="cats_food">Food</string>
    <string name="cats_health">Health</string>
    <string name="cats_household">Household</string>
    <string name="cats_interest">Interest</string>
    <string name="cats_misc_income">Misc incomes</string>
    <string name="cats_misc_expense">Misc expenses</string>
    <string name="cats_salary">Salary</string>
    <string name="cats_transport">Transport</string>
</resources>

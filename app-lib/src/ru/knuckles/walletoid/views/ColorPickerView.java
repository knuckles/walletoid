/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class was modified for use in Walletoid project. See LICENSE file
 * for more info.
 */

/**
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.graphics.*;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import ru.knuckles.walletoid.BuildConfig;
import ru.knuckles.walletoid.Utils;

import java.util.Locale;

public class ColorPickerView extends View {
    public interface OnColorChangedListener {
        void colorChanged(@ColorInt int color);
    }

    public ColorPickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // Force Android accept shaders of the same kind in ComposeShader.
        // Without this a Paint with ComposeShader will produce no image at all.
        // Ref: https://stackoverflow.com/questions/12445583/
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        _ok = getContext().getText(android.R.string.ok);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final float x = event.getX() - getWidth() / 2;
        final float y = event.getY() - getHeight() / 2;
        final float degree = (float) Math.toDegrees(Math.atan2(y, x));
        final float distance = (float) Math.sqrt(x * x + y * y);
        final boolean inCenter = distance <= _smallCircleRadius;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                _trackingCenter = inCenter;
                if (inCenter) {
                    _highlightCenter = true;
                    invalidate();
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (_trackingCenter) {
                    if (_highlightCenter != inCenter) {
                        _highlightCenter = inCenter;
                        invalidate();
                    }
                }
                else {
                    trackColor(degree, distance);
                }
                break;

            case MotionEvent.ACTION_UP:
                if (_trackingCenter) {
                    if (inCenter) {
                        _listener.colorChanged(_currentPaint.getColor());
                    }
                    _trackingCenter = false;    // so we draw w/o halo
                    invalidate();
                }
                else {
                    trackColor(degree, distance);
                }
                break;
        }
        return true;
    }

    public void setOnColorChangedListener(OnColorChangedListener listener) {
        _listener = listener;
    }

    public float getSaturation() {
        return _colorHSV[1];
    }

    public void setSaturation(float newSat) {
        _colorHSV[1] = Utils.clampFloat(newSat, 0, 1);
        selectedColorChanged();
    }

    public float getLightness() {
        return _colorHSV[2];
    }

    public void setLightness(float newLight) {
        _colorHSV[2] = Utils.clampFloat(newLight, 0, 1);
        selectedColorChanged();
    }

    public void setColor(int color) {
        Color.colorToHSV(color, _colorHSV);
        selectedColorChanged();
    }

    private void selectedColorChanged() {
        _currentPaint.setColor(Color.HSVToColor(_colorHSV));
        _textPaint.setColor(getFGColor(_colorHSV));
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(getWidth() / 2, getHeight() / 2);

        // Outer gradient ring
        canvas.drawOval(_ringDimensions, _rainbowPaint);

        // Inner selection circle
        _currentPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(0, 0, _smallCircleRadius, _currentPaint);
        canvas.drawText(
                _ok, 0, _ok.length(),
                -_okBounds.width() / 2, _okBounds.height() / 2,
                _textPaint);

        // Color components for debugging.
        if (BuildConfig.DEBUG) {
            canvas.drawText(
                    String.format(Locale.ROOT, "[%.0f, %.2f, %.2f]",
                            _colorHSV[0], _colorHSV[1], _colorHSV[2]),
                    -this.getWidth() / 2, -getHeight() / 2 + _debugTextPaint.getTextSize(),
                    _debugTextPaint);
        }

        if (!_trackingCenter)
            return;

        final int colorBackup = _currentPaint.getColor();
        _currentPaint.setAlpha(_highlightCenter ? 0xFF : 0x80);
        _currentPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(0, 0, _smallCircleRadius + _currentPaint.getStrokeWidth(), _currentPaint);

        _currentPaint.setColor(colorBackup);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        final int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        final int availWidth = widthSize - getPaddingLeft() - getPaddingRight();
        final int availHeight = heightSize - getPaddingTop() - getPaddingBottom();
        final int availDiameter = Math.min(availWidth, availHeight);
        _outerRadius = availDiameter / 2;

        // Here we overlay the circular rainbow sweep with a radial gradient from dark to white.
        // When used to draw a circle this shader creates a rainbow disk whose inner side is dark,
        // and outer one is bright.
        final RadialGradient satGradient = new RadialGradient(
                0, 0,
                _outerRadius,
                V_RGB_VALUES,
                DISK_BOUND_POINTS,
                Shader.TileMode.CLAMP);
        _rainbowPaint.setShader(
                new ComposeShader(satGradient, RAINBOW_SHADER, PorterDuff.Mode.MULTIPLY));
        _rainbowPaint.setStrokeWidth(DISK_FILL_RATE * _outerRadius);

        final float ringRadius = _outerRadius * (1 - 0.5f * DISK_FILL_RATE);
        _ringDimensions.set(-ringRadius, -ringRadius, ringRadius, ringRadius);

        _smallCircleRadius = (int) (CENTER_RADIUS_PART * _outerRadius);
        _textPaint.setTextSize(_smallCircleRadius * 0.75f);
        _textPaint.getTextBounds(_ok.toString(), 0, _ok.length(), _okBounds);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(
                        availDiameter + getPaddingLeft() + getPaddingRight(),
                        MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(
                        availDiameter + getPaddingTop() + getPaddingBottom(),
                        MeasureSpec.EXACTLY));
    }

    /**
     * Computes selected color based on position on the color disk.
     * @param degree Angle [0..359)
     * @param distance Distance from center in pixels.
     */
    private void trackColor(float degree, float distance) {
        degree = (degree % 360);
        if (degree < 0)
            degree += 360;
        _colorHSV[0] = degree;
        distance /= _outerRadius;
        distance = Utils.clampFloat(distance, DISK_BOUND_POINTS[0], DISK_BOUND_POINTS[1]);
        _colorHSV[2] = V_EDGE_VALUES[0] +
                V_RANGE_SIZE / DISK_FILL_RATE * (distance - DISK_BOUND_POINTS[0]);
        selectedColorChanged();
    }

    private static int getFGColor(float[] bgColor) {
        return bgColor[2] > 0.5 ? Color.BLACK : Color.WHITE;
    }

    private final static float CENTER_RADIUS_PART = 0.2f;
    // Visual inner and outer sides boundaries in percentage of available space:
    private final static float[] DISK_BOUND_POINTS = new float[] {0.3f, 1.0f};
    private final static float DISK_FILL_RATE = DISK_BOUND_POINTS[1] - DISK_BOUND_POINTS[0];
    // Minimal and maximal value component of color:
    private final static float[] V_EDGE_VALUES = new float[] {1.0f / 8, 1.0f};
    private final static float V_RANGE_SIZE = V_EDGE_VALUES[1] - V_EDGE_VALUES[0];
    // RGB values representing color value range (used in shader).
    private final static int[] V_RGB_VALUES = new int[] {0xFF222222, Color.WHITE };
    private final static int[] RAINBOW = new int[] {
            Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA, Color.RED
    };
    private final static SweepGradient RAINBOW_SHADER = new SweepGradient(0, 0, RAINBOW, null);

    private final static Paint _rainbowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    static {
        _rainbowPaint.setStyle(Paint.Style.STROKE);
    }

    private final float[] _colorHSV = new float[3];
    private final Paint _currentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint _textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    {
        setColor(RAINBOW[0]);
        _currentPaint.setStrokeWidth(5);
    }
    private final Paint _debugTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    {
        _debugTextPaint.setTextSize(40);
    }

    private int _outerRadius;
    private int _smallCircleRadius;
    private final RectF _ringDimensions = new RectF();
    private final Rect _okBounds = new Rect();
    private CharSequence _ok;
    private boolean _trackingCenter;
    private boolean _highlightCenter;

    private OnColorChangedListener _listener;
}

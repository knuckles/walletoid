/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.DatePicker;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.R;

import java.text.DateFormat;
import java.util.Calendar;

import static android.app.DatePickerDialog.OnDateSetListener;

// TODO: Rename to DayIncrementButton
public class CalendarDatePicker extends LeftRightSwitcher {
    public interface ChangeObserver {
        void onCalendarDateSet(Calendar calendar);
    }

    public CalendarDatePicker(Context context) throws Exception{
        this(context, null);
    }

    public CalendarDatePicker(Context context, AttributeSet attrs) throws Exception {
        super(context, attrs);
        addSwitchObserver(_switchObserver);
    }

    public void setCalendar(Calendar calendar) {
        this._calendar = calendar;
        updateText();
    }

    public void setObserver(ChangeObserver observer) {
        _observer = observer;
    }

    // PRIVATE

    private void onDateChanged() {
        updateText();
        if (_observer != null)
            _observer.onCalendarDateSet(_calendar);
    }

    private void updateText() {
        setText(getPrettyDateText());
    }

    private String getPrettyDateText() {
        switch (CalendarUtils.daysBetween(_calendar, Calendar.getInstance())) {
            case 0:
                return getContext().getString(R.string.today);
            case 1:
                return getContext().getString(R.string.yesterday);
            case -1:
                return getContext().getString(R.string.tomorrow);
        }
        return _dateFormat.format(_calendar.getTime());
    }

    private SwitchObserver _switchObserver = new SwitchObserver() {
        @Override
        public void onCenterClick() {
            if (_dateDialog == null) {
                // Instantiating dialog with date components like 0,0,0 on Android 2.3 versions throws!
                _dateDialog = new DatePickerDialog(getContext(),
                        _dateSetListener,
                        _calendar.get(Calendar.YEAR),
                        _calendar.get(Calendar.MONTH),
                        _calendar.get(Calendar.DATE));
            }
            else {
                _dateDialog.updateDate(
                        _calendar.get(Calendar.YEAR),
                        _calendar.get(Calendar.MONTH),
                        _calendar.get(Calendar.DATE));
            }
            _dateDialog.show();
        }

        @Override
        public void onLeftSwitch() {
            _calendar.add(Calendar.DATE, -1);
            onDateChanged();
        }

        @Override
        public void onRightSwitch() {
            _calendar.add(Calendar.DATE, 1);
            onDateChanged();
        }
    };

    private final OnDateSetListener _dateSetListener = new OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            _calendar.set(year, month, day);
            onDateChanged();
        }
    };

    // My data.
    private DatePickerDialog _dateDialog;
    private Calendar _calendar;
    private ChangeObserver _observer;

    private final DateFormat _dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
}

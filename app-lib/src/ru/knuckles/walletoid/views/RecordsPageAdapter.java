/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletFilter;

import java.util.Calendar;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

public class RecordsPageAdapter extends PagerAdapter {
    RecordsPageAdapter(LayoutInflater inflater, boolean singlePage) {
        _pivotDate = _dateRange.getRangeStart();
        _inflater = inflater;
        _singlePage = singlePage;
    }

    public int getZeroIndex() { return _singlePage ? 0 : HALF_PAGE_RANGE; }

    public RecordsListView getListAtPosition(int position) {
        return _listMap.get(position - getZeroIndex());
    }

    public void copyDateRangeAtPosition(final int position, final WalletFilter filter) {
        _dateRange.setRange(_pivotDate, _dateRange.getInterval());
        _dateRange.inc(position - getZeroIndex());
        filter.setDateRange(
                _dateRange.getRangeStart().getTimeInMillis(),
                _dateRange.getRangeEnd().getTimeInMillis());
    }

    public CalendarUtils.TimeInterval getInterval() {
        return _dateRange.getInterval();
    }

    public void setWallet(Wallet wallet) {
        _wallet = wallet;
        for (RecordsListView recList : _listMap.values()) {
            recList.setWallet(wallet);
        }
    }

    public void setCategoryFilter(Category.Kind kind, long categoryId) {
        _catKind = kind;
        _catId = categoryId;
        // Update all existing lists.
        for (RecordsListView recList : _listMap.values()) {
            recList.getCurrentFilter().setCategory(_catKind, _catId);
        }
    }

    public void setRange(final Calendar startDate, final CalendarUtils.TimeInterval interval) {
        _dateRange.setRange(startDate, interval);
        updatePivot();
    }

    public void setMonth(int year, int month) {
        _dateRange.setStart(year, month, 1);
        updatePivot();
    }

    public void setFilterQuery(final String filterQuery) {
        _filterQuery = filterQuery;
        // Update all existing lists.
        for (RecordsListView recList : _listMap.values()) {
            recList.getCurrentFilter().setQuery(_filterQuery);
        }
    }

    public void setRevSorting(boolean revSorting) {
        _revSorting = revSorting;
        // Update all existing lists.
        for (RecordsListView recList : _listMap.values()) {
            recList.getCurrentFilter().setRevSorting(_revSorting);
        }
    }

    public void setSelectionRangeListener(final RecordsListView.SelectionRangeListener listener) {
        _selRangeListener = listener;
    }

    public void setSelectionContextMenuProvider(
            final RecordsListView.SelectionContextMenuProvider scmp) {
        _scmp = scmp;
    }

    public void setRecordClickListener(final AdapterView.OnItemClickListener listener) {
        _recClickListener = listener;
    }

    public void setAllowRangeSelection(final boolean allowRangeSelection) {
        _allowRangeSelection = allowRangeSelection;
    }

    public void resetRecordSelection() {
        for (RecordsListView recList : _listMap.values()) {
            recList.resetSelection();
        }
    }

    // PagerAdapter

    @Override
    public int getCount() {
        return _singlePage ? 1 : HALF_PAGE_RANGE * 2;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        position -= getZeroIndex();

        final boolean createLayout = _viewCache.empty();
        final View layout = createLayout ?
                _inflater.inflate(R.layout.records_list_view, null) : _viewCache.pop();
        final RecordsListView listView = (RecordsListView) layout.findViewById(R.id.recordsList);
        if (createLayout) {
            final View emptyHint = layout.findViewById(R.id.noRecordsHint);
            listView.setEmptyView(emptyHint);
        }

        // Calculate time range for new list.
        _dateRange.setRange(_pivotDate, _dateRange.getInterval());
        _dateRange.inc(position);

        // Prepare record list filter.
        listView.setFilter(new WalletFilter(
                _dateRange.getRangeStart(),
                _dateRange.getRangeEnd(),
                _catKind,
                _catId
        ));
        listView.getCurrentFilter().setQuery(_filterQuery);
        listView.getCurrentFilter().setRevSorting(_revSorting);
        listView.setWallet(_wallet);

        // Set external data.
        listView.setSelectionRangeListener(_selRangeListener);
        listView.setSelectionContextMenuProvider(_scmp);
        listView.setRecordClickListener(_recClickListener);
        listView.setAllowRangeSelection(_allowRangeSelection);

        // FIXME: Scroll to latest item.
        //listView.smoothScrollToPosition(listView.getCount() - 1);

        _listMap.put(position, listView);
        logUsage();

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(final ViewGroup container, int position, final Object object) {
        position -= getZeroIndex();

        if (!(object instanceof View))
            throw new IllegalStateException();
        final View layout = (View) object;
        container.removeView(layout);
        if (_viewCache.size() < VIEW_CACHE_SIZE)
            _viewCache.push(layout);

        final RecordsListView listView = _listMap.get(position);
        _listMap.remove(position);

        // Drop references to external objects.
        listView.setSelectionRangeListener(null);
        listView.setSelectionContextMenuProvider(null);
        listView.setRecordClickListener(null);

        listView.setWallet(null);
        listView.setFilter(new WalletFilter());

        logUsage();
    }

    @Override
    public boolean isViewFromObject(final View view, final Object o) {
        return view == o;
    }

    // PRIVATE

    private void updatePivot() {
        _pivotDate = _dateRange.getRangeStart();
        for (Map.Entry<Integer, RecordsListView> e : _listMap.entrySet()) {
            copyDateRangeAtPosition(e.getKey() + getZeroIndex(), e.getValue().getCurrentFilter());
        }
    }

    private void logUsage() {
        Log.d(getClass().getSimpleName(),
                "Active pages: " + _listMap.size() +
                ". Cached pages: " + _viewCache.size());
    }

    private Wallet _wallet;
    private Calendar _pivotDate;
    private final CalendarUtils.DateRange _dateRange =
            new CalendarUtils.DateRange(CalendarUtils.TimeInterval.MONTH);
    private final LayoutInflater _inflater;
    private final boolean _singlePage;
    private final Map<Integer, RecordsListView> _listMap = new TreeMap<>();
    private final Stack<View> _viewCache = new Stack<>();
    private final static int VIEW_CACHE_SIZE = 3;
    // Having wider range is theoretically possible, but Android's ViewPager is written in a stupid
    // way, as it sometimes loops from current page index (which in our case can be huge) to zero,
    // freezing the UI thread for more than just noticeable amount of time.
    private final static int HALF_PAGE_RANGE = 12 * 20;  // 20 years each side.

    private Category.Kind _catKind = Category.Kind.ALL;
    private long _catId = Category.UNKNOWN_ID;
    private boolean _revSorting = false;

    // Passed to lists.
    private boolean _allowRangeSelection;
    private AdapterView.OnItemClickListener _recClickListener;
    private RecordsListView.SelectionContextMenuProvider _scmp;
    private RecordsListView.SelectionRangeListener _selRangeListener;
    private String _filterQuery;
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.database.Wallet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

// TODO: There's likely to be a lot in common with RecordsPageAdapter. Maybe unify?
/**
 * PagerAdapter controlling a collection of StatPageViews.
 */
public class StatPageAdapter extends PagerAdapter implements CalendarUtils.DateRange.Observer {
    public static class ZoomController{
        public ZoomController(ViewPager pager, StatPageAdapter adapter) {
            _pager = pager;
            _adapter = adapter;
            _pager.addOnPageChangeListener(_pageSelectListener);
            _pager.setAdapter(_adapter);
            _pager.setCurrentItem(_adapter.getZeroIndex());
            _selectedIntervalIndex = Math.max(0,
                    Arrays.binarySearch(_intervals, _adapter.getPivotRange().getInterval()));
        }

        public void zoom(boolean zoomIn) {
            final int newIndex = _selectedIntervalIndex + (zoomIn ? 1 : -1);
            if (newIndex < 0 || newIndex >= _intervals.length)
                return;

            final Calendar prevRangeStart =
                    _adapter.getRangeForPage(_pager.getCurrentItem()).getRangeStart();
            final Calendar newPivotDate = zoomIn && _pagerPivotDate != null ?
                    _pagerPivotDate : prevRangeStart;


            final Wallet wallet = _adapter.getWallet();
            _adapter.setWallet(null);  // Optimize out unneeded queries.
            // This will overwrite _pagerPivotDate in page change listener, so store it afterwards.
            _pager.setCurrentItem(_adapter.getZeroIndex(), false);  // Reset pager position.
            _adapter.getPivotRange().setRange(newPivotDate, _intervals[newIndex]);
            _adapter.setWallet(wallet);  // Restore data link.
            _selectedIntervalIndex = newIndex;

            if (!zoomIn) {
                // Backup current pivot in order to get to it later, when narrowing interval.
                _pagerPivotDate = prevRangeStart;
            }
            else if (_pagerPivotDate == null) {  // Just after page switching.
                _pagerPivotDate = _adapter.getRangeForPage(_pager.getCurrentItem()).getRangeStart();
            }
        }

        private final ViewPager.SimpleOnPageChangeListener _pageSelectListener =
                new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {
                _pagerPivotDate = null;
            }
        };

        private int _selectedIntervalIndex;
        private Calendar _pagerPivotDate = null;

        private final ViewPager _pager;
        private final StatPageAdapter _adapter;

        // Const.
        private static final DateFormat _logDateFormat =
                new SimpleDateFormat("dd-MM-yy", Locale.getDefault());

        private static final CalendarUtils.TimeInterval[] _intervals = {
                CalendarUtils.TimeInterval.YEAR,
                CalendarUtils.TimeInterval.MONTH,
                CalendarUtils.TimeInterval.WEEK,
        };
    }

    public StatPageAdapter(Context context) {
        _context = context;
        _pivotRange.addObserver(this);
    }

    public int getZeroIndex() { return HALF_PAGE_RANGE; }

    /**
     * @param position is what's used by a ViewPager. Negative values not allowed.
     */
    public CalendarUtils.DateRange getRangeForPage(int position) {
        position -= getZeroIndex();
        if (_pageMap.containsKey(position))
            return new CalendarUtils.DateRange(_pageMap.get(position).getRange());
        return mkRangeForPage(position);
    }

    public Wallet getWallet() { return _wallet; }

    public void setWallet(Wallet wallet) {
        if (_wallet == wallet)
            return;
        _wallet = wallet;
        for (StatPageView page : _pageMap.values()) {
            page.setWallet(wallet);
        }
    }

    public CalendarUtils.DateRange getPivotRange() {
        return _pivotRange;
    }

    // PagerAdapter

    @Override
    public int getCount() {
        return HALF_PAGE_RANGE * 2;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        position -= getZeroIndex();

        final boolean createLayout = _viewCache.empty();
        final StatPageView pageView = createLayout ? new StatPageView(_context) : _viewCache.pop();
        pageView.setRange(mkRangeForPage(position));
        pageView.setWallet(_wallet);

        _pageMap.put(position, pageView);

        container.addView(pageView);
        return pageView;
    }

    @Override
    public void destroyItem(final ViewGroup container, int position, final Object object) {
        position -= getZeroIndex();

        if (!(object instanceof StatPageView))
            throw new IllegalStateException();
        final StatPageView pageView = (StatPageView) object;
        container.removeView(pageView);
        if (_viewCache.size() < VIEW_CACHE_SIZE)
            _viewCache.push(pageView);
        _pageMap.remove(position);
    }

    @Override
    public boolean isViewFromObject(final View view, final Object o) {
        return view == o;
    }

    // CalendarUtils.DateRange.Observer
    @Override
    public void onPeriodChanged(final Calendar start, final Calendar end) {
        for (Map.Entry<Integer, StatPageView> e : _pageMap.entrySet()) {
            e.getValue().setRange(mkRangeForPage(e.getKey()));
        }
    }

    // PRIVATE

    /**
     * @param index is bidirectional. Negative values signify pages to the "left" of pivot.
     */
    private CalendarUtils.DateRange mkRangeForPage(int index) {
        final CalendarUtils.DateRange pageRange = new CalendarUtils.DateRange(_pivotRange);
        pageRange.inc(index);
        return pageRange;
    }

    private final Context _context;
    private Wallet _wallet;
    private final CalendarUtils.DateRange _pivotRange =
            new CalendarUtils.DateRange(CalendarUtils.TimeInterval.MONTH);
    private final Map<Integer, StatPageView> _pageMap = new TreeMap<>();
    private final Stack<StatPageView> _viewCache = new Stack<>();
    private final static int VIEW_CACHE_SIZE = 3;
    private final static int HALF_PAGE_RANGE = 12 * 20;  // 20 years each side.
}

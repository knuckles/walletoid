/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.knuckles.walletoid.R;

import java.util.Arrays;

public class StatProgressBar extends LinearLayout {
    public StatProgressBar(Context context) {
        super(context);
        init();
    }

    public StatProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StatProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public int getProgress() {
        return _progress;
    }

    public void setProgress(int progress) {
        if (progress < 0 || progress > 100)
            throw new IllegalArgumentException();
        _progress = progress;
        updateGauge();
    }

    public CharSequence getLeftText() {
        return _leftTextView.getText();
    }

    public void setLeftText(CharSequence text) {
        _leftTextView.setText(text);
    }

    public CharSequence getRightText() {
        return _rightTextView.getText();
    }

    public void setRightText(CharSequence text) {
        _rightTextView.setText(text);
    }

    public int getBackgroundColor() {
        ColorDrawable backDrawable = (ColorDrawable) _gaugeView.getBackground();
        return backDrawable.getColor();
    }

    //@Override
    public void setBackColor(int color) {
        final ShapeDrawable bg = new ShapeDrawable(new RoundRectShape(_ROUND_RADII, null, null));
        bg.getPaint().setColor(color);
        _gaugeView.setBackgroundDrawable(bg);
    }

    // PRIVATE

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.stat_progress_bar, this, true);
        _leftTextView = (TextView) findViewById(R.id.leftTextView);
        _rightTextView = (TextView) findViewById(R.id.rightTextView);
        _gaugeView = findViewById(R.id.gaugeView);

        if (isInEditMode()) {
            setLeftText("Sample left text");
            setRightText("Right");
            setBackColor(Color.RED);
            setProgress(30);
        }
    }

    private void updateGauge() {
        _gaugeView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        0,
                        LayoutParams.MATCH_PARENT,
                        _progress
                )
        );
    }

    private final static float[] _ROUND_RADII = new float[8];
    static {
        final float dpRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,
                Resources.getSystem().getDisplayMetrics());
        Arrays.fill(_ROUND_RADII, dpRadius);
    }

    private TextView _leftTextView;
    private TextView _rightTextView;
    private View _gaugeView;
    private int _progress = 0;
}

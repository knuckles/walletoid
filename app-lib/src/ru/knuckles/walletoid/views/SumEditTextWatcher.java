/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import ru.knuckles.walletoid.BuildConfig;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.arithmetic_evaluator.Evaluator;
import ru.knuckles.walletoid.arithmetic_evaluator.Expression;

import java.math.BigDecimal;

public class SumEditTextWatcher implements TextWatcher, View.OnFocusChangeListener {
    public interface Observer {
        void onInputChecked(String input, boolean isValidExpr);
    }

    public SumEditTextWatcher(Observer observer, EditText inputField, Context context) {
        _observer = observer;
        _inputField = inputField;

        // Read theme values.
        _errorEditTextColor = context.getResources().getColor(R.color.red);
        TypedArray themeValues = context.getTheme().obtainStyledAttributes(new int[] {
            android.R.attr.editTextColor
        });
        try {
            _themeEditTextColor = themeValues.getColor(0, Color.BLACK);
        } finally {
            themeValues.recycle();
        }

        _inputField.setOnFocusChangeListener(this);
        if (_inputField.isFocused()) {
            _inputField.addTextChangedListener(this);
        }
    }

    public String getUserInput() {
        return !_userInputText.isEmpty() ? _userInputText : _inputField.getText().toString();
    }

    public boolean inputIsValid() {
        return _inputIsValid;
    }

    public BigDecimal getEvaluatedSum() {
        return _lastEvaluatedSum;
    }

    public boolean sumIsConstExpr() {
        return _lastSumIsConstExpr;
    }

    public void applyPendingEdits() {
        // Do nothing if input isn't in progress.
        if (!_timerActive) {
            return;
        }
        stopTimer();
        updateSum();
    }

    private void updateSum() {
        _userInputText = "";
        _lastEvaluatedSum = BigDecimal.ZERO;
        _lastSumIsConstExpr = true;

        final String input = _inputField.getText().toString();
        if (input.isEmpty()) {
            _inputIsValid = true;
            return;
        }

        _inputIsValid = false;
        try {
            Expression expression = Evaluator.parseString(input);
            _lastSumIsConstExpr = expression.getClass().equals(Expression.Constant.class);
            _lastEvaluatedSum = expression.getValue();
            _userInputText = _inputField.getText().toString();
            _inputIsValid = true;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(((Object) this).getClass().getName(), e.getMessage());
            }
            _inputField.setTextColor(_errorEditTextColor);
        }
        finally {
            if (_observer != null)
                _observer.onInputChecked(getUserInput(), _inputIsValid);
        }
    }

    // View.OnFocusChangeListener
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (!_userInputText.isEmpty()) {
                _inputField.setText(_userInputText);
            }
            _inputField.addTextChangedListener(this);
        }
        else {
            _inputField.removeTextChangedListener(this);
            stopTimer();
            updateSum();
            if (_inputIsValid && !_lastSumIsConstExpr && !_inputField.hasFocus()) {
                _inputField.setText(_lastEvaluatedSum.toString());
            }
        }
    }

    // TextWatcher
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        _inputField.setTextColor(_themeEditTextColor);
        startTimer();
    }

    private void startTimer() {
        _throttle.cancel();
        _throttle.start();
        _timerActive = true;
    }

    private void stopTimer() {
        _throttle.cancel();
        _timerActive = false;
    }

    private boolean _timerActive = false;
    private CountDownTimer _throttle = new CountDownTimer(500, 500) {
        @Override
        public void onTick(long millisUntilFinished) {
        }

        @Override
        public void onFinish() {
            _timerActive = false;
            updateSum();
        }
    };

    private final Observer _observer;
    private final EditText _inputField;
    private final int _themeEditTextColor;
    private final int _errorEditTextColor;

    private String _userInputText = "";
    private BigDecimal _lastEvaluatedSum = BigDecimal.ZERO;
    private boolean _lastSumIsConstExpr = true;
    private boolean _inputIsValid = true;
}

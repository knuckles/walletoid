/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.annotation.ColorInt;
import android.support.v4.graphics.ColorUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Utils;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Currency;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

public class RecordLineBinder {
    public RecordLineBinder(Wallet wallet, @ColorInt int listBGColor, boolean repetitionInfo) {
        _wallet = wallet;
        _listBGColor = listBGColor;
        _repetitionInfo = repetitionInfo;
    }

    // TODO: Try to optimise out String instances with Cursor.copyStringToBuffer & CharArrayBuffer.
    // TODO: See https://cyrilmottier.com/2011/07/05/listview-tips-tricks-2-section-your-listview/
    public void bindView(View view, Context context, Record record, boolean displayDate) {
        final ViewHolder vh = getViewHolder(view);

        fillDateInfo(vh, displayDate ? record : null);

        final boolean shouldShowRepMark = !_repetitionInfo &&
                (_wallet.recordIsScheduled(record.getId()) ||
                        (record.getTemplateRecId() != Record.UNKNOWN_ID &&
                                _wallet.recordIsScheduled(record.getTemplateRecId())));
        vh.markText.setVisibility(_repetitionInfo ? View.GONE : View.VISIBLE);
        vh.markText.setText(shouldShowRepMark ?
                context.getString(R.string.repeated_rec_prefix) :
                null);

        vh.descrText.setText(record.getDescr());

        // Sum cell.
        final Category recCategory = _wallet.getCategory(record.getCatID());
        final Currency recCurrency = _wallet.getCurrency(record.getCurrencyID());
        final Utils.ApproxValue approxSum = new Utils.ApproxValue(record.getEffectiveSum().amount);
        final String sumStr = context.getString(
                recCategory.getKind().isIncome() ?
                        R.string.pos_sum_tpl :
                        R.string.neg_sum_tpl,
                approxSum.value,
                approxSum.suffix,
                recCurrency.getSymbol() != null ? recCurrency.getSymbol() : recCurrency.getName()
        );

        final ShapeDrawable bg = new ShapeDrawable(_SUM_FIELD_SHAPE);
        bg.getPaint().setColor(recCategory.getColor());
        vh.sumBlock.setBackground(bg);

        vh.sumText.setText(sumStr);
        @ColorInt final int resultCellBG =
                ColorUtils.compositeColors(recCategory.getColor(), _listBGColor);
        vh.sumText.setTextColor(getContrastColor(resultCellBG));
    }

    private void fillDateInfo(ViewHolder vh, Record record) {
        if (record == null) {
            vh.dateText.setText(null);
            return;
        }

        DateFormat dateFormat = _dfDate;
        final CalendarUtils.TimeInterval repInterval = record.getRepetitionInterval();
        if (_repetitionInfo && repInterval != null) {
            switch (repInterval) {
                case YEAR:
                    dateFormat = _dfMonth;
                    break;
                case MONTH:
                    dateFormat = _dfDate;
                    break;
                case WEEK:
                    dateFormat = _dfDayOfWeek;
                    break;
                case DAY:
                    dateFormat = _dfTime;
                    break;
            }
        }
        final String dateStr = dateFormat.format(record.getTimestamp());
        vh.dateText.setText(dateStr);
    }

    private static float[] _hsvColorBuf = new float[3];
    private static int getContrastColor(@ColorInt int srcColor) {
        Color.colorToHSV(srcColor, _hsvColorBuf);
        float lightness = _hsvColorBuf[2];
        return lightness > 0.5 ? Color.BLACK : Color.WHITE;
    }

    private static ViewHolder getViewHolder(View view) {
        ViewHolder vh = (ViewHolder) view.getTag();
        if (vh == null) {
            view.setTag(vh = new ViewHolder());
            vh.dateText = (TextView) view.findViewById(R.id.DateText);
            vh.markText = (TextView) view.findViewById(R.id.markText);
            vh.sumBlock = view.findViewById(R.id.recSumContainer);
            vh.sumText = (TextView) view.findViewById(R.id.SumText);
            vh.descrText = (TextView) view.findViewById(R.id.DescrText);
        }
        return vh;
    }

    private static class ViewHolder {
        TextView dateText;
        TextView markText;
        TextView sumText;
        TextView descrText;
        View sumBlock;
    }

    private final static Shape _SUM_FIELD_SHAPE;
    static {
        final float dpRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,
                Resources.getSystem().getDisplayMetrics());
        final float[] radii = new float[8];  // Corner radii
        Arrays.fill(radii, dpRadius);
        _SUM_FIELD_SHAPE = new RoundRectShape(radii, null, null);
    }

    private final Wallet _wallet;
    @ColorInt private final int _listBGColor;
    // Whether to show record repetition info instead of its date.
    private final boolean _repetitionInfo;

    // Should not be static in order to pick up locale settings upon recreation.
    private final Locale _defLocale = Locale.getDefault();
    private final DateFormat _dfMonth = new SimpleDateFormat("MMM", _defLocale);
    private final DateFormat _dfDate = new SimpleDateFormat("d", _defLocale);
    private final DateFormat _dfDayOfWeek = new SimpleDateFormat("E", _defLocale);
    private final DateFormat _dfTime = new SimpleDateFormat("H:m", _defLocale);
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.CalendarUtils.DateRange;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.TextStyling;
import ru.knuckles.walletoid.Utils;
import ru.knuckles.walletoid.activities.IntentExtras;
import ru.knuckles.walletoid.activities.RecordsViewActivity;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.database.Currency;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This is a compound view, consisting of a PlotView and a custom List view, displaying Wallet
 * expenditure statistics for a given time range.
 */
public class StatPageView extends LinearLayout {
    public StatPageView(final Context context) {
        super(context);
        init();
    }

    public StatPageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StatPageView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.stat_page, this);

        _statsTitle = (TextView) findViewById(R.id.statsTitleLabel);
        _plotView = (PlotView) findViewById(R.id.statPlot);
        _plotView.setRulerLabeler(_plotLabeler);

        _catStatsList = (ListView) findViewById(R.id.catStatList);
        _catStatsList.setOnItemClickListener(_listItemClickHandler);
    }

    public void setWallet(final Wallet wallet) {
        if (_wallet != null) {
            _wallet.events.removeAnyEventObserver(_walletObserver);
        }
        _wallet = wallet;
        if (_wallet != null) {
            _wallet.events.addAnyEventObserver(_walletObserver);
        }

        _plotLabeler.setCurrency(_wallet != null ? _wallet.getReferenceCurrency() : null);
        updatePlotLabeler();
        updateDataViews();
    }

    public DateRange getRange() {
        return _dateRange;
    }

    public void setRange(DateRange range) {
        _dateRange = range;
        if (_dateRange != null) {
            updatePlotLabeler();
        }
        updateDataViews();
    }

    // PRIVATE

    private void updateTitle(boolean haveData) {
        if (!haveData) {
            _statsTitle.setText(R.string.no_data_for_period);
            return;
        }

        final int unitsResId;
        switch (_dateRange.getInterval()) {
            case YEAR:
                unitsResId = R.string.per_month;
                break;
            case MONTH:
                unitsResId = R.string.per_week;
                break;
            case WEEK:
                unitsResId = R.string.per_day;
                break;
            default:
                throw new IllegalArgumentException();
        }
        _statsTitle.setText(
                getContext().getString(R.string.expenses_breakdown_per,
                        getContext().getString(unitsResId)));
    }

    private void updatePlotLabeler() {
        if (_plotLabeler == null)
            return;
        _plotLabeler.setStartingDate(_dateRange.getRangeStart());
        _plotLabeler.setInterval(_dateRange.getInterval());
    }

    private void updateDataViews() {
        _catStatsList.setAdapter(_catSumAdapter = _wallet == null ?
                null :
                new RecordAdapter(getContext(), _wallet));
        if (_catSumAdapter != null)
            _catSumAdapter.setNotifyOnChange(false);
        if (_wallet == null) {
            _plotView.setPoints(null);
            updateTitle(false);
            return;
        }

        final Calendar rangeStart = _dateRange.getRangeStart();
        final Calendar rangeEnd = _dateRange.getRangeEnd();

        final CalendarUtils.TimeInterval granularity =
                CalendarUtils.TimeInterval.values()[_dateRange.getInterval().ordinal() + 1];
        final List<Record> groupedRecords = _wallet.groupByDateAndCategory(
                new WalletFilter(rangeStart, rangeEnd, Category.Kind.EXPENSE),
                granularity,
                _wallet.getReferenceCurrency());

        final int partitioningCalendarField =
                _intervalToCalendarFieldMap.get(_dateRange.getInterval());
        final List<PlotView.Column> columns =
                partitionRecords(groupedRecords, rangeStart, partitioningCalendarField);

        updateTitle(!groupedRecords.isEmpty());
        _plotView.setPoints(columns);
        updateCategoriesSums(groupedRecords, rangeStart);
    }

    /**
     * @param records Records, grouped by date and category.
     * @param partitioningField Calendar field which value will be used to partition
     *                          |groupedRecords|.
     */
    private List<PlotView.Column> partitionRecords(final List<Record> records,
            Calendar startDate, int partitioningField) {
        int fieldMinValue = (partitioningField == Calendar.DAY_OF_WEEK) ?
                startDate.getFirstDayOfWeek() :
                startDate.getMinimum(partitioningField);
        final int fieldValueRange = startDate.getActualMaximum(partitioningField) -
                startDate.getMinimum(partitioningField);
        final int nColumns = fieldValueRange + 1;

        final Map<Long, Category> catMap = _wallet.getCategoriesMap();

        final List<PlotView.Column> columns = new ArrayList<>(nColumns);
        for (int i = 0; i < nColumns; i++)
            columns.add(new PlotView.Column());

        for (Record record : records) {
            int colIndex = record.getCalendar().get(partitioningField) - fieldMinValue;
            // Week days indices are Locale dependent and wrap around.
            if (partitioningField == Calendar.DAY_OF_WEEK) {
                colIndex = (colIndex + Calendar.SATURDAY) % Calendar.SATURDAY;
            }
            if (colIndex >= nColumns)
                throw new AssertionError("Unexpectedly big Calendar field value.");
            final PlotView.Column column = columns.get(colIndex);

            final Category category = catMap.get(record.getCatID());
            column.add(new PlotView.ColoredValue(category.getColor(),
                    record.getSum().amount.floatValue()));
        }
        return columns;
    }

    private void updateCategoriesSums(List<Record> groupedRecords, Calendar startDate) {
        // Calculate sum of each category.
        final Map<Long, Record.Sum> categorySums = new TreeMap<>();
        for (Record record : groupedRecords) {
            Record.Sum categorySum = categorySums.get(record.getCatID());
            categorySum = (categorySum == null) ?
                    record.getSum() :
                    categorySum.add(record.getSum());
            categorySums.put(record.getCatID(), categorySum);
        }

        // Convert sums to Records and calculate their total.
        final List<Record> categoryRecords = new ArrayList<>(categorySums.size() + 1);
        Record.Sum totalSum = new Record.Sum(_wallet.getReferenceCurrencyId());
        for (Map.Entry<Long, Record.Sum> entry : categorySums.entrySet()) {
            categoryRecords.add(
                    new Record(entry.getKey(), startDate.getTimeInMillis(), entry.getValue()));
            totalSum = totalSum.add(entry.getValue());
        }

        // Add 'total' Record.
        if (categoryRecords.size() > 1)  // Otherwise, it's of little sense.
            categoryRecords.add(new Record(startDate.getTimeInMillis(), totalSum));

        Collections.sort(categoryRecords, new Comparator<Record>() {
            @Override
            public int compare(Record lhs, Record rhs) {
                // Sort descending.
                final int sumDiff = rhs.getSum().amount.compareTo(lhs.getSum().amount);
                if (sumDiff != 0)
                    return sumDiff;
                // Can't do just lhs - rhs, since, the combination of UNKNOWN_ID and MISC_ID
                // result in an overflow and zero result.
                return lhs.getCatID() < rhs.getCatID() ?
                        -1 :
                        lhs.getCatID() > rhs.getCatID() ? 1 : 0;
            }
        });

        _catSumAdapter.clear();
        _catSumAdapter.addAll(categoryRecords);
        _catSumAdapter.setTotalSum(totalSum.amount.doubleValue());
        _catSumAdapter.notifyDataSetChanged();
    }

    // For records representing sums for a period as category portion in a total sum.
    static private class RecordAdapter extends ArrayAdapter<Record> {
        public RecordAdapter(Context context, Wallet wallet) {
            super(context, R.layout.stat_progress_bar, R.id.leftTextView);
            _wallet = wallet;
            _catMap = _wallet.getCategoriesMap(Category.Kind.ALL, true);
        }

        public void setTotalSum(double totalSum) {
            _totalSum = totalSum;
        }

        // ArrayAdapter:

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            StatProgressBar progressBar = convertView == null ? new StatProgressBar(getContext())
                    : (StatProgressBar) convertView;

            final Record rec = getItem(position);
            final boolean isTotalRec = rec.getCatID() == Category.UNKNOWN_ID;
            final Category cat = _catMap.get(rec.getCatID());
            progressBar.setBackColor(cat.getColor());
            progressBar.setLeftText(isTotalRec ?
                    TextStyling.bold(getContext().getString(R.string.total)) :
                    cat.getName());

            final Utils.ApproxValue approxSum = new Utils.ApproxValue(rec.getSum().amount);
            final String sumString = getContext().getString(
                    R.string.neg_sum_long_tpl,
                    approxSum.value,
                    approxSum.suffix,
                    _wallet.getCurrency(rec.getCurrencyID()).getSymbol());
            progressBar.setRightText(isTotalRec ? TextStyling.bold(sumString) : sumString);
            final float sum = rec.getSum().amount.floatValue();
            progressBar.setProgress(_totalSum > 0 ? (int) (sum * 100 / _totalSum) : 0);

            return progressBar;
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getCatID();
        }

        private final Wallet _wallet;
        private final Map<Long, Category> _catMap;
        private double _totalSum;
    };

    private static class StatPlotLabeler extends PlotView.RulerLabeler {
        void setCurrency(Currency currency) {
            if (currency != null) {
                _currencyLabel = currency.getSymbol().isEmpty() ?
                        currency.getName() : currency.getSymbol();
            }
            else
                _currencyLabel = null;
        }

        void setStartingDate(Calendar calendar) {
            _startDate = calendar;
        }

        void setInterval(CalendarUtils.TimeInterval interval) {
            _interval = interval;
        }

        @Override
        public String getYAxisName() {
            return _currencyLabel;
        }

        @Override
        public String makeXLabel(int rulerNr, int of) {
            switch (_interval) {
                case YEAR:
                    return labelMonth(rulerNr);
                case MONTH:
                    return labelWeek(rulerNr);
                case WEEK:
                    return labelDay(rulerNr);
            }
            return super.makeXLabel(rulerNr, of);
        }

        private String labelMonth(final int rulerNr) {
            if (rulerNr < 0 || rulerNr >= _shortMonths.length)
                return null;
            return _shortMonths[rulerNr];
        }

        private String labelWeek(final int rulerNr) {
            Calendar rulerDate = (Calendar) _startDate.clone();
            final int lastMonthDay = _startDate.getActualMaximum(Calendar.DAY_OF_MONTH);
            int nWeeks = lastMonthDay / rulerDate.getMaximum(Calendar.DAY_OF_WEEK);
            if (lastMonthDay % rulerDate.getMaximum(Calendar.DAY_OF_WEEK) > 0)
                nWeeks++;

            if (rulerNr < 0 || rulerNr > nWeeks - 1)
                return null;

            rulerDate.add(Calendar.WEEK_OF_MONTH, rulerNr);
            final int weekStartDay = rulerDate.get(Calendar.DAY_OF_MONTH);
            final int weekEndDay;
            if (rulerNr == nWeeks - 1) {
                weekEndDay = lastMonthDay;
            }
            else {
                rulerDate.add(Calendar.WEEK_OF_MONTH, 1);
                weekEndDay = rulerDate.get(Calendar.DAY_OF_MONTH) - 1;
            }
            return String.format(Locale.ROOT, "%d - %d", weekStartDay, weekEndDay);
        }

        private String labelDay(final int rulerNr) {
            Calendar rulerDate = (Calendar) _startDate.clone();
            rulerDate.add(Calendar.DAY_OF_YEAR, rulerNr);
            return _dayFormat.format(rulerDate.getTime());
        }

        private Calendar _startDate;
        private CalendarUtils.TimeInterval _interval;
        private String _currencyLabel;

        private final DateFormatSymbols _dfs = new DateFormatSymbols();
        private final String[] _shortMonths = _dfs.getShortMonths();
        private final DateFormat _dayFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
    };

    private final AdapterView.OnItemClickListener _listItemClickHandler =
            new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long rowID) {
            final Intent viewDetailsIntent =
                    new Intent(getContext(), RecordsViewActivity.class);
            // Fix #25 Crash after returning from statistics screen.
            viewDetailsIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
            viewDetailsIntent.putExtra(
                    IntentExtras.DATE_TIMESTAMP,
                    _dateRange.getRangeStart().getTimeInMillis());
            viewDetailsIntent.putExtra(
                    IntentExtras.RANGE_DURATION,
                    _dateRange.getInterval().ordinal());
            viewDetailsIntent.putExtra(IntentExtras.CATEGORY, rowID);
            getContext().startActivity(viewDetailsIntent);
        }
    };

    private final WalletEvents.AnyEventObserver _walletObserver =
            new WalletEvents.AnyEventObserver(WalletEvents.WE_ALL_EVENTS) {
        @Override
        protected void onWalletChanged(final long eventFlag) {
            updateDataViews();
        }
    };

    // Const.
    private static final Map<CalendarUtils.TimeInterval, Integer> _intervalToCalendarFieldMap =
            new TreeMap<CalendarUtils.TimeInterval, Integer>() {{
        put(CalendarUtils.TimeInterval.YEAR, Calendar.MONTH);
        put(CalendarUtils.TimeInterval.MONTH, Calendar.DAY_OF_WEEK_IN_MONTH);
        put(CalendarUtils.TimeInterval.WEEK, Calendar.DAY_OF_WEEK);
    }};
    // No static, as it needs reinstantiation when Locale changes.
    private final StatPlotLabeler _plotLabeler = new StatPlotLabeler();;


    // Controls.
    private TextView _statsTitle;
    private PlotView _plotView;
    private ListView _catStatsList;

    // Other.
    private Wallet _wallet;
    private RecordAdapter _catSumAdapter;
    private DateRange _dateRange;
}

/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletEvents;
import ru.knuckles.walletoid.database.WalletFilter;

import java.util.Set;
import java.util.TreeSet;

import static ru.knuckles.walletoid.database.WalletEvents.*;

public class RecordsListView extends ListView
        implements
        View.OnCreateContextMenuListener,
        WalletFilter.FilterObserver {
    public interface SelectionRangeListener {
        /**
         * Notifies about selection range change.
         * @param firstRecordId first selected record ID.
         * @param position List-wise selection position.
         *                 First – selection start index. Second - selection length.
         */
        void OnSelectionRangeChanged(long firstRecordId, Pair<Integer, Integer> position);
    }

    public interface SelectionContextMenuProvider {
        void createContextMenu(RecordsListView view, ContextMenu menu,
                long firstSelectedRecordId, Pair<Integer, Integer> selection);
    }

    public RecordsListView(Context context) {
        super(context);
        init();
    }

    public RecordsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RecordsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setAllowRangeSelection(boolean allow) {
        _allowRangeSelection = allow;
    }

    public WalletFilter getCurrentFilter() {
        return _filter;
    }

    public void setFilter(WalletFilter filter) {
        if (_filter != null)
            _filter.removeObserver(this);
        if (filter != null)
            filter.addObserver(this);
        _filter = filter;
        onFilterChanged(new WalletFilter(_filter));
    }

    public void setWallet(Wallet wallet) {
        if (_wallet == wallet)
            return;

        if (_wallet != null) {
            _wallet.events.removeAnyEventObserver(_walletObserver);
            if (_recordsCursor != null)
                _recordsCursor.close();
            _recordsCursor = null;
            setAdapter(null);
        }

        _wallet = wallet;
        if (_wallet != null) {
            _wallet.events.addAnyEventObserver(_walletObserver);
            queryWallet();
        }
    }

    public void setSelectionRangeListener(RecordsListView.SelectionRangeListener listener) {
        _selectionListener = listener;
    }

    public void setSelectionContextMenuProvider(SelectionContextMenuProvider provider) {
        _selectionMenuProvider = provider;
    }

    public void setRecordClickListener(OnItemClickListener listener) {
        _extRecordClickListener = listener;
    }

    public void resetSelection() {
        if (_adapter == null)
            return;
        _adapter.resetSelection();
        notifySelectionChanged();
    }

    // OnCreateContextMenuListener:
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        if (_selectionMenuProvider == null)
            return;
        final Pair<Integer, Integer> selection = _adapter.getSelection();
        if (selection.second < 1)
            return;
        _selectionMenuProvider.createContextMenu(this, menu,
                _adapter.getItemId(selection.first), selection);
    }

    // FilterObserver

    @Override
    public void onFilterChanged(WalletFilter filter) {
        if (_wallet != null)
            queryWallet();
    }

    // ---------------------------------------------------------------------------------------------

    protected void init() {
        setFilter(new WalletFilter());
        setOnItemClickListener(_recordClickListener);
        setOnItemLongClickListener(_recordLongClickListener);
        setOnCreateContextMenuListener(this);
    }

    protected void queryWallet() {
        if (_recordsCursor != null)
            _recordsCursor.close();

        Set<Long> masterRecordIds = new TreeSet<>();
        long start = System.currentTimeMillis();
        _recordsCursor = _wallet.getRecordsCursor(_filter, null, masterRecordIds);
        long time = System.currentTimeMillis() - start;
        Log.d(getClass().getSimpleName(), "Wallet query took " + time + "ms");
        _adapter = new RecordCursorAdapter(getContext(), _recordsCursor, masterRecordIds, _wallet);
        start = System.currentTimeMillis();
        setAdapter(_adapter);
        time = System.currentTimeMillis() - start;
        Log.d(getClass().getSimpleName(), "Populating record list took " + time + "ms");
    }

    private final OnItemClickListener _recordClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final boolean startedSelection =
                    _adapter.getSelection().first != RecordCursorAdapter.INVALID_POSITION;
            if (!_allowRangeSelection || !startedSelection) {
                if (_extRecordClickListener != null) {
                    try {
                        _extRecordClickListener.onItemClick(parent, view, position, id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return;
            }

            if (indexInSelection(position)) {
                _adapter.resetSelection();
            }
            else {
                _adapter.setSelectionEnd(position);
            }
            notifySelectionChanged();
        }
    };

    private final OnItemLongClickListener _recordLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            if (!_allowRangeSelection)
                return false;
            if (indexInSelection(position))
                return false;
            _adapter.setSelectionStart(position);
            _adapter.setSelectionEnd(position);
            notifySelectionChanged();
            return true;
        }
    };

    private boolean indexInSelection(int index) {
        final Pair<Integer, Integer> selection = _adapter.getSelection();
        return selection.first <= index && (index - selection.first) < selection.second;
    }

    private void notifySelectionChanged() {
        if (_selectionListener == null)
            return;
        final Pair<Integer, Integer> selection = _adapter.getSelection();
        final long recID = selection.first == RecordCursorAdapter.INVALID_POSITION
            ? Record.UNKNOWN_ID : _adapter.getItemId(selection.first);
        try {
            _selectionListener.OnSelectionRangeChanged(recID, selection);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final WalletEvents.AnyEventObserver _walletObserver =
            new WalletEvents.AnyEventObserver(WalletEvents.WE_ALL_EVENTS) {
        @Override
        public void onWalletChanged(final long eventFlag) {
            if (
                    (eventFlag & WE_RECORD_ADDED) > 0 ||
                    (eventFlag & WE_RECORD_REMOVED) > 0 ||
                    (eventFlag & WE_BLK_REC_REMOVAL) > 0 ||
                    (eventFlag & WE_BLK_COPY_COMPLETE) > 0) {
                // Reset selection in case some records were (possibly) added or removed.
                resetSelection();
            }
            _recordsCursor.requery();
        }
    };

    private static final Drawable TRANSPARENT = new ColorDrawable(Color.TRANSPARENT);

    private boolean _allowRangeSelection = false;
    private WalletFilter _filter;
    private Wallet _wallet;
    private RecordCursorAdapter _adapter;
    private Cursor _recordsCursor;
    private SelectionRangeListener _selectionListener;
    private OnItemClickListener _extRecordClickListener;
    private SelectionContextMenuProvider _selectionMenuProvider;
}

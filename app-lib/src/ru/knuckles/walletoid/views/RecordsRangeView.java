/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletFilter;
import ru.knuckles.walletoid.dialogs.CategorySelectDialog;
import ru.knuckles.walletoid.dialogs.CategorySelectDialog.OnCategorySelectedListener;
import ru.knuckles.walletoid.dialogs.MonthSelectDialog;
import ru.knuckles.walletoid.dialogs.MonthSelectDialog.OnMonthSelectedListener;
import ru.knuckles.walletoid.views.LeftRightSwitcher.SwitchObserver;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import static ru.knuckles.walletoid.database.WalletFilter.FilterObserver;

/**
 * This composite view consists of category and date selectors on top of {@link RecordsListView},
 * which is responsible for displaying {@link Wallet} contents.
 * Recently there's no longer a single list view, but rather a pager with pseudo-unlimited set of
 * those.
 */
public class RecordsRangeView extends LinearLayout {
    public RecordsRangeView(Context context, boolean showFilterControls) {
        super(context);
        _showFilterControls = showFilterControls;
        init();
    }

    public RecordsRangeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _showFilterControls = attrs.getAttributeBooleanValue(null, "show_filter_controls", true);
        init();
    }

    public RecordsListView getCurrentView() {
        return _pageAdapter.getListAtPosition(_pager.getCurrentItem());
    }

    /**
     * Copy of the filter, which defines the displayed content, i.e. listed records.
     */
    public WalletFilter copyFilter() {
        return new WalletFilter(_filter);
    }

    public CalendarUtils.TimeInterval getInterval() {
        return _pageAdapter.getInterval();
    }

    /**
     * Observe changes to the filter, which defines the displayed content, i.e. listed records.
     */
    public void addFilterObserver(FilterObserver observer) {
        _filter.addObserver(observer);
    }

    public void removeFilterObserver(FilterObserver observer) {
        _filter.removeObserver(observer);
    }

    public void setRange(Calendar startDate, CalendarUtils.TimeInterval interval) {
        _pageAdapter.setWallet(null);  // Optimize out unneeded queries.
        _pager.setCurrentItem(_pageAdapter.getZeroIndex(), false);
        _pageAdapter.setRange(startDate, interval);
        _pageAdapter.setWallet(_wallet);

        afterAdapterRangeChanged(_pager.getCurrentItem());
    }

    public void setCategoryFilter(Category.Kind kind, long categoryId) {
        _pageAdapter.setCategoryFilter(kind, categoryId);
        _filter.setCategory(kind, categoryId);
        if (_wallet != null)
            updateCatNameView();
    }

    public void setFilterQuery(String query) {
        _filter.setQuery(query);
        _pageAdapter.setFilterQuery(query);
    }

    public void setRevSorting(boolean revSorting) {
        _filter.setRevSorting(revSorting);
        _pageAdapter.setRevSorting(revSorting);
    }
    // TODO: an end must be put to this filter parameter passing madness.

    public void setSelectionRangeListener(RecordsListView.SelectionRangeListener listener) {
        _pageAdapter.setSelectionRangeListener(listener);
    }

    public void setSelectionContextMenuProvider(RecordsListView.SelectionContextMenuProvider scmp) {
        _pageAdapter.setSelectionContextMenuProvider(scmp);
    }

    public void setItemClickListener(AdapterView.OnItemClickListener listener) {
        _pageAdapter.setRecordClickListener(listener);
    }

    public void setAllowRangeSelection(boolean allow) {
        _pageAdapter.setAllowRangeSelection(allow);
    }

    public void resetSelection() {
        _pageAdapter.resetRecordSelection();
    }

    // TODO: This should not be public.
    public String getCategoryFilterDesc() {
        // TODO: Observe Wallet and update text when Category has changed.
        // TODO: What if Category is removed?
        final String text;
        if (_filter.getCategoryID() != Category.UNKNOWN_ID) {
            text = _wallet.getCategory(_filter.getCategoryID()).getName();
        }
        else if (_filter.getCategoriesKind() != Category.Kind.ALL) {
            int resId = _filter.getCategoriesKind() == Category.Kind.INCOME ?
                    R.string.incomes : R.string.expenses;
            text = getContext().getString(resId);
        }
        else {
            text = getContext().getString(R.string.all_categories);
        }
        return text;
    }

    public void setWallet(Wallet wallet) {
        _pageAdapter.setWallet(wallet);
        _wallet = wallet;
        if (_wallet == null)
            return;
        // Some filters may have been already applied.
        updateCatNameView();
    }

    public void saveStateToBundle(Bundle bundle) {
        bundle.putLong(FILTER_START_DATE_KEY, _filter.getDateRangeStart().getTimeInMillis());
        bundle.putLong(FILTER_END_DATE_KEY, _filter.getDateRangeEnd().getTimeInMillis());
        bundle.putLong(FILTER_CATEGORY_KEY, _filter.getCategoryID());
        bundle.putInt(FILTER_KIND_KEY, _filter.getCategoriesKind().ordinal());
    }

    public void restoreStateFromBundle(Bundle bundle) {
        _filter.setFilters(
            bundle.getLong(FILTER_START_DATE_KEY),
            bundle.getLong(FILTER_END_DATE_KEY),
            bundle.getLong(FILTER_CATEGORY_KEY)
        );
        _filter.setCategory(
                Category.Kind.values()[bundle.getInt(FILTER_KIND_KEY)],
                bundle.getLong(FILTER_CATEGORY_KEY));
    }

    // Private--------------------------------------------------------------------------------------

    private void init() {
        final int viewVis = _showFilterControls ? View.VISIBLE : View.GONE;
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.records_range_view, this, true);

        _pageAdapter = new RecordsPageAdapter(inflater, !_showFilterControls);

        _categoryButton = (Button) findViewById(R.id.categoryButton);
        _categoryButton.setVisibility(viewVis);
        _categoryButton.setOnClickListener(_catBtnClickHandler);

        _pager = (ViewPager) findViewById(R.id.RecordsPager);
        _pager.setAdapter(_pageAdapter);
        _pager.setCurrentItem(_pageAdapter.getZeroIndex());
        _pager.addOnPageChangeListener(_pageChangeListener);

        _periodPicker = (LeftRightSwitcher) findViewById(R.id.periodPicker);
        _periodPicker.setVisibility(viewVis);
        _periodPicker.addSwitchObserver(_periodSwitchObserver);  // To switch pages.
    }

    private void updateCatNameView() {
        _categoryButton.setText(getCategoryFilterDesc());
    }

    private void afterAdapterRangeChanged(final int position) {
        _pageAdapter.copyDateRangeAtPosition(position, _filter);
        _dateFormat = INTERVAL_FORMATS.get(_pageAdapter.getInterval());
        _periodPicker.setText(_dateFormat.format(_filter.getDateRangeStart().getTime()));
    }

    // Event listeners

    private final SwitchObserver _periodSwitchObserver = new SwitchObserver() {
        // TODO: Partial copy from CalendarRangePicker.

        @Override
        public void onCenterClick() {
            if (_pageAdapter.getInterval() != CalendarUtils.TimeInterval.MONTH)
                return;  // TODO: This dialog is unfit to select anything than a month.

            if (dialog == null) {
                dialog = new MonthSelectDialog(getContext(), _onMonthSelectedListener);
            }

            final Calendar start = _filter.getDateRangeStart();
            dialog.prepare(start.get(Calendar.YEAR), start.get(Calendar.MONTH));
            dialog.show();
        }

        @Override
        public void onLeftSwitch() {
            _pager.setCurrentItem(_pager.getCurrentItem() - 1, true);
        }

        @Override
        public void onRightSwitch() {
            _pager.setCurrentItem(_pager.getCurrentItem() + 1, true);
        }

        private MonthSelectDialog dialog;
    };

    private OnMonthSelectedListener _onMonthSelectedListener = new OnMonthSelectedListener() {
        @Override
        public void onMonthSelected(final MonthSelectDialog srcDlg, final int year,
                final int month) {
            _pageAdapter.setWallet(null);  // Optimize out unneeded queries.
            _pager.setCurrentItem(_pageAdapter.getZeroIndex(), false);
            _pageAdapter.setMonth(year, month);
            _pageAdapter.setWallet(_wallet);

            afterAdapterRangeChanged(_pager.getCurrentItem());
        }
    };

    private final OnClickListener _catBtnClickHandler = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final Dialog dialog = new CategorySelectDialog(
                    getContext(),
                    new ArrayList<>(_wallet.getCategoriesMap(Category.Kind.ALL, true).values()),
                    _catSelListener,
                    true);
            dialog.show();
        }
    };

    private final OnCategorySelectedListener _catSelListener = new OnCategorySelectedListener() {
        @Override
        public void onCategoryKindSelected(CategorySelectDialog srcDlg, Category.Kind catKind) {
            setCategoryFilter(catKind, Category.UNKNOWN_ID);
        }

        @Override
        public void onCategorySelected(CategorySelectDialog srcDlg, long selectedCatID) {
            setCategoryFilter(Category.Kind.ALL, selectedCatID);
        }
    };

    private final ViewPager.OnPageChangeListener _pageChangeListener =
            new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {
            afterAdapterRangeChanged(position);
            _pageAdapter.resetRecordSelection();
        }
    };

    // Const.
    private final Map<CalendarUtils.TimeInterval, DateFormat> INTERVAL_FORMATS =
            CalendarRangePicker.createIntervalFormatsMap();

    // Controls
    private Button _categoryButton;
    private LeftRightSwitcher _periodPicker;
    private ViewPager _pager;

    // Other data
    private Wallet _wallet;
    private final WalletFilter _filter = new WalletFilter();
    private RecordsPageAdapter _pageAdapter;
    private DateFormat _dateFormat = INTERVAL_FORMATS.get(CalendarUtils.TimeInterval.MONTH);

    private final boolean _showFilterControls;

    // PrefUtils
    private static final String FILTER_START_DATE_KEY = "FILTER_START_DATE";
    private static final String FILTER_END_DATE_KEY = "FILTER_END_DATE";
    private static final String FILTER_CATEGORY_KEY = "FILTER_CATEGORY";
    private static final String FILTER_KIND_KEY = "FILTER_CAT_KIND";
}

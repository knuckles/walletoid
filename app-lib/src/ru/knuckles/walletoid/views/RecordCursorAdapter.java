/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ResourceCursorAdapter;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.CursorUtils;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;

import java.util.Set;

public class RecordCursorAdapter extends ResourceCursorAdapter {
    RecordCursorAdapter(Context context, Cursor c, Set<Long> masterRecordIds, Wallet wallet) {
        super(context, R.layout.recordline, c, true);  // TODO: Autorequery is 'discouraged'.

        // Read theme values.
        TypedArray themeValues = context.getTheme().obtainStyledAttributes(new int[] {
                android.R.attr.colorBackground,
                android.R.attr.textColorHighlight
        });
        final int listBGColor;
        try {
            listBGColor = themeValues.getColor(0, Color.WHITE);
            _selectionBGColor = themeValues.getColor(1, Color.BLUE);
        } finally {
            themeValues.recycle();
        }

        _binder = new RecordLineBinder(wallet, listBGColor, false);
        _recFactory = new CursorUtils.RecordFactory(wallet, masterRecordIds);
    }

    public Pair<Integer, Integer> getSelection() {
        final boolean validRange = validRange();
        final int length = validRange ? _selectionEnd - _selectionStart + 1 : 0;
        return new Pair<>(_selectionStart, length);
    }

    public void setSelectionStart(int start) {
        if (_selectionStart == start)
            return;
        _selectionStart = start;
        fixRange();
        notifyDataSetChanged();
    }

    public void setSelectionEnd(int end) {
        if (_selectionEnd == end)
            return;
        _selectionEnd = end;
        fixRange();
        notifyDataSetChanged();
    }

    public void resetSelection() {
        if (_selectionStart == INVALID_POSITION && _selectionEnd == INVALID_POSITION)
            return;
        _selectionStart = _selectionEnd = INVALID_POSITION;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if (view == null)
            return null;

        // Set selection background.
        view.setBackgroundColor(validRange() && positionSelected(position) ?
                _selectionBGColor :
                _normalBGColor);
        // TODO: Set text color to something contrasting.

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Record currentRec = _recFactory.extractObject(cursor).second;

        // Determine if current record is on the same day as previous one.
        boolean sameDay = false;
        if (cursor.move(-1)) {  // Move to previous record.
            try {
                final Record prevRec = _recFactory.extractObject(cursor).second;
                sameDay = CalendarUtils.isSameDay(
                        currentRec.getCalendar(), prevRec.getCalendar());
            } finally {
                cursor.move(1);  // Restore cursor position.
            }
        }

        _binder.bindView(view, context, currentRec, !sameDay);
    }

    private void fixRange() {
        if (!validRange())
            return;
        final int min = Math.min(_selectionStart, _selectionEnd);
        final int max = Math.max(_selectionStart, _selectionEnd);
        _selectionStart = min;
        _selectionEnd = max;
    }

    private boolean validRange() {
        return _selectionEnd != INVALID_POSITION && _selectionStart != INVALID_POSITION;
    }

    private boolean positionSelected(final int position) {
        return _selectionStart <= position && position <= _selectionEnd;
    }

    static final int INVALID_POSITION = -1;

    private final RecordLineBinder _binder;
    private final CursorUtils.RecordFactory _recFactory;
    private int _selectionStart = INVALID_POSITION;
    private int _selectionEnd = INVALID_POSITION;
    private final int _selectionBGColor;
    private final static int _normalBGColor = Color.TRANSPARENT;
}

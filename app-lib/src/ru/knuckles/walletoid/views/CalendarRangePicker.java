/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.CalendarUtils.DateRange;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.dialogs.MonthSelectDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import static ru.knuckles.walletoid.dialogs.MonthSelectDialog.OnMonthSelectedListener;

public class CalendarRangePicker extends LeftRightSwitcher {
    public CalendarRangePicker(Context context) {
        this(context, null);
    }

    public CalendarRangePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        updateText();
        addSwitchObserver(_switchObserver);
        _range.addObserver(_rangeObserver);
    }

    public DateRange range() { return _range; }

    // Private.

    private void updateText() {
        final DateFormat format = INTERVAL_FORMATS.get(_range.getInterval());
        if (format == null)
            throw new IllegalArgumentException();

        setText(_dateFormat.format(_range.getRangeStart().getTime()));
    }

    private final SwitchObserver _switchObserver = new SwitchObserver() {
        @Override
        public void onCenterClick() {
            if (_range.getInterval() != CalendarUtils.TimeInterval.MONTH)
                return;  // TODO: This dialog is unfit to select anything than a month.

            if (dialog == null)
                dialog = new MonthSelectDialog(getContext(), _onMonthSelectedListener);

            final Calendar start = _range.getRangeStart();
            dialog.prepare(start.get(Calendar.YEAR), start.get(Calendar.MONTH));
            dialog.show();
        }

        @Override
        public void onLeftSwitch() { _range.inc(-1); }

        @Override
        public void onRightSwitch() { _range.inc(1); }

        private MonthSelectDialog dialog;
    };

    private final DateRange.Observer _rangeObserver = new DateRange.Observer() {
        @Override
        public void onPeriodChanged(final Calendar start, final Calendar end) {
            final DateFormat format = INTERVAL_FORMATS.get(_range.getInterval());
            if (format == null)
                throw new IllegalArgumentException();
            _dateFormat = format;
            updateText();
        }
    };

    private OnMonthSelectedListener _onMonthSelectedListener = new OnMonthSelectedListener() {
        @Override
        public void onMonthSelected(MonthSelectDialog srcDlg, int year, int month) {
            _range.setStart(year, month, 1);
        }
    };

    // Constants.
    private final static Map<TimeInterval, String> INTERVAL_FORMAT_STRINGS =
            new TreeMap<TimeInterval, String>() {{
        put(TimeInterval.YEAR, "yyyy");
        put(TimeInterval.MONTH, "LLL/yyyy");
        // Week year support (needed to fix #84) is only available starting with API v25.
        put(TimeInterval.WEEK, Build.VERSION.SDK_INT >= 25 ? "YYYY:w" : "yyyy:w");
        put(TimeInterval.DAY, "d.LL.yyyy");
    }};

    public static Map<TimeInterval, DateFormat> createIntervalFormatsMap() {
        final TreeMap<TimeInterval, DateFormat> result = new TreeMap<TimeInterval, DateFormat>();
        for (Map.Entry<TimeInterval, String> e : INTERVAL_FORMAT_STRINGS.entrySet()) {
            result.put(e.getKey(), new SimpleDateFormat(e.getValue()));
        }
        return result;
    }

    // My data.
    private final Map<TimeInterval, DateFormat> INTERVAL_FORMATS = createIntervalFormatsMap();
    private DateRange _range = new DateRange(TimeInterval.MONTH);
    private DateFormat _dateFormat = INTERVAL_FORMATS.get(_range.getInterval());
}

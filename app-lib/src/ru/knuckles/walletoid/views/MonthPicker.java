/**
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import java.text.DateFormatSymbols;
import java.util.Arrays;

public class MonthPicker extends NumberPicker{
    public MonthPicker(Context context) {
        super(context);
        init();
    }

    public MonthPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MonthPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setMinValue(0);
        setMaxValue(monthNames.length - 1);
        setDisplayedValues(monthNames);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    private String[] monthNames = new DateFormatSymbols().getMonths();
    {
        // This is a workaround for the JDK bug: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4146173
        if (monthNames.length > 12)
            monthNames = Arrays.copyOf(monthNames, 12);
    }
}

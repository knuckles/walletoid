/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import ru.knuckles.walletoid.R;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class LeftRightSwitcher extends LinearLayout {
    public interface SwitchObserver {
        void onCenterClick();
        void onLeftSwitch();
        void onRightSwitch();
    }

    public LeftRightSwitcher(final Context context) {
        this(context, null);
    }

    public LeftRightSwitcher(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(getContext()).inflate(R.layout.left_right_switcher, this);

        _periodButton = (Button) findViewById(R.id.periodButton);
        _periodButton.setOnClickListener(_periodButtonClickLister);

        Button leftButton = (Button) findViewById(R.id.decBtn);
        leftButton.setOnClickListener(_leftButtonClickListener);

        Button rightButton = (Button) findViewById(R.id.incBtn);
        rightButton.setOnClickListener(_rightButtonClickListener);
    }

    public void addSwitchObserver(SwitchObserver observer) {
        _observers.add(observer);
    }

    public void removeObserver(SwitchObserver observer) {
        _observers.remove(observer);
    }

    public void setText(final String text) {
        _periodButton.setText(text);
    }

    // Listeners.
    private final OnClickListener _periodButtonClickLister = new OnClickListener() {
        @Override
        public void onClick(View view) {
            for (SwitchObserver observer : _observers) {
                try {
                    observer.onCenterClick();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final OnClickListener _leftButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View incBtn) {
            for (SwitchObserver observer : _observers) {
                try {
                    observer.onLeftSwitch();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final OnClickListener _rightButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View incBtn) {
            for (SwitchObserver observer : _observers) {
                try {
                    observer.onRightSwitch();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Set<SwitchObserver> _observers = new TreeSet<>(new Comparator<SwitchObserver>() {
        @Override
        public int compare(final SwitchObserver o1, final SwitchObserver o2) {
            return o1.hashCode() - o2.hashCode();
        }
    });

    private Button _periodButton;
}

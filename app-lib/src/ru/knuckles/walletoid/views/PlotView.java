/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import ru.knuckles.walletoid.Utils;

import java.util.*;

public class PlotView extends View {

    public static class ColoredValue extends Pair<Integer, Float> {
        /**
         * Constructor for a Pair.
         *
         * @param first  the first object in the Pair
         * @param second the second object in the pair
         */
        public ColoredValue(Integer first, Float second) {
            super(first, second);
        }

        @ColorInt public int color() { return first; }
        public float value() { return second; }

        public static Comparator<ColoredValue> comparator = new Comparator<ColoredValue>() {
            @Override
            public int compare(ColoredValue lhs, ColoredValue rhs) {
                return (int) Math.signum(lhs.value() - rhs.value());
            }
        };
    }

    public static class Column extends ArrayList<ColoredValue> {
        public float height() {
            float result = 0;
            for (ColoredValue cv : this) {
                result += cv.value();
            }
            return result;
        }
    }

    public static class RulerLabeler {
        public String getXAxisName() { return null; }
        public String getYAxisName() { return null; }

        public String makeXLabel(int rulerNr, int of) {
            return String.valueOf(rulerNr);
        }

        public String makeYLabel(int rulerNr, int of, Utils.ApproxValue stepping) {
            return String.format(Locale.getDefault(), Y_LABEL_TPL, rulerNr * stepping.value) +
                    (rulerNr == 0 ? "" : stepping.suffix);
        }

        protected static String Y_LABEL_TPL = "%.0f";
    }

    public PlotView(Context context) {
        this(context, null);
    }

    public PlotView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlotView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setPoints(isInEditMode() ? testPoints : null);

        // Theme setup.
        TypedArray themeValues = getContext().getTheme().obtainStyledAttributes(new int[] {
                android.R.attr.textColorHint
        });
        try {
            @ColorInt final int color = themeValues.getColor(0, Color.RED);
            _rulersPaint.setColor(color);
            _textPaint.setColor(color);
        }
        finally {
            themeValues.recycle();
        }
    }

    /**
     * This must be called from a UI thread.
     */
    public void setPoints(List<Column> columns) {
        _columns = (columns == null) ? new ArrayList<Column>() : columns;
        for (Column column : _columns) {
            if (column == null)
                continue;
            Collections.sort(column, Collections.reverseOrder(ColoredValue.comparator));
        }
        recalcBoundaries();
        invalidate();
    }

    public RulerLabeler getRulerLabeler() {
        if (_labeler == null)
            _labeler = new RulerLabeler();
        return _labeler;
    }

    public void setRulerLabeler(RulerLabeler labeler) {
        _labeler = labeler;
    }

    public boolean getLogScale() {
        return _logScale;
    }

    public boolean setLogScale(boolean logScale) {
        _logScale = logScale;
        rescale();
        invalidate();
        return _logScale;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        rescale();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(
                xToDrawCoord(0) - _xOffset,
                yToDrawCoord(-_maxY) + _textPaint.getTextSize());

        final RulerLabeler labeler = getRulerLabeler();

        // Vertical rulers.
        final int numVRulers = _columns.size() + 1;
        for (int i = 0; i <= numVRulers ; i++) {
            final float x  = xToDrawCoord(i);
            canvas.drawLine(x, yToDrawCoord(0),
                            x, yToDrawCoord(_maxY),
                            _rulersPaint);
            final String textMark = labeler.makeXLabel(i, numVRulers);
            if (textMark != null)
                canvas.drawText(
                        textMark,
                        x + H_SPACE,
                        _textPaint.getTextSize() * V_SPACING,
                        _textPaint);
            if (i == numVRulers) {
                final String axisLabel = labeler.getXAxisName();
                if (axisLabel != null) {
                    canvas.drawText(
                            axisLabel,
                            x - _textPaint.measureText(axisLabel),
                            _textPaint.getTextSize() * V_SPACING,
                            _textPaint);
                }
            }
        }

        // Horizontal rulers.
        final double yLimit = (_maxY / _yStep.originalValue);
        if (Double.isInfinite(yLimit))
            throw new IllegalStateException("_maxY " + String.valueOf(_maxY) + ", " +
                    _yStep.toString());
        final int numHRulers = (int) yLimit;
        final float textVSpacing = (V_SPACING - 1) * _textPaint.getTextSize();
        for (int i = 0; i <= numHRulers; i++) {
            final float y = yToDrawCoord(i * _yStep.originalValue);
            canvas.drawLine(xToDrawCoord(0), y,
                            xToDrawCoord(_columns.size()), y,
                            _rulersPaint);
            if (i != 0) {
                final String textMark = labeler.makeYLabel(i, numHRulers, _yStep);
                if (textMark != null) {
                    canvas.drawText(
                            textMark,
                            _xOffset - _textPaint.measureText(textMark) - H_SPACE,
                            y - textVSpacing,
                            _textPaint);
                }
            }
            if (i == numHRulers) {
                final String axisLabel = labeler.getYAxisName();
                if (axisLabel != null) {
                    canvas.drawText(
                            axisLabel,
                            _xOffset + H_SPACE,
                            y - textVSpacing,
                            _textPaint);
                }
            }
        }

        // Draw data points.

        // Let columns take 75% max available space;
        final float barWidth = (float) (COL_WIDTH_PERCENTAGE * _colWidth);
        final float barOffset = (float) ((_colWidth - barWidth) / 2.0);
        for (int i = 0; i < _columns.size(); i++) {
            Column column  = _columns.get(i);
            if (column == null)
                continue;
            float prevY = 0;
            for (ColoredValue cv : column) {
                _dataPaint.setColor(cv.color());
                float left = xToDrawCoord(i) + barOffset;
                canvas.drawRect(
                        left,
                        yToDrawCoord(prevY + cv.value()),
                        left + barWidth,
                        yToDrawCoord(prevY),
                        _dataPaint
                );
                prevY += cv.value();
            }
        }
    }

    private float xToDrawCoord(double x) {
        return Math.min((float) (_xOffset + x * _colWidth), getWidth() - 1);
    }

    private float yToDrawCoord(double y) {
        if (_logScale)
            y = (y == 0) ? 0f : (float) (Math.signum(y) * Math.log10(Math.abs(y)));
        return (float) (-y * _yScale);
    }

    private void rescale() {
        // Offset Y axis to accommodate the biggest value label.
        final int lastIdx = (int) (_maxY / _yStep.originalValue);
        final String longestLabel = getRulerLabeler().makeYLabel(lastIdx, lastIdx, _yStep);
        _xOffset = longestLabel == null ? 0 :
                (int) (_textPaint.measureText(longestLabel) + H_SPACE);
        _colWidth = (getWidth() - _xOffset);
        if (_columns.size() > 0)
            _colWidth /= _columns.size();
        // Leave space for textual labels on top and bottom.
        _yScale = (getHeight() - (int) (_textPaint.getTextSize() * V_SPACING * 2)) /
                  (_logScale ? (float) Math.log10(_yRange) : _yRange);
    }

    private void recalcBoundaries() {
        // Calculate data value ranges.
        _maxY = 0;
        if (_columns != null) {
            for (Column column : _columns) {
                if (column == null)
                    continue;
                final float colHeight = column.height();
                if (colHeight > _maxY)
                    _maxY = colHeight;
            }
        }

        _yRange = Math.abs(_maxY);
        final double yStep = adaptiveStep(_yRange);
        _yStep = new Utils.ApproxValue(yStep);

        // Adjust displayed boundaries.
        _maxY = Math.ceil(_maxY / yStep) * yStep;
        _yRange = Math.abs(_maxY);

        rescale();
    }

    private static double adaptiveStep(double range) {
        final int rangeOrder = Utils.valueOrder(range);
        final double stepMul = Math.pow(10, rangeOrder);
        final double stepBase = Math.ceil(range / Math.pow(10, rangeOrder + 1) / 2.0);
        return stepBase * stepMul;
    }

    // Data
    private List<Column> _columns;
    private double _maxY = 0;
    private double _yRange = 0;
    private double _colWidth = 1;
    private double _yScale = 1;
    private int _xOffset = 0;
    private Utils.ApproxValue _yStep = new Utils.ApproxValue(1);
    private final Paint _rulersPaint = new Paint();
    private final Paint _dataPaint;
    private final Paint _textPaint;


    // Look-n-feel settings.
    private boolean _logScale = false;
    private RulerLabeler _labeler;
    private final static int TEXT_SIZE = 12;  // DIPs
    private final static int RULERS_ALPHA = 150;
    private final static int RULERS_WIDTH = 0;
    private final static int DATA_ALPHA = 0;
    private final static float COL_WIDTH_PERCENTAGE = 0.5f;
    private final static float V_SPACING = 1.2f;  // Vertical text space multiplier.
    private static float H_SPACE = 0;  // Horizontal spacing between text labels and other elements.

    {
        _rulersPaint.setAntiAlias(false);
        _rulersPaint.setStrokeWidth(RULERS_WIDTH);
        _rulersPaint.setAlpha(RULERS_ALPHA);

        _textPaint = new Paint(_rulersPaint);
        _textPaint.setAntiAlias(true);
        _textPaint.setTextSize(
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE,
                        Resources.getSystem().getDisplayMetrics()));
        H_SPACE = _textPaint.measureText(" ");

        _dataPaint = new Paint(_rulersPaint);
        _dataPaint.setAlpha(DATA_ALPHA);
        _dataPaint.setAntiAlias(true);
        _dataPaint.setStrokeWidth(0);
    }

    private final static List<Column> testPoints = new ArrayList<Column>() {{
        add(new Column() {{
            add(new ColoredValue(Color.RED, 10f));
            add(new ColoredValue(Color.YELLOW, 20f));
            add(new ColoredValue(Color.BLUE, 30f));
        }});

        add(new Column() {{
            add(new ColoredValue(Color.RED, 30f));
            add(new ColoredValue(Color.YELLOW, 20f));
            add(new ColoredValue(Color.BLUE, 10f));
        }});

        add(new Column() {{
            add(new ColoredValue(Color.RED, 20f));
            add(new ColoredValue(Color.YELLOW, 10f));
            add(new ColoredValue(Color.BLUE, 30f));
        }});
    }};
}

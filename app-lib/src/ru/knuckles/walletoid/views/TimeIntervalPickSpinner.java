/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.R;

import java.util.ArrayList;
import java.util.List;

public class TimeIntervalPickSpinner
        extends Spinner {

    public TimeIntervalPickSpinner(Context context) {
        super(context);
        init();
    }

    public TimeIntervalPickSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TimeInterval selectedInterval() {
        return TimeInterval.values()[getSelectedItemPosition()];
    }

    public void setSelectedInterval(TimeInterval interval) {
        this.setSelection(interval.ordinal());
    }

    // PRIVATE

    private static class IntervalAdapter {
        static List<IntervalAdapter> getAllIntervalAdapters(Context context) {
            TimeInterval[] allValues = TimeInterval.values();
            List<IntervalAdapter> result = new ArrayList<>(allValues.length);
            for (TimeInterval i : allValues) {
                result.add(new IntervalAdapter(context, i));
            }
            return result;
        }

        IntervalAdapter(Context context, TimeInterval interval) {
            this.context = context;
            this.interval = interval;
        }

        @Override
        public String toString() {
            return context.getString(LABEL_RESOURCE_IDS[interval.ordinal()]);
        }

        final static int[] LABEL_RESOURCE_IDS = new int[] {
            R.string.each_year,
            R.string.each_month,
            R.string.each_week,
            R.string.each_day
        };

        private Context context;
        private TimeInterval interval;
    }

    private void init() {
        final ArrayAdapter adapter = new ArrayAdapter<IntervalAdapter>(
                getContext(),
                android.R.layout.simple_spinner_item,
                IntervalAdapter.getAllIntervalAdapters(getContext())) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setText(IntervalAdapter.LABEL_RESOURCE_IDS[position]);
                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        setAdapter(adapter);
    }
}

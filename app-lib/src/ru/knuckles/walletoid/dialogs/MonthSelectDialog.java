/**
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.NumberPicker;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.views.MonthPicker;

import java.util.Calendar;

public class MonthSelectDialog extends Dialog implements android.view.View.OnClickListener {

    public interface OnMonthSelectedListener {
        void onMonthSelected(MonthSelectDialog srcDlg, int year, int month);
    }

    private OnMonthSelectedListener _listener;

    // Controls
    private MonthPicker _monthPicker;
    private NumberPicker _yearSelector;

    public MonthSelectDialog(Context context, OnMonthSelectedListener listener) {
        super(context);

        _listener = listener;

        setContentView(R.layout.month_select_dlg);
        setTitle(R.string.month_dlg_title);
        getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        _monthPicker = (MonthPicker) findViewById(R.id.monthPicker);

        final Calendar cal = Calendar.getInstance();
        _yearSelector = (NumberPicker) findViewById(R.id.yearText);
        _yearSelector.setMinValue(cal.getMinimum(Calendar.YEAR));
        _yearSelector.setMaxValue(cal.getMaximum(Calendar.YEAR));

        ((Button) findViewById(R.id.AcceptMonthBtn)).setOnClickListener(this);
        ((Button) findViewById(R.id.CancelMonthBtn)).setOnClickListener(this);
    }

    // OnClickListener
    @Override
    public void onClick(View v) {
        final int vid = v.getId();
        if (vid == R.id.AcceptMonthBtn) {
            onOKClick(v);
        }
        dismiss();
    }

    private void onOKClick(View v) {
        _listener.onMonthSelected(this, _yearSelector.getValue(), (int) _monthPicker.getValue());
    }

    public void prepare(int year, int month) {
        _yearSelector.setValue(year);
        _monthPicker.setValue(month);
    }
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.views.ColorPickerView;

public class CategoryEditDialog extends Dialog implements OnClickListener {

    public interface OnCategoryEditedListener {
        void onCategoryEdited(CategoryEditDialog dlg, Category newCategory);
    }

    private View.OnClickListener _colorViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new ColorPickerDialog(
                CategoryEditDialog.this.getContext(),
                new ColorPickerView.OnColorChangedListener() {
                    @Override
                    public void colorChanged(int color) {
                        _selectedColor = color;
                        updateCatColorView();
                    }
                },
                _selectedColor
            ).show();
        }
    };
    
    public CategoryEditDialog(Context context, Category editedCategory, boolean allowKindChange,
            OnCategoryEditedListener listener) {
        super(context);
        _allowKindChange = allowKindChange;
        assert(editedCategory != null);

        _listener = listener;

        setContentView(R.layout.category_edit_dlg);
        setTitle(R.string.rec_category);
        getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        
        _catNameInput = (EditText) findViewById(R.id.categoryNameInput);
        _catColorView = (TextView) findViewById(R.id.categoryColorView);
        _incomeCheckBox = (CheckBox) findViewById(R.id.categoryIncomeCheck);
        if (!allowKindChange)
            _incomeCheckBox.setVisibility(View.GONE);

        _catColorView.setOnClickListener(_colorViewClickListener);
        findViewById(R.id.AcceptCatEditBtn).setOnClickListener(this);
        findViewById(R.id.CancelCatEditBtn).setOnClickListener(this);
        
        updateCategoryData(editedCategory);
    }
    
    // OnClickListener
    @Override
    public void onClick(View v) {
        final int vid = v.getId();
        if (vid == R.id.AcceptCatEditBtn) {
            onOKClick(v);
        }
        else if (vid == R.id.CancelCatEditBtn) {
                onCancelClick(v);
        }
    }

    public void updateCategoryData(Category editedCategory) {
        _catNameInput.setText(editedCategory.getName());
        _selectedColor = editedCategory.getColor();
        updateCatColorView();
        if (_allowKindChange)
            _incomeCheckBox.setChecked(editedCategory.getKind().isIncome());
        _origCategory = editedCategory;
    }

    public static Drawable createCategoryCircle(int color) {
        final ShapeDrawable bg = new ShapeDrawable(_OVAL);
        bg.setBounds(0, 0, (int) _OVAL.getWidth(), (int) _OVAL.getHeight());
        bg.getPaint().setColor(color);
        return bg;
    }
    
    @SuppressLint({ "OnClick", "OnClick" })
    private void onCancelClick(View v) {
        dismiss();
    }

    private void onOKClick(View v) {
        try {
            if (_listener == null)
                return;
            Category result = new Category(
                _origCategory.getId(),
                _catNameInput.getText().toString(),
                _selectedColor,
                _allowKindChange ?
                    (_incomeCheckBox.isChecked() ? Category.Kind.INCOME : Category.Kind.EXPENSE ) :
                    _origCategory.getKind()
            );
            _listener.onCategoryEdited(this, result);
        }
        finally {
            dismiss();
        }
    }

    private void updateCatColorView() {
        _catColorView.setCompoundDrawables(createCategoryCircle(_selectedColor), null, null, null);
    }

    final private static OvalShape _OVAL = new OvalShape();
    static {
        final float radPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,
                Resources.getSystem().getDisplayMetrics());
        _OVAL.resize(radPx, radPx);
    }

    // PRIVATE
    private int _selectedColor;
    private Category _origCategory;
    private final OnCategoryEditedListener _listener;
    private final boolean _allowKindChange;

    // Controls
    private EditText _catNameInput;
    private TextView _catColorView;
    private CheckBox _incomeCheckBox;
}

/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryMappingDialog extends Dialog {
    public CategoryMappingDialog(Context context,
                                 Map<Long, Category> keyCategories,
                                 Map<Long, Category> valCategories,
                                 Map<Long, Long> mapping) {
        super(context);
        assert keyCategories != null;
        assert valCategories != null;
        assert mapping != null;
        _keyCategories = keyCategories;
        _valCategories = valCategories;
        _mapping = mapping;
        init();
    }

    private void init() {
        setTitle(R.string.app_name);
        getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        ListView categoriesMapList = new ListView(getContext());

        _catsAdapter = new MappingAdapter(
            getContext(),
            android.R.layout.simple_list_item_1,
            new ArrayList<Category>(_keyCategories.values()));
        categoriesMapList.setAdapter(_catsAdapter);
        categoriesMapList.setOnItemClickListener(_listClickListener);

        setContentView(categoriesMapList);
    }

    private class MappingAdapter extends ArrayAdapter<Category> {
        public MappingAdapter(Context context, int textViewResourceId, List<Category> objects) {
            super(context, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);

            Category currKeyCategory = getItem(position);
            Category mappedTo = _mapping.containsKey(currKeyCategory.getId()) ?
                _valCategories.get(_mapping.get(currKeyCategory.getId())) : null;
            String destName = mappedTo != null ?
                mappedTo.getName() :
                getContext().getString(R.string.new_cat_marker);
            view.setText(String.format("%s -> %s", currKeyCategory.getName(), destName));

            return view;
        }
    }

    private AdapterView.OnItemClickListener _listClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, final int position, long rowId) {
            final long fromCatId = _catsAdapter.getItem(position).getId();
            Dialog catSelectDlg = new CategorySelectDialog(
                getContext(),
                new ArrayList<>(_valCategories.values()),
                new CategorySelectDialog.OnCategorySelectedListener() {
                    @Override
                    public void onCategoryKindSelected(CategorySelectDialog srcDlg, Category.Kind catKind) {
                        // Should not be called.
                    }

                    @Override
                    public void onCategorySelected(CategorySelectDialog srcDlg, long toCatID) {
                        if (toCatID == Category.UNKNOWN_ID)
                            _mapping.remove(fromCatId);
                        else
                            _mapping.put(fromCatId, toCatID);
                        _catsAdapter.notifyDataSetChanged();
                    }
                },
                false,
                getContext().getString(R.string.move_records_to));
            catSelectDlg.show();
        }
    };

    private final Map<Long, Category> _keyCategories;
    private final Map<Long, Category> _valCategories;
    private final Map<Long, Long> _mapping;
    private MappingAdapter _catsAdapter;
}

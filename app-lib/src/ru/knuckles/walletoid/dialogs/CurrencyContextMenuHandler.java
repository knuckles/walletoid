/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Currency;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.dialogs.CurrencySelectDialog.OnCurrencySelectedListener;

/**
 * This class handles context menu creation for different views displaying @link{Currency}. The provided menu allows
 * to add new, edit, and remove currencies.
 * It supports the following types of views:
 * Spinner – @link{Spinner.getSelectedItemId} must return currency Id to show menu for.
 * View – view's tag must point to a @link{Currency} instance presented by that view.
 */
public class CurrencyContextMenuHandler implements View.OnCreateContextMenuListener {
    public CurrencyContextMenuHandler(Context context, Wallet wallet) {
        _context = context;
        _wallet = wallet;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View contextView, ContextMenuInfo menuInfo) {
        _contextCurrency = getContextCurrency(contextView);
        if (_contextCurrency == null) {
            Log.d(this.getClass().getSimpleName(), "Could not determine context Category");
            return;
        }

        final MenuInflater inflater = new MenuInflater(_context);
        inflater.inflate(R.menu.currency_context, menu);

        final MenuItem addItem = menu.findItem(R.id.menu_add_currency);
        addItem.setOnMenuItemClickListener(_addEditItemListener);
        addItem.setVisible(!isIndividualCurrencyView(contextView));

        final MenuItem editItem = menu.findItem(R.id.menu_edit_currency);
        editItem.setOnMenuItemClickListener(_addEditItemListener);
        editItem.setTitle(String.format(editItem.getTitle().toString(), _contextCurrency.getName()));

        final MenuItem removeItem = menu.findItem(R.id.menu_remove_currency);
        removeItem.setOnMenuItemClickListener(_removeItemListener);
        removeItem.setTitle(String.format(removeItem.getTitle().toString(), _contextCurrency.getName()));
    }

    private final MenuItem.OnMenuItemClickListener _addEditItemListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            final Currency currency = item.getItemId() == R.id.menu_edit_currency ?
                    _contextCurrency :
                    new Currency();
            final Dialog currencyDialog =
                new CurrencyEditDialog(_context, currency, _wallet.getReferenceCurrency(), _currencyEditListener);
            currencyDialog.setTitle(_context.getString(R.string.edit_currency, currency.getName()));
            currencyDialog.show();
            return true;
        }
    };

    private final MenuItem.OnMenuItemClickListener _removeItemListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (_contextCurrency.getId() == _wallet.getReferenceCurrencyId()) {
                Toast.makeText(
                    _context,
                    R.string.cant_remove_currency,
                    Toast.LENGTH_SHORT).show();
                return true;
            }

            Dialog currSelectDialog = new CurrencySelectDialog(_context, _wallet,
                new OnCurrencySelectedListener() {
                    @Override
                    public void onCurrencySelected(long selectedCurrencyId) {
                        _wallet.removeCurrency(_contextCurrency.getId(), selectedCurrencyId);
                    }
                });
            currSelectDialog.show();
            return true;
        }
    };

    private final CurrencyEditDialog.CurrencyEditListener _currencyEditListener = new CurrencyEditDialog.CurrencyEditListener() {
        @Override
        public void onCurrencyEdited(Currency currency, boolean valueChanged) {
            if (currency.getId() == Currency.UNKNOWN_ID)
                _wallet.addCurrency(currency);
            else
                _wallet.updateCurrency(currency);
        }
    };

    private Currency getContextCurrency(View contextView) {
        if (contextView instanceof Spinner) {
            final long currencyId = ((Spinner) contextView).getSelectedItemId();
            return currencyId == Currency.UNKNOWN_ID ? null : _wallet.getCurrency(currencyId);
        }
        if (isIndividualCurrencyView(contextView))
            return (Currency) contextView.getTag();
        return null;
    }

    private boolean isIndividualCurrencyView(View contextView) {
        return contextView instanceof RadioButton;
    }

    private final Context _context;
    private final Wallet _wallet;
    private Currency _contextCurrency = null;
}

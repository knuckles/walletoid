/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.Spinner;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletSchema_v7;
import ru.knuckles.walletoid.dialogs.CategorySelectDialog.OnCategorySelectedListener;

import static ru.knuckles.walletoid.dialogs.CategoryEditDialog.OnCategoryEditedListener;

/**
 * Context menu provider for a view displaying a {@link Category}.
 * View's tag must reference a non-null {@link Category}.
 */
public class CategoryContextMenuHandler implements View.OnCreateContextMenuListener {
    public CategoryContextMenuHandler(Context context, Wallet wallet, Category.Kind catKind) {
        _context = context;
        _wallet = wallet;
        _catKind = catKind;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View contextView, ContextMenuInfo menuInfo) {
        if (menuInfo instanceof AdapterContextMenuInfo) {
            AdapterContextMenuInfo adaptedMenuInfo = (AdapterContextMenuInfo) menuInfo;
            _contextCategory = _wallet.getCategory(adaptedMenuInfo.id);
        }
        else if (menuInfo instanceof ExpandableListContextMenuInfo) {
            ExpandableListContextMenuInfo expListMenuInfo =
                    (ExpandableListContextMenuInfo) menuInfo;
            _contextCategory = _wallet.getCategory(expListMenuInfo.id);
        }
        else {
            _contextCategory = getContextCategory(contextView);
        }
        if (_contextCategory == null) {
            Log.d(this.getClass().getSimpleName(), "Could not determine context Category");
            return;
        }

        final MenuInflater _inflater = new MenuInflater(_context);
        _inflater.inflate(R.menu.category_spinner, menu);
        menu.findItem(R.id.menu_add_category).setOnMenuItemClickListener(_addEditItemListener);
        final MenuItem editItem = menu.findItem(R.id.menu_edit_category);
        editItem.setOnMenuItemClickListener(_addEditItemListener);
        editItem.setTitle(String.format(editItem.getTitle().toString(), _contextCategory.getName()));
        final MenuItem removeItem = menu.findItem(R.id.menu_remove_category);
        removeItem.setOnMenuItemClickListener(_removeItemListener);
        removeItem.setTitle(String.format(removeItem.getTitle().toString(), _contextCategory.getName()));
    }

    private MenuItem.OnMenuItemClickListener _addEditItemListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            final Category category = item.getItemId() == R.id.menu_edit_category ?
                _contextCategory :
                new Category(_context.getResources().getColor(R.color.gray), _catKind);
            CategoryEditDialog catEditDlg = new CategoryEditDialog(
                _context,
                category,
                false,
                _categoryEditHandler);

            catEditDlg.show();
            return true;
        }
    };

    // This CategoryEditDialog callback listener propagates user changes to the Wallet.
    private OnCategoryEditedListener _categoryEditHandler = new OnCategoryEditedListener() {
        @Override
        public void onCategoryEdited(CategoryEditDialog dlg, Category category) {
            try {
                if (category.getId() == Category.UNKNOWN_ID)
                    _wallet.addCategory(category);
                else
                    _wallet.updateCategory(category);
            }
            catch (Exception e) {
                Toast.makeText(_context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private MenuItem.OnMenuItemClickListener _removeItemListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO: Remove category asynchronously
            if (_contextCategory.getId() == WalletSchema_v7.CAT_ID_MISC_EXPENSE ||
                _contextCategory.getId() == WalletSchema_v7.CAT_ID_MISC_INCOME) {
                Toast.makeText(_context, R.string.cant_remove_category, Toast.LENGTH_SHORT).show();
                return true;
            }

            new CategorySelectDialog(
                _context,
                _wallet.getCategoriesList(_contextCategory.getKind()),
                _catSelectionListener,
                false,
                _context.getString(R.string.move_records_to)
            ).show();
            return true;
        }
    };

    private OnCategorySelectedListener _catSelectionListener = new OnCategorySelectedListener() {
        @Override
        public void onCategoryKindSelected(CategorySelectDialog srcDlg, Category.Kind catKind) {
            // Should never be called, since kind selection is disabled during dialog initialization.
        }

        @Override
        public void onCategorySelected(CategorySelectDialog srcDlg, long selectedCatID) {
            try {
                _wallet.removeCategory(_contextCategory.getId(), selectedCatID);
                Toast.makeText(_context, R.string.cat_removed, Toast.LENGTH_SHORT).show();
            }
            catch (SQLException e) {
                e.printStackTrace();
                Toast.makeText(_context, R.string.cat_removal_db_error, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private Category getContextCategory(View contextView) {
        if (contextView instanceof Spinner) {
            final long categoryId = ((Spinner) contextView).getSelectedItemId();
            return categoryId == Category.UNKNOWN_ID ? null : _wallet.getCategory(categoryId);
        }
        if (isIndividualCurrencyView(contextView))
            return (Category) contextView.getTag();
        return null;
    }

    private boolean isIndividualCurrencyView(View contextView) {
        return !(contextView instanceof Spinner);
    }

    private final Context _context;
    private final Wallet _wallet;
    private final Category.Kind _catKind;
    private Category _contextCategory;
};

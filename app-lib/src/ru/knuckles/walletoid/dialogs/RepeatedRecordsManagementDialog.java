/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.TextStyling;
import ru.knuckles.walletoid.activities.IntentExtras;
import ru.knuckles.walletoid.activities.RecordEditActivity;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletEvents.AnyEventObserver;
import ru.knuckles.walletoid.views.RecordLineBinder;

import java.util.*;

import static ru.knuckles.walletoid.database.WalletEvents.WE_ALL_RECORD_EVT;
import static ru.knuckles.walletoid.database.WalletEvents.WE_BLK_REC_REMOVAL;

public class RepeatedRecordsManagementDialog extends Dialog {
    public RepeatedRecordsManagementDialog(Context context, Wallet wallet) {
        super(context);
        _wallet = wallet;

        // Read theme values.
        TypedArray themeValues = context.getTheme().obtainStyledAttributes(new int[] {
                android.R.attr.colorBackground
        });
        @ColorInt final int listBGColor;
        try {
            listBGColor = themeValues.getColor(0, Color.WHITE);
        } finally {
            themeValues.recycle();
        }

        _binder = new RecordLineBinder(wallet, listBGColor, true);
        _catsAdapter = new RepeatedRecordsExpListAdapter();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.repeating_records_dialog);

        _repRecsList = (ExpandableListView) findViewById(R.id.repeatedRecList);
        _repRecsList.setEmptyView(findViewById(R.id.noScheduledRecordsHint));
        _repRecsList.setOnChildClickListener(_recItemClickListener);
        _repRecsList.setOnGroupClickListener(_groupItemClickListener);
        _repRecsList.setAdapter(_catsAdapter);
        _repRecsList.setGroupIndicator(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        _wallet.events.addAnyEventObserver(_recObserver);
        _catsAdapter.recreateItems();
        setTitle(_catsAdapter.getGroupCount() > 0 ?
                R.string.autorepeat :
                R.string.no_scheduled_records_title);
        openGroups();
    }

    @Override
    protected void onStop() {
        _wallet.events.removeAnyEventObserver(_recObserver);
        super.onStop();
    }

    private void openGroups() {
        for (int i = 0; i < _catsAdapter.getGroupCount(); ++i) {
            _repRecsList.expandGroup(i);
        }
    }

    /**
     * This comparator orders records based on their date and the given TimeInterval.
     */
    static private class RecDateComponentCmp implements Comparator<Record> {
        public void setComparisonInterval(TimeInterval ti) {
            _ti = ti;
        }

        @Override
        public int compare(final Record r1, final Record r2) {
            if (_ti == null)
                return 0;
            final Calendar c1 = r1.getCalendar();
            final Calendar c2 = r2.getCalendar();
            switch (_ti) {
                case YEAR:
                    return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
                case MONTH:
                    return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
                case WEEK:
                    return c1.get(Calendar.DAY_OF_WEEK) - c2.get(Calendar.DAY_OF_WEEK);
                case DAY:
                    final int hourDiff = c1.get(Calendar.HOUR) - c2.get(Calendar.HOUR);
                    if (hourDiff != 0)
                        return hourDiff;
                    final int minuteDiff = c1.get(Calendar.MINUTE) - c2.get(Calendar.MINUTE);
                    if (minuteDiff != 0)
                        return minuteDiff;
                    return c1.get(Calendar.SECOND) - c2.get(Calendar.SECOND);
            }
            return 0;
        }

        private TimeInterval _ti;
    }

    private final class RepeatedRecordsExpListAdapter
            extends BaseExpandableListAdapter {
        RepeatedRecordsExpListAdapter() {
            _inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void recreateItems() {
            _groups.clear();
            for (Record r : _wallet.getScheduledRecords()) {
                final List<Record> records;
                if (_groups.containsKey(r.getRepetitionInterval()))
                    records = _groups.get(r.getRepetitionInterval());
                else {
                    records = new ArrayList<>();
                    _groups.put(r.getRepetitionInterval(), records);
                }
                records.add(r);
            }
            for (Map.Entry<TimeInterval, List<Record>> e : _groups.entrySet()) {
                _recDateComponentCmp.setComparisonInterval(e.getKey());
                Collections.sort(e.getValue(), _recDateComponentCmp);
            }

            _displayedIntervals.clear();
            _displayedIntervals.addAll(_groups.keySet());

            notifyDataSetChanged();
        }

        @Override
        public int getGroupCount() {
            return _groups.size();
        }

        @Override
        public long getGroupId(final int groupPosition) {
            return _displayedIntervals.get(groupPosition).ordinal();
        }

        @Override
        public Object getGroup(final int groupPosition) {
            return getGroupList(groupPosition);
        }

        @Override
        public int getChildrenCount(final int groupPosition) {
            return getGroupList(groupPosition).size();
        }

        @Override
        public long getChildId(final int groupPosition, final int childPosition) {
            return getRecord(groupPosition, childPosition).getId();
        }

        @Override
        public Object getChild(final int groupPosition, final int childPosition) {
            return getRecord(groupPosition, childPosition);
        }

        @Override
        public boolean hasStableIds() { return true; }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) { return true; }

        @Override
        public View getGroupView(final int groupPosition, final boolean isExpanded,
                final View convertView, final ViewGroup parent) {
            final View groupView = convertView != null ?
                    convertView :
                    _inflater.inflate(android.R.layout.simple_expandable_list_item_1, parent,
                            false);

            final TextView textView = (TextView) groupView.findViewById(android.R.id.text1);
            final TimeInterval groupInterval = _displayedIntervals.get(groupPosition);
            textView.setText(TextStyling.bold(getContext().getString(
                    INTERVAL_LABEL_RESOURCE_IDS[groupInterval.ordinal()])));
            return groupView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition,
                final boolean isLastChild,
                final View convertView, final ViewGroup parent) {
            View childView;
            if (convertView == null) {
                childView = _inflater.inflate(R.layout.recordline, parent, false);
            } else {
                childView = convertView;
            }
            _binder.bindView(childView, getContext(), getRecord(groupPosition, childPosition),
                    true);
            return childView;
        }

        private List<Record> getGroupList(final int groupPosition) {
            return _groups.get(_displayedIntervals.get(groupPosition));
        }

        private Record getRecord(final int groupPosition, final int childPosition) {
            return getGroupList(groupPosition).get(childPosition);
        }

        private final Map<TimeInterval, List<Record>> _groups = new TreeMap<>();
        private final List<TimeInterval> _displayedIntervals =
                new ArrayList<>(TimeInterval.values().length);
        RecDateComponentCmp _recDateComponentCmp = new RecDateComponentCmp();
        private final LayoutInflater _inflater;
    }

    private AnyEventObserver _recObserver = new AnyEventObserver(
            WE_ALL_RECORD_EVT | WE_BLK_REC_REMOVAL) {
        @Override
        protected void onWalletChanged(final long eventFlag) {
            _catsAdapter.recreateItems();
            openGroups();
        }
    };

    private final OnChildClickListener _recItemClickListener = new OnChildClickListener() {
        @Override
        public boolean onChildClick(final ExpandableListView parent, final View v,
                final int groupPosition, final int childPosition, final long id) {
            Intent editRecIntent = new Intent(getContext(), RecordEditActivity.class);
            editRecIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
            editRecIntent.putExtra(IntentExtras.RECORD_ID, id);
            getContext().startActivity(editRecIntent);
            return true;
        }
    };

    private final OnGroupClickListener _groupItemClickListener = new OnGroupClickListener() {
        @Override
        public boolean onGroupClick(final ExpandableListView parent, final View v,
                final int groupPosition, final long id) {
            return true;  // Prevent groups from closing.
        }
    };

    private final static int[] INTERVAL_LABEL_RESOURCE_IDS = new int[] {
            R.string.repeat_record_yearly,
            R.string.repeat_record_monthly,
            R.string.repeat_record_weekly,
            R.string.repeat_record_daily
    };

    private final Wallet _wallet;
    private final RecordLineBinder _binder;
    private ExpandableListView _repRecsList;
    private RepeatedRecordsExpListAdapter _catsAdapter;
}

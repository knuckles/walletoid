/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class was modified for use in Walletoid project. See LICENSE file
 * for more info.
 */

/**
 * Copyright (c) 2013, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.widget.SeekBar;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.views.ColorPickerView;

public class ColorPickerDialog extends Dialog {

    private final ColorPickerView.OnColorChangedListener _listener;
    private final int _initialColor;

    public ColorPickerDialog(Context context,
                             ColorPickerView.OnColorChangedListener listener,
                             int initialColor) {
        super(context);
        
        _listener = listener;
        _initialColor = initialColor;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.pick_color);
        setContentView(R.layout.color_picker_dialog);

        final ColorPickerView colorPickerView = (ColorPickerView) findViewById(R.id.colorPickerView);
        colorPickerView.setColor(_initialColor);
        colorPickerView.setOnColorChangedListener(new ColorPickerView.OnColorChangedListener() {
            public void colorChanged(int color) {
                _listener.colorChanged(color);
                dismiss();
            }
        });

        SeekBar saturationBar = (SeekBar) findViewById(R.id.saturationSeekBar);
        saturationBar.setProgress((int) (colorPickerView.getSaturation() * 100));
        saturationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                colorPickerView.setSaturation(i / 100.0f);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }
}

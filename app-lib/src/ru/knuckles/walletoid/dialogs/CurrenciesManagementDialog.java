/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Currency;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletEvents;
import ru.knuckles.walletoid.dialogs.CurrencyEditDialog.CurrencyEditListener;

public class CurrenciesManagementDialog extends Dialog {
    public CurrenciesManagementDialog(Context context, Wallet wallet) {
        super(context);
        setContentView(R.layout.currencies_list);
        setTitle(context.getText(R.string.currencies));

        assert wallet != null;
        _wallet = wallet;

        _currenciesRadioList = (RadioGroup) findViewById(R.id.currenciesRadioList);
        _addButton = (Button) findViewById(R.id.addButton);
        _addButton.setOnClickListener(_onAddClickListener);

        _contextMenuHandler = new CurrencyContextMenuHandler(getContext(), _wallet);
    }

    private void refillCurrenciesList() {
        _currenciesRadioList.removeAllViews();
        long refId = _wallet.getReferenceCurrencyId();
        final ViewGroup.MarginLayoutParams btnLayoutParams =
            new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (Currency currency : _wallet.getCurrenciesList()) {
            RadioButton currencyButton = new RadioButton(getContext());
            currencyButton.setTag(currency);
            currencyButton.setText(currency.getName());
            currencyButton.setOnCreateContextMenuListener(_contextMenuHandler);
            currencyButton.setLayoutParams(btnLayoutParams);
            currencyButton.setMinLines(2);
            _currenciesRadioList.addView(currencyButton);
            if (currency.getId() == refId)
                _currenciesRadioList.check(currencyButton.getId());
        }
        _currenciesRadioList.setOnCheckedChangeListener(_refCurSelectionListener);
    }

    // Dialog:
    @Override
    protected void onStart() {
        super.onStart();
        refillCurrenciesList();
        _wallet.events.addAnyEventObserver(_walletObserver);
        _wallet.beginTransaction();
    }

    @Override
    protected void onStop() {
        _wallet.events.removeAnyEventObserver(_walletObserver);
        _wallet.transactionSucceeded();
        _wallet.endTransaction();
        super.onStop();
    }

    private final View.OnClickListener _onAddClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog currencyDialog =
                new CurrencyEditDialog(getContext(), new Currency(), _wallet.getReferenceCurrency(),
                    _currencyEditListener);
            currencyDialog.setTitle(getContext().getString(R.string.add_new_currency));
            currencyDialog.show();
        }
    };

    private final CurrencyEditListener _currencyEditListener = new CurrencyEditListener() {
        @Override
        public void onCurrencyEdited(Currency currency, boolean valueChanged) {
            _wallet.addCurrency(currency);
        }
    };

    private RadioGroup.OnCheckedChangeListener _refCurSelectionListener =
            new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final RadioGroup group, final int checkedId) {
            final View checkedButton = _currenciesRadioList.findViewById(checkedId);
            if (checkedButton == null)
                return;

            final Currency currency = (Currency) checkedButton.getTag();
            if (currency != null)
                _wallet.setReferenceCurrencyId(currency.getId());
        }
    };

    private WalletEvents.AnyEventObserver _walletObserver =
            new WalletEvents.AnyEventObserver(WalletEvents.WE_ALL_CURRENCY_EVT | WalletEvents.WE_BLK_CURRENCY_UPD) {
        @Override
        public void onWalletChanged(final long eventFlag) {
            refillCurrenciesList();
        }
    };

    private final Wallet _wallet;
    private final View.OnCreateContextMenuListener _contextMenuHandler;

    // Controls
    private final RadioGroup _currenciesRadioList;
    private final Button _addButton;
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletEvents;
import ru.knuckles.walletoid.dialogs.CategoryEditDialog.OnCategoryEditedListener;

/**
 * Lists {@link Category} categories with ability to add, edit and remove them.
 */
public class CategoryManagementDialog extends CategorySelectDialog {
    public CategoryManagementDialog(Context context, Wallet wallet) {
        /**
         * We pass null as categories list. Instead we update the list ourselves in {@link #onStart}.
         */
        super(context, null, null, false, context.getString(R.string.manage_categories));
        _wallet = wallet;
    }

    // Dialog:

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _catsList.setOnCreateContextMenuListener(
                new CategoryContextMenuHandler(getContext(), _wallet, Category.Kind.ALL));
        _addButton = new Button(getContext());
        _addButton.setText(R.string.add_new_category);
        _addButton.setVisibility(View.VISIBLE);
        _addButton.setOnClickListener(_addClickListener);
        _layout.addView(_addButton);
    }

    @Override
    protected void onStart() {
        _wallet.events.addAnyEventObserver(_walletObserver);
        createCategoryItems(_wallet.getCategoriesList());
        super.onStart();
    }

    @Override
    protected void onStop() {
        _wallet.events.removeAnyEventObserver(_walletObserver);
        super.onStop();
    }

    // ExpandableListView.OnChildClickListener:
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
        v.showContextMenu();
        return true;
    }

    private WalletEvents.AnyEventObserver _walletObserver =
            new WalletEvents.AnyEventObserver(WalletEvents.WE_ALL_CATEGORY_EVT) {
        @Override
        public void onWalletChanged(final long eventFlag) {
            createCategoryItems(_wallet.getCategoriesList());
        }
    };

    private View.OnClickListener _addClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog categoryDialog = new CategoryEditDialog(
                    getContext(),
                    new Category(getContext().getResources().getColor(R.color.gray), Category.Kind.ALL),
                    true,
                    _categoryEditListener);
            categoryDialog.show();
        }
    };

    private OnCategoryEditedListener _categoryEditListener = new OnCategoryEditedListener() {
        @Override
        public void onCategoryEdited(CategoryEditDialog dlg, Category newCategory) {
            try {
                _wallet.addCategory(newCategory);
            }
            catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private final Wallet _wallet;

    // Controls
    private Button _addButton;
}

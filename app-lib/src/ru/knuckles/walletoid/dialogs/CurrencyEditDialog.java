/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Currency;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CurrencyEditDialog extends Dialog implements View.OnClickListener {
    public interface CurrencyEditListener {
        void onCurrencyEdited(Currency currency, boolean valueChanged);
    }

    public CurrencyEditDialog(Context context, Currency editedCurrency, Currency referenceCurrency,
                              CurrencyEditListener listener) {
        super(context);
        assert editedCurrency != null;
        assert referenceCurrency != null;
        _currency = editedCurrency;
        _referenceCurrency = referenceCurrency;
        _listener = listener;
        final boolean isRefCurrency = editedCurrency.getId() == referenceCurrency.getId();

        setContentView(R.layout.currency_edit_dlg);

        _nameInput = (EditText) findViewById(R.id.currencyNameInput);
        _symbolInput = (EditText) findViewById(R.id.currencySymbolInput);
        _rateBlock = (LinearLayout) findViewById(R.id.currencyRateBlock);
        _rateBlock.setVisibility(isRefCurrency ? View.GONE : View.VISIBLE);
        _rateMeasures = (TextView) findViewById(R.id.currencyRateMeasures);
        _rateInput = (EditText) findViewById(R.id.currencyRateInput);
        _rateInput.addTextChangedListener(_rateInputWatcher);

        findViewById(R.id.AcceptCatEditBtn).setOnClickListener(this);
        findViewById(R.id.CancelCatEditBtn).setOnClickListener(this);

        updateEdits(editedCurrency);
    }

    private void updateEdits(Currency currency) {
        _nameInput.setText(currency.getName());
        _symbolInput.setText(currency.getSymbol());
        updateRateControls(currency);
    }

    private void updateRateControls(Currency currency) {
        _invertedRate = currency.getValue() < 1;
        final double displayedRate = !_invertedRate ?
                currency.getValue() :
                1.0d / currency.getValue();
        // Android has a bug, that doesn't allow to input decimal separators other than dot even
        // with appropriate locale active. As we expect the number to be entered with dot as
        // separator, we also preformat it that way.
        _rateInput.setText(String.format(Locale.US, "%,.2f", displayedRate));
        final String currencySym1 =  _invertedRate ?
                currency.getSymbol() :
                _referenceCurrency.getSymbol();
        final String currencySym2 = !_invertedRate ?
                currency.getSymbol() :
                _referenceCurrency.getSymbol();
        _rateMeasures.setText(String.format("%s = 1 %s", currencySym1, currencySym2));
    }

    @Override
    public void onClick(View v) {
        final int vid = v.getId();
        if (vid == R.id.AcceptCatEditBtn) {
            onOKClick(v);
        }
        else if (vid == R.id.CancelCatEditBtn) {
            onCancelClick(v);
        }
    }

    private void onCancelClick(View v) {
        dismiss();
    }

    private void onOKClick(View v) {
        if (_listener == null) {
            dismiss();
            return;
        }

        try {
            final Currency result = getCurrency();
            _listener.onCurrencyEdited(result, result.getValue() != _currency.getValue());
            dismiss();
        }
        catch (ParseException e) {
            Toast.makeText(getContext(), R.string.invalid_num_format, Toast.LENGTH_SHORT).show();
        }
    }

    private Currency getCurrency() throws ParseException {
        final double inputValue = _numFormat.parse(_rateInput.getText().toString()).doubleValue();
        return new Currency(
            _currency.getId(),
            _nameInput.getText().toString(),
            _symbolInput.getText().toString(),
            !_invertedRate ? inputValue : 1.0d / inputValue);
    }

    private final TextWatcher _rateInputWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            // Just in case Android fixes input one day, we replace comma with dot to still be able to parse it back.
            String replaced = s.toString().replaceAll(",", ".");
            if (s.toString().equals(replaced))
                return;
            s.clear();
            s.append(replaced);
        }
    };


    // See comments in updateRateControls for explanation about Locale.
    private final NumberFormat _numFormat = NumberFormat.getInstance(Locale.ROOT);
    private boolean _invertedRate = false;

    private final Currency _currency;
    private final Currency _referenceCurrency;
    private final CurrencyEditListener _listener;
    private final EditText _nameInput;

    private final EditText _symbolInput;
    private final EditText _rateInput;
    private final TextView _rateMeasures;
    private final LinearLayout _rateBlock;
}

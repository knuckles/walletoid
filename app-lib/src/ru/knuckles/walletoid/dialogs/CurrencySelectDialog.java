/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.Currency;
import ru.knuckles.walletoid.database.Wallet;

import java.util.List;

public class CurrencySelectDialog extends Dialog implements AdapterView.OnItemClickListener {
    public interface OnCurrencySelectedListener {
        void onCurrencySelected(long selectedCurrencyId);
    }

    public CurrencySelectDialog(Context context, Wallet wallet, OnCurrencySelectedListener listener) {
        this(context, wallet, listener, context.getString(R.string.set_currency));
    }

    public CurrencySelectDialog(Context context, Wallet wallet, OnCurrencySelectedListener listener, String title) {
        super(context);
        _wallet = wallet;
        _listener = listener;

        setTitle(title);
    }

    private void createCategoryItems(List<Currency> categories) {
        _currsAdapter = new ArrayAdapter<Currency>(
            getContext(),
            android.R.layout.simple_list_item_1,
            categories) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setText(getItem(position).getName());
                return view;
            }
        };
        _currsList.setAdapter(_currsAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _layout = new LinearLayout(getContext());
        final ViewGroup.MarginLayoutParams params =
            new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        _layout.setLayoutParams(params);
        _layout.setOrientation(LinearLayout.VERTICAL);
        setContentView(_layout);

        _currsList = new ListView(getContext());
        _currsList.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 1));
        _currsList.setVerticalFadingEdgeEnabled(true);
        _currsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        _currsList.setClickable(true);
        _currsList.setOnItemClickListener(this);
        _layout.addView(_currsList);
    }

    @Override
    protected void onStart() {
        super.onStart();
        createCategoryItems(_wallet.getCurrenciesList());
    }

    // OnItemClickListener -------------------------------------------------------------------------

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        _listener.onCurrencySelected(_currsAdapter.getItem(pos).getId());
        dismiss();
    }

    private Wallet _wallet;
    private OnCurrencySelectedListener _listener;
    private ArrayAdapter<Currency> _currsAdapter;

    // Controls
    protected LinearLayout _layout;
    private ListView _currsList;
}

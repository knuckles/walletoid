/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.TextStyling;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Category.Kind;

import java.util.ArrayList;
import java.util.List;

/**
 * Dialog allows selection from a given list of {@link Category} categories.
 */
public class CategorySelectDialog extends Dialog
        implements ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener {

    public interface OnCategorySelectedListener {
        void onCategoryKindSelected(CategorySelectDialog srcDlg, Kind catKind);
        void onCategorySelected(CategorySelectDialog srcDlg, long selectedCatID);
    }

    public CategorySelectDialog(Context context, List<Category> categories, OnCategorySelectedListener listener,
            boolean allowKindSelection) {
        this(context, categories, listener, allowKindSelection,
                context.getString(R.string.pick_category));
    }

    /**
     * @param context Dialog context.
     * @param categories List of categories to choose from. They will be partitioned into groups according to their
     *                   {@link Kind}. In presence of a category with id equal to {@link Category#UNKNOWN_ID}
     *                   additional group named "All" is added and will contain only this one category.
     * @param listener Selection events listener.
     * @param allowKindSelection When |true|, group elements become selectable, and listener is notified through
     *                           {@link OnCategorySelectedListener#onCategoryKindSelected} method.
     * @param title Dialog title.
     */
    public CategorySelectDialog(Context context, List<Category> categories, OnCategorySelectedListener listener,
            boolean allowKindSelection, String title) {
        super(context);
        _listener = listener;
        _categories = categories;
        _allowKindSelection = allowKindSelection;
        setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _layout = new LinearLayout(getContext());
        final ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        _layout.setLayoutParams(params);
        _layout.setOrientation(LinearLayout.VERTICAL);
        setContentView(_layout);

        _catsList = new ExpandableListView(getContext());
        _catsList.setLayoutParams(
                new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 1));
        _catsList.setVerticalFadingEdgeEnabled(true);
        _catsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        _catsList.setClickable(true);
        _catsList.setOnChildClickListener(this);
        _catsList.setOnGroupClickListener(this);
        if (_allowKindSelection)
            _catsList.setGroupIndicator(null);
        _layout.addView(_catsList);

        createCategoryItems(_categories);
    }

    // ExpandableListView.OnGroupClickListener:
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        final Kind catKind = Kind.values()[(int) id];
        if (catKind == Kind.ALL)
            selectCategory(Category.UNKNOWN_ID);
        if (!_allowKindSelection)
            return false;

        selectKind(catKind);
        return true;  // Disable group folding.
    }

    // ExpandableListView.OnChildClickListener:
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
        selectCategory(id);
        return false;
    }

    private void selectCategory(long id) {
        if (_listener == null)
            return;
        _listener.onCategorySelected(this, id);
        dismiss();
    }

    private void selectKind(Kind catKind) {
        if (_listener == null)
            return;
        _listener.onCategoryKindSelected(this, catKind);
        dismiss();
    }

    protected void createCategoryItems(List<Category> categories) {
        _catsList.setAdapter((ExpandableListAdapter) null);
        if (categories == null)
            return;
        _catsAdapter = new CategoryExpandableListAdapter(getContext(), categories);
        _catsList.setAdapter(_catsAdapter);
        for (int i = 0; i < _catsAdapter.getGroupCount(); ++i) {
            _catsList.expandGroup(i);
        }
    }

    private final OnCategorySelectedListener _listener;
    private final boolean _allowKindSelection;
    protected List<Category> _categories;

    // Controls
    protected LinearLayout _layout;
    protected ExpandableListView _catsList;
    protected ExpandableListAdapter _catsAdapter;

    private static final class CategoryExpandableListAdapter extends BaseExpandableListAdapter {
        CategoryExpandableListAdapter(Context context, List<Category> categories) {
            _inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            _context = context;
            List<Category> destList;

            boolean _listAllCategory = false;
            for (Category cat : categories) {
                if (cat.getId() == Category.UNKNOWN_ID) {
                    _listAllCategory = true;
                    continue;
                }
                destList = cat.getKind().isIncome() ? _incomeCats : _expenseCats;
                destList.add(cat);
            }
            if (_listAllCategory)
                _groups.add(new Group(Kind.ALL, _all, R.string.all_categories));
            _groups.add(new Group(Kind.EXPENSE, _expenseCats, R.string.expenses));
            _groups.add(new Group(Kind.INCOME, _incomeCats, R.string.incomes));
        }

        @Override
        public int getGroupCount() {
            return _groups.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            final int result = getGroupCategories(groupPosition).size();
            Log.d(this.getClass().getSimpleName(), String.format("getChildrenCount(%d) == %d", groupPosition, result));
            return result;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return _groups.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return getCategory(groupPosition, childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return _groups.get(groupPosition).kind.ordinal();
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return getCategory(groupPosition, childPosition).getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View groupView;
            if (convertView == null) {
                groupView = _inflater.inflate(android.R.layout.simple_expandable_list_item_1, parent, false);
            } else {
                groupView = convertView;
            }

            final TextView textView = (TextView) groupView.findViewById(android.R.id.text1);
            textView.setText(TextStyling.bold(_context.getText(_groups.get(groupPosition).labelResourceId)));

            return groupView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                                 ViewGroup parent) {
            View childView;
            if (convertView == null) {
                childView = _inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                childView = convertView;
            }
            final Category category = getCategory(groupPosition, childPosition);
            childView.setTag(category);  // Used by CategoryContextMenuHandler.
            final TextView textView = (TextView) childView.findViewById(android.R.id.text1);
            textView.setText(category.getName());
            final Drawable circle = CategoryEditDialog.createCategoryCircle(category.getColor());
            textView.setCompoundDrawables(circle, null, null, null);
            textView.setCompoundDrawablePadding(circle.getBounds().width() / 2);

            return childView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        // PRIVATE

        private Category getCategory(int groupPosition, int childPosition) {
            return getGroupCategories(groupPosition).get(childPosition);
        }

        private List<Category> getGroupCategories(int groupPosition) {
            return _groups.get(groupPosition).categories;
        }

        private static class Group {
            private Group(Kind kind, List<Category> categories, int labelResourceId) {
                this.kind = kind;
                this.categories = categories;
                this.labelResourceId = labelResourceId;
            }
            public final Kind kind;
            public final List<Category> categories;
            public final int labelResourceId;
        }

        private final List<Category> _all = new ArrayList<>();
        private final List<Category> _incomeCats = new ArrayList<>();
        private final List<Category> _expenseCats = new ArrayList<>();
        private final ArrayList<Group> _groups = new ArrayList<>();

        private final LayoutInflater _inflater;
        private final Context _context;
    }
}

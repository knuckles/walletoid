/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

public class PrefUtils {
    public static final String PREF_DEFAULT_CURRENCY_EDITED = "currencies.default.edited";
    public static final String PREF_LAST_RECORD_CURRENCY = "records.editor.last_currency";
    public static final String PREF_LAST_RECORD_DATE = "records.editor.last_record_date";
    public static final String PREF_LAST_COMMIT_DATE = "records.editor.last_commit_date";
    public static final String PREF_REVERSED_REC_ORDER = "records.view.reversed";
}

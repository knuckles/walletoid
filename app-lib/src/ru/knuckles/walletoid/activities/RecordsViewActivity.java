/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.PrefUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Walletoid;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.views.RecordsRangeView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static ru.knuckles.walletoid.CalendarUtils.Cal;

/**
 * This is a very basic activity, which hosts a {@link RecordsRangeView}, attached to a
 * {@link Wallet} wallet.
 * It can also handle record selection and report it back through
 * {@link IntentExtras#RECORD_ID}.
 * {@link Wallet} instance is obtained by {@link IntentExtras#WALLET_NAME} key
 * value, stored in the
 * Intent extras. By default {@link WalletManager#MAIN_DB_NAME} is used.
 * Displayed range of records may be controlled by extra intent parameters:
 * {@link IntentExtras#DATE_TIMESTAMP},
 * {@link IntentExtras#RANGE_DURATION},
 * {@link IntentExtras#CATEGORY},
 * {@link IntentExtras#CATEGORY_KIND}.
 */
public class RecordsViewActivity extends Activity {
    // Intent extras.
    public static String EXTRA_SELECT = "select records";
    public static String EXTRA_TITLE = "title";

    // Activity:

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences prefs =
                getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE);
        _recordsRangeView = createContent();
        _recordsRangeView.setItemClickListener(_listItemClickHandler);
        _recordsRangeView.setRevSorting(prefs.getBoolean(PrefUtils.PREF_REVERSED_REC_ORDER, true));
        prefs.registerOnSharedPreferenceChangeListener(_prefListener);

        _wm = Walletoid.getInstance().getWalletManager();

        attachWallet(getWalletFromIntent());

        // This can be done only after Wallet has been attached.
        if (getIntent().hasExtra(EXTRA_TITLE))
            setTitle(getIntent().getStringExtra(EXTRA_TITLE));
        else {
            // TODO: Pass this title through |EXTRA_TITLE|.
            setTitle(getString(R.string.records_during,
                    _recordsRangeView.getCategoryFilterDesc(),
                    formatDateRange()));
        }
    }

    @Override
    protected void onDestroy() {
        detachWallet();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        _recordsRangeView.saveStateToBundle(bundle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        _recordsRangeView.restoreStateFromBundle(bundle);
    }

    // Own protected.

    protected RecordsRangeView createContent() {
        // TODO: This condition is too weak.
        boolean showFilterControls = !getIntent().hasExtra(IntentExtras.DATE_TIMESTAMP);
        RecordsRangeView recordsRangeView = new RecordsRangeView(this, showFilterControls);
        this.setContentView(recordsRangeView);
        return recordsRangeView;
    }

    protected void attachWallet(Wallet wallet) {
        if (wallet == null)
            throw new AssertionError();
        _wallet = wallet;
        setInitialFilters();

        _recordsRangeView.setWallet(_wallet);
        // TODO: Check that current filters are applied.
    }

    protected void detachWallet() {
        if (_wallet == null)
            return;
        _recordsRangeView.setWallet(null);
        final String name = _wallet.getName();
        _wallet = null;
        _wm.releaseWallet(name);
    }

    protected void editRecord(long rowID) {
        Intent editRecIntent = new Intent(this, RecordEditActivity.class);
        editRecIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
        editRecIntent.putExtra(IntentExtras.RECORD_ID, rowID);
        startActivity(editRecIntent);
    }

    // PRIVATE

    private Wallet getWalletFromIntent() {
        final Bundle extras = getIntent().getExtras();
        final String walletName = extras != null ? extras.
                getString(IntentExtras.WALLET_NAME) : null;
        return walletName == null ?
               _wm.getMainWallet(true) :
               _wm.getWallet(walletName, false, true);
    }

    private void setInitialFilters() {
        final Intent intent = getIntent();

        final Calendar startCal;
        if (intent.hasExtra(IntentExtras.DATE_TIMESTAMP)) {
            startCal = Cal(intent.getLongExtra(
                    IntentExtras.DATE_TIMESTAMP, System.currentTimeMillis()));
        } else {
            // Try to display the latest month that has a record. If none, pick current month.
            final Record mostRecentRecord = _wallet.getMostRecentRecord();
            startCal = (mostRecentRecord != null) ?
                mostRecentRecord.getCalendar() :
                Calendar.getInstance();
            CalendarUtils.setCalendarToMonthStart(startCal);
        }

        final TimeInterval duration = TimeInterval.fromOrdinal(intent.getIntExtra(
                IntentExtras.RANGE_DURATION, TimeInterval.MONTH.ordinal()));
        final long categoryId = intent.getLongExtra(
                IntentExtras.CATEGORY, Category.UNKNOWN_ID);
        final Category.Kind catKind = Category.Kind.fromOrdinal(intent.getIntExtra(
                IntentExtras.CATEGORY_KIND, Category.Kind.ALL.ordinal()));

        _recordsRangeView.setRange(startCal, duration);
        _recordsRangeView.setCategoryFilter(catKind, categoryId);
    }

    private AdapterView.OnItemClickListener _listItemClickHandler =
            new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long rowID) {
            if (rowID == Record.UNKNOWN_ID)
                return;
            if (getIntent().getBooleanExtra(EXTRA_SELECT, false)) {
                selectRecordId(rowID);
            }
            else {
                editRecord(rowID);
            }
        }
    };

    private void selectRecordId(long id) {
        Intent intent = getIntent();
        intent.putExtra(IntentExtras.RECORD_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    private String formatDateRange() {
        final WalletFilter filter = _recordsRangeView.copyFilter();
        final Calendar start = filter.getDateRangeStart();
        if (start != null) {
            switch (_recordsRangeView.getInterval()) {
                case YEAR:
                    return String.valueOf(start.get(Calendar.YEAR));
                case MONTH:
                    return monthformat.format(start.getTime());
                case WEEK: {
                    Calendar lastDay = filter.getDateRangeEnd();
                    lastDay.add(Calendar.MILLISECOND, -1);
                    return getString(R.string.day_range,
                            dayFormat.format(start.getTime()),
                            dayFormat.format(lastDay.getTime()));
                }
                case DAY:
                    return dayFormat.format(start.getTime());
            }
        }
        return getString(R.string.ellipsis);
    }

    private final SharedPreferences.OnSharedPreferenceChangeListener _prefListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(final SharedPreferences prefs, final String key) {
            if (key.equals(PrefUtils.PREF_REVERSED_REC_ORDER))
                _recordsRangeView.setRevSorting(prefs.getBoolean(key, true));
        }
    };

    // Controls
    protected RecordsRangeView _recordsRangeView;

    protected WalletManager _wm;
    protected Wallet _wallet;

    // TODO: Use locale specific formats.
    private final DateFormat monthformat = new SimpleDateFormat("LLL/yyyy");
    private final DateFormat dayFormat = new SimpleDateFormat("d.LL.yy");
}

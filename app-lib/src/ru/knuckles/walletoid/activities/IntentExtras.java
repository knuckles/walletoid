/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

public class IntentExtras {
    public static final String DATE_TIMESTAMP = "date start";  // Timestamp (long)
    public static final String RANGE_DURATION = "duration";  // TimeInterval value ordinal
    public static final String CATEGORY = "category";  // Id (long)
    public static final String WALLET_NAME = "wallet";  // String name of the wallet.
    public static final String RECORD_ID = "record id";  // Long
    public static final String CATEGORY_KIND = "category kind";  // Ordinal value of Category.Kind
    public static final String NOTIFICATION_ID = "notification id";  // Int
}

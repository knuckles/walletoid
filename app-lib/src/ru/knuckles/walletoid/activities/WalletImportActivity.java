/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Walletoid;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.dialogs.CategoryMappingDialog;
import ru.knuckles.walletoid.views.RecordsListView;

import java.util.Map;
import java.util.TreeMap;

public class WalletImportActivity extends Activity {
    // Activity event handlers ---------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.import_activity);

        _walletManager = Walletoid.getInstance().getWalletManager();
        _mainWallet = _walletManager.getMainWallet(false);
        Bundle extras = getIntent().getExtras();
        _tempWalletName = extras.getString(IntentExtras.WALLET_NAME);
        _tempWallet = _walletManager.getWallet(_tempWalletName, false, false);
        _categoriesMapping = new TreeMap<>();
        _currenciesMapping = new TreeMap<>();

        _recordsListView = (RecordsListView) findViewById(R.id.recordsList);
        _recordsListView.getCurrentFilter().setDateRange(0, Long.MAX_VALUE);
        _recordsListView.setWallet(_tempWallet);

        guessInitialCategoryMapping();
        guessInitialCurrencyMapping();
    }

    private void guessInitialCategoryMapping() {
        for (Category srcCategory : _tempWallet.getCategoriesList()) {
            Category destCategory = _mainWallet.findCategoryByName(srcCategory.getName());
            if (destCategory != null)
                _categoriesMapping.put(srcCategory.getId(), destCategory.getId());
        }
    }

    private void guessInitialCurrencyMapping() {
        for (Currency srcCurrency : _tempWallet.getCurrenciesList()) {
            Currency destCurrency = _mainWallet.findCurrencyByName(srcCurrency.getName());
            if (destCurrency != null)
                _currenciesMapping.put(srcCurrency.getId(), destCurrency.getId());
        }
    }

    @Override
    public void onDestroy() {
        if (_mainWallet != null)
            Walletoid.getInstance().getWalletManager().releaseMainWallet();
        if (_tempWallet != null)
            _walletManager.releaseWalletAndDeleteFile(_tempWalletName);
        super.onDestroy();
    }

    // Layout event handlers.

    public void onYesClick(View button) {
        _importTask = new WalletCopyingTask(_mainWallet);
        _importTask.execute(
                new WalletCopyParams(_tempWallet, _categoriesMapping, _currenciesMapping));

        _dialog = new ProgressDialog(this);
        _dialog.setIndeterminate(true);
        _dialog.setCancelable(false);
        _dialog.setCanceledOnTouchOutside(false);
        _dialog.setMessage(getString(R.string.import_in_progress));
        _dialog.setOnKeyListener(_dialogKeyListener);

        _dialog.show();
    }

    public void onNoClick(View button) {
        finish();
    }

    public void onSetupClick(View button) {
        Dialog mapDialog = new CategoryMappingDialog(
            this,
            _tempWallet.getCategoriesMap(),
            _mainWallet.getCategoriesMap(),
            _categoriesMapping);
        mapDialog.show();
    }

    // This listener prevents dialog from closing, but cancels current WalletCopyingTask.
    private DialogInterface.OnKeyListener _dialogKeyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                _importTask.cancel(false);
                _dialog.setMessage(getString(R.string.import_cancelling));
                return true;
            }
            return false;
        }
    };

    private static class WalletCopyParams {
        WalletCopyParams(Wallet wallet_, Map<Long, Long> categoryMap_,
                Map<Long, Long> currencyMap_) {
            wallet = wallet_;
            categoryMap = categoryMap_;
            currencyMap = currencyMap_;
        }

        public final Wallet wallet;
        public final Map<Long, Long> categoryMap;
        public final Map<Long, Long> currencyMap;
    }

    // Asynchronous task, which arranges copying data from _tempWallet to _mainWallet in background.
    private class WalletCopyingTask extends AsyncTask<WalletCopyParams, Void, Boolean> {

        WalletCopyingTask(Wallet destination) {
            assert destination != null;
            _wallet = destination;
        }

        public Exception getException() {
            return _copyException;
        }

        @Override
        protected Boolean doInBackground(WalletCopyParams... copySources) {
            try {
                doCopy(copySources);
                return true;
            }
            catch (Exception e) {
                _copyException = e;
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            finishTask(result);
        }

        @Override
        protected void onCancelled() {
            finishTask(false);
        }

        private void finishTask(boolean result) {
            onImportTaskFinished(result);
        }

        private void doCopy(WalletCopyParams[] copySources) throws Exception {
            for (WalletCopyParams copySource : copySources) {
                if (isCancelled())
                    return;
                _wallet.copyFrom(copySource.wallet, copySource.categoryMap, copySource.currencyMap);
            }
        }

        private Exception _copyException = null;
        private final Wallet _wallet;
    }

    // Run from WalletCopyingTask instance, when it finishes execution.
    private void onImportTaskFinished(boolean succeeded) {
        if (_importTask != null) {
            if (_importTask.isCancelled())
                Log.d(getClass().getSimpleName(), "Import task was cancelled");
            else if (!succeeded)
                Toast.makeText(this, "Could not copy data to Wallet: " +
                        _importTask.getException().toString(), Toast.LENGTH_LONG).show();
        }
        _importTask = null;
        if (_dialog != null) {
            _dialog.dismiss();
            _dialog = null;
        }
        if (succeeded)
            finish();
    }

    // Data.
    private RecordsListView _recordsListView;

    private String _tempWalletName;
    private Wallet _tempWallet;
    private WalletManager _walletManager;
    private Wallet _mainWallet;
    private Map<Long, Long> _categoriesMapping;
    private Map<Long, Long> _currenciesMapping;
    private WalletCopyingTask _importTask;
    private ProgressDialog _dialog;
}

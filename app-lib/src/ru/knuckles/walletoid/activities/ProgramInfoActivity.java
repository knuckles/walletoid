/**
 * Copyright (c) 2013, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import ru.knuckles.walletoid.R;

import java.util.ArrayList;
import java.util.List;

import static android.widget.ExpandableListView.OnChildClickListener;

public class ProgramInfoActivity extends Activity {
    // Activity event handlers ---------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_screen);

        String version = "1.0";
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        setTitle(getString(R.string.app_name) + " | " + getString(R.string.app_version, version));

        _txtMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("txt");

        _libData = new ArrayList<LibDatum>();
        _libData.add(new LibDatum(
            getString(R.string.opencsv_lib_name),
            getString(R.string.opencv_link),
            getString(R.string.licence_apache20),
            // This file extension is a hack to get around asset compression,
            // which causes AssetManager.openFd to fail on such files.
            "apache_20",
            null
        ));
        _libData.add(new LibDatum(
            getString(R.string.colordialog_lib_name),
            getString(R.string.colordialog_link),
            getString(R.string.licence_apache20),
            "apache_20",
            null
        ));
        _libData.add(new LibDatum(
            getString(R.string.robot_logo_name),
            getString(R.string.robot_link),
            getString(R.string.robot_notice),
            null,
            getString(R.string.ccby30_link)
        ));
        _libData.add(new LibDatum(
            getString(R.string.icons_descr),
            getString(R.string.icons_link_chanut),
            getString(R.string.flaticons_license),
            null,
            getString(R.string.flaticons_license_link)
        ));
        setupList();
    }

    private List<LibDatum> _libData;

    private void setupList() {
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.thirdPartyListView);
        listView.setAdapter(new LibsListAdapter(this, _libData));
        listView.setOnChildClickListener(_libInfoItemClickListener);
    }

    private OnChildClickListener _libInfoItemClickListener = new OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPos, int childPos, long id) {
            if (childPos != 1)  // Not a "license" item.
                return false;

            LibDatum libDatum = _libData.get(groupPos);
            Intent readIntent = new Intent(Intent.ACTION_VIEW);
            if (libDatum.licenseFileName != null)
                readIntent.setDataAndType(
                    Uri.parse("content://" +
                        getString(R.string.assets_provider_auth) + "/" +
                        libDatum.licenseFileName),
                    _txtMimeType);
            else
                readIntent.setData(Uri.parse(libDatum.licenseURL));

            try {
                startActivity(readIntent);
            }
            catch (Exception e) {
                Toast.makeText(ProgramInfoActivity.this, R.string.err_viewing, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };

    private String _txtMimeType;
    private static class LibDatum {
        LibDatum(String name_, String home_, String licenseDescr_, String licenseFileName_, String licenseURL_) {
            this.name = name_;
            this.home = home_;
            this.licenseDescr = licenseDescr_;
            this.licenseFileName = licenseFileName_;
            this.licenseURL = licenseURL_;
        }

        public String name;
        public String home;
        public String licenseDescr;
        public String licenseFileName;
        public String licenseURL;
    }

    private static class LibsListAdapter extends BaseExpandableListAdapter {
        LibsListAdapter(Context context, List <LibDatum> libData) {
            _context = context;
            _libData = libData;
            _inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getGroupCount() {
            return _libData.size();
        }

        @Override
        public int getChildrenCount(int groupPos) {
            return 2;  // Homepage link and licence.
        }

        @Override
        public Object getGroup(int groupPos) {
            return _libData.get(groupPos);
        }

        @Override
        public Object getChild(int groupPos, int childPos) {
            LibDatum libDatum = _libData.get(groupPos);
            switch (childPos) {
                case 0:
                    return libDatum.home;
                case 1:
                    return libDatum.licenseDescr;
            }
            return null;
        }

        @Override
        public long getGroupId(int groupPos) {
            return groupPos;
        }

        @Override
        public long getChildId(int groupPos, int childPos) {
            return childPos;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPos, boolean isExpanded, View convertView, ViewGroup parent) {
            View groupView;
            if (convertView == null) {
                groupView = _inflater.inflate(android.R.layout.simple_expandable_list_item_1, parent, false);
            } else {
                groupView = convertView;
            }
            final TextView textView = (TextView) groupView.findViewById(android.R.id.text1);
            textView.setTextAppearance(_context, android.R.style.TextAppearance_Medium);
            textView.setText(_libData.get(groupPos).name);
            return textView;
        }

        @Override
        public View getChildView(int groupPos, int childPos, boolean isLastChild, View convertView, ViewGroup parent) {
            View childView;
            if (convertView == null) {
                childView = _inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                childView = convertView;
            }
            final TextView textView = (TextView) childView.findViewById(android.R.id.text1);
            textView.setTextAppearance(_context, android.R.style.TextAppearance_Small);
            textView.setAutoLinkMask(Linkify.WEB_URLS);
            textView.setText(getChild(groupPos, childPos).toString());
            return textView;
        }

        @Override
        public boolean isChildSelectable(int groupPos, int childPos) {
            return true;
        }

        private final Context _context;
        private final List<LibDatum> _libData;
        private final LayoutInflater _inflater;
    }
}

/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import ru.knuckles.walletoid.PrefUtils;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Walletoid;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletManager;
import ru.knuckles.walletoid.dialogs.CategoryManagementDialog;
import ru.knuckles.walletoid.dialogs.CurrenciesManagementDialog;
import ru.knuckles.walletoid.dialogs.RepeatedRecordsManagementDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PreferenceActivity extends ListActivity implements DialogInterface.OnDismissListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _prefs = getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE);
        _recRevSorting = _prefs.getBoolean(PrefUtils.PREF_REVERSED_REC_ORDER, true);
        _wm = Walletoid.getInstance().getWalletManager();
        _wallet = _wm.getMainWallet(false);

        _listItemCommands = new Runnable[] {
                new RunnableDialog(new CurrenciesManagementDialog(this, _wallet)),
                new RunnableDialog(new CategoryManagementDialog(this, _wallet)),
                new RunnableDialog(new RepeatedRecordsManagementDialog(this, _wallet)),
                new Runnable() {
                    @Override
                    public void run() {
                        _recRevSorting = !_recRevSorting;
                        update();
                    }
                }
            /*
            new Runnable() {
                @Override
                public void run() {
                    final RecordDictionary dict = _wm.getWalletDictionary(_wallet.getName());
                    dict.addObserver(_dictObserver);
                    switchDictProgressIndicator(true);
                    dict.rebuildFromWallet();
                }
            },
            */
        };

        for (int captionResId : CAPTION_RES_IDS) {
            final Map<String, String> line = new TreeMap<>();
            line.put(MAIN_TEXT, getString(captionResId));
            _listData.add(line);
        }

        _listAdapter = new SimpleAdapter(
                this,
                _listData,
                android.R.layout.simple_list_item_2,
                new  String[] { MAIN_TEXT, SUB_TEXT },
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        setListAdapter(_listAdapter);
        update();
    }

    @Override
    protected void onDestroy() {
        Walletoid.getInstance().getWalletManager().releaseMainWallet();
        SharedPreferences.Editor editor = _prefs.edit();
        editor.putBoolean(PrefUtils.PREF_REVERSED_REC_ORDER, _recRevSorting);
        editor.apply();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        if (position >= _listItemCommands.length)
            return;

        final Runnable command = _listItemCommands[position];
        if (command == null)  // This item just displays some info, not doing anything on tap.
            return;
        command.run();
    }

    // DialogInterface.OnDismissListener
    @Override
    public void onDismiss(DialogInterface dialog) {
        update();
    }

    private void update() {
        final Map<String, String> currLine = _listData.get(0);
        currLine.put(SUB_TEXT,
                getString(R.string.reference_currency_is,
                        _wallet.getReferenceCurrency().getName()));

        final Map<String, String> catsLine = _listData.get(1);
        catsLine.put(SUB_TEXT, getString(R.string.num_categories_details,
                _wallet.getCategoriesMap(Category.Kind.EXPENSE).size(),
                _wallet.getCategoriesMap(Category.Kind.INCOME).size()));

        final Map<String, String> repeatsLine = _listData.get(2);
        repeatsLine.put(SUB_TEXT,
                getString(R.string.num_records_details,
                        _wallet.getRecordsCount(),
                        _wallet.getScheduledRecordsIdMap().size()));  // TODO: Optimize query.

        final Map<String, String> recSortingLine = _listData.get(3);
        recSortingLine.put(SUB_TEXT,
                getString(_recRevSorting ? R.string.rec_sorting_new2old
                                         : R.string.rec_sorting_old2new));

        _listAdapter.notifyDataSetChanged();
    }

    private class RunnableDialog implements Runnable {
        RunnableDialog(Dialog dialog) {
            _dialog = dialog;
            _dialog.setOnDismissListener(PreferenceActivity.this);
        }

        @Override
        public void run() {
            _dialog.show();
        }

        private final Dialog _dialog;
    }

    /*

    private void switchDictProgressIndicator(boolean on) {
        final Map<String, String> dictLine = _listData.get(3);
        dictLine.put(SUB_TEXT, on ? "...": null);
        _listAdapter.notifyDataSetChanged();
    }

    private View getViewByPosition(int pos) {
        final ListView listView = getListView();
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition )
            return listView.getAdapter().getView(pos, null, listView);

        return listView.getChildAt(pos - firstListItemPosition);
    }

    WeakReference<RecordDictionary.Observer> _dictObserver =
            new WeakReference<>(new RecordDictionary.Observer() {
        @Override
        public void onReadyStateChanged(final boolean ready) {
            if (!_taskStarted) {
                _taskStarted = true;
                return;
            }
            _taskStarted = false;
            switchDictProgressIndicator(false);
        }
        protected boolean _taskStarted = false;
    });
     */

    private SharedPreferences _prefs;
    private boolean _recRevSorting;
    private WalletManager _wm;
    private Wallet _wallet;
    private Runnable[] _listItemCommands;  // List item click actions.
    private final List<Map<String, String>> _listData = new ArrayList<>();
    private SimpleAdapter _listAdapter;

    private static final int[] CAPTION_RES_IDS = {
        R.string.currencies,
        R.string.categories,
        R.string.num_records,
        R.string.rec_sorting_order,
        //R.string.rebuild_rec_dict,
    };

    static private final String MAIN_TEXT = "1";
    static private final String SUB_TEXT = "2";
}

/*
 * Copyright (c) 2018, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import ru.knuckles.walletoid.*;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.Utils;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.database.WalletEvents.CategoryObserver;
import ru.knuckles.walletoid.database.WalletEvents.CurrencyObserver;
import ru.knuckles.walletoid.dialogs.CategoryContextMenuHandler;
import ru.knuckles.walletoid.dialogs.CurrencyContextMenuHandler;
import ru.knuckles.walletoid.views.CalendarDatePicker;
import ru.knuckles.walletoid.views.RecordsListView;
import ru.knuckles.walletoid.views.SumEditTextWatcher;
import ru.knuckles.walletoid.views.TimeIntervalPickSpinner;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class RecordEditActivity extends Activity
        implements OnTimeSetListener, CalendarDatePicker.ChangeObserver, SumEditTextWatcher.Observer {
    // Intent data.
    public static final String EXTRA_REPEAT_RECORD_ID = "repeat record id";  // Long
    public static final String EXTRA_REPEAT_RECORD_TS = "repeat record ts";  // Long

    // Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_edit_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // Read last used record date.
        final SharedPreferences prefs = getSharedPreferences(
                getString(R.string.app_id),
                Context.MODE_PRIVATE
        );
        final long millisSinceLastCommit =
                System.currentTimeMillis() - prefs.getLong(PrefUtils.PREF_LAST_COMMIT_DATE, 0);
        if (TimeUnit.MILLISECONDS.toMinutes(millisSinceLastCommit) < 2) {
            final long lastUsedTS = prefs.getLong(PrefUtils.PREF_LAST_RECORD_DATE, 0);
            _lastCommittedDate = lastUsedTS > 0 ? CalendarUtils.Cal(lastUsedTS) : null;
        }

        // Read Intent data.
        final Bundle intentExtras = getIntent().getExtras();
        final String walletName =
                intentExtras.containsKey(IntentExtras.WALLET_NAME) ?
                intentExtras.getString(IntentExtras.WALLET_NAME) :
                WalletManager.MAIN_DB_NAME;
        final WalletManager wMan = Walletoid.getInstance().getWalletManager();
        _wallet = wMan.getWallet(walletName, false, true);
        _dict = wMan.getWalletDictionary(walletName);
        _initialRecord = createRecordFromBundle(intentExtras);
        _recordCatKind = _wallet.getCategory(_initialRecord.getCatID()).getKind();

        // Get layout controls.
        _saveButton = (Button) findViewById(R.id.recSaveButton);
        _datePicker = (CalendarDatePicker) findViewById(R.id.datePicker);
        _datePicker.setObserver(this);
        _timeBtn = (Button) findViewById(R.id.recTimeBtn);
        _sumEdit = (EditText) findViewById(R.id.recSumEdit);
        _currencySpinner = (Spinner) findViewById(R.id.recCurrencySpinner);
        _descrEdit = (EditText) findViewById(R.id.recDescrEdit);
        _catSpinner = (Spinner) findViewById(R.id.recCatSpin);
        _slaveRecsLayout = (LinearLayout) findViewById(R.id.slaveRecordsLayout);
        _slaveRecsView = (RecordsListView) findViewById(R.id.slaveRecordsView);
        _recRepeatCheckBox = (CheckBox) findViewById(R.id.repeatRecCheckBox);
        _recRepeatSpinner = (TimeIntervalPickSpinner) findViewById(R.id.repeatRecIntervalSpinner);

        // Setup controls.
        _sumEditWatcher = new SumEditTextWatcher(this, _sumEdit, this);
        _descrEdit.addTextChangedListener(this._descrChangeListener);
        _currencySpinner.setOnCreateContextMenuListener(new CurrencyContextMenuHandler(this, _wallet));
        _catSpinner.setOnCreateContextMenuListener(new CategoryContextMenuHandler(this, _wallet, _recordCatKind));
        attachCurrSpinnerToDB();
        attachCatSpinnerToDB();

        updateControlsToRecValues(_initialRecord);

        // Notifications.
        if (intentExtras.containsKey(IntentExtras.NOTIFICATION_ID)) {
            final int notificationId = intentExtras.getInt(IntentExtras.NOTIFICATION_ID);
            final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
            Toast.makeText(this,
                    getString(R.string.confirm_repeated_record, getText(R.string.save_and_return)),
                    Toast.LENGTH_LONG).show();
        }

        // Listen to wallet events.
        _wallet.events.addCategoryObserver(_recObserver);
        _wallet.events.addCurrencyObserver(_curObserver);
    }

    @Override
    protected void onDestroy() {
        if (!_accepted)
            maybeReaddNotification();
        _wallet.events.removeCategoryObserver(_recObserver);
        _wallet.events.removeCurrencyObserver(_curObserver);
        Walletoid.getInstance().getWalletManager().releaseWallet(_wallet.getName());
        _wallet = null;

        super.onDestroy();
    }

    private void maybeReaddNotification() {
        // Readd notification, if Record was not confirmed.

        final Bundle intentExtras = getIntent().getExtras();
        if (!intentExtras.containsKey(IntentExtras.NOTIFICATION_ID))
            return;

        final long templateRecId = intentExtras.getLong(EXTRA_REPEAT_RECORD_ID, Record.UNKNOWN_ID);
        if (templateRecId == Record.UNKNOWN_ID)
            return;

        final long timestamp = intentExtras.getLong(EXTRA_REPEAT_RECORD_TS, 0);
        RepeatedRecordsScheduler.notifyOnRecordRepeat(getApplicationContext(), templateRecId, timestamp);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putLong("date-time", _selectedDate.getTimeInMillis());
        bundle.putInt("cat-id", _catSpinner.getSelectedItemPosition());
        bundle.putInt("currency", _currencySpinner.getSelectedItemPosition());
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        _selectedDate.setTimeInMillis(bundle.getLong("date-time"));
        updateDateControls();
        _catSpinner.setSelection(bundle.getInt("cat-id"));
        _currencySpinner.setSelection(bundle.getInt("currency"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode != RESULT_OK)
            return;
        switch (ActivityRequests.values()[requestCode]) {
            // See |startMasterSelection|
            case RECORD_SELECTION: {
                long masterRecId = intent.getLongExtra(IntentExtras.RECORD_ID, Record.UNKNOWN_ID);
                setMasterRecord(_wallet.findRecord(masterRecId));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            return false;  // On older devices we display |_recTopPanel|.
        if (!super.onCreateOptionsMenu(menu))
            return false;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.rec_editor_menu, menu);
        _menu = menu;
        updateMasterLinkButton();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            return false;
        super.onOptionsItemSelected(item);
        final int id = item.getItemId();
        if (id == R.id.menu_recLink) {
            onLinkButtonClicked(null);
        }
        else {
            return false;
        }
        return true;
    }

    // CalendarDatePicker.DateRangeObserver
    @Override
    public void onCalendarDateSet(Calendar calendar) {
        _selectedDate.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        if (!_timeSelectedManually) {
            suggestTimeForDate(_selectedDate);
        }
        updateDateControls();
    }

    // OnTimeSetListener
    @Override
    public void onTimeSet(TimePicker timeDlg, int hour, int minute) {
        _selectedDate.set(Calendar.HOUR_OF_DAY, hour);
        _selectedDate.set(Calendar.MINUTE, minute);
        _timeSelectedManually = true;
        updateDateControls();
    }

    // Called by RecTimeBtn
    public void pickTime(View v) {
        // TODO: Cache dialog object.
        final TimePickerDialog dialog = new TimePickerDialog(this, this,
            _selectedDate.get(Calendar.HOUR_OF_DAY), _selectedDate.get(Calendar.MINUTE),
            true);
        dialog.show();
    }

    public void onLinkButtonClicked(View view) {
        if (_masterRec == null)
            startMasterSelection();
        else
            setMasterRecord(null);
    }

    private void startMasterSelection() {
        Calendar monthStart = (Calendar) _selectedDate.clone();
        CalendarUtils.setCalendarToMonthStart(monthStart);
        Intent recordSelectionIntent = new Intent(this, RecordsViewActivity.class);
        recordSelectionIntent.putExtra(RecordsViewActivity.EXTRA_SELECT, true);
        recordSelectionIntent.putExtra(RecordsViewActivity.EXTRA_TITLE, getString(R.string.select_main_record));
        recordSelectionIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
        recordSelectionIntent.putExtra(IntentExtras.DATE_TIMESTAMP, monthStart.getTimeInMillis());
        recordSelectionIntent.putExtra(IntentExtras.CATEGORY_KIND, _recordCatKind.invert().ordinal());
        startActivityForResult(recordSelectionIntent, ActivityRequests.RECORD_SELECTION.ordinal());
    }

    public void acceptRecord(View v) {
        _sumEditWatcher.applyPendingEdits();
        if (!_sumEditWatcher.inputIsValid())
            return;

        // Prepare record.
        Record resultRec = new Record(
            _initialRecord.getId(),
            _catSpinner.getSelectedItemId(),
            _selectedDate.getTimeInMillis(),
            new Record.Sums(new Record.Sum(
                    _sumEditWatcher.getEvaluatedSum(),
                    _currencySpinner.getSelectedItemId())),
            _sumEditWatcher.sumIsConstExpr() ? null : _sumEditWatcher.getUserInput(),
            _descrEdit.getEditableText().toString()
        );
        resultRec.setTemplateRecId(_initialRecord.getTemplateRecId());

        // Update database.
        // TODO: This part became too complex and too critical to live without tests.
        _wallet.beginTransaction();
        try {
            // Create or update record.
            if (editingExistingRecord()) {
                _wallet.updateRecord(resultRec);
            }
            else {
                long newRecId = _wallet.addRecord(resultRec);
                // Recreate object to have it ready with its actual ID.
                resultRec = new Record(resultRec, newRecId);
            }

            // Update record links.
            if (_masterRec != null) {
                _wallet.linkRecords(resultRec.getId(), _masterRec.getId());
            }
            else if (_wallet.findMasterRecord(resultRec.getId()) != null) {
                _wallet.unlinkRecord(resultRec.getId());
            }

            // Update record schedule.
            final Pair<Long, Boolean> scheduledIdInfo = scheduledRecordId(_wallet, _initialRecord);
            final boolean wasScheduled = scheduledIdInfo != null;
            final boolean ownSchedule = wasScheduled && scheduledIdInfo.second;
            final TimeInterval selectedInterval = _recRepeatSpinner.selectedInterval();
            final TimeInterval prevInterval = wasScheduled ?
                    _wallet.getRepetitionInterval(scheduledIdInfo.first) :
                    selectedInterval;
            final boolean timeParamsChanged = (selectedInterval != prevInterval) ||
                    (_selectedDate.getTimeInMillis() != _initialRecord.getTimestamp());

            // TODO: Optimize rescheduling to emit only 1 Wallet notification per interval change.

            if (wasScheduled &&
                    (!_recRepeatCheckBox.isChecked() ||
                     (timeParamsChanged && !ownSchedule))) {
                _wallet.unscheduleRecord(scheduledIdInfo.first);
            }

            if (_recRepeatCheckBox.isChecked() && (!wasScheduled || timeParamsChanged)) {
                _wallet.scheduleRecord(resultRec.getId(), selectedInterval);
            }

            _wallet.transactionSucceeded();
        }
        finally {
            _wallet.endTransaction();
        }

        // Remember last used currency editor values.
        try {
            final SharedPreferences.Editor editor =
                    getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE).edit();
            editor.putLong(PrefUtils.PREF_LAST_RECORD_CURRENCY, _currencySpinner.getSelectedItemId());
            editor.putLong(PrefUtils.PREF_LAST_RECORD_DATE, _selectedDate.getTimeInMillis());
            editor.putLong(PrefUtils.PREF_LAST_COMMIT_DATE, System.currentTimeMillis());
            editor.apply();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Could not write preference. " + e.toString());
        }

        // Set activity result and return.
        setResult(RESULT_OK, getIntent());
        _accepted = true;
        finish();
    }

    // Called by _sumEditWatcher
    @Override
    public void onInputChecked(String input, boolean isValidExpr) {
        _saveButton.setEnabled(_sumEditWatcher.inputIsValid());
    }

    // Private

    private boolean editingExistingRecord() {
        return _initialRecord.getId() != Record.UNKNOWN_ID;
    }

    /**
     * @return ID of the record or the one of its template, if either is scheduled.
     */
    private static Pair<Long, Boolean> scheduledRecordId(Wallet wallet, Record record) {
        if (wallet.recordIsScheduled(record.getId()))
            return new Pair<>(record.getId(), true);
        if (wallet.recordIsScheduled(record.getTemplateRecId()))
            return new Pair<>(record.getTemplateRecId(), false);
        return null;
    }

    private Record createRecordFromBundle(Bundle bundle) {
        final long existingRecId =
                bundle.getLong(IntentExtras.RECORD_ID, Record.UNKNOWN_ID);
        if (existingRecId != Record.UNKNOWN_ID)
            return _wallet.getRecord(existingRecId);

        final long templateRecId = bundle.getLong(EXTRA_REPEAT_RECORD_ID, Record.UNKNOWN_ID);
        if (templateRecId != Record.UNKNOWN_ID) {
            final long timestamp = bundle.getLong(EXTRA_REPEAT_RECORD_TS, 0);
            return _wallet.getRecord(templateRecId).makeRepeated(timestamp);
        }

        final int desiredKindIndex = Utils.clampInt(
                bundle.getInt(IntentExtras.CATEGORY_KIND, 0),
                Category.Kind.INCOME.ordinal(),
                Category.Kind.EXPENSE.ordinal());
        final Category.Kind desiredRecordKind = Category.Kind.values()[desiredKindIndex];
        final long defaultCatId = desiredRecordKind == Category.Kind.INCOME ?
                WalletSchema_v5.CAT_ID_MISC_INCOME :
                WalletSchema_v2.CAT_ID_MISC_EXPENSE;
        SharedPreferences prefs =
                getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE);
        long lastCurrencyId = prefs.getLong(PrefUtils.PREF_LAST_RECORD_CURRENCY,
                _wallet.getReferenceCurrencyId());

        final long recTimestamp;
        if (bundle.containsKey(IntentExtras.DATE_TIMESTAMP)) {
            final Calendar recDateTime =
                    CalendarUtils.Cal(bundle.getLong(IntentExtras.DATE_TIMESTAMP));
            suggestTimeForDate(recDateTime);
            recTimestamp = recDateTime.getTimeInMillis();
        } else {
            recTimestamp = System.currentTimeMillis();
        }

        return new Record(
                defaultCatId,
                recTimestamp,
                new Record.Sum(lastCurrencyId),
                ""
        );
    }

    private void suggestTimeForDate(Calendar dest) {
        if (_lastCommittedDate != null && CalendarUtils.isSameDay(dest, _lastCommittedDate)) {
            // Use recently committed time. Simplifies input of several records having same time.
            CalendarUtils.copyFieldValue(Calendar.HOUR_OF_DAY, _lastCommittedDate, dest);
            CalendarUtils.copyFieldValue(Calendar.MINUTE, _lastCommittedDate, dest);
        }
        else {  // Without a better guess just set time to 12:00.
            dest.set(Calendar.HOUR_OF_DAY, 12);
            dest.set(Calendar.MINUTE, 0);
        }
    }

    private void updateControlsToRecValues(Record record) {
        setMasterRecord(_wallet.findMasterRecord(record.getId()));

        // Date.
        _selectedDate = record.getCalendar();
        updateDateControls();

        // Other Record properties.
        _sumEdit.setText(null);
        final String sumEditorText;
        if (!record.getSumExpression().isEmpty())
            sumEditorText = record.getSumExpression();
        else {
            final BigDecimal sumAmount = record.getSum().amount;
            sumEditorText = sumAmount.equals(BigDecimal.ZERO) ? null : sumAmount.toString();
        }
        if (sumEditorText != null)
            _sumEdit.append(sumEditorText);
        _descrEdit.setText(record.getDescr());
        Utils.setSpinnerSelectionToItemWithId(_catSpinner, record.getCatID());
        Utils.setSpinnerSelectionToItemWithId(_currencySpinner, record.getCurrencyID());

        // Repetition.
        TimeInterval selectedInterval = TimeInterval.MONTH;
        final Pair<Long, Boolean> scheduledIdInfo = scheduledRecordId(_wallet, _initialRecord);
        if (scheduledIdInfo != null) {
            try {
                selectedInterval = _wallet.getRepetitionInterval(scheduledIdInfo.first);
                _recRepeatCheckBox.setChecked(true);
            }
            catch (Exception e) {
                e.printStackTrace();
                RepeatedRecordsScheduler.unscheduleRecord(
                        getApplicationContext(), scheduledIdInfo.first);
            }
        }
        _recRepeatSpinner.setSelectedInterval(selectedInterval);

        // Slave records.
        final boolean hasSlaves = editingExistingRecord() && _wallet.hasSlaveRecords(_initialRecord.getId());
        _slaveRecsLayout.setVisibility(hasSlaves ? View.VISIBLE : View.INVISIBLE);
        _slaveRecsView.getCurrentFilter().setMasterRecordID(_initialRecord.getId());
        _slaveRecsView.setWallet(hasSlaves ? _wallet : null);
    }

    private void updateDateControls() {
        _datePicker.setCalendar(_selectedDate);
        _timeBtn.setText(_timeFormat.format(_selectedDate.getTime()));
    }

    private void attachCatSpinnerToDB() {
        if (_catsCursor != null) {
            stopManagingCursor(_catsCursor);
            _catsCursor.close();
        }

        _catsCursor = _wallet.getCategoriesCursor(_recordCatKind);
        startManagingCursor(_catsCursor);
        _catsAdapter = new SimpleCursorAdapter(
            this,
            android.R.layout.simple_spinner_item,
            _catsCursor,
            new String[] { WalletSchema_v4.TBL_CATEGORIES.cat_name.name },
            new int[] {android.R.id.text1} );
        _catsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _catSpinner.setAdapter(_catsAdapter);
    }

    private void attachCurrSpinnerToDB() {
        if (_currsCursor != null) {
            stopManagingCursor(_currsCursor);
            _currsCursor.close();
        }

        _currsCursor = _wallet.getCurrenciesCursor();
        startManagingCursor(_currsCursor);
        _currsAdapter = new SimpleCursorAdapter(
            this,
            android.R.layout.simple_spinner_item,
            _currsCursor,
            new String[] { WalletSchema_v4.TBL_CURRENCIES.curr_name.name },
            new int[] {android.R.id.text1} );
        _currsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _currencySpinner.setAdapter(_currsAdapter);
    }

    private void setUniversalTitle(String title) {
        setTitle(title);
    }

    private void setMasterRecord(Record master) {
        _masterRec = master;
        updateMasterLinkButton();
        updateMasterRecordDisplay();
    }

    private void updateMasterRecordDisplay() {
        setUniversalTitle(_masterRec != null ?
                getString(R.string.record_summary_template,
                        _masterRec.getDescr(),
                        _wallet.getCurrency(_masterRec.getSum().currencyId).getSymbol(),
                        _masterRec.getSum().amount.floatValue()
                ) :
                getString(_recordCatKind.isExpense() ? R.string.expense_title : R.string.income_title)
        );
    }

    private void updateMasterLinkButton() {
        MenuItem linkMenuItem = _menu == null ? null : _menu.findItem(R.id.menu_recLink);
        if (linkMenuItem == null)
            return;
        linkMenuItem.setTitle(_masterRec == null ?
                R.string.master_rec_btn_choose :
                R.string.master_rec_btn_unset);
    }

    private final CategoryObserver _recObserver = new CategoryObserver() {
        @Override
        public void onCategoryAdded(long id) {
            // TODO: set to added category, not the last item.
            if (_catsCursor.requery())
                Utils.setSpinnerSelectionToItemWithId(_catSpinner, id);
        }

        @Override
        public void onCategoryUpdated(long id) {
            _catsCursor.requery();
        }

        @Override
        public void onCategoryRemoved(long id, long toId) {
            if (_catsCursor.requery())
                Utils.setSpinnerSelectionToItemWithId(_catSpinner, toId);
        }
    };

    private final CurrencyObserver _curObserver = new CurrencyObserver() {
        @Override
        public void onCurrencyAdded(long id) {
            if (_currsCursor.requery())
                Utils.setSpinnerSelectionToItemWithId(_currencySpinner, id);
        }

        @Override
        public void onCurrencyUpdated(long id) {
            _currsCursor.requery();
        }

        @Override
        public void onCurrencyRemoved(long removedCurId, long acceptorCurId) {
            if (_currsCursor.requery())
                Utils.setSpinnerSelectionToItemWithId(_currencySpinner, acceptorCurId);
        }
    };

    private TextWatcher _descrChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            long probableCatId = _dict.guessCategory(_recordCatKind, s.toString());
            if (probableCatId == Category.UNKNOWN_ID)
                return;
            // FIXME: This crashes if probableCatId is not there.
            Utils.setSpinnerSelectionToItemWithId(RecordEditActivity.this._catSpinner, probableCatId);
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    private enum ActivityRequests {
        RECORD_SELECTION
    }

    private final static DateFormat _timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);

    // Views
    private Button _saveButton;
    private CalendarDatePicker _datePicker;
    private Button _timeBtn;
    private EditText _sumEdit;
    private Spinner _currencySpinner;
    private EditText _descrEdit;
    private Spinner _catSpinner;
    private LinearLayout _slaveRecsLayout;
    private RecordsListView _slaveRecsView;
    private CheckBox _recRepeatCheckBox;
    private TimeIntervalPickSpinner _recRepeatSpinner;

    // UI related.
    private Menu _menu;
    private SumEditTextWatcher _sumEditWatcher;
    private Cursor _catsCursor;
    private Cursor _currsCursor;
    private SimpleCursorAdapter _catsAdapter;
    private SimpleCursorAdapter _currsAdapter;
    private Calendar _selectedDate = Calendar.getInstance();
    private boolean _timeSelectedManually = false;

    // Other data
    private Wallet _wallet;
    private RecordDictionary _dict;
    private Category.Kind _recordCatKind;
    private Record _initialRecord;
    private Record _masterRec = null;
    private boolean _accepted = false;  // Whether we accepted the edited Record.
    private Calendar _lastCommittedDate;
}

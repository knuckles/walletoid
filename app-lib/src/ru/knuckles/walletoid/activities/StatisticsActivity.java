/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.CalendarUtils.DateRange;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Walletoid;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.dialogs.MonthSelectDialog;
import ru.knuckles.walletoid.dialogs.MonthSelectDialog.OnMonthSelectedListener;
import ru.knuckles.walletoid.views.CalendarRangePicker;
import ru.knuckles.walletoid.views.LeftRightSwitcher;
import ru.knuckles.walletoid.views.StatPageAdapter;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Map;

public class StatisticsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.statscreen);
        _periodPicker = (LeftRightSwitcher) findViewById(R.id.periodPicker);
        _periodPicker.addSwitchObserver(_periodSwitchObserver);

        String walletName = getIntent().getStringExtra(IntentExtras.WALLET_NAME);
        _wallet = Walletoid.getInstance().getWalletManager().getWallet(walletName, false, false);

        _pager = (ViewPager) findViewById(R.id.statPager);
        _pager.addOnPageChangeListener(_pageChangeListener);
        _pageAdapter = new StatPageAdapter(this);
        _pageZoomCtrl = new StatPageAdapter.ZoomController(_pager, _pageAdapter);

        setDefaultFilterConstraints(getIntent());
        _pageAdapter.setWallet(_wallet);

        // Update date controls.
        displayedRangeChanged(_pager.getCurrentItem());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _pager.removeOnPageChangeListener(_pageChangeListener);
        _periodPicker.removeObserver(_periodSwitchObserver);
        _pageAdapter.setWallet(null);
        Walletoid.getInstance().getWalletManager().releaseWallet(_wallet.getName());
        _wallet = null;
    }

    public void onZoomBtnClick(View zoomBtn) {
        final int increment = Integer.parseInt(zoomBtn.getTag().toString());
        _pageZoomCtrl.zoom(increment > 0);
        displayedRangeChanged(_pager.getCurrentItem());
    }

    // Private

    private void setDefaultFilterConstraints(Intent intent) {
        // Display current month by default
        final Calendar startDate = CalendarUtils.getCurrentMonthStart();
        if (intent.hasExtra(IntentExtras.DATE_TIMESTAMP))
            startDate.setTimeInMillis(
                    intent.getLongExtra(IntentExtras.DATE_TIMESTAMP, 0));
        _pageAdapter.getPivotRange().setRange(startDate, TimeInterval.MONTH);
    }

    private void displayedRangeChanged(final int position) {
        final DateRange range = _pageAdapter.getRangeForPage(position);
        _periodPicker.setText(
                INTERVAL_FORMATS.get(range.getInterval()).
                        format(range.getRangeStart().getTime())
        );
    }

    private final ViewPager.OnPageChangeListener _pageChangeListener =
            new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {
            displayedRangeChanged(position);
        }
    };

    private final LeftRightSwitcher.SwitchObserver _periodSwitchObserver =
            new LeftRightSwitcher.SwitchObserver() {
        // TODO: Partial copy from CalendarRangePicker.

        @Override
        public void onCenterClick() {
            if (_pageAdapter.getPivotRange().getInterval() != CalendarUtils.TimeInterval.MONTH)
                return;  // TODO: This dialog is unfit to select anything than a month.

            if (dialog == null) {
                dialog = new MonthSelectDialog(StatisticsActivity.this, _onMonthSelectedListener);
            }

            final DateRange range = _pageAdapter.getRangeForPage(_pager.getCurrentItem());
            final Calendar start = range.getRangeStart();
            dialog.prepare(start.get(Calendar.YEAR), start.get(Calendar.MONTH));
            dialog.show();
        }

        @Override
        public void onLeftSwitch() {
            scrollPages(-1);
        }

        @Override
        public void onRightSwitch() {
            scrollPages(1);
        }

        private void scrollPages(int pages) {
            _pager.setCurrentItem(_pager.getCurrentItem() + pages, true);
        }

        private MonthSelectDialog dialog;
    };

    private OnMonthSelectedListener _onMonthSelectedListener = new OnMonthSelectedListener() {
        @Override
        public void onMonthSelected(final MonthSelectDialog srcDlg, final int year,
                final int month) {
            _pageAdapter.setWallet(null);  // Optimize out unneeded queries.
            _pager.setCurrentItem(_pageAdapter.getZeroIndex(), false);
            _pageAdapter.getPivotRange().setStart(year, month, 1);
            _pageAdapter.setWallet(_wallet);

            displayedRangeChanged(_pager.getCurrentItem());
        }
    };

    private final Map<CalendarUtils.TimeInterval, DateFormat> INTERVAL_FORMATS =
            CalendarRangePicker.createIntervalFormatsMap();

    // Controls.
    private LeftRightSwitcher _periodPicker;
    private ViewPager _pager;

    // Other data.
    private Wallet _wallet;
    private StatPageAdapter _pageAdapter;
    private StatPageAdapter.ZoomController _pageZoomCtrl;
}

/*
 * Copyright (c) 2018, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.MenuItem.OnMenuItemClickListener;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;
import ru.knuckles.walletoid.*;
import ru.knuckles.walletoid.Utils;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.dialogs.CategorySelectDialog;
import ru.knuckles.walletoid.dialogs.CurrencyEditDialog;
import ru.knuckles.walletoid.dialogs.CurrencySelectDialog;
import ru.knuckles.walletoid.importing.csv.CSVImporter;
import ru.knuckles.walletoid.views.RecordsListView;
import ru.knuckles.walletoid.views.RecordsListView.SelectionContextMenuProvider;
import ru.knuckles.walletoid.views.RecordsListView.SelectionRangeListener;
import ru.knuckles.walletoid.views.RecordsRangeView;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;

import static ru.knuckles.walletoid.Utils.joinStrings;
import static ru.knuckles.walletoid.activities.IntentExtras.DATE_TIMESTAMP;

public class MainActivity extends RecordsViewActivity {
    // RecordsViewActivity

    @Override
    protected RecordsRangeView createContent() {
        setContentView(R.layout.main_activity);
        _incomeButton = (Button) findViewById(R.id.NewIncomeBtn);
        _expenseButton = (Button) findViewById(R.id.NewExpenseBtn);

        RecordsRangeView recordsRangeView = (RecordsRangeView) findViewById(R.id.monthView);
        recordsRangeView.setAllowRangeSelection(true);
        return recordsRangeView;
    }

    @Override
    protected void attachWallet(Wallet wallet) {
        super.attachWallet(wallet);
        _wallet.events.addAnyEventObserver(_walletObserver);
        updateTotalSums(_recordsRangeView.copyFilter());
    }

    @Override
    protected void detachWallet() {
        if (_wallet != null)
            _wallet.events.removeAnyEventObserver(_walletObserver);
        super.detachWallet();
        updateTotalSums(null);
    }

    // Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_name);
        maybeInitDefaultCurrency();
    }

    @Override
    protected void onStart() {
        super.onStart();
        _recordsRangeView.setSelectionRangeListener(_rowSelectionListener);
        _recordsRangeView.setSelectionContextMenuProvider(_selectionContextMenuProvider);
        _recordsRangeView.addFilterObserver(_walletFilterObserver);
    }

    @Override
    protected void onStop() {
        _recordsRangeView.setSelectionRangeListener(null);
        _recordsRangeView.setSelectionContextMenuProvider(null);
        _recordsRangeView.removeFilterObserver(_walletFilterObserver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (BuildConfig.DEBUG) {
            List<String> leakedWalletNames = _wm.checkRefCounts();
            if (leakedWalletNames != null && !leakedWalletNames.isEmpty())
                Log.w(getClass().getSimpleName(),
                        "Leaked wallets: " + joinStrings(leakedWalletNames));
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!super.onCreateOptionsMenu(menu))
            return false;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(_queryTextListener);
        searchView.setOnCloseListener(_searchCloseListener);

        _menu = menu;
        return true;
    }

    private void updateOptionsMenu() {
        final boolean recsSelected = _selectionFilter != null && _selectionFilter.getLimit() >= 1;
        _menu.setGroupVisible(R.id.main_menu_group_normal, !recsSelected);
        _menu.setGroupVisible(R.id.main_menu_group_selection, recsSelected);

        final MenuItem dupItem = _menu.findItem(R.id.duplicate_record);
        if (dupItem != null) {
            dupItem.setVisible(_selectionFilter != null && _selectionFilter.getLimit() == 1);
        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!super.onPrepareOptionsMenu(menu))
            return false;
        MenuItem bakRestoreItem = menu.findItem(R.id.menu_restore_backup);
        if (bakRestoreItem != null)
            bakRestoreItem.setEnabled(_wm.backupFileExists());
        return true;
    }

    // This handles only app bar action items.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        final int iid = item.getItemId();
        if (iid == R.id.menu_show_stat) {
            Intent statIntent = new Intent(this, StatisticsActivity.class);
            statIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
            statIntent.putExtra(
                    DATE_TIMESTAMP,
                    _recordsRangeView.copyFilter().getDateRangeStart().getTimeInMillis());
            startActivity(statIntent);
        }
        /* #94: Disable import while it's broken.
        else if (iid == R.id.menu_csv_import) {
            chooseFileToImport();
        }
        */
        else if (iid == R.id.menu_make_backup) {
            if (!maybeRequestDBBackupPermissions(ActivityRequests.DB_BACKUP))
                makeDBBackup();
        }
        else if (iid == R.id.menu_preferences) {
            startActivity(new Intent(this, PreferenceActivity.class));
        }
        else if (iid == R.id.menu_restore_backup) {
            confirmDBRestore();

        }
        else if (iid == R.id.menu_about_app) {
            startActivity(new Intent(this, ProgramInfoActivity.class));
        }
        else if (iid == R.id.duplicate_record) {
            offerRecordDuplicate(_selectionFilter);
        }
        else if (iid == R.id.set_currency) {
            offerCurrencyChange(_selectionFilter);
        }
        else if (iid == R.id.set_category) {
            offerCategoryChange(_selectionFilter);
        }
        else if (iid == R.id.remove_records) {
            offerRecordsRemoval(_selectionFilter);
        }
        else {
            return false;  // Unhandled
        }
        
        return true;  // Handled
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode != RESULT_OK)
            return;
        switch (ActivityRequests.values()[requestCode]) {
            case CSV_FILE_SELECTION: {
                try {
                    startImport(getContentResolver().openInputStream(intent.getData()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
            int[] grantResults) {
        boolean allGranted = true;
        for (int r : grantResults) {
            if (r != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        int errorMessageId = 0;
        switch (ActivityRequests.values()[requestCode]) {
            case DB_BACKUP: {
                if (allGranted)
                    makeDBBackup();
                else
                    errorMessageId = R.string.backups_require_storage_access;
                break;
            }
            case DB_RESTORE: {
                if (allGranted)
                    restoreDBBackup();
                else
                    errorMessageId = R.string.backups_require_storage_access;
                break;
            }
        }

        if (errorMessageId != 0)
            Toast.makeText(this, errorMessageId, Toast.LENGTH_LONG).show();
    }

    // Click handlers ------------------------------------------------------------------------------

    public void onNewRecordBtnClick(View v) {
        Intent newRecIntent = new Intent(this, RecordEditActivity.class);
        newRecIntent.putExtra(IntentExtras.WALLET_NAME, _wallet.getName());
        newRecIntent.putExtra(
            IntentExtras.CATEGORY_KIND,
            Integer.decode(v.getTag().toString())
        );
        // Guess desired date for a new record. Maybe it should lie somewhere inside a viewed range.
        final RecordsListView currentRecList = this._recordsRangeView.getCurrentView();
        final int firstVisiblePos = currentRecList.getFirstVisiblePosition();
        final boolean suggestDate = firstVisiblePos > 0
                || !CalendarUtils.isSameMonth(
                        CalendarUtils.getCurrentMonthStart(),
                        this._recordsRangeView.copyFilter().getDateRangeStart());
        if (suggestDate) {
            final int lastVisiblePos = currentRecList.getLastVisiblePosition();
            if (lastVisiblePos >= firstVisiblePos) {  // Otherwise the list is likely empty.
                final int centeredPos = firstVisiblePos + (lastVisiblePos - firstVisiblePos) / 2;
                final Record centeredRec =
                        _wallet.getRecord(currentRecList.getItemIdAtPosition(centeredPos));
                final Calendar centeredDate = centeredRec.getCalendar();
                // Reset time to noon:
                CalendarUtils.truncateCalendarFieldsUntil(centeredDate, Calendar.HOUR_OF_DAY);
                centeredDate.set(Calendar.HOUR_OF_DAY, 12);

                newRecIntent.putExtra(IntentExtras.DATE_TIMESTAMP, centeredDate.getTimeInMillis());
            }
        }
        startActivity(newRecIntent);
    }

    // Actions -------------------------------------------------------------------------------------

    private void confirmDBRestore() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getText(android.R.string.dialog_alert_title));
        alertDialog.setMessage(getString(R.string.are_you_sure));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getText(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (!maybeRequestDBBackupPermissions(ActivityRequests.DB_RESTORE))
                            restoreDBBackup();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getText(android.R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void offerCategoryChange(final WalletFilter filter) {
        if (filter == null)
            throw new IllegalStateException(ERR_REQ_FILTER);
        Dialog catSelectDlg = new CategorySelectDialog(
                MainActivity.this,
                _wallet.getCategoriesList(),
                new CategorySelectDialog.OnCategorySelectedListener() {
                    @Override
                    public void onCategoryKindSelected(CategorySelectDialog srcDlg,
                            Category.Kind catKind) {
                        // Should not be called.
                    }

                    @Override
                    public void onCategorySelected(CategorySelectDialog srcDlg,
                            long selectedCatID) {
                        _wallet.updateRecords(filter, selectedCatID, Currency.UNKNOWN_ID);
                    }
                },
                false,
                getString(R.string.set_category));
        catSelectDlg.show();
    }

    private void offerCurrencyChange(final WalletFilter filter) {
        if (filter == null)
            throw new IllegalStateException(ERR_REQ_FILTER);
        CurrencySelectDialog currSelectDlg = new CurrencySelectDialog(
                MainActivity.this,
                _wallet,
                new CurrencySelectDialog.OnCurrencySelectedListener() {
                    @Override
                    public void onCurrencySelected(long selectedCurrencyID) {
                        _wallet.updateRecords(filter, Category.UNKNOWN_ID, selectedCurrencyID);
                    }
                },
                getString(R.string.set_category));
        currSelectDlg.show();
    }

    private void offerRecordDuplicate(final WalletFilter filter) {
        if (filter == null || filter.getLimit() != 1)
            throw new IllegalStateException(ERR_REQ_FILTER);
        final List<Record> selectedRecords = _wallet.getRecordsList(filter, null);
        if (selectedRecords == null || selectedRecords.size() != 1)
            throw new IllegalStateException(ERR_REQ_FILTER);

        final Intent recEditIntent = new Intent(this, RecordEditActivity.class);
        recEditIntent.putExtra(RecordEditActivity.EXTRA_REPEAT_RECORD_ID,
                selectedRecords.get(0).getId());
        recEditIntent.putExtra(RecordEditActivity.EXTRA_REPEAT_RECORD_TS,
                System.currentTimeMillis());
        startActivity(recEditIntent);
    }

    private void offerRecordsRemoval(final WalletFilter filter) {
        if (filter == null)
            throw new IllegalStateException(ERR_REQ_FILTER);

        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.confirm_records_removal))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        final long removed = _wallet.removeRecords(filter);
                        Toast.makeText(MainActivity.this,
                                getString(R.string.removed_n_records, removed),
                                Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void chooseFileToImport() {
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        String csvMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("csv");
        if (csvMimeType == null)
            csvMimeType = "file/*";
        fileIntent.setType(csvMimeType);
        try {
            startActivityForResult(fileIntent, ActivityRequests.CSV_FILE_SELECTION.ordinal());
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(this, R.string.need_file_chooser, Toast.LENGTH_LONG).show();
        }
    }

    private void startImport(InputStream csvStream) throws Exception {
        CSVImporter.Delegate importDelegate = new CSVImporter.Delegate() {
            @Override
            public void onImportFinished(String walletName) {
                if (walletName == null)
                    return;
                Intent previewIntent = new Intent(MainActivity.this, WalletImportActivity.class);
                previewIntent.putExtra(IntentExtras.WALLET_NAME, walletName);
                startActivity(previewIntent);
            }
        };
        InputStreamReader fileReader = new InputStreamReader(csvStream);
        new CSVImporter(this, fileReader, importDelegate, WalletSchema_v4.DEF_CURRENCY_ID);
    }

    private void makeDBBackup() {
        if (_wallet == null)
            return;

        detachWallet();
        try {
            _wm.backupMainWallet();
            Toast.makeText(this, R.string.backup_created, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(this, R.string.backup_error, Toast.LENGTH_SHORT).show();
            Log.e(getClass().getSimpleName(), "Backup creation error", e);
        }
        finally {
            attachWallet(_wm.getMainWallet(false));
        }
    }

    private void restoreDBBackup() {
        if (_wallet == null)
            return;

        // TODO: Try to move scheduler calls to WalletManager.
        RepeatedRecordsScheduler.removeAlarms(getApplicationContext());

        detachWallet();
        try {
            _wm.restoreMainWallet();
            Toast.makeText(this, R.string.backup_restored, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(this, R.string.backup_error, Toast.LENGTH_SHORT).show();
            Log.e(getClass().getSimpleName(), "Backup loading error", e);
        }
        finally {
            attachWallet(_wm.getMainWallet(false));
            RepeatedRecordsScheduler.setAlarms(getApplicationContext());
        }
    }

    // View updaters -------------------------------------------------------------------------------

    private void updateTotalSums(final WalletFilter filter) {
        if (_wallet == null) {
            _incomeButton.setText(null);
            _expenseButton.setText(null);
            return;
        }

        // TODO: This is slow (~300-500ms)! Optimize or move to background.
        final Pair<Record.Sum, Record.Sum> sums =
                _wallet.getTotalSums(filter, _wallet.getReferenceCurrency());
        final Record.Sum incomes = sums.first;
        final Record.Sum expenses = sums.second;

        final Utils.ApproxValue approxIncomes = new Utils.ApproxValue(incomes.amount);
        final Utils.ApproxValue approxExpenses = new Utils.ApproxValue(expenses.amount);

        _incomeButton.setText(getString(R.string.pos_sum_long_tpl,
                approxIncomes.value,
                approxIncomes.suffix,
                _wallet.getCurrency(incomes.currencyId).getSymbol()));
        _expenseButton.setText(getString(R.string.neg_sum_long_tpl,
                approxExpenses.value,
                approxExpenses.suffix,
                _wallet.getCurrency(expenses.currencyId).getSymbol()));
    }

    // Handler delegates ---------------------------------------------------------------------------

    private final SelectionRangeListener _rowSelectionListener = new SelectionRangeListener() {
        @Override
        public void OnSelectionRangeChanged(long firstRecordId, Pair<Integer, Integer> selection) {
            _selectionFilter = _recordsRangeView.copyFilter();
            if (selection != null && selection.second > 0) {
                _selectionFilter.setLimitOffset(selection.second, selection.first);
            }

            updateTotalSums(_selectionFilter);
            updateOptionsMenu();  // Show special menu items for selection.
        }
    };

    // Creates and handles long click menu for selected records.
    private SelectionContextMenuProvider _selectionContextMenuProvider =
            new SelectionContextMenuProvider() {

        // Sets category to all selected records.
        private final OnMenuItemClickListener _categoryItemHandler = new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                offerCategoryChange(_filter);
                return true;
            }
        };

        // Sets currency to all selected records.
        private final OnMenuItemClickListener _currencyItemHandler = new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                offerCurrencyChange(_filter);
                return true;
            }
        };

        // Duplicates selected record.
        private final OnMenuItemClickListener _dupItemHandler = new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                offerRecordDuplicate(_filter);
                return true;
            }
        };

        // Removes all selected records.
        private final OnMenuItemClickListener _removeItemHandler = new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                offerRecordsRemoval(_filter);
                return true;
            }
        };

        @Override
        public void createContextMenu(RecordsListView view, ContextMenu menu,
                long firstSelectedRecordId, Pair<Integer, Integer> selection) {
            _filter = _recordsRangeView.copyFilter();
            _filter.setLimitOffset(selection.second, selection.first);
            _inflater.inflate(R.menu.main_menu, menu);

            // Hide unneeded items.
            menu.setGroupEnabled(R.id.main_menu_group_normal, false);
            menu.setGroupVisible(R.id.main_menu_group_normal, false);
            menu.setGroupVisible(R.id.main_menu_group_selection, true);
            final MenuItem searchItem = menu.findItem(R.id.menu_search);
            searchItem.setVisible(false);
            searchItem.setEnabled(false);

            menu.findItem(R.id.set_category).setOnMenuItemClickListener(_categoryItemHandler);
            menu.findItem(R.id.set_currency).setOnMenuItemClickListener(_currencyItemHandler);
            menu.findItem(R.id.remove_records).setOnMenuItemClickListener(_removeItemHandler);
            final MenuItem dupItem = menu.findItem(R.id.duplicate_record);
            dupItem.setOnMenuItemClickListener(_dupItemHandler);
            dupItem.setVisible(selection.second == 1);
            dupItem.setEnabled(selection.second == 1);
        }

        WalletFilter _filter = null;
        private final MenuInflater _inflater = new MenuInflater(MainActivity.this);
    };

    private WalletEvents.AnyEventObserver _walletObserver = new WalletEvents.AnyEventObserver(
            WalletEvents.WE_ALL_RECORD_EVT |
            WalletEvents.WE_CURRENCY_UPDATED |  // Currencies may change their values!
            WalletEvents.WE_BLK_COPY_COMPLETE |
            WalletEvents.WE_BLK_CURRENCY_UPD
    ) {
        @Override
        public void onWalletChanged(final long eventFlag) {
            updateTotalSums(_recordsRangeView.copyFilter());
        }
    };

    private WalletFilter.FilterObserver _walletFilterObserver = new WalletFilter.FilterObserver() {
        public void onFilterChanged(final WalletFilter filter) {
            updateTotalSums(filter);
        }
    };

    private final CurrencyEditDialog.CurrencyEditListener _currencyListener =
            new CurrencyEditDialog.CurrencyEditListener() {
        @Override
        public void onCurrencyEdited(Currency currency, boolean valueChanged) {
            _wallet.updateCurrency(currency);

            final SharedPreferences.Editor editor =
                getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE).edit();
            editor.putBoolean(PrefUtils.PREF_DEFAULT_CURRENCY_EDITED, true);
            editor.apply();
        }
    };

    private SearchView.OnQueryTextListener _queryTextListener =
            new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(final String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(final String newText) {
            _recordsRangeView.setFilterQuery(newText.length() > 0 ? newText + "*" : null);
            _recordsRangeView.resetSelection();
            return true;
        }
    };

    private SearchView.OnCloseListener _searchCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            _recordsRangeView.setFilterQuery(null);
            _recordsRangeView.resetSelection();
            updateOptionsMenu();
            return false;
        }
    };

    // Misc.

    private enum ActivityRequests {
        CSV_FILE_SELECTION,
        DB_BACKUP,
        DB_RESTORE,
    }

    private boolean maybeRequestDBBackupPermissions(ActivityRequests request) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return false;
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED)
            return false;

        requestPermissions(
                new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                request.ordinal());
        return true;
    }

    private void maybeInitDefaultCurrency() {
        final SharedPreferences prefs = getSharedPreferences(getString(R.string.app_id), Context.MODE_PRIVATE);
        if (prefs.getBoolean(PrefUtils.PREF_DEFAULT_CURRENCY_EDITED, false))
            return;
        final Currency refCurrency = _wallet.getCurrenciesList().get(0);
        final CurrencyEditDialog currEditDlg =
            new CurrencyEditDialog(this, refCurrency, refCurrency, _currencyListener);
        currEditDlg.setTitle(R.string.specify_default_currency);
        currEditDlg.show();
    }

    // Controls
    private Button _incomeButton;
    private Button _expenseButton;
    private Menu _menu;

    // Other
    private WalletFilter _selectionFilter = null;
    private final static String ERR_REQ_FILTER = "Constraint filter required!";
}

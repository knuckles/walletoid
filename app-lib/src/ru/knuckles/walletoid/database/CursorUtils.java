/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.database.Cursor;
import android.util.Pair;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;

import java.math.BigDecimal;
import java.util.*;

import static ru.knuckles.walletoid.database.WalletSchema_v7.*;

public class CursorUtils {
    // FACTORIES

    /**
     * Creates some {@link Wallet} object ({@link Category}, {@link Record}, etc) from a given
     * {@link Cursor}.
     * @param <T> Destination object type.
     */
    public abstract static class ObjectFactory<T> {
        protected ObjectFactory(SchemaDecl.ColumnDef idColumn) {
            _idColumn = idColumn;
        }

        /**
         * @return Whether row contains 'null' object.
         */
        public boolean isNull(Cursor cursor) {
            return cursor.isNull(cursor.getColumnIndexOrThrow(_idColumn.name));
        }

        public Pair<Long, T> extractObject(Cursor cursor) {
            _cursor = cursor;
            return new Pair<>(getObjectId(), makeObject());
        }

        /**
         * Same as {@link #makeObject}, but for a case, when {@link Cursor} cannot be read (empty).
         * @return blank object.
         */
        public Pair<Long, T> makeDefaultObject() { return null; }

        public NoSuchElementException getNoElementException(Object queryContext) {
            return new NoSuchElementException(
                    "Element not found: " + (queryContext != null ? queryContext.toString() : "?"));
        }

        protected Long getObjectId() {
            return _cursor.getLong(column(_idColumn));
        }

        /**
         * Creates some Wallet object from a given {@link Cursor}.
         * @return a pair, where first is object's ID (row id), second is the object itself.
         */
        protected abstract T makeObject();

        protected Cursor cursor() {
            return _cursor;
        }

        protected int column(SchemaDecl.ColumnDef column) {
            return  _cursor.getColumnIndexOrThrow(column.name);
        }

        protected int columnMaybe(SchemaDecl.ColumnDef column) {
            return  _cursor.getColumnIndex(column.name);
        }

        private final SchemaDecl.ColumnDef _idColumn;
        private Cursor _cursor = null;
    }

    public static class CategoryFactory extends ObjectFactory<Category> {
        public CategoryFactory() {
            super(TBL_CATEGORIES.cat_id);
        }

        @Override
        public Category makeObject() {
            return new Category(
                getObjectId(),
                cursor().getString(column(TBL_CATEGORIES.cat_name)),
                cursor().getInt(column(TBL_CATEGORIES.color)),
                cursor().getShort(column(TBL_CATEGORIES.is_income)) > 0 ?
                Category.Kind.INCOME :
                Category.Kind.EXPENSE
            );
        }

        @Override
        public NoSuchElementException getNoElementException(Object queryContext) {
            return new NoSuchElementException(
                    String.format(Locale.getDefault(), ERR_UNKNOWN_CATEGORY_ID, queryContext));
        }
    };

    static class UncategorizedSumFactory extends ObjectFactory<Pair<Record.Sum, Boolean>> {
        UncategorizedSumFactory() {
            super(TBL_RECORDS.curr_id);
        }

        @Override
        protected Long getObjectId() { return null; }  // It's just a sum, it has no designated id.

        @Override
        public Pair<Record.Sum, Boolean> makeObject() {
            final long currencyId = cursor().getLong(column(TBL_RECORDS.curr_id));
            final String sumStr = cursor().getString(column(TBL_RECORDS.sum));
            Boolean isIncome = null;
            final int incomeColIdx = columnMaybe(TBL_CATEGORIES.is_income);
            if (incomeColIdx >= 0)
                isIncome = cursor().getLong(incomeColIdx) > 0;
            return new Pair<>(new Record.Sum(new BigDecimal(sumStr), currencyId), isIncome);
        }

        @Override
        public Pair<Long, Pair<Record.Sum, Boolean>> makeDefaultObject() {
            return new Pair<>((long) -1, new Pair<>(new Record.Sum(Currency.UNKNOWN_ID), null));
        }
    }

    public static class RecordFactory extends ObjectFactory<Record> {
        public RecordFactory(Wallet wallet, Set<Long> masterRecordIds) {
            this(wallet, true, true, true, masterRecordIds);
        }

        /**
         * @param wallet Wallet instance to use for additional data. May be null if |getSlavesSum|
         *               is |false|.
         * @param requireId Throw if record ID is absent.
         * @param requireSumExpression Throw if sum expression is absent.
         * @param getSlavesSum Whether to query for record's slave records sums.
         * @param masterRecordIds Set of possible record IDs that have slave records. May be null,
         *                        but actual set is a significant performance optimization, that
         *                        prevents excessive database queries.
         */
        RecordFactory(Wallet wallet, boolean requireId, boolean requireSumExpression,
                boolean getSlavesSum, Set<Long> masterRecordIds) {
            super(TBL_RECORDS.rec_id);
            if (getSlavesSum && (wallet == null))
                throw new IllegalArgumentException(
                        "Wallet is required to be able to fetch record slave sums.");
            _reqId = requireId;
            _reqSumExpr = requireSumExpression;
            _masterRecordIds = masterRecordIds;
            _wallet = wallet;
            _getSlavesSum = getSlavesSum;
            _uncatSumFact = new UncategorizedSumFactory();
        }

        @Override
        protected Long getObjectId() {
            return _reqId ? cursor().getLong(column(TBL_RECORDS.rec_id)) : Record.UNKNOWN_ID;
        }

        @Override
        public Record makeObject() {
            final long recId = getObjectId();
            final int catIdColIdx = columnMaybe(TBL_RECORDS.cat_id);
            final long categoryId = catIdColIdx >= 0 ?
                    cursor().getLong(catIdColIdx) : Category.UNKNOWN_ID;
            final long timestamp = cursor().getLong(column(TBL_RECORDS.date_time));
            final String sumExpr = _reqSumExpr ?
                    cursor().getString(column(TBL_RECORDS.sum_expr)) : null;
            final int descrColIdx = columnMaybe(TBL_RECORDS.descr);
            final String description = descrColIdx >= 0 ? cursor().getString(descrColIdx) : null;
            final int templateIdColIdx = column(TBL_RECORDS.tpl_rec_id);
            final long templateId = cursor().isNull(templateIdColIdx) ?
                    Record.UNKNOWN_ID : cursor().getLong(templateIdColIdx);
            final int repIntervalColIdx = columnMaybe(
                    TBL_REPEATING_RECORDS.rec_interval);
            final TimeInterval repInterval = repIntervalColIdx >= 0 ?
                    TimeInterval.fromOrdinal(cursor().getInt(repIntervalColIdx)) : null;

            // Extract record own and slave sums.
            final Record.Sum ownSum = _uncatSumFact.extractObject(cursor()).second.first;
            Pair<Record.Sum, Record.Sum> slavesSums = null;
            if (_getSlavesSum) {
                if (_masterRecordIds == null || _masterRecordIds.contains(recId)) {
                    slavesSums = _wallet.getSlaveRecordsSum(
                            recId, _wallet.getCurrency(ownSum.currencyId));
                }
            }
            Record.Sum effectiveSlavesSum = null;
            if (slavesSums != null) {
                // Either income or expense part should be zero here, as per requirement of
                // Wallet.linkRecords to only link records of opposite kinds.
                // TODO: Anyway only take income or expense part of those.
                effectiveSlavesSum = slavesSums.first.add(slavesSums.second);
            }
            final Record.Sums sums = new Record.Sums(ownSum, effectiveSlavesSum);

            final Record record =
                    new Record(recId, categoryId, timestamp, sums, sumExpr, description);
            record.setTemplateRecId(templateId);
            record.setRepetitionInterval(repInterval);
            return record;
        }

        @Override
        public Pair<Long, Record> makeDefaultObject() {
            return new Pair<>(Record.UNKNOWN_ID, null);
        }

        @Override
        public NoSuchElementException getNoElementException(Object queryContext) {
            return new NoSuchElementException(
                    String.format(Locale.getDefault(), ERR_UNKNOWN_RECORD_ID, queryContext));
        }

        private UncategorizedSumFactory _uncatSumFact;
        private final boolean _reqId;
        private final boolean _reqSumExpr;
        private final Wallet _wallet;
        private final Set<Long> _masterRecordIds;
        private final boolean _getSlavesSum;
    }

    static class CategorySumFactory extends RecordFactory {
        CategorySumFactory() {
            // Require minimal amount of columns.
            super(null, false, false, false, null);
        }
    }

    public static class CurrencyFactory extends ObjectFactory<Currency> {
        public CurrencyFactory() {
            super(TBL_CURRENCIES.curr_id);
        }

        @Override
        public Currency makeObject() {
            return new Currency(
                getObjectId(),
                cursor().getString(column(TBL_CURRENCIES.curr_name)),
                cursor().getString(column(TBL_CURRENCIES.curr_symbol)),
                cursor().getDouble(column(TBL_CURRENCIES.value))
            );
        }

        @Override
        public NoSuchElementException getNoElementException(Object queryContext) {
            return new NoSuchElementException(
                    String.format(Locale.getDefault(), ERR_UNKNOWN_CURRENCY_ID, queryContext));
        }
    }

    // This factory extracts value out of single Cursor column.
    static abstract class ColumnValueFactory<T> extends ObjectFactory<T> {
        ColumnValueFactory(String columnName, T defaultValue) {
            this(-1, defaultValue);
            _columnName = columnName;
        }

        ColumnValueFactory(int columnIndex, T defaultValue) {
            super(null);
            _defaultValue = defaultValue;
            _columnIndex = columnIndex;
        }

        @Override
        public boolean isNull(final Cursor cursor) {
            return cursor.isNull(getColumnIndex());
        }

        @Override
        protected Long getObjectId() { return Long.MIN_VALUE; }  // No real Ids are expected.

        @Override
        public Pair<Long, T> makeDefaultObject() {
            return new Pair<>(Long.MIN_VALUE, _defaultValue);
        }

        protected int getColumnIndex() {
            if (_columnIndex < 0)
                _columnIndex = cursor().getColumnIndexOrThrow(_columnName);
            return _columnIndex;
        }

        final private T _defaultValue;
        private String _columnName = null;
        private int _columnIndex;
    }

    static class LongFactory extends ColumnValueFactory<Long> {
        LongFactory(String columnName, Long defaultValue) {
            super(columnName, defaultValue);
        }

        LongFactory(int columnIndex, Long defaultValue) {
            super(columnIndex, defaultValue);
        }

        @Override
        protected Long makeObject() {
            return cursor().getLong(getColumnIndex());
        }
    }

    static class StringFactory extends ColumnValueFactory<String> {
        StringFactory(String columnName, String defaultValue) {
            super(columnName, defaultValue);
        }

        StringFactory(int columnIndex, String defaultValue) {
            super(columnIndex, defaultValue);
        }

        @Override
        protected String makeObject() {
            return cursor().getString(getColumnIndex());
        }
    }

    static class LinkFactory extends ObjectFactory<Pair<Long, Long>> {
        LinkFactory() {
            super(TBL_LINKS.slave_id);
        }

        @Override
        public Pair<Long, Long> makeObject() {
            final long slaveId = getObjectId();
            final long masterId = cursor().getLong(column(TBL_LINKS.master_id));
            return new Pair<>(masterId, slaveId);
        }
    }

    static class RepetitionInfoFactory extends ObjectFactory<TimeInterval> {
        RepetitionInfoFactory() {
            super(TBL_REPEATING_RECORDS.rec_interval);
        }

        @Override
        protected Long getObjectId() { return cursor().getLong(0); }

        @Override
        public TimeInterval makeObject() {
            final int intervalIndex = cursor().getInt(column(TBL_REPEATING_RECORDS.rec_interval));
            return TimeInterval.fromOrdinal(intervalIndex);
        }

        @Override
        public NoSuchElementException getNoElementException(Object queryContext) {
            return new NoSuchElementException(
                String.format(Locale.getDefault(), ERR_RECORD_NOT_SCHEDULED, queryContext));
        }
    }

    // ACCUMULATORS

    /**
     * Accumulates a series of values of type |T| and turns them into a single value of type |U|.
     * @param <T> input type (multiple values)
     * @param <U> output type (single value)
     */
    public abstract static class ObjectAccumulator<T, U> {
        protected ObjectAccumulator() {
            resetResult();
        }

        public abstract void accumulate(Long id, T obj);
        public abstract U getResult();
        public boolean shouldProceed() { return true; }
        public abstract void resetResult();
    }

    /**
     * Accumulates record sums into two separate values: incomes and expenses, based on record kind.
     */
    static class RecordSummAccumulator extends ObjectAccumulator<Record, Pair<Record.Sum, Record.Sum>> {
        RecordSummAccumulator(long currency, Set<Long> incomeCategories, Set<Long> expenseCategories) {
            _currency = currency;
            _incomeCategories = incomeCategories;
            _expenseCategories = expenseCategories;
            resetResult();
        }

        @Override
        public void accumulate(Long id, Record record) {
            if (_incomeCategories.contains(record.getCatID()))
                _incomes = _incomes.add(record.getEffectiveSum());
            else if (_expenseCategories.contains(record.getCatID()))
                _expenses = _expenses.add(record.getEffectiveSum());
            else
                throw new RuntimeException("Could not classify record category.");
        }

        @Override
        public Pair<Record.Sum, Record.Sum> getResult() {
            return new Pair<>(_incomes, _expenses);
        }

        @Override
        public void resetResult() {
            _incomes = new Record.Sum(_currency);
            _expenses = new Record.Sum(_currency);
        }

        private Record.Sum _incomes;
        private Record.Sum _expenses;
        private final long _currency;
        private final Set<Long> _incomeCategories;
        private final Set<Long> _expenseCategories;
    }

    static class ListAccumulator<T> extends ObjectAccumulator<T, List<T>> {
        ListAccumulator(int maxElements) {
            assert(maxElements >= 0);
            _maxElements = maxElements;
            resetResult();
        }

        @Override
        public void accumulate(Long id, T obj) {
            _list.add(obj);
        }

        @Override
        public boolean shouldProceed() {
            return _maxElements == 0 || _list.size() < _maxElements;
        }

        @Override
        public void resetResult() {
            _list = new ArrayList<>();
        }

        @Override
        public List<T> getResult() {
            return _list;
        }

        private List<T> _list;
        private final long _maxElements;
    }

    static class MapAccumulator<T> extends ObjectAccumulator<T, Map<Long, T>> {
        @Override
        public void accumulate(Long id, T obj) {
            _acc.put(id, obj);
        }

        @Override
        public Map<Long, T> getResult() {
            return _acc;
        }

        @Override
        public void resetResult() {
            _acc = new TreeMap<>();
        }

        private Map<Long, T> _acc;
    }

    static class SetAccumulator<T> extends ObjectAccumulator<T, Set<T>> {
        @Override
        public void accumulate(Long id, T obj) {
            _acc.add(obj);
        }

        @Override
        public Set<T> getResult() {
            return _acc;
        }

        @Override
        public void resetResult() {
            _acc = new TreeSet<>();
        }

        private Set<T> _acc;
    }

    // FUNCTIONS

    // Gets a single object from the Cursor.
    public static <T> T extractObject(Cursor cursor, ObjectFactory<T> factory,
            boolean allowDefaultObject, Object queryContext) {
        final List<T> elementList = accumulateObjects(cursor, factory, new ListAccumulator<>(1));
        final T accResult = !elementList.isEmpty() ? elementList.get(0) : null;
        if (accResult != null)
            return accResult;

        if (allowDefaultObject) {
            final Pair<Long, T> objPair = factory.makeDefaultObject();
            return objPair.second;
        }
        throw factory.getNoElementException(queryContext);
    }

    // Gets a List of objects from the Cursor.
    public static  <T> List<T> enumObjects(Cursor cursor, ObjectFactory<T> factory) {
        return accumulateObjects(cursor, factory, new ListAccumulator<>(0));
    }

    // Gets a List of objects from the Cursor.
    public static  <T> Map<Long, T> mapObjects(Cursor cursor, ObjectFactory<T> factory) {
        return accumulateObjects(cursor, factory, new MapAccumulator<>());
    }

    // Accumulates objects in a cursor.
    public static <T, U> U accumulateObjects(Cursor objCursor, ObjectFactory<T> factory,
            ObjectAccumulator<T, U> acc) {
        if (objCursor == null)
            return acc.getResult();
        try {
            if (objCursor.moveToFirst()) {
                do {
                    final Pair<Long, T> objPair = factory.extractObject(objCursor);
                    acc.accumulate(objPair.first, objPair.second);
                } while (objCursor.moveToNext() && acc.shouldProceed());
            }
        }
        finally {
            objCursor.close();
        }
        return acc.getResult();
    }

    static final String ERR_UNKNOWN_RECORD_ID = "Unknown record ID: %s";
    private static final String ERR_UNKNOWN_CATEGORY_ID = "Unknown category ID: %s";
    private static final String ERR_UNKNOWN_CURRENCY_ID = "Unknown currency ID: %s";
    private static final String ERR_RECORD_NOT_SCHEDULED = "Record %s is not scheduled to repeat";
}

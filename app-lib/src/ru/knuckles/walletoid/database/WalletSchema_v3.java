/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import ru.knuckles.walletoid.BuildConfig;

import static ru.knuckles.walletoid.database.SchemaDecl.ForeignKeyAction.CASCADE;

/**
 * This schema adds links between records, making it possible to express some relation between them.
 * It also adds index by date to records table.
 */
public class WalletSchema_v3 extends WalletSchema_v2 {
    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void setup(Context context, SQLiteDatabase db, int oldVersion) {
        if (oldVersion >= VERSION)
            return;
        super.setup(context, db, oldVersion);

        if (upgrading(oldVersion)) {
            // TODO: Create new indices automatically in WalletDBHelper.
            Utils.createIndex(db, TBL_RECORDS.date_time);
        }
    }

    @Override
    public void onOpen(final SQLiteDatabase db) {
        super.onOpen(db);
        // TODO: Try SQLiteDatabase.setForeignKeyConstraintsEnabled
        db.execSQL("PRAGMA foreign_keys = ON;");  // Required for proper operation.
        if (BuildConfig.DEBUG) {
            SQLiteStatement statement = db.compileStatement("PRAGMA foreign_keys");
            if (statement.simpleQueryForLong() == 0)
                throw new AssertionError("Foreign keys not enabled.");
        }
    }

    public static class RecordsTable extends WalletSchema_v1.RecordsTable {
        // Add index to column.
        @SchemaDecl.Column(index = 1, type = "DATETIME DEFAULT CURRENT_TIMESTAMP")
        @SchemaDecl.Index(name = "rec_date_index")
        public SchemaDecl.ColumnDef date_time;
    }
    public static final RecordsTable TBL_RECORDS = new RecordsTable();

    /**
     * This table holds links between records.
     * If a record is linked with another one, it will have a row in this table.
     * First column holds the dependent ("slave") record ID.
     * Second column holds the the superior ("master") record ID.
     * Only slave-to-master relation is stored here in its explicit form.
     * See https://bitbucket.org/knuckles/walletoid/issue/17/ to get the idea.
     */
    public static final class LinksTable extends SchemaDecl.TableDef {
        LinksTable() {
            super("links");
        }

        @SchemaDecl.Column(index = 0, type = "INTEGER PRIMARY KEY")
        @SchemaDecl.ForeignKey(table = "records", column = "rec_id",
                onDelete = CASCADE, onUpdate = CASCADE)
        public SchemaDecl.ColumnDef slave_id;

        @SchemaDecl.Column(index = 1, type = "INTEGER NOT NULL")
        @SchemaDecl.Index(name = "links_master_index")
        @SchemaDecl.ForeignKey(table = "records", column = "rec_id",
                onDelete = CASCADE, onUpdate = CASCADE)
        public SchemaDecl.ColumnDef master_id;
    }

    public static final LinksTable TBL_LINKS = new LinksTable();

    private static final int VERSION = 3;
}

/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.database.SchemaDecl.*;

import static ru.knuckles.walletoid.database.SchemaDecl.ForeignKeyAction.RESTRICT;

/**
 * This schema just adds INCOME 'misc' category in addition to existing EXPENSE counterpart
 * (see schema v2).
 */
public class WalletSchema_v5 extends WalletSchema_v4 {
    public static final long CAT_ID_MISC_INCOME = CAT_ID_MISC_EXPENSE - 1;

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void populate(Context context, SQLiteDatabase db, int oldVersion,
            boolean allowExtraData) {
        if (oldVersion >= VERSION)
            return;
        super.populate(context, db, oldVersion, allowExtraData);

        // Add 'misc income' category.
        Resources ctxResources = context.getResources();
        SQLiteStatement insStatement = db.compileStatement(
                SqlTemplates.INSERT_INTO(
                        TBL_CATEGORIES.tableName,
                        TBL_CATEGORIES.columns.keySet(),
                        null));

        insStatement.bindLong(1, CAT_ID_MISC_INCOME);
        insStatement.bindLong(2, RAW_INCOME);
        insStatement.bindString(3, ctxResources.getString(R.string.cats_misc_income));
        insStatement.bindLong(4, ctxResources.getColor(R.color.cats_misc_income));
        insStatement.executeInsert();
        // TODO: Handle row ID being already taken by another category (very unlikely, but...).

        // Prevent both "misc" categories from being deleted.
        final ContentValues x = new ContentValues();
        x.put(CAT_CONSTRAINT_TBL.id.name, CAT_ID_MISC_INCOME);
        db.insert(CAT_CONSTRAINT_TBL.tableName, null, x);
        x.clear();
        x.put(CAT_CONSTRAINT_TBL.id.name, CAT_ID_MISC_EXPENSE);
        db.insert(CAT_CONSTRAINT_TBL.tableName, null, x);
    }

    private static final int VERSION = 5;

    // This table holds foreign key references to categories table, preventing some categories from deletion.
    protected static class CategorySonstraintsTable extends TableDef {
        public CategorySonstraintsTable() {
            super("undeletable_categories");
        }

        @Column(index = 0, type = "INTEGER PRIMARY KEY")
        @ForeignKey(table = "categories", column = "cat_id", onDelete = RESTRICT, onUpdate = RESTRICT)
        public ColumnDef id;
    }
    public final static CategorySonstraintsTable CAT_CONSTRAINT_TBL = new CategorySonstraintsTable();
}

/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.database.SchemaDecl.ColumnDef;

import java.util.Map;

import static ru.knuckles.walletoid.database.SchemaDecl.ForeignKeyAction.*;
import static ru.knuckles.walletoid.database.Utils.addNewColumns;

public class WalletSchema_v6 extends WalletSchema_v5 {
    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void setup(Context context, SQLiteDatabase db, int oldVersion) {
        if (oldVersion >= VERSION)
            return;
        super.setup(context, db, oldVersion);

        if (!upgrading(oldVersion))
            return;

        // TODO: Add new columns automatically in WalletDBHelper.
        final Map<String, ColumnDef> newRecordsTableColumns = SchemaDecl.TableDef.collectNewColumns(
                WalletSchema_v5.RecordsTable.class,
                WalletSchema_v6.RecordsTable.class);
        addNewColumns(db, TBL_RECORDS, newRecordsTableColumns.values());
    }

    private static final int VERSION = 6;

    public static class RecordsTable extends WalletSchema_v5.RecordsTable {
        // Template record ID (the one, this was copied from).
        @SchemaDecl.Column(index = 7, type = "INTEGER")
        public ColumnDef tpl_rec_id;
    }

    public static final RecordsTable TBL_RECORDS = new RecordsTable();

    static final class RepeatingRecordsTable extends SchemaDecl.TableDef {
        public RepeatingRecordsTable() {
            super("repeating_records");
        }

        @SchemaDecl.Column(index = 0, type = "INTEGER PRIMARY KEY")
        @SchemaDecl.ForeignKey(table = "records", column = "rec_id", onDelete = CASCADE, onUpdate = CASCADE)
        public ColumnDef rec_id;

        @SchemaDecl.Column(index = 1, type = "INTEGER NOT NULL")
        public ColumnDef rec_interval;

        public ContentValues rowValues(long recId, CalendarUtils.TimeInterval interval) {
            ContentValues vals = new ContentValues();
            vals.put(rec_id.name, recId);
            vals.put(rec_interval.name, interval.ordinal());
            return vals;
        }
    }

    public static final RepeatingRecordsTable TBL_REPEATING_RECORDS = new RepeatingRecordsTable();
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class WalletSchema_v7 extends WalletSchema_v6 {
    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void setup(Context context, SQLiteDatabase db, int oldVersion) {
        if (oldVersion >= VERSION)
            return;
        super.setup(context, db, oldVersion);

        createFTSTable(db);
        if (upgrading(oldVersion)) {
            // Comparing number of rows in two tables to detect a need to reindex is not an
            // option, since they seem to always match.
            reindexRecordDescriptions(db);
        }
    }

    private static void createFTSTable(final SQLiteDatabase db) {
        // FTS4 'content' option is available since SQLite-3.7.9 (API 16+).
        db.execSQL(String.format(
                "CREATE VIRTUAL TABLE %s USING fts4 (content='%s', %s)",
                FTS_TABLE_NAME, TBL_RECORDS.tableName, TBL_RECORDS.descr.name));

        final String DEL_TRIGGER_TPL =
                "CREATE TRIGGER fts_%1$s_before_%3$s BEFORE %3$s ON %1$s BEGIN\n" +
                    "DELETE FROM %2$s WHERE docid=old.rowid;\n" +
                "END;";
        db.execSQL(String.format(DEL_TRIGGER_TPL, TBL_RECORDS.tableName, FTS_TABLE_NAME, "UPDATE"));
        db.execSQL(String.format(DEL_TRIGGER_TPL, TBL_RECORDS.tableName, FTS_TABLE_NAME, "DELETE"));

        final String INS_TRIGGER_TPL =
                "CREATE TRIGGER fts_%1$s_after_%3$s AFTER %3$s ON %1$s BEGIN\n" +
                    "INSERT INTO %2$s(docid, %4$s) VALUES(new.rowid, new.%4$s);\n" +
                "END;";
        db.execSQL(String.format(INS_TRIGGER_TPL, TBL_RECORDS.tableName, FTS_TABLE_NAME,
                "UPDATE", TBL_RECORDS.descr.name));
        db.execSQL(String.format(INS_TRIGGER_TPL, TBL_RECORDS.tableName, FTS_TABLE_NAME,
                "INSERT", TBL_RECORDS.descr.name));
    }

    public static void reindexRecordDescriptions(final SQLiteDatabase db) {
        db.execSQL(String.format("INSERT INTO %1$s(%1$s) VALUES('rebuild')", FTS_TABLE_NAME));
    }

    public final static String FTS_TABLE_NAME = "records_fts";

    private final static int VERSION = 7;
}

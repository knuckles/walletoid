/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import ru.knuckles.walletoid.database.Category.Kind;
import ru.knuckles.walletoid.database.WalletEvents.BulkObserver;
import ru.knuckles.walletoid.database.WalletEvents.CategoryObserver;
import ru.knuckles.walletoid.database.WalletEvents.RecordObserver;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.CancellationException;

import static ru.knuckles.walletoid.database.CursorUtils.accumulateObjects;

/**
 * This class keeps a dictionary of record description words with their counts for each category.
 * Its primary goal is to guess potential category based on description. Each time you create or
 * modify a record, this dictionary learns on words from its description and the category you
 * specified for that record.
 * NOTE: Upon first instantiation wallet is analyzed in the background, hence dictionary will not
 * guess anything, until it's all done.
 */
public class RecordDictionary {
    public interface Observer {
        /**
         * Despite the name, it may be called with false, even though
         * {@link RecordDictionary#ready()} was already false. That should happen in the case, when
         * dictionary load task failed.
         */
        void onReadyStateChanged(boolean ready);
    }

    RecordDictionary(Wallet wallet, File storageFile) {
        if (wallet == null || storageFile == null)
            throw new AssertionError();
        _wallet = wallet;
        _storageFile = storageFile;
        _readTask = new DictReadTask(storageFile, _fileReadHandler).execute();
    }

    public void close() {
        ignore();
        _wallet = null;
        if (_readTask != null)
            _readTask.cancel(false);
        if (_rebuildTask != null)
            _rebuildTask.cancel(false);
        if (_dirty)
            persist();
        // TODO: Some async IO maybe?
    }

    public boolean ready() {
        return _incomeRecWordCounts != null && _expenseRecWordCounts != null;
    }

    /**
     * Start listening to dictionary events.
     * @param observer weak referenced observer. When you need to stop receiving notifications,
     *                 simply clear the reference (in that case you'll want to keep it somewhere).
     */
    public void addObserver(WeakReference<Observer> observer) {
        _observers.add(observer);
    }

    public long guessCategory(Kind catKind, String description) {
        if (!ready())
            return Category.UNKNOWN_ID;

        final Map<Long, Float> catProbs = new TreeMap<>();
        final String[] words = description.split(WORD_SEPARATORS);
        for (String word : words) {
            word = word.trim().toLowerCase();
            if (word.length() < MIN_WORD_LEN)
                continue;
            final Map<Long, Float> catProb = categorizeWord(catKind, word);
            if (catProb == null)
                continue;

            for (Map.Entry<Long, Float> e : catProb.entrySet()) {
                float currentProb = catProbs.containsKey(e.getKey()) ? catProbs.get(e.getKey()) : 0;
                catProbs.put(e.getKey(), currentProb + e.getValue());
            }
        }

        long probableCatId = Category.UNKNOWN_ID;
        float biggestProb = 0;
        for (Map.Entry<Long, Float> e : catProbs.entrySet()) {
            if (e.getValue() > biggestProb) {
                biggestProb = e.getValue();
                probableCatId = e.getKey();
            }
        }

        return probableCatId;
    }

    // PRIVATE

    private void observe() {
        _wallet.events.addRecordObserver(_recObserver);
        _wallet.events.addCategoryObserver(_catObserver);
        _wallet.events.addBulkObserver(_blkObserver);
    }

    private void ignore() {
        _wallet.events.removeRecordObserver(_recObserver);
        _wallet.events.removeCategoryObserver(_catObserver);
        _wallet.events.removeBulkObserver(_blkObserver);
    }

    // WalletEvents observers:

    private final RecordObserver _recObserver = new RecordObserver() {
        @Override
        public void onRecordAdded(long id) {
            addRecordWords(_wallet.getRecord(id));
            _dirty = true;
        }

        @Override
        public void onRecordUpdated(Record oldRecord) {
            final Record newRecord = _wallet.getRecord(oldRecord.getId());
            if (oldRecord.getCatID() == newRecord.getCatID() &&
                oldRecord.getDescr().equals(newRecord.getDescr()))
                return;

            removeRecordWords(oldRecord);
            addRecordWords(newRecord);
            _dirty = true;
        }

        @Override
        public void onRecordRemoved(Record oldRecord) {
            removeRecordWords(oldRecord);
            _dirty = true;
        }
    };

    private final CategoryObserver _catObserver = new CategoryObserver() {
        @Override
        public void onCategoryRemoved(long removedCatId, long acceptorCatId) {
            Map<Integer, Map<Long, Long>> dict =
                    dictForCategoryKind(_wallet.getCategory(removedCatId).getKind());
            // All description words should now belong to the new category.
            // Just move them in our dictionary as well.
            for (Map<Long, Long> wordScores : dict.values()) {
                if (!wordScores.containsKey(removedCatId))
                    continue;
                long acceptorCatWordCount = wordScores.get(removedCatId);
                wordScores.remove(removedCatId);
                if (wordScores.containsKey(acceptorCatId))
                    acceptorCatWordCount += wordScores.get(acceptorCatId);
                wordScores.put(acceptorCatId, acceptorCatWordCount);
            }
            _dirty = true;
        }
    };

    private final BulkObserver _blkObserver = new BulkObserver() {
        @Override
        public void onCopyCompleted() {
            rebuildFromWallet();
        }

        @Override
        public void onBulkCategorySet(List<Record> removedRecords, long newCatId) {
            if (removedRecords == null) {
                rebuildFromWallet();
                return;
            }
            final Category.Kind catKind = _wallet.getCategory(newCatId).getKind();
            final Map<Integer, Map<Long, Long>> newDict = dictForCategoryKind(catKind);
            for (Record oldRecord : removedRecords) {
                removeRecordWords(oldRecord);
                addWords(newCatId, oldRecord.getDescr(), newDict);
            }
            _dirty = true;
        }

        @Override
        public void onBulkRecordsRemoval(List<Record> removedRecords) {
            if (removedRecords == null) {
                rebuildFromWallet();
                return;
            }
            for (Record record : removedRecords) {
                removeRecordWords(record);
            }
            _dirty = true;
        }
    };

    private Map<Integer, Map<Long, Long>> dictForRecord(Record record) {
        return dictForCategoryKind(_wallet.getCategory(record.getCatID()).getKind());
    }

    private Map<Integer, Map<Long, Long>> dictForCategoryKind(Kind catKind) {
        return catKind == Kind.INCOME ? _incomeRecWordCounts : _expenseRecWordCounts;
    }

    private Map<Long, Float> categorizeWord(Kind catKind, String word) {
        Map<Integer, Map<Long, Long>> dict = dictForCategoryKind(catKind);
        Map<Long, Long> catCounts = dict.get(word.hashCode());
        if (catCounts == null)
            return null;
        long totalOccurrences = 0;
        for (Long count : catCounts.values()) {
            totalOccurrences += count;
        }
        Map<Long, Float> result = new TreeMap<>();
        for (Map.Entry<Long, Long> catCount : catCounts.entrySet()) {
            result.put(catCount.getKey(), (float) catCount.getValue() / (float) totalOccurrences);
        }
        return result;
    }

    public void rebuildFromWallet() {
        ignore();  // FIXME: Some events may be lost.
        _incomeRecWordCounts = null;
        _expenseRecWordCounts = null;
        _dirty = true;
        _rebuildTask = new DictBuilderTask(_wallet, _rebuildHandler).execute();
        notifyObservers();
    }

    private void addRecordWords(Record record) {
        addWords(record.getCatID(), record.getDescr(), dictForRecord(record));
    }

    private void removeRecordWords(Record record) {
        removeWords(record.getCatID(), record.getDescr(), dictForRecord(record));
    }

    private static void addWords(long catId, String description,
            Map<Integer, Map<Long, Long>> dict) {
        for (String word : description.split(WORD_SEPARATORS)) {
            word = word.trim().toLowerCase();
            if (word.length() < MIN_WORD_LEN)
                continue;
            final int wordKey = word.hashCode();
            Map<Long, Long> wordCounters = dict.get(wordKey);
            if (wordCounters == null) {
                // Start counting this word for category.
                wordCounters = new TreeMap<>();
                wordCounters.put(catId, (long) 1);
                dict.put(wordKey, wordCounters);
                continue;
            }
            // Increment word count for category.
            long wordCount = wordCounters.containsKey(catId) ? wordCounters.get(catId) + 1 : 1;
            wordCounters.put(catId, wordCount);
        }
    }

    private static void removeWords(long catId, String description,
            Map<Integer, Map<Long, Long>> dict) {
        for (String word : description.split(WORD_SEPARATORS)) {
            word = word.trim().toLowerCase();
            if (word.length() < MIN_WORD_LEN)
                continue;
            final int wordKey = word.hashCode();
            Map<Long, Long> wordCounters = dict.get(wordKey);
            if (wordCounters == null || !wordCounters.containsKey(catId))
                continue;  // Strange situation, but can't do any better anyway.
            // Decrement word count for category.
            final long wordCount = wordCounters.get(catId) - 1;
            if (wordCount > 0) {
                wordCounters.put(catId, wordCount);
            }
            else {
                wordCounters.remove(catId);
                if (wordCounters.size() == 0)
                    dict.remove(wordKey);
            }
        }
    }

    private final static class WordCount extends TreeMap<Integer, Map<Long, Long>> {}

    private interface DataReadyHandler {
        void onDone(WordCount incomeRecsDict, WordCount expenseRecsDict);
    }

    private static abstract class DictLoadTask
            extends AsyncTask<Void, Integer, Pair<WordCount, WordCount>> {
        DictLoadTask(DataReadyHandler cb) {
            _cb = cb;
        }

        @Override
        protected void onPreExecute() {
            _taskStarted = System.currentTimeMillis();
            Log.i(LOG_KEY, String.format("Starting %s...", getClass().getSimpleName()));
        }

        @Override
        protected void onPostExecute(Pair<WordCount, WordCount> dicts) {
            final WordCount incomeRecsDict = dicts != null ? dicts.first : null;
            final WordCount expenseRecsDict = dicts != null ? dicts.second : null;

            final float elapsedS = (float) (System.currentTimeMillis() - _taskStarted) / 1000f;
            final long incomeWords = incomeRecsDict != null ? incomeRecsDict.size() : 0;
            final long expenseWords = expenseRecsDict != null ? expenseRecsDict.size() : 0;
            Log.i(LOG_KEY, String.format(
                    "Task %s took %.2fs to finish. Got %d (i) + %d (e) distinct words.",
                    getClass().getSimpleName(), elapsedS, incomeWords, expenseWords));

            if (_cb != null) {
                _cb.onDone(incomeRecsDict, expenseRecsDict);
            }
        }

        @Override
        protected void onCancelled() {
            final float elapsedS = (float) (System.currentTimeMillis() - _taskStarted) / 1000f;
            Log.i(LOG_KEY, String.format(
                    "Task %s was cancelled after %.2fs of execution.",
                    getClass().getSimpleName(), elapsedS));
            if (_cb != null) {
                _cb.onDone(null, null);
            }
        }

        private final DataReadyHandler _cb;
        private long _taskStarted = 0;  // Timestamp in millis, when the background task started.
    }

    private static class DictReadTask extends DictLoadTask {
        DictReadTask(File storageFile, DataReadyHandler cb) {
            super(cb);
            _storageFile = storageFile;
        }

        @Override
        protected Pair<WordCount, WordCount> doInBackground(Void... params) {
            if (!_storageFile.exists())
                return null;

            try {
                InputStream fis = new FileInputStream(_storageFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                final WordCount incomeRecsDict = (WordCount) ois.readObject();
                final WordCount expenseRecsDict = (WordCount) ois.readObject();
                return new Pair<>(incomeRecsDict, expenseRecsDict);
            } catch (Exception e) {
                Log.e(LOG_KEY, "Failed deserializing the dictionary. " + e.toString());
            }
            return null;
        }

        private final File _storageFile;
    }

    private static class DictBuilderTask extends DictLoadTask {
        DictBuilderTask(Wallet wallet, DataReadyHandler cb) {
            super(cb);
            assert wallet != null;
            _wallet = wallet;
        }

        @Override
        protected Pair<WordCount, WordCount> doInBackground(Void... params) {
            try {
                return accumulateObjects(
                        // TODO: Limit records to by recency or count.
                        _wallet.getRecordsCursor(null, null, null),
                        new CursorUtils.RecordFactory(_wallet, null),
                        new RecordDescriptionsAccumulator()
                );
            }
            catch (CancellationException e) {
                return null;
            }
        }

        private final class RecordDescriptionsAccumulator
                extends CursorUtils.ObjectAccumulator<Record, Pair<WordCount, WordCount>> {
            RecordDescriptionsAccumulator() {
                resetResult();
            }

            @Override
            public void accumulate(final Long id, final Record record) {
                if (isCancelled())
                    throw new CancellationException();
                Kind recKind = _wallet.getCategory(record.getCatID()).getKind();
                addWords(record.getCatID(), record.getDescr(),
                        recKind == Kind.INCOME ? _incomeRecsDict : _expenseRecsDict
                );
                _recProcessed++;
                publishProgress(_recProcessed);
            }

            @Override
            public Pair<WordCount, WordCount> getResult() { return _result; }

            @Override
            public void resetResult() {
                _incomeRecsDict = new WordCount();
                _expenseRecsDict = new WordCount();
                _result = new Pair<>(_incomeRecsDict, _expenseRecsDict);
                _recProcessed = 0;
            }

            private int _recProcessed;
            private WordCount _incomeRecsDict;
            private WordCount _expenseRecsDict;
            private Pair<WordCount, WordCount> _result;
        }

        private final Wallet _wallet;
    }

    private void set(WordCount incomeRecsDict, WordCount expenseRecsDict) {
        _incomeRecWordCounts = incomeRecsDict;
        _expenseRecWordCounts = expenseRecsDict;
        observe();
        notifyObservers();
    }

    private void notifyObservers() {
        for (Iterator<WeakReference<Observer>> iterator = _observers.iterator();
             iterator.hasNext(); ) {
            final Observer o = iterator.next().get();
            if (o == null) {
                iterator.remove();
            }
            else {
                try {
                    o.onReadyStateChanged(ready());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final DataReadyHandler _fileReadHandler = new DataReadyHandler() {
        @Override
        public void onDone(WordCount incomeRecsDict, WordCount expenseRecsDict) {
            _readTask = null;
            if (_wallet == null)
                return;  // Already closed.

            if (incomeRecsDict != null && expenseRecsDict != null)
                set(incomeRecsDict, expenseRecsDict);
            else
                rebuildFromWallet();
        }
    };

    private final DataReadyHandler _rebuildHandler = new DataReadyHandler() {
        @Override
        public void onDone(WordCount incomeRecsDict, WordCount expenseRecsDict) {
            _rebuildTask = null;
            if (_wallet == null)
                return;  // Already closed.

            set(incomeRecsDict, expenseRecsDict);
            persist();
        }
    };

    private void persist() {
        if (!ready())
            return;

        try {
            OutputStream fos = new FileOutputStream(_storageFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(_incomeRecWordCounts);
            oos.writeObject(_expenseRecWordCounts);
            oos.close();
            fos.close();
            _dirty = false;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(LOG_KEY, String.format("Serialized dict took %d bytes.", _storageFile.length()));
    }

    private static final String LOG_KEY = RecordDictionary.class.getSimpleName();
    private static final int MIN_WORD_LEN = 3;
    private static final String WORD_SEPARATORS = "\\W";

    private Wallet _wallet;
    private final File _storageFile;

    private AsyncTask<Void, Integer, Pair<WordCount, WordCount>> _readTask = null;
    private AsyncTask<Void, Integer, Pair<WordCount, WordCount>> _rebuildTask = null;

    // Main data. Mapping meaning: { word hash -> { category id -> word counter } }.
    private WordCount _incomeRecWordCounts;
    private WordCount _expenseRecWordCounts;
    private boolean _dirty = false;  // Data has changed and must be eventually persisted.

    Set<WeakReference<Observer>> _observers = new TreeSet<>(
            new Comparator<WeakReference<Observer>>() {
        @Override
        public int compare(WeakReference<Observer> o1, WeakReference<Observer> o2) {
            final int o1hash = o1.get() == null ? 0 : o1.get().hashCode();
            final int o2hash = o2.get() == null ? 0 : o2.get().hashCode();
            return o1hash - o2hash;
        }
    });
}

/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.util.Pair;
import ru.knuckles.walletoid.CalendarUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;

import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_RECORDS;

public class Record implements Cloneable {
    // Must guarantee immutability!
    public static class Sum {
        public Sum(long currencyId) {
            this(BigDecimal.ZERO, currencyId);
        }

        public Sum(BigDecimal amount, long currencyId) {
            if (amount == null || currencyId < UNKNOWN_ID) throw new AssertionError();
            if (amount.compareTo(BigDecimal.ZERO) < 0) throw new IllegalArgumentException(SUM_MUST_BE_POSITIVE);
            this.amount = amount;
            this.currencyId = currencyId;
        }

        public Sum(double amount, long currencyId) {
            this(new BigDecimal(amount), currencyId);
        }

        @Override
        public String toString() {
            return String.format(Locale.ROOT, "%s (%d)", amount, currencyId);
        }

        @Override
        public boolean equals(Object other) {
            return
                this == other ||
                other != null &&
                this.getClass().isAssignableFrom(other.getClass()) &&
                this.equals((Sum) other);
        }

        public boolean equals(Sum other) {
            return currencyId == other.currencyId && amountEquals(other);
        }

        public boolean amountEquals(Sum other) {
            return other != null && amount.equals(other.amount);
        }

        public final BigDecimal amount;
        public final long currencyId;

        public Sum add(Sum other) {
            boolean blank = amount.equals(BigDecimal.ZERO) && currencyId == Currency.UNKNOWN_ID;
            if (!blank && currencyId != other.currencyId)
                throw new IllegalArgumentException(
                        String.format(Locale.getDefault(), ERR_CURRENCY_MISMATCH,
                                currencyId, other.currencyId));
            return new Sum(amount.add(other.amount), other.currencyId);
        }

        static final String ERR_CURRENCY_MISMATCH = "Currency mismatch: %d vs %d";
    }

    // Must guarantee immutability!
    public static class Sums extends Pair<Sum, Sum> implements Cloneable {
        public Sums(Sum own) {
            this(own, null);
        }

        public Sums(Sum own, Sum slaves) {
            super(own, (slaves != null) ? slaves : new Sum(BigDecimal.ZERO, own.currencyId));
            assert own != null;
            if (own().currencyId != slaves().currencyId)
                throw new AssertionError(
                        String.format(Locale.ROOT, Sum.ERR_CURRENCY_MISMATCH,
                                own().currencyId, slaves().currencyId));
        }

        public Sum own() { return first; }

        public Sum slaves() { return second; }

        public Sum getEffectiveSum() {
            return new Sum(first.amount.add(second.amount), first.currencyId);
        }

        public boolean numericallyEquals(final Sums others) {
            return this == others ||
                    (others != null &&
                     first.amountEquals(others.first) &&
                     (second == others.second ||
                      second != null && second.amountEquals(others.second)));
        }
    }

    // Use this ID for new records if they're not yet inserted to a database.
    public final static long UNKNOWN_ID = -1;

    public Record(long timestamp, Sum sum) {
        this(Category.UNKNOWN_ID, timestamp, sum, null);
    }

    public Record(long catID, long timestamp, Sum sum) {
        this(catID, timestamp, sum, null);
    }

    public Record(long catID, long timestamp, Sum sum, String descr) {
        this(UNKNOWN_ID, catID, timestamp, sum, descr);
    }

    public Record(long catID, long timestamp, BigDecimal amount, long currencyId, String descr) {
        this(catID, timestamp, new Sum(amount, currencyId), descr);
    }

    public Record(long ID, long catID, long timestamp, BigDecimal amount, long currencyId,
            String descr) {
        this(ID, catID, timestamp, new Sum(amount, currencyId), descr);
    }

    public Record(long ID, long catID, long timestamp, Sum sum, String descr) {
        this(ID, catID, timestamp, new Sums(sum), null, descr);
    }

    // FIXME: Review usage
    public Record(Record record, long id) {
        this(id, record._catID, record._timestamp, record._sums, record._sumExpr,
                record._descr);
    }

    public Record(long ID, long catID, long timestamp, Sums sums, String sumExpression,
            String descr) {
        if (ID < UNKNOWN_ID) throw new AssertionError();
        _id = ID;
        if (catID < Category.UNKNOWN_ID) throw new AssertionError();
        _catID = catID;
        _timestamp = timestamp;
        assert sums != null;
        _sums = sums;
        _sumExpr = sumExpression != null && !sumExpression.isEmpty() ? sumExpression : "";
        _descr = descr != null ? descr : "";
    }

    public Record makeRepeated(long timestamp) {
        final Record result = new Record(this, UNKNOWN_ID);
        result.setTemplateRecId(_id);
        if (timestamp > 0)
            result.setTimestamp(timestamp);
        return result;
    }

    @Override
    public boolean equals(Object other) {
        return
            this == other ||
            other != null &&
            this.getClass().isAssignableFrom(other.getClass()) &&
            this.equals((Record) other);
    }

    public boolean equals(Record other) {
        return (_id == other._id) && similarTo(other);
    }

    // Checks whether the other record is identical to another one except its ID.
    public boolean similarTo(Record other) {
        return visAttributesMatch(other) &&
                _catID == other._catID &&
                _templateRecId == other._templateRecId;
    }

    public boolean visAttributesMatch(Record other) {
        return this == other || other != null &&
                _timestamp == other._timestamp &&
                // TODO: This is very wrong as it just ignores currencies.
                _sums.numericallyEquals(other._sums) &&
                _descr.equals(other._descr) &&
                _sumExpr.equals(other._sumExpr);
    }

    public String toString() {
        return String.format(Locale.ROOT,
                "%d/%d %s \"%s\"",
                _id, _catID, _sums.own(), _descr);
    }

    ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(TBL_RECORDS.date_time.name, _timestamp);
        values.put(TBL_RECORDS.sum.name, _sums.own().amount.toString());
        values.put(TBL_RECORDS.curr_id.name, _sums.own().currencyId);
        values.put(TBL_RECORDS.cat_id.name, _catID);
        values.put(TBL_RECORDS.descr.name, _descr);
        if (!_sumExpr.isEmpty())
            values.put(TBL_RECORDS.sum_expr.name, _sumExpr);
        if (_templateRecId > UNKNOWN_ID)
            values.put(TBL_RECORDS.tpl_rec_id.name, _templateRecId);

        return values;
    }

    public long getId() {
        return _id;
    }

    public long getCatID() {
        return _catID;
    }

    public long setCatID(long id) {
        return _catID = id;
    }

    public long getCurrencyID() {
        return _sums.own().currencyId;
    }

    public long getTimestamp() {
        return _timestamp;
    }

    public long setTimestamp(long newTimestamp) {
        return _timestamp = newTimestamp;
    }

    /**
     * @return Calendar instances initialized with the value of timestamp.
     * NB: The returned value should be considered read-only. Modifications will not be propagated to the Record.
     */
    public Calendar getCalendar() {
        return CalendarUtils.Cal(_timestamp);
    }

    public Sum getSum() {
        return _sums.own();
    }

    public Sum getSlavesSum() {
        return _sums.slaves();
    }

    public Sum getEffectiveSum() {
        return _sums.getEffectiveSum();
    }

    public Sum setSum(Sum sum) {
        _sums = new Sums(sum, new Sum(_sums.slaves().amount, sum.currencyId));
        return sum;
    }

    public String getSumExpression() {
        return _sumExpr;
    }

    public String setSumExpression(String expr) {
        return _sumExpr = (expr != null && !expr.isEmpty()) ? expr : "";
    }

    public String getDescr() {
        return _descr;
    }
    public String setDescr(String descr) {
        return _descr = descr;
    }

    public long getTemplateRecId() { return _templateRecId; }
    public void setTemplateRecId(long _templateRecId) { this._templateRecId = _templateRecId; }

    // NOTE: This property is NOT directly persisted in the database.
    public CalendarUtils.TimeInterval getRepetitionInterval() { return _repetitionInterval; }
    public void setRepetitionInterval(final CalendarUtils.TimeInterval repetitionInterval) {
        _repetitionInterval = repetitionInterval;
    }

    private final long _id;
    private long _catID;
    private long _timestamp = 0;
    private Sums _sums;
    private String _descr;
    private String _sumExpr;
    private long _templateRecId = UNKNOWN_ID;
    private CalendarUtils.TimeInterval _repetitionInterval;

    private static final String SUM_MUST_BE_POSITIVE = "Sum must be positive.";
}

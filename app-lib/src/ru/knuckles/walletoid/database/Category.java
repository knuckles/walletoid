/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.support.annotation.ColorInt;

import java.util.Locale;

import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_CATEGORIES;

public class Category {
    public enum Kind {
        ALL,
        INCOME,
        EXPENSE;

        public static Kind fromOrdinal(int ord) {
            return values()[ord];
        }

        public boolean isUndefined() { return this == ALL; }
        public boolean isIncome() { return this == INCOME; }
        public boolean isExpense() { return this == EXPENSE; }

        public  Kind invert() {
            if (isUndefined())
                return ALL;
            return isIncome() ? EXPENSE : INCOME;
        }
    }

    // Use this ID for querying all categories or for new categories which are not yet in the DB.
    public final static long UNKNOWN_ID = -1;

    public Category(@ColorInt int color, Kind kind) {
        this(UNKNOWN_ID, "", color, kind);
    }

    public Category(String name, @ColorInt int color, Kind kind) {
        this(UNKNOWN_ID, name, color, kind);
    }

    public Category(long id, String name, @ColorInt int color, Kind kind) {
        _id = id;
        _name = name;
        _color = color;
        _kind = kind;
    }

    public Category(Category from, long id) {
        this(id, from._name, from._color, from._kind);
    }

    public long getId() { return _id; }
    public Kind getKind() { return _kind; }
    public String getName() {return _name;}
    @ColorInt public int getColor() {return _color;}

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(TBL_CATEGORIES.cat_name.name, _name);
        values.put(TBL_CATEGORIES.is_income.name, _kind == Kind.INCOME);
        values.put(TBL_CATEGORIES.color.name, _color);
        return values;
    }

    @Override
    public String toString() {
        return String.format(Locale.ROOT, "%s (%d): '%s'", getClass().getSimpleName(), _id, _name);
    }

    @Override
    public boolean equals(Object other) {
        return
            this == other ||
            other != null &&
            this.getClass().isAssignableFrom(other.getClass()) &&
            this.equals((Category) other);
    }

    public boolean equals(Category other) {
        return similarTo(other) &&
                _id == other._id;
    }

    public boolean similarTo(Category other) {
        return
                this == other ||
                other != null &&
                _name.equals(other._name) &&
                _color == other._color &&
                _kind == other._kind;
    }

    // PRIVATE:

    private final long _id;
    private final String _name;
    @ColorInt private final int _color;
    private final Kind _kind;
}

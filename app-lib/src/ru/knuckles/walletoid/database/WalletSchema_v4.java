/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import ru.knuckles.walletoid.database.SchemaDecl.*;

import static ru.knuckles.walletoid.database.SchemaDecl.ForeignKeyAction.CASCADE;
import static ru.knuckles.walletoid.database.SchemaDecl.ForeignKeyAction.RESTRICT;
import static ru.knuckles.walletoid.database.Utils.recreateTable;

/**
 * This schema's major feature is the introduction of currencies.
 * It also adds a table for auxiliary data in a form of key-value pairs.
 */
public class WalletSchema_v4 extends WalletSchema_v3 {
    @Override
    public int getVersion() {
        return VERSION;
    }

    public static final long DEF_CURRENCY_ID = 1;
    /**
     * This table holds description of available currencies.
     */
    public static class CurrenciesTable extends TableDef {
        CurrenciesTable() {
            super("currencies");
        }

        @Column(index = 0, type = "INTEGER PRIMARY KEY")
        public ColumnDef curr_id;

        @Column(index = 1, type = "CHARACTER NOT NULL")
        public ColumnDef curr_name;

        @Column(index = 2, type = "CHARACTER NOT NULL")
        public ColumnDef curr_symbol;

        @Column(index = 3, type = "DECIMAL(20, 4) NOT NULL")
        public ColumnDef value;
    }
    public static final CurrenciesTable TBL_CURRENCIES = new CurrenciesTable();

    /**
     * Redefine Records table. See v1 definition for details.
     */
    public static class RecordsTable extends WalletSchema_v3.RecordsTable {
        // Shadow previously declared column to add foreign key.
        @Column(index = 2, type = "INTEGER NOT NULL")
        @ForeignKey(table = "categories", column = "cat_id", onDelete = RESTRICT, onUpdate = CASCADE)
        public ColumnDef cat_id;

        @Column(index = 5, type = "CHARACTER")
        public ColumnDef sum_expr;

        // TODO: Need checked reference to default currency id value.
        @Column(index = 6, type = "INTEGER NOT NULL DEFAULT 1" /* DEF_CURRENCY_ID */ )
        @ForeignKey(table = "currencies", column = "curr_id", onDelete = RESTRICT, onUpdate = CASCADE)
        public ColumnDef curr_id;
    }
    public static final RecordsTable TBL_RECORDS = new RecordsTable();

    /**
     * Table holding auxiliary data in the form of name-value records.
     */
    static final class AuxTable extends TableDef {
        AuxTable() {
            super("aux");
        }

        @Column(index = 0, name="name", type = "CHARACTER UNIQUE NOT NULL")
        @Index(name = "aux_name_index")
        public ColumnDef name_;

        @Column(index = 1, type = "INTEGER")
        public ColumnDef value;

        public static String PREF_REFERENCE_CURRENCY = "currencies.reference.id";
    }
    public static final AuxTable TBL_AUX = new AuxTable();

    @Override
    public void setup(Context context, SQLiteDatabase db, int oldVersion) {
        if (oldVersion >= VERSION)
            return;
        super.setup(context, db, oldVersion);

        if (upgrading(oldVersion)) {
            // Recreate records table, as it needs new constraint, and SQLite can't add one.
            recreateTable(db, WalletSchema_v1.TBL_RECORDS, TBL_RECORDS);
        }
    }

    @Override
    public void populate(final Context context, final SQLiteDatabase db, final int oldVersion,
            final boolean allowExtraData) {
        if (oldVersion >= VERSION)
            return;
        super.populate(context, db, oldVersion, allowExtraData);
        insertDefaultCurrency(db);
    }

    private void insertDefaultCurrency(final SQLiteDatabase db) {
        // TODO: insert based on user country.
        // See: http://stackoverflow.com/questions/3659809/where-am-i-get-country
        final SQLiteStatement insCurrStatement = db.compileStatement(
            SqlTemplates.INSERT_INTO(
                TBL_CURRENCIES.tableName,
                TBL_CURRENCIES.columns.keySet(),
                null));
        insCurrStatement.bindLong(1, DEF_CURRENCY_ID);
        insCurrStatement.bindString(2, "USD");
        insCurrStatement.bindString(3, "$");
        insCurrStatement.bindDouble(4, 1.0);
        final long defaultCurrencyId = insCurrStatement.executeInsert();
        if (defaultCurrencyId == -1)
            throw new AssertionError();

        // Remember default currency.
        final ContentValues values = new ContentValues(1);
        values.put(TBL_AUX.name_.name, AuxTable.PREF_REFERENCE_CURRENCY);
        values.put(TBL_AUX.value.name, defaultCurrencyId);
        db.replaceOrThrow(TBL_AUX.tableName, null, values);
    }

    private static final int VERSION = 4;
}

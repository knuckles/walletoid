/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.util.Pair;
import ru.knuckles.walletoid.CalendarUtils;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.RepeatedRecordsScheduler;
import ru.knuckles.walletoid.database.CursorUtils.*;

import java.util.*;

import static ru.knuckles.walletoid.Utils.joinStrings;
import static ru.knuckles.walletoid.database.CursorUtils.*;
import static ru.knuckles.walletoid.database.SqlTemplates.*;
import static ru.knuckles.walletoid.database.Utils.mergeTables;
import static ru.knuckles.walletoid.database.WalletSchema_v7.*;

public class Wallet {

    Wallet(Context context, String name, SQLiteDatabase db) {
        if (context == null || name.isEmpty() || db == null)
            throw new IllegalArgumentException();

        _context = context;
        _name = name;
        _logLabel = getClass().getSimpleName() + "-" + name;
        _dbr = db;

        _supportCTE = Utils.dbSupportsCTE(db);
        _recCountStmt = _dbr.compileStatement("SELECT count() FROM " + TBL_RECORDS.tableName);
        _lastChangesCountStmt = _dbr.compileStatement("SELECT changes();");

        events = new WalletEvents();
    }

    void close() {
        if (_dbr != null)
            _dbr.close();
        _dbr = null;
        _lastChangesCountStmt = null;
        events.clear();
    }

    public String getName() {
        return _name;
    }

    public final WalletEvents events;

    /**
     * Copies data to this Wallet. THIS WILL MOST LIKELY MODIFY THE SOURCE DATABASE!
     * As the destination DB in a general case will have different foreign key IDs, we need to
     * update them while copying. Previously we processed records in hunks, selecting by category
     * and inserting them with updated category ID. Now, after introducing second foreign key
     * column - currency ID, it became impossible.
     * Solution is to modify source DB so that its IDs won't interfere with existing ones:
     * 1. Increment new source DB categories IDs up by this DB largest category ID value. Remap
     * existing categories IDs based on categoryMapping. Records should update foreign keys
     * automatically. Copy new categories with their new IDs.
     * 2. Do the same with currencies.
     * 3. Increment source DB records IDs by this DB largest record ID value (links should update
     * automatically). Copy records with their new IDs.
     * 4. Copy record links.
     * This process is transactional.
     *
     * NOTE: Record repetition data is NOT copied, since it's considered to be rather a
     * preference than real data.
     *
     * @param otherWallet     is the source of data to copy.
     * @param categoryMapping defines, how categories in otherWallet will be handled.
     *                        Each key is a category ID in otherWallet. Each value is a category
     *                        ID in this wallet. Records from source
     *                        wallet will be copied with category IDs updated to their
     *                        corresponding values in this mapping.
     *                        Only categories NOT PRESENT as keys in this mapping will be added.
     *                        Upon this process their IDs will,
     *                        most likely, change because old IDs may be used by existing
     *                        categories here. Again, records are updated
     *                        automatically.
     * @param currencyMapping defines, how currencies on otherWallet will be handled. Similar to
     *                        categoryMapping param.
     */
    public void copyFrom(Wallet otherWallet, Map<Long, Long> categoryMapping,
            Map<Long, Long> currencyMapping) {
        _dbr.execSQL(
            "ATTACH DATABASE ? AS ?",
            new Object[] {
                otherWallet._dbr.getPath(),
                ATTACHED_DB_NAME
            });
        try {
            _dbr.beginTransaction();
            try {
                copyCategories(ATTACHED_DB_NAME, categoryMapping);
                copyCurrencies(ATTACHED_DB_NAME, currencyMapping);
                copyRecords(ATTACHED_DB_NAME);
                copyLinks(ATTACHED_DB_NAME);
                _dbr.setTransactionSuccessful();
            }
            finally {
                _dbr.endTransaction();
            }
        }
        finally {
            _dbr.execSQL("DETACH DATABASE ?", new Object[] { ATTACHED_DB_NAME });
        }

        events.copyNotifier.notifyObservers(null, null);
    }

    private void copyCategories(String attachedDBName, Map<Long, Long> categoryMapping) {
        // FIXME: Consider correctly mapping undeletable categories.
        mergeTables(_dbr, TBL_CATEGORIES, attachedDBName, TBL_CATEGORIES.cat_id.name,
                categoryMapping);
        _catsCache = null;
    }

    private void copyCurrencies(String attachedDBName, Map<Long, Long> currencyMapping) {
        mergeTables(_dbr, TBL_CURRENCIES, attachedDBName, TBL_CURRENCIES.curr_id.name,
                currencyMapping);
        _currsCache = null;
    }

    private void copyRecords(String attachedDbName) {
        final Collection<String> columnList = new ArrayList<String>() {{
            add(TBL_RECORDS.date_time.name);
            add(TBL_RECORDS.cat_id.name);
            add(TBL_RECORDS.sum.name);
            add(TBL_RECORDS.descr.name);
            add(TBL_RECORDS.sum_expr.name);
            add(TBL_RECORDS.curr_id.name);
        }};
        _dbr.execSQL(INSERT_SELECT(TBL_RECORDS.tableName, columnList,
                attachedDbName + "." + TBL_RECORDS, null));
        _repRecsCache = null;
    }

    private void copyLinks(String attachedDbName) {
        _dbr.execSQL(INSERT_SELECT(TBL_LINKS.tableName, TBL_LINKS.columns.keySet(),
                attachedDbName + "." + TBL_LINKS, null));
    }

    /**
     * @return Map from category ID to {@link Category}.
     */
    public Map<Long, Category> getCategoriesMap() {
        return getCategoriesMap(Category.Kind.ALL);
    }

    /**
     * @param catsKind Kind of categories to return.
     * @return Same as above, but only for a specified kind of categories.
     */
    public Map<Long, Category> getCategoriesMap(Category.Kind catsKind) {
        return getCategoriesMap(catsKind, false);
    }

    /**
     *
     * @param catsKind Kind of categories to return.
     * @param withAll Include "All" pseudocategory, if true.
     * @return Same as above, but can add "All" psudocategory.
     */
    public Map<Long, Category> getCategoriesMap(Category.Kind catsKind, boolean withAll) {
        if (_catsCache == null)
            _catsCache = mapObjects(getCategoriesCursor(Category.Kind.ALL), _catFactory);
        if (catsKind == Category.Kind.ALL && !withAll)
            return _catsCache;

        Map<Long, Category> catMap;
        if (catsKind == Category.Kind.ALL) {
            catMap = new TreeMap<>(_catsCache);
        } else {
            catMap = new TreeMap<>();
            for (Map.Entry<Long, Category> e : _catsCache.entrySet()) {
                if (e.getValue().getKind() != catsKind)
                    continue;
                catMap.put(e.getKey(), e.getValue());
            }
        }
        if (withAll) {
            catMap.put(Category.UNKNOWN_ID, new Category(
                Category.UNKNOWN_ID,
                _context.getString(R.string.all_categories),
                0,
                Category.Kind.ALL));
        }
        return catMap;
    }

    public List<Category> getCategoriesList() {
        return getCategoriesList(Category.Kind.ALL);
    }

    public List<Category> getCategoriesList(Category.Kind catsKind) {
        return new ArrayList<>(getCategoriesMap(catsKind).values());
    }

    // NOTE: The returned Cursor MUST be called for moveToFirst before usage!
    public Cursor getCategoriesCursor(Category.Kind catsKind) {
        String catFilter = null;
        switch (catsKind) {
            case INCOME:
                catFilter = TBL_CATEGORIES.is_income + "= 1";
                break;
            case EXPENSE:
                catFilter = TBL_CATEGORIES.is_income + "= 0";
                break;
            default:
                break;
        }
        return getCategoriesCursor(catFilter);
    }

    private Cursor getCategoriesCursor(long catID) {
        return getCategoriesCursor(TBL_CATEGORIES.cat_id.expr().eq(catID).toSQL());
    }

    private Category readCategory(long catID) {
        return extractObject(getCategoriesCursor(catID), _catFactory, false, catID);
    }

    // NOTE: The returned Cursor MUST be called for moveToFirst before usage!
    private Cursor getCategoriesCursor(String filterClause) {
        return _dbr.query(
            TBL_CATEGORIES.tableName,    // table
            new String[] {
                // "_id" column is required by SimpleCursorAdapter!
                TBL_CATEGORIES.cat_id.name + " as _id",
                TBL_CATEGORIES.cat_id.name,
                TBL_CATEGORIES.is_income.name,
                TBL_CATEGORIES.cat_name.name,
                TBL_CATEGORIES.color.name
            },                              // columns
            filterClause,                   // selection
            null,                           // selectionArgs
            null,                           // groupBy
            null,                           // having
            TBL_CATEGORIES.is_income + " DESC");   // orderBy
    }

    public Category getCategory(long catID) {
        Map<Long, Category> categories = getCategoriesMap();
        Category result = categories.get(catID);
        if (result != null)
            return result;
        throw new NoSuchElementException(
                String.format(Locale.ROOT, "Couldn't find category %d", catID));
    }

    public Cursor getCurrenciesCursor() {
        return getCurrenciesCursor(Category.UNKNOWN_ID);
    }

    // NOTE: The returned Cursor MUST be called for moveToFirst before usage!
    private Cursor getCurrenciesCursor(long currID) {
        final String filter = currID < WalletSchema_v4.DEF_CURRENCY_ID ? null :
            String.format(Locale.ROOT, "%s = %d", TBL_CURRENCIES.curr_id.name, currID);
        return _dbr.query(
            TBL_CURRENCIES.tableName,    // table
            new String[] {
                // "_id" column is required by SimpleCursorAdapter!
                TBL_CURRENCIES.curr_id.name + " as _id",
                TBL_CURRENCIES.curr_id.name,
                TBL_CURRENCIES.curr_name.name,
                TBL_CURRENCIES.curr_symbol.name,
                TBL_CURRENCIES.value.name
            },                              // columns
            filter,                         // selection
            null,                           // selectionArgs
            null,                           // groupBy
            null,                           // having
            null);                          // orderBy
    }

    private Currency readCurrency(long currID) {
        return extractObject(getCurrenciesCursor(currID), _currencyFactory, false, currID);
    }

    @SuppressWarnings("WeakerAccess")
    public Map<Long, Currency> getCurrenciesMap() {
        if (_currsCache == null)
            _currsCache = mapObjects(getCurrenciesCursor(), _currencyFactory);
        return new TreeMap<>(_currsCache);
    }

    public List<Currency> getCurrenciesList() {
        return enumObjects(getCurrenciesCursor(), _currencyFactory);
    }

    public Currency getCurrency(long id) {
        Map<Long, Currency> currs = getCurrenciesMap();
        Currency result = currs.get(id);
        if (result != null)
            return result;
        throw new NoSuchElementException(String.format(Locale.ROOT, "No Currency: %d", id));
    }

    /**
     * Get records sums, grouped by date and then by category, expressed in requested currency.
     * @param filter Filter to obtain the INITIAL set of records to aggregate.
     * @param granularity Time interval to group records in.
     * @param currency Currency to express results in. In case of |null|, reference currency is
     *                 used.
     * @return a list of grouped records. Sums are expressed in current reference currency.
     */
    public List<Record> groupByDateAndCategory(WalletFilter filter, TimeInterval granularity,
            Currency currency) {
        if (currency == null)
            currency = getReferenceCurrency();
        final String PERIOD_COL = "period";
        final String dateFieldExpr = makePeriodQuery(granularity);
        final String subquery = makeRecordsQuery(false, filter, currency);
        return enumObjects(
                // This query will group records by date period and record category.
                _dbr.query(
                        BRACE(subquery),  // table
                        new String[] {  // columns
                            "*",
                            String.format(Locale.ROOT, "sum(%1$s) as %1$s", TBL_RECORDS.sum.name),
                            dateFieldExpr + " AS " + PERIOD_COL,
                            String.format("'category-' || %s || ' during ' || %s as %s",
                                    TBL_RECORDS.cat_id.name, dateFieldExpr, TBL_RECORDS.descr.name)
                        },
                        null,  // where
                        null,  // args
                        joinStrings(new String[] {  // groupBy
                            PERIOD_COL,
                            TBL_RECORDS.cat_id.name
                        }),
                        null,                       // having
                        TBL_RECORDS.date_time.name + " ASC",  // order by
                        null                        // limit

                ),
                new CategorySumFactory()
        );
    }

    public List<Record> getRecordsList(WalletFilter filter, Currency currency) {
        return enumObjects(getRecordsCursor(filter, currency, null), _recordFactory);
    }

    List<Record> findSlaveRecords(long masterID) {
        return enumObjects(getSlaveRecordsCursor(masterID), _recordFactory);
    }

    private Cursor getSlaveRecordsCursor(long masterID) {
        final WalletFilter f = new WalletFilter();
        f.setMasterRecordID(masterID);
        return getRecordsCursor(f, null, null);
    }

    /**
     * @param currency Currency to present results in. If |null|, original currencies are preserved.
     * @param outMasterRecordIds is an out-parameter, which will hold Ids of records in the returned
     *                           cursor, that have slave records. May be null.
     * @return Same as above, but optionally limited.
     */
    public Cursor getRecordsCursor(WalletFilter filter, Currency currency,
            Set<Long> outMasterRecordIds) {
        final String recQuery = makeRecordsQuery(false, filter, currency);

        if (outMasterRecordIds != null) {
            // This allows to get a set of all records, having slave records.
            // Utilizing it further allows to significantly reduce time wasted for idle slave sum
            // queries.
            final Cursor slavesCur = _dbr.rawQuery(
                "SELECT DISTINCT " + TBL_RECORDS.rec_id.qName + " FROM " +
                    INNER_JOIN(
                        AS(BRACE(recQuery), TBL_RECORDS.tableName), TBL_RECORDS.rec_id.qName,
                        TBL_LINKS.tableName, TBL_LINKS.master_id.qName),
                null);
            outMasterRecordIds.clear();
            outMasterRecordIds.addAll(
                accumulateObjects(slavesCur, _idFactory, new SetAccumulator<>()));
        }

        return _dbr.rawQuery(recQuery, null);
    }

    /**
     * Gets sum of slave records.
     * @param masterID Master record ID.
     * @param currency Currency to express sums in. Must be non-null. Should match master record
     *                 currency and could be automatically deduced from |masterID|, but left as
     *                 explicit argument for clarity and performance.
     * @return pair of sums, where first is incomes, and second is expenses.
     */
    Pair<Record.Sum, Record.Sum> getSlaveRecordsSum(long masterID, Currency currency) {
        final WalletFilter f = new WalletFilter();
        f.setMasterRecordID(masterID);
        return getTotalSums(f, currency);
    }

    public Pair<Record.Sum, Record.Sum> getTotalSums(WalletFilter filter) {
        return getTotalSums(filter, getReferenceCurrency());
    }

    /**
     * Sums up records along with their slave records recursively.
     * Note: Double check that |offset| and |length| really make sense with this exact |filter|.
     * @param filter A filter to select records.
     * @param currency Currency to calculate results in. MUST be non-null.
     * @return the sum of all records, that match given conditions, expressed in requested currency.
     */
    public Pair<Record.Sum, Record.Sum> getTotalSums(WalletFilter filter, Currency currency) {
        if (currency == null)
            throw new IllegalArgumentException("Currency required. Got null.");

        if (!_supportCTE) {
            final Set<Long> masterRecordIds = new TreeSet<>();
            final Cursor recCursor = getRecordsCursor(filter, currency, masterRecordIds);
            return accumulateObjects(recCursor,
                    new RecordFactory(this, masterRecordIds),
                    new RecordSummAccumulator(
                            currency.getId(),
                            getCategoriesMap(Category.Kind.INCOME).keySet(),
                            getCategoriesMap(Category.Kind.EXPENSE).keySet()));
        }

        // This initial set if records must be obtained with original filter and without any
        // modifications, because that's what a user sees in the main screen list.
        final String initialSetQuery = makeRecordsQuery(false, filter, null);

        // We take "is income" flag of original record from the initial set and propagate it
        // onto slave records in order to make slave records appear both in incomes and expenses
        // (as record linking implies).
        String[] initialColumns = new String[] {
                TBL_RECORDS.rec_id.name,
                TBL_CATEGORIES.is_income.name
        };

        final String recursiveSetName = "RecordsTree";
        final String totalsQuery = String.format(
                Locale.ROOT,
                // NOTE: UNION ALL is way lighter on memory consumption that UNION, as it doesn't
                //  have to keep all results in order to deduplicate them. But we need
                // deduplication in case we have master records with slaves of the same category
                // kind (income or expense), because otherwise those sums will be counted twice:
                // individually and as slaves of its master.
                "WITH RECURSIVE %s (%s) AS (%s UNION ALL %s) %s",
                recursiveSetName,  // Recursive set name
                joinStrings(initialColumns),  // Recursive set columns
                // Initial record set. Wrapped in order to allow LIMIT clause inside, which is
                // otherwise prohibited.
                SELECT_FROM_WHERE(initialColumns, BRACE(initialSetQuery), (String) null),
                // Slave records
                SQLiteQueryBuilder.buildQueryString(
                        false,  // Distinct
                        TBL_LINKS.tableName + ", " + recursiveSetName,  // From
                        new String[] {  // Columns
                            TBL_LINKS.slave_id.name + " AS " + TBL_RECORDS.rec_id.name,
                            TBL_CATEGORIES.is_income.name
                        },
                        WHERE_RAW(new ArrayList<String>() {{
                            add(TBL_LINKS.master_id.expr().eq(
                                    recursiveSetName + "." + TBL_RECORDS.rec_id.name).toSQL());
                        }}),
                        null, null, null, null
                ),
                // Resulting set
                SQLiteQueryBuilder.buildQueryString(
                        false,  // Distinct
                        joinStrings(new String[] {  // From
                            TBL_RECORDS.tableName,
                            TBL_CURRENCIES.tableName,
                            recursiveSetName,
                        }),
                        new String[] {  // Columns
                            String.format(Locale.ROOT, "SUM(%1$s * %2$s / %3$f) as %1$s",
                                    TBL_RECORDS.sum.name, TBL_CURRENCIES.value.name,
                                    currency.getValue()),
                            String.format(Locale.ROOT, "%d AS %s",
                                    currency.getId(), TBL_RECORDS.curr_id.name),
                            TBL_CATEGORIES.is_income.name
                        },
                        WHERE_RAW(new ArrayList<String>() {{
                            add(TBL_RECORDS.curr_id.qName + " = " + TBL_CURRENCIES.curr_id.qName);
                            add(TBL_RECORDS.rec_id.qName + "=" +
                                    recursiveSetName + "." + TBL_RECORDS.rec_id.name);
                        }}),
                        TBL_CATEGORIES.is_income.name,  // Group by
                        null, null, null
                )
        );

        // Extract at most 2 rows containing sums with the above query.
        final List<Pair<Record.Sum, Boolean>> sums = accumulateObjects(
                _dbr.rawQuery(totalsQuery, null),
                new UncategorizedSumFactory(),
                new ListAccumulator<>(2));

        // Sort out which one is where.
        Record.Sum incomes = null;
        Record.Sum expenses = null;
        if (sums.size() > 0) {
            Pair<Record.Sum, Boolean> firstPair = sums.get(0);
            final boolean firstIsIncome = firstPair.second != null && firstPair.second;
            if (firstIsIncome)
                incomes = firstPair.first;
            else
                expenses = firstPair.first;
            if (sums.size() > 1) {
                Pair<Record.Sum, Boolean> secondPair = sums.get(1);
                final boolean secondIsIncome = secondPair.second != null && secondPair.second;
                if (secondIsIncome)
                    incomes = secondPair.first;
                else
                    expenses = secondPair.first;
            }
        }
        final Record.Sum nullSum = new Record.Sum(currency.getId());
        if (incomes == null)
            incomes = nullSum;
        if (expenses == null)
            expenses = nullSum;

        if (incomes.currencyId != currency.getId() || expenses.currencyId != currency.getId())
            throw new IllegalStateException();

        return new Pair<>(incomes, expenses);
    }

    // TODO: Update slave records currency along with its master.
    public void updateRecord(Record record) {
        final Record oldValue = getRecord(record.getId());
        if (record.similarTo(oldValue) &&
                // TODO: Remove this extra check Record.similarTo respects record currencies.
                record.getSum().currencyId == oldValue.getSum().currencyId)
            return;
        int rowsUpdated = _dbr.update(TBL_RECORDS.tableName, record.toContentValues(),
            TBL_RECORDS.rec_id.expr().eq(record.getId()).toSQL(), null);
        if (rowsUpdated == 0)
            throw new InternalError(COULD_NOT_UPDATE_DB);
        events.recUpdNotifier.notifyObservers(oldValue, null);
    }

    /**
     * Update fields of records, which fall inside a given |filter| and |offset/limit| range.
     */
    public long updateRecords(WalletFilter filter, long categoryId, long currencyId) {
        List<Record> originalRecords = null;
        if (events.recBulkCatSetNotifier.hasObservers() ||
             events.recBulkCurSetNotifier.hasObservers()) {
            originalRecords = maybeGetLimitedRecordsList(filter);
        }

        final ContentValues values = new ContentValues();
        if (categoryId != Category.UNKNOWN_ID) {
            values.put(TBL_RECORDS.cat_id.name, categoryId);
        }
        if (currencyId != Currency.UNKNOWN_ID) {
            values.put(TBL_RECORDS.curr_id.name, currencyId);
        }

        final String idsSubquery = makeRecordsQuery(true, filter, null);
        final long affectedRows;
        if (_supportCTE) {
            _dbr.execSQL(String.format(
                    "WITH upd(id) AS (%s) UPDATE %s SET %s WHERE %s IN upd",
                    idsSubquery, TBL_RECORDS.tableName, joinStrings(values),
                    TBL_RECORDS.rec_id.name));
            affectedRows = _lastChangesCountStmt.simpleQueryForLong();
        }
        else {
            final String whereClause =
                    String.format(Locale.ROOT, "%s IN (%s)", TBL_RECORDS.rec_id, idsSubquery);
            affectedRows = _dbr.update(TBL_RECORDS.tableName, values, whereClause, null);
        }

        if (categoryId != Category.UNKNOWN_ID)
            events.recBulkCatSetNotifier.notifyObservers(originalRecords, categoryId);
        if (currencyId != Currency.UNKNOWN_ID)
            events.recBulkCurSetNotifier.notifyObservers(originalRecords, currencyId);

        return affectedRows;
    }

    public long addRecord(Record record) {
        long rowID = _dbr.insertOrThrow(TBL_RECORDS.tableName, null, record.toContentValues());
        events.recAddNotifier.notifyObservers(rowID, null);
        return rowID;
    }

    public long getRecordsCount() {
        return _recCountStmt.simpleQueryForLong();
    }

    public Record getMostRecentRecord() {
        Cursor recCursor = _dbr.query(
            TBL_RECORDS.tableName,                       // table
            null,                                   // columns
            null,                                   // selection
            null,                                   // selectionArgs
            null,                                   // groupBy
            null,                                   // having
            TBL_RECORDS.date_time.name + " DESC",  // orderBy
            "1");                                   // limit
        return extractObject(recCursor, _recordFactory, true, Record.UNKNOWN_ID);
    }

    /**
     * Returns a {@link Record} by its ID.
     * @param recID ID of the record to return.
     * @return a record. Will throw, if record was not found.
     */
    public Record getRecord(long recID) {
        return extractObject(getRecordCursor(recID), _recordFactory, false, recID);
    }

    /**
     * Same as getRecord, but returns null instead of throwing.
     * {@inheritDoc getRecord}
     */
    public Record findRecord(long recID) {
        return extractObject(getRecordCursor(recID), _recordFactory, true, recID);
    }

    public List<Record> findRepetitionsInCurrentPeriod(long templateId, TimeInterval interval) {
        final CalendarUtils.DateRange range = new CalendarUtils.DateRange(interval);
        final Cursor cursor =
                WalletSchema_v6.TBL_RECORDS
                .select()
                .where(
                        TBL_RECORDS.tpl_rec_id.expr().eq(templateId)
                        .and(TBL_RECORDS.date_time.expr().ge(
                                range.getRangeStart().getTimeInMillis()))
                        .and(TBL_RECORDS.date_time.expr().lt(
                                range.getRangeEnd().getTimeInMillis()))
                ).exec(_dbr);
        return enumObjects(cursor, _recordFactory);
    }

    private Cursor getRecordCursor(long recId) {
        if (recId == Record.UNKNOWN_ID)
            return null;
        final Cursor cur = getRecordsCursor(new WalletFilter(recId), null, null);
        if (cur.getCount() > 1)
            throw new AssertionError();
        return cur;
    }

    public long addCurrency(Currency currency) {
        Log.d(_logLabel, String.format(Locale.ROOT,
                "Adding Currency: '%s'", currency.getName()));

        final long rowId =
                _dbr.insertOrThrow(TBL_CURRENCIES.tableName, null, currency.toContentValues());
        // Make the very first Currency the default one.
        if (getReferenceCurrencyId() == Currency.UNKNOWN_ID)
            setReferenceCurrencyId(rowId);
        _currsCache = null;

        events.currAddNotifier.notifyObservers(rowId, null);
        return rowId;
    }

    public void updateCurrency(Currency currency) {
        doUpdateCurrency(currency);
        events.currUpdNotifier.notifyObservers(currency.getId(), null);
    }

    private void doUpdateCurrency(Currency currency) {
        boolean succeeded = _dbr.update(TBL_CURRENCIES.tableName, currency.toContentValues(),
            TBL_CURRENCIES.curr_id.expr().eq(currency.getId()).toSQL(), null) > 0;
        if (!succeeded)
            throw new IllegalArgumentException();
        if (_currsCache != null)
            _currsCache.put(currency.getId(), readCurrency(currency.getId()));
    }

    /**
     * Deletes a category and moves all its records to another one.
     * @param id ID of the currency to remove.
     * @param moveToId ID of the destination currency for existing records.
     * NOTE: the function will SILENTLY DO NOTHING if |id| equals to |moveToId|.
     */
    public void removeCurrency(long id, long moveToId) {
        // Don't do unnecessary work.
        if (id == moveToId)
            return;

        // Check that currencies even exist.
        final Currency removedCurrency = getCurrency(id);
        final Currency destCurrency = getCurrency(moveToId);

        Log.d(_logLabel, String.format(Locale.ROOT,
                "Removing Currency %d ('%s'). Replacing it with %d ('%s)",
                removedCurrency.getId(), removedCurrency.getName(),
                destCurrency.getId(), destCurrency.getName()));

        _dbr.beginTransaction();
        try {
            changeRecordsCurrency(id, moveToId);
            deleteCurrency(id);
            _dbr.setTransactionSuccessful();
        }
        finally {
            _dbr.endTransaction();
        }

        events.currRemNotifier.notifyObservers(id, moveToId);
    }

    @SuppressWarnings("UnusedReturnValue")
    private int changeRecordsCurrency(long oldCurrID, long newCurrID) {
        if (oldCurrID == newCurrID)
            return 0;

        ContentValues updatedValues = new ContentValues();
        updatedValues.put(TBL_RECORDS.curr_id.name, newCurrID);
        // TODO: Update sums according to currency exchange rates.
        return _dbr.update(
            TBL_RECORDS.tableName,
            updatedValues,
            TBL_RECORDS.curr_id.name + "=" + oldCurrID,
            null);
    }

    @SuppressWarnings("UnusedReturnValue")
    int deleteCurrency(long currencyId) {
        int result = _dbr.delete(
            TBL_CURRENCIES.tableName,
            TBL_CURRENCIES.curr_id.name + "=" + currencyId,
            null);
        if (_currsCache != null)
            _currsCache.remove(currencyId);
        return result;
    }

    public long getReferenceCurrencyId() {
        return getAuxLongValue(AuxTable.PREF_REFERENCE_CURRENCY);
    }

    /**
     * Updates currencies value such, that the specified currency had a value of 1, while preserving
     * all categories relative values. It is required to correctly reduce the sum of several
     * records in different currencies.
     * @param currencyId Currency to set as the reference one.
     */
    public void setReferenceCurrencyId(long currencyId) {
        if (getReferenceCurrencyId() == currencyId)
            return;
        try {
            _dbr.beginTransaction();
            final Currency newRef = getCurrency(currencyId);
            _dbr.execSQL(String.format(
                    Locale.ROOT,
                    "UPDATE %1$s SET %2$s = %2$s / %3$f;",
                    TBL_CURRENCIES.tableName, TBL_CURRENCIES.value.name, newRef.getValue()));
            newRef.setValue(1.0);
            doUpdateCurrency(newRef);  // Avoid firing change event. Below is enough of them.
            setAuxLongValue(AuxTable.PREF_REFERENCE_CURRENCY, currencyId);
            _dbr.setTransactionSuccessful();
        }
        finally {
            _dbr.endTransaction();
        }
        _currsCache = null;
        events.currPrimarySelectNotifier.notifyObservers(currencyId, null);
        events.currBulkUpdateNotifier.notifyObservers(null, null);
    }

    public Currency getReferenceCurrency() {
        return getCurrency(getReferenceCurrencyId());
    }

    @SuppressWarnings("WeakerAccess")
    public boolean hasCategoryNamed(String name) {
        return findCategoryByName(name) != null;
    }

    public Category findCategoryByName(String name) {
        for (Category category: getCategoriesList(Category.Kind.ALL)) {
            if (category.getName().compareToIgnoreCase(name) == 0)
                return category;
        }
        return null;
    }

    public Currency findCurrencyByName(String name) {
        for (Currency currency: getCurrenciesList()) {
            if (currency.getName().compareToIgnoreCase(name) == 0)
                return currency;
        }
        return null;
    }

    public long addCategory(Category category) {
        // TODO: Add column uniqueness constraint.
        if (hasCategoryNamed(category.getName()))
            throw new IllegalArgumentException(
                    _context.getString(R.string.cat_name_duplicate, category.getName()));

        Log.d(_logLabel, String.format(Locale.ROOT,
                "Adding Category: '%s'", category.getName()));

        final long rowID =
                _dbr.insertOrThrow(TBL_CATEGORIES.tableName, null, category.toContentValues());
        _catsCache = null;
        events.catAddNotifier.notifyObservers(rowID, null);
        return rowID;
    }

    public void updateCategory(Category category) {
        if (category.getId() == Category.UNKNOWN_ID)
            throw new IllegalArgumentException("Missing object ID.");
        final Category existingCat = findCategoryByName(category.getName());
        if (existingCat != null && existingCat.getId() != category.getId())
            throw new IllegalArgumentException(
                    _context.getString(R.string.cat_name_duplicate, category.getName()));

        int updated = _dbr.update(TBL_CATEGORIES.tableName, category.toContentValues(),
                TBL_CATEGORIES.cat_id.expr().eq(category.getId()).toSQL(), null);
        if (updated != 1)
            throw new IllegalStateException("Could not update single Category: " + category);

        if (_catsCache != null)
            _catsCache.put(category.getId(), readCategory(category.getId()));

        events.catUpdNotifier.notifyObservers(category.getId(), null);
    }

    /**
     * Deletes a category and moves all its records to another one.
     * @param id ID of the category to remove.
     * @param moveToId ID of the destination category for existing records.
     * NOTE: the function will SILENTLY DO NOTHING if |id| equals to |moveToId|.
     */
    public void removeCategory(long id, long moveToId) {
        // Don't do unnecessary work.
        if (id == moveToId)
            return;

        // Check that categories even exist.
        final Category remCategory = getCategory(id);
        final Category destCategory = getCategory(moveToId);

        Log.d(_logLabel, String.format(Locale.ROOT,
                "Removing Category %d ('%s'). Replacing it with %d ('%s)",
                remCategory.getId(), remCategory.getName(),
                destCategory.getId(), destCategory.getName()));

        _dbr.beginTransaction();
        try {
            changeRecordsCategory(id, moveToId);
            deleteCategory(id);
            _dbr.setTransactionSuccessful();
        }
        finally {
            _dbr.endTransaction();
        }
        events.catRemNotifier.notifyObservers(id, moveToId);
    }

    @SuppressWarnings("UnusedReturnValue")
    private int changeRecordsCategory(long oldCatID, long newCatID) {
        if (oldCatID == newCatID)
            return 0;

        ContentValues updatedValues = new ContentValues();
        updatedValues.put(TBL_RECORDS.cat_id.name, newCatID);
        return _dbr.update(
            TBL_RECORDS.tableName,
            updatedValues,
            TBL_RECORDS.cat_id.name + "=" + oldCatID,
            null);
    }

    @SuppressWarnings("UnusedReturnValue")
    int deleteCategory(long catID) {
        int result = _dbr.delete(
            TBL_CATEGORIES.tableName,
            TBL_CATEGORIES.cat_id.name + "=" + catID,
            null);
        if (_catsCache != null)
            _catsCache.remove(catID);
        return result;
    }

    public void removeRecord(long recID) {
        final Record record = getRecord(recID);

        final int removed = _dbr.delete(
                TBL_RECORDS.tableName,
                TBL_RECORDS.rec_id.name + "=" + recID,
                null);
        if (removed != 1) {
            throw new NoSuchElementException(String.format(Locale.getDefault(),
                    CursorUtils.ERR_UNKNOWN_RECORD_ID, recID));
        }

        // TODO: Move record unscheduling somewhere else.
        try {
            if (_repRecsCache != null)
                _repRecsCache.remove(recID);
            RepeatedRecordsScheduler.unscheduleRecord(_context.getApplicationContext(), recID);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        events.recRemNotifier.notifyObservers(record, null);
    }

    public long removeRecords(WalletFilter filter) {
        List<Record> deletedRecords = null;
        if (events.recBulkRemovalNotifier.hasObservers()) {
            deletedRecords = maybeGetLimitedRecordsList(filter);
        }

        _repRecsCache = null;
        if (deletedRecords == null) {
            RepeatedRecordsScheduler.removeAlarms(_context.getApplicationContext());
        }
        try {

            final String idsSubquery = makeRecordsQuery(true, filter, null);
            long removed;
            if (_supportCTE) {
                // This is supposedly more efficient than the later way.
                _dbr.execSQL(String.format(
                        "WITH delete_ids(id) AS (%s) DELETE FROM %s WHERE %s in delete_ids",
                        idsSubquery, TBL_RECORDS.tableName, TBL_RECORDS.rec_id.qName));
                removed = _lastChangesCountStmt.simpleQueryForLong();
            }
            else {
                removed = _dbr.delete(
                        TBL_RECORDS.tableName,
                        String.format(Locale.ROOT, "%s in (%s)",
                                TBL_RECORDS.rec_id.name, idsSubquery),
                        null
                );
            }
            events.recBulkRemovalNotifier.notifyObservers(deletedRecords, null);
            return removed;
        }
        finally {
            // TODO: Move record rescheduling somewhere else.
            try {
                if (deletedRecords != null) {
                    for (Record r : deletedRecords) {
                        RepeatedRecordsScheduler.
                                unscheduleRecord(_context.getApplicationContext(), r.getId());
                    }
                }
                else {
                    RepeatedRecordsScheduler.setAlarms(_context.getApplicationContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates a link between two records, also modifying the way their effective sums are
     * calculated. Only records of different kinds (expense to income, or vice versa) can be linked.
     *
     * A slave record always increases master record effective sum.
     *
     * Example:
     * Income "A" for 300 units is a master record;
     * Expense "B" for 50 units is a slave record of "A".
     * "A" will be displayed and aggregated as if it's worth 350 units.
     * "B" will be displayed and aggregated as is - 50 units.
     *
     * TODO: think about link chains and circular links!
     *
     * @param slaveID  is the dependent ("slave") record ID.
     * @param masterID is the main ("master") record ID.
     * @return new link ID.
     */
    @SuppressWarnings("UnusedReturnValue")
    public long linkRecords(long slaveID, long masterID) {
        final Record slave = getRecord(slaveID);
        final Record master = getRecord(masterID);

        final Category slaveCategory = getCategory(slave.getCatID());
        final Category masterCategory = getCategory(master.getCatID());
        if (slaveCategory.getKind() == masterCategory.getKind())
            throw new IllegalArgumentException("Record kinds must differ");

        beginTransaction();
        try {
            ContentValues linkValues = new ContentValues();
            linkValues.put(TBL_LINKS.slave_id.name, slaveID);
            linkValues.put(TBL_LINKS.master_id.name, masterID);
            long linkId = _dbr.replaceOrThrow(TBL_LINKS.tableName, null, linkValues);
            transactionSucceeded();

            // We treat setting master record as if slave record was edited.
            events.recUpdNotifier.notifyObservers(slave, null);
            return linkId;
        }
        finally {
            endTransaction();
        }
    }

    /**
     * Breaks a links between records.
     * @param slaveID is the dependent ("slave") record ID, which will be unlinked from its master.
     * @return a boolean indicating whether the links was actually deleted.
     */
    public boolean unlinkRecord(long slaveID) {
        final Record slave = getRecord(slaveID);
        final boolean deleted =
                _dbr.delete(TBL_LINKS.tableName, TBL_LINKS.slave_id + "=" + slaveID, null) > 0;
        // We treat setting master record as if slave record was edited.
        if (deleted)
            events.recUpdNotifier.notifyObservers(slave, null);
        return deleted;
    }

    public Record findMasterRecord(long slaveID) {
        return findRecord(findMasterRecordID(slaveID));
    }

    @SuppressWarnings("WeakerAccess")
    public long findMasterRecordID(long slaveID) {
        Cursor linkCursor = _dbr.query(
            TBL_LINKS.tableName,
            new String[] {
                TBL_LINKS.master_id.name
            },
            TBL_LINKS.slave_id.name + "=" + slaveID,
            null,   // selectionArgs
            null,   // groupBy
            null,   // having
            null    // orderBy
        );
        final Long masterId = extractObject(linkCursor,
                new LongFactory(TBL_LINKS.master_id.name, (long) -1), true, Record.UNKNOWN_ID);
        return masterId != null ? masterId : Record.UNKNOWN_ID;
    }

    public boolean hasSlaveRecords(long masterID) {
        return Utils.countRows(_dbr, TBL_LINKS.tableName, makeLinksFilter(masterID)) > 0;
    }

    public List<Record> getScheduledRecords() {
        final WalletQueryParams qParams = makeRecQueryParams(null, false, false, true, null);
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(qParams.sourceTables);
        qb.appendWhere(qParams.selection);
        qb.appendWhere(TBL_REPEATING_RECORDS.rec_interval + " IS NOT NULL");
        final String query = qb.buildQuery(
                qParams.columns,
                null,  // selection
                null,  // Group by
                null,  // Having
                TBL_RECORDS.date_time.qName,  // Order by
                null  // Limit
        );
        return enumObjects(_dbr.rawQuery(query, null), _recordFactory);
    }

    public Map<Long, TimeInterval> getScheduledRecordsIdMap() {
        return new TreeMap<>(getScheduledRecordsInternal());
    }

    private Map<Long, TimeInterval> getScheduledRecordsInternal() {
        if (_repRecsCache == null)
            _repRecsCache = mapObjects(
                    _dbr.rawQuery(
                            SELECT_FROM_WHERE(null, TBL_REPEATING_RECORDS.tableName, (String) null),
                            null),
                    _repInfoFactory);
        return _repRecsCache;
    }

    /**
     * Retrieves repetition interval for a given {@link Record}. If none recorded, throws.
     */
    public TimeInterval getRepetitionInterval(long recId) {
        Map<Long, TimeInterval> map = getScheduledRecordsInternal();
        if (map.containsKey(recId))
            return map.get(recId);
        throw _repInfoFactory.getNoElementException(recId);
    }

    public boolean recordIsScheduled(long recId) {
        return getScheduledRecordsInternal().containsKey(recId);
    }

    // TODO: Disallow scheduling records created from already scheduled templates.
    public void scheduleRecord(long recordId, TimeInterval interval) {
        final Record record = getRecord(recordId);
        final long rowId = _dbr.replaceOrThrow(TBL_REPEATING_RECORDS.tableName, null,
                TBL_REPEATING_RECORDS.rowValues(recordId, interval));
        if (_repRecsCache != null)
            _repRecsCache.put(recordId, interval);
        RepeatedRecordsScheduler.scheduleRecord(
                _context.getApplicationContext(),
                record, interval, true);
        if (rowId >= 0 )
            events.recUpdNotifier.notifyObservers(record, null);
    }

    public void unscheduleRecord(long id) {
        final Record record = getRecord(id);
        final int deleted = _dbr.delete(TBL_REPEATING_RECORDS.tableName,
                TBL_REPEATING_RECORDS.rec_id.expr().eq(id).toSQL(), null);
        if (_repRecsCache != null)
            _repRecsCache.remove(id);
        RepeatedRecordsScheduler.unscheduleRecord(_context.getApplicationContext(), id);
        if (deleted > 0 )
            events.recUpdNotifier.notifyObservers(record, null);
    }

    public void beginTransaction() {
        _dbr.beginTransaction();
        _transactionSucceeded = false;
    }

    public void transactionSucceeded() {
        _dbr.setTransactionSuccessful();
        _transactionSucceeded = true;
    }

    public void endTransaction() {
        if (_dbr.inTransaction() && !_transactionSucceeded) {
            _catsCache = null;
            _currsCache = null;
            _repRecsCache = null;
        }
        _dbr.endTransaction();
    }

    /**
     * Selects from auxiliary data table and returns a value by the given key.
     * @return Stored value or -1, if |name| was not found.
     */
    private long getAuxLongValue(String name) {
        return extractObject(_dbr.rawQuery(
                _auxValueQuery,
                new String[] {name}),
                new LongFactory(1, (long) -1),
                true,
                name);
    }

    private void setAuxLongValue(String name, long value) {
        final ContentValues values = new ContentValues(1);
        values.put(TBL_AUX.name_.name, name);
        values.put(TBL_AUX.value.name, value);
        _dbr.replaceOrThrow(TBL_AUX.tableName, null, values);
    }

    private List<Record> maybeGetLimitedRecordsList(WalletFilter filter) {
        if (filter.getLimit() < 0 ||
            filter.getLimit() > WalletEvents.BulkObserver.MAX_BACKED_UP_RECORDS)
            return null;
        return getRecordsList(filter, null);
    }

    /**
     * @param idsOnly If |true|, only the record id column is fetched. Otherwise, all of them.
     * @param filter Obvious.
     * @param currency Currency to express record sums in. Only relevant, if IdsOnly = false.
     * @return query string to fetch records matching a given filter.
     */
    private static String makeRecordsQuery(boolean idsOnly, WalletFilter filter,
            Currency currency) {
        final WalletQueryParams qParams =
                makeRecQueryParams(filter, idsOnly, true, false, currency);
        return SQLiteQueryBuilder.buildQueryString(
                false,
                qParams.sourceTables,
                qParams.columns,
                qParams.selection,
                null,  // Group by
                null,  // Having
                TBL_RECORDS.date_time.qName +
                        (filter != null && filter.isRevSorting() ? " DESC" : " ASC"),  // Order by
                filter != null ? LIMIT_OFFSET(filter.getLimit(), filter.getOffset()) : null
        );
    }

    private static WalletQueryParams makeRecQueryParams(WalletFilter filter,
            final boolean idsOnly, boolean withCategoryData, boolean withRepetitionData,
            Currency targetCurrency) {
        if (idsOnly) {
            // Throw in case of contradicting arguments.
            withCategoryData = false;
            withRepetitionData = false;
            targetCurrency = null;
        }
        final List<String> columnList = new ArrayList<>();
        columnList.add(TBL_RECORDS.rec_id.qName);
        if (!idsOnly) {
            columnList.add(AS(TBL_RECORDS.rec_id.qName, "_id"));
            columnList.add(targetCurrency == null ?  // sum column
                    TBL_RECORDS.sum.qName :
                    String.format(Locale.ROOT, "%1$s * %2$s / %3$f as %1$s",
                            TBL_RECORDS.sum.name, TBL_CURRENCIES.value.qName,
                            targetCurrency.getValue()));
            columnList.add(targetCurrency == null ? // currency column (+ original currency)
                    TBL_RECORDS.curr_id.qName :
                    String.format(Locale.ROOT, "%d as %s",
                            targetCurrency.getId(), TBL_RECORDS.curr_id.name));
            columnList.add(TBL_RECORDS.date_time.qName);
            columnList.add(TBL_RECORDS.cat_id.qName);
            columnList.add(TBL_RECORDS.sum_expr.qName);
            columnList.add(TBL_RECORDS.descr.qName);
            columnList.add(TBL_RECORDS.tpl_rec_id.qName);
        }

        String source = TBL_RECORDS.tableName;

        final List<String> allFilterTokens = makeRecordsFilter(filter);

        final List<String> catsFilterTokens = makeCatKindFilter(filter);
        if (catsFilterTokens != null) {
            allFilterTokens.addAll(catsFilterTokens);
        }
        if (catsFilterTokens != null || withCategoryData) {
            source = INNER_JOIN(source, TBL_RECORDS.cat_id.qName,
                    TBL_CATEGORIES.tableName, TBL_CATEGORIES.cat_id.qName);
            columnList.add(TBL_CATEGORIES.is_income.qName);
        }

        if (filter != null && filter.getQuery() != null) {
            allFilterTokens.add(String.format(
                    "%1$s IN (SELECT docid FROM %2$s WHERE %2$s MATCH '%3$s')",
                    TBL_RECORDS.rec_id.qName, WalletSchema_v7.FTS_TABLE_NAME, filter.getQuery()));

            /*
            // This also kinda works, but for some reason makes Cursor internally store qualified
            // column names, thus making them inaccessible via Cursor.getColumnIndex.
            source = INNER_JOIN(
                    String.format(
                            "(SELECT docid FROM %1$s WHERE %1$s MATCH '%2$s') AS fts_ids",
                            WalletSchema_v7.FTS_TABLE_NAME, filter.getQuery()),
                    "fts_ids.docid",
                    source, TBL_RECORDS.rec_id.qName);
            */
        }

        final List<String> linkFilterTokens = makeLinksFilter(filter);
        if (linkFilterTokens != null) {
            source = INNER_JOIN(source, TBL_RECORDS.rec_id.qName,
                    TBL_LINKS.tableName, TBL_LINKS.slave_id.qName);
            allFilterTokens.addAll(linkFilterTokens);
        }

        if (targetCurrency != null) {
            source = INNER_JOIN(source, TBL_RECORDS.curr_id.qName,
                    TBL_CURRENCIES.tableName, TBL_CURRENCIES.curr_id.qName);
        }

        if (withRepetitionData) {
            source += " LEFT OUTER JOIN " + TBL_REPEATING_RECORDS.tableName +
                    ON(TBL_RECORDS.rec_id.qName, TBL_REPEATING_RECORDS.rec_id.qName);
            columnList.add(TBL_REPEATING_RECORDS.rec_interval.qName);
        }

        return new WalletQueryParams(columnList, source, joinStrings(" AND ", allFilterTokens));
    }

    static private String makePeriodQuery(TimeInterval granularity) {
        final Calendar now = Calendar.getInstance();
        final long utcOffset = now.getTimeZone().getOffset(now.getTimeInMillis());
        final String timestampExpr = String.format(Locale.ROOT,
                "(%s + %d) / 1000", TBL_RECORDS.date_time.name, utcOffset);
        switch (granularity) {
            case YEAR:
                return FORMAT_TIMESTAMP(timestampExpr, "%Y");  // %Y: year (0000-9999).
            case MONTH:
                return FORMAT_TIMESTAMP(timestampExpr, "%Y/%m");  // %m: month (01-12).
            case DAY:
                return FORMAT_TIMESTAMP(timestampExpr, "%Y,%j");  // %j: day of year (1-366)
            case WEEK:
                return FORMAT_TIMESTAMP(timestampExpr, "%Y/%m:") +
                        // %d: day of month
                        " || (" + FORMAT_TIMESTAMP(timestampExpr, "%d") + " / 7)";
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Determine and prepare arguments for a DB query, based on supplied filter and other flags.
     */
    private static class WalletQueryParams {

        private WalletQueryParams(final List<String> columnList, final String sourceTables,
                final String selection) {
            this.columns = new String[columnList.size()];
            columnList.toArray(this.columns);
            this.sourceTables = sourceTables;
            this.selection = selection;
        }

        // Column set.
        final String[] columns;
        // Denotes the data source. Namely a table name, or a more complex expression, like JOIN.
        final String sourceTables;
        // A WHERE expression body to satisfy the given filter.
        final String selection;
    }

    private static final String _auxValueQuery = SQLiteQueryBuilder.buildQueryString(
            false, TBL_AUX.tableName, null,
            String.format(Locale.ROOT, "%s = ?", TBL_AUX.name_),
            null, null, null, null);

    // NOTE: All these caches must be dropped in case of transaction rollback!
    private Map<Long, Category> _catsCache = null;  // TODO: Always return a copy of it.
    private Map<Long, Currency> _currsCache = null;
    private Map<Long, TimeInterval> _repRecsCache = null;

    private ObjectFactory<Record> _recordFactory = new RecordFactory(this, null);
    private final static ObjectFactory<Long> _idFactory = new LongFactory(0, Record.UNKNOWN_ID);
    private final static ObjectFactory<Currency> _currencyFactory = new CurrencyFactory();
    private final static ObjectFactory<Category> _catFactory = new CategoryFactory();
    private final static ObjectFactory<TimeInterval> _repInfoFactory = new RepetitionInfoFactory();

    private static final String COULD_NOT_UPDATE_DB = "Could not update database.";
    private static final String ATTACHED_DB_NAME = "other";

    private final Context _context;
    private final String _name;
    private final String _logLabel;
    private SQLiteDatabase _dbr;
    private boolean _transactionSucceeded = false;
    private SQLiteStatement _lastChangesCountStmt;
    private SQLiteStatement _recCountStmt;
    private boolean _supportCTE = false;
}

/**
 * Copyright (c) 2015, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static ru.knuckles.walletoid.Utils.joinStrings;
import static ru.knuckles.walletoid.database.SqlTemplates.*;

public class Utils {

    static boolean dbSupportsPrintf(SQLiteDatabase db) {
        Cursor c = null;
        try {
            c = db.rawQuery("printf(%s), 'test'", null);
            String test = CursorUtils.extractObject(c, new CursorUtils.StringFactory(0, null), false, null);
            return test.equals("test");
        }
        catch (SQLiteException e) {
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally {
            if (c != null)
                c.close();
        }
    }

    static boolean dbSupportsCTE(SQLiteDatabase db) {
        Cursor c = null;
        try {
            c = db.rawQuery(
                    "WITH RECURSIVE cnt(x) AS (" +
                        "VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x<2" +
                    ") SELECT SUM(x) FROM cnt",
                    null);
            long test = CursorUtils.extractObject(c,
                    new CursorUtils.LongFactory(0, (long) -1), true, null);
            return test == 3;
        }
        catch (SQLiteException e) {
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally {
            if (c != null)
                c.close();
        }
    }

    static long getTableBiggestRowId(SQLiteDatabase db, String table, String column) {
        return db.compileStatement(BIGGEST_ROWID(table, column)).simpleQueryForLong();
    }

    static void shiftRowIds(SQLiteDatabase db, String table, String column, long by) {
        // FIXME: This fails when there are rows with maximal row ids. E.g. misc categories.
        final long biggestId = getTableBiggestRowId(db, table, column);
        final SQLiteStatement shiftRowIdStmt =
            db.compileStatement(String.format("UPDATE %s SET %s = %s + ?", table, column, column));
        if (biggestId > by) {
            shiftRowIdStmt.bindLong(1, biggestId);
            shiftRowIdStmt.execute();
            shiftRowIdStmt.bindLong(1, by - biggestId);
            shiftRowIdStmt.execute();
        }
        else {
            shiftRowIdStmt.bindLong(1, by);
            shiftRowIdStmt.execute();
        }
    }

    static <T> void remapColumnValues (SQLiteDatabase db, String table, String column, Map<T, T> mapping) {
        final SQLiteStatement updateStmt =
            db.compileStatement(String.format("UPDATE %s SET %s = ? WHERE %s = ?", table, column, column));
        for (Map.Entry<T, T> e : mapping.entrySet()) {
            updateStmt.bindString(1, e.getValue().toString());  // SET
            updateStmt.bindString(2, e.getKey().toString());  // WHERE  // + myBiggestCatId
            updateStmt.execute();
        }
    }

    static <T> Map<Long, T> incKeys(Map<Long, T> map, long inc) {
        Map<Long, T> result = new TreeMap<>();
        for (Map.Entry<Long, T> p : map.entrySet()) {
            result.put(p.getKey() + inc, p.getValue());
        }
        return result;
    }

    static <T> Map<T, Long> incValues(Map<T, Long> map, long inc) {
        Map<T, Long> result = new TreeMap<>();
        for (Map.Entry<T, Long> p : map.entrySet()) {
            result.put(p.getKey(), p.getValue() + inc);
        }
        return result;
    }

    static void createTable(SQLiteDatabase db, SchemaDecl.TableDef tableDef) {
        db.execSQL(tableDef.getCreateExpr());
        for (SchemaDecl.ColumnDef col : tableDef.columns.values()) {
            if (col.indexName != null && !col.indexName.isEmpty())
                createIndex(db, col);
        }
    }

    static void createIndex(SQLiteDatabase db, SchemaDecl.ColumnDef col) {
        db.execSQL(CREATE_INDEX(col.indexName, col.parentTable.tableName, col.name));
    }

    static void mergeTables(final SQLiteDatabase db, final SchemaDecl.TableDef destTable,
            final String attachedDB, final String idsColumnName, Map<Long, Long> idsMapping) {
        final String srcTableQName = attachedDB + "." + destTable.tableName;
        final long dstBiggestRowId = getTableBiggestRowId(db, destTable.tableName, idsColumnName);

        // Move source table IDs so they won't interfere with those in destination table.
        shiftRowIds(db, srcTableQName, idsColumnName, dstBiggestRowId);

        // Change mapped IDs.
        idsMapping = incKeys(idsMapping, dstBiggestRowId);
        remapColumnValues(db, srcTableQName, idsColumnName, idsMapping);

        db.execSQL(SqlTemplates.INSERT_SELECT(destTable.tableName, destTable.columns.keySet(),
                attachedDB + "." + destTable.tableName,
                String.format("WHERE %s NOT IN (%s)",
                        idsColumnName,
                        joinStrings(idsMapping.values())
                )
        ));
    }

    /**
     * NOTE: You want to wrap the call to this function in a transaction.
     * NOTE: And likely to switch OFF foreign keys during its run.
     */
    static void recreateTable(SQLiteDatabase db, SchemaDecl.TableDef oldTable, SchemaDecl.TableDef newTable) {
        if (!oldTable.tableName.equals(newTable.tableName))
            throw new AssertionError("Table names must match, otherwise it makes no sense!");
        final String backupTableName = newTable + "_bak";


        db.beginTransaction();
        try {
            db.execSQL("PRAGMA foreign_keys = OFF;");

            dropTableIndices(db, oldTable.tableName);
            db.execSQL(String.format("ALTER TABLE %s RENAME TO %s",
                    newTable.tableName, backupTableName));
            createTable(db, newTable);
            db.execSQL(INSERT_SELECT(
                    newTable.tableName, oldTable.columns.keySet(), backupTableName, null));
            db.execSQL("DROP TABLE " + backupTableName);

            db.execSQL("PRAGMA foreign_keys = ON;");
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }
    }

    static void dropTableIndices(SQLiteDatabase db, String tableName) {
        final CursorUtils.StringFactory sf = new CursorUtils.StringFactory(0, null);
        final Iterable<String> indicesNames = CursorUtils.enumObjects(
                db.rawQuery("SELECT name FROM sqlite_master WHERE type == 'index' AND tbl_name == ?",
                        new String[] { tableName }), sf);
        for (String indexName : indicesNames) {
            db.execSQL("DROP INDEX " + indexName);
        }
    }

    /**
     * NOTE: You want to wrap the call to this function in a transaction.
     */
    static void addNewColumns(SQLiteDatabase db, SchemaDecl.TableDef table,
            Collection<SchemaDecl.ColumnDef> newColumns) {
        for (SchemaDecl.ColumnDef column : newColumns) {
            db.execSQL(String.format("ALTER TABLE %s ADD COLUMN %s", table.tableName, column.toDefString()));
        }
    }

    static Collection<String> dumpMasterDefs(SQLiteDatabase db) {
        return CursorUtils.enumObjects(
                db.rawQuery("select sql from sqlite_master;", null),
                new CursorUtils.StringFactory(0, ""));
    }

    static long countRows(SQLiteDatabase db, String table, List<String> where) {
        final SQLiteStatement s = db.compileStatement(
                SELECT_FROM_WHERE(new String[] { "COUNT()" }, table, where));
        return s.simpleQueryForLong();
    }
}

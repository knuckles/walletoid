/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

import static ru.knuckles.walletoid.Utils.*;

/**
 * A collection of helper classes to aid in database schema declaration.
 */
public class SchemaDecl {

    /**
     * Declares table column. Only non-static, public fields of {@link ColumnDef} ColumnDef  type
     * (or its subclasses) are considered when reflecting.
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Column {
        /**
         * Column index. Specify, if you want to have table columns in a particular order.
         * Without it, or with values being the same, column order is undefined due to Java
         * reflection shortcomings.
         */
        int index() default 0;

        /**
         * Specify to override column name. By default it is taken from annotated field name.
         */
        String name() default "";

        String type();
    }

    /**
     * Declares column index. Add next to {@link Column} column to mark it as indexed.
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Index {
        String name();
    }

    /**
     * Declares foreign key for annotated column.
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ForeignKey {
        String table();
        String column();
        ForeignKeyAction onDelete();
        ForeignKeyAction onUpdate();
    }

    public enum ForeignKeyAction {
        NO_ACTION,
        SET_NULL,
        SET_DEFAULT,
        CASCADE,
        RESTRICT;

        public String toString() {
            switch (this) {
                case NO_ACTION:
                    return "NO ACTION";
                case SET_NULL:
                    return "SET NULL";
                case SET_DEFAULT:
                    return "SET DEFAULT";
                case CASCADE:
                    return "CASCADE";
                case RESTRICT:
                    return "RESTRICT";
            }
            throw new IllegalArgumentException();
        }
    }

    abstract static class ColumnSource {
        public SelectResult select() {
            return new SelectResult(null).from(this);
        }

        public SelectResult select(ColumnDef column) {
            final ArrayList<ColumnDef> cs = new ArrayList<>(1);
            cs.add(column);
            return select(cs);
        }

        public SelectResult select(Collection<ColumnDef> columns) {
            return new SelectResult(columns).from(this);
        }

        public JoinResult join(ColumnSource other) {
            return join(other, JoinResult.Kind.INNER);
        }

        public JoinResult join(ColumnSource other, JoinResult.Kind kind) {
            return new JoinResult(this, other, kind);
        }

        abstract String toSQL();
    }

    static class JoinResult extends ColumnSource {
        public enum Kind {
            INNER,
            LEFT_OUTER,
            RIGHT_OUTER,
            CROSS;

            public String toString() {
                switch (this) {
                    case INNER:
                        return "INNER";
                    case LEFT_OUTER:
                        return "LEFT OUTER";
                    case RIGHT_OUTER:
                        return "RIGHT OUTER";
                    case CROSS:
                        return "CROSS";
                }
                return null;
            }
        }

        public JoinResult on(BoolExpr e) {
            _e = e;
            return this;
        }

        JoinResult(ColumnSource s1, ColumnSource s2, Kind kind) {
            _s1 = s1;
            _s2 = s2;
            _kind = kind;
        }

        @Override
        String toSQL() {
            _sb.setLength(0);
            _sb.append(_s1.toSQL());
            _sb.append(" ");
            _sb.append(_kind.toString());
            _sb.append(" JOIN ");
            _sb.append(_s2.toSQL());
            if (_e != null) {
                _sb.append(" ON ");
                _sb.append(_e.toSQL());
            }
            return _sb.toString();
        }

        private final StringBuilder _sb = new StringBuilder();
        private final ColumnSource _s1;
        private final ColumnSource _s2;
        private final Kind _kind;
        private BoolExpr _e;
    }

    public static class SelectResult extends ColumnSource {
        SelectResult(Collection<ColumnDef> columns) {
            _columns = columns;
            if (columns != null && columns.size() > 0)
                _source = head(columns).parentTable;
        }

        public SelectResult from(ColumnSource source) {
            _source = source;
            return this;
        }

        public SelectResult where(BoolExpr be) {
            _be = be;
            return this;
        }

        public SelectResult limit(long lim) {
            _limit = lim;
            return this;
        }

        public String toSQL() {
            _sb.setLength(0);
            _sb.append(String.format("SELECT %s FROM (%s)",
                    _columns == null ? "*" : joinStrings(_columns),
                    _source.toSQL()));
            if (_be != null) {
                _sb.append(" WHERE ");
                _sb.append(_be.toSQL());
            }
            if (_limit > 0) {
                _sb.append(" LIMIT ");
                _sb.append(_limit);
            }
            return _sb.toString();
        }

        public Cursor exec(SQLiteDatabase db) {
            final String q = toSQL();
            return db.rawQuery(q, null);
        }

        private final StringBuilder _sb = new StringBuilder();
        private Collection<ColumnDef> _columns;
        private ColumnSource _source;
        private BoolExpr _be;
        private long _limit = 0;
    }

    public static class ExprPart {
        public enum Op {
            AND,
            OR,
            EQ,
            GE,
            GT,
            LE,
            LT;

            @Override
            public String toString() {
                switch (this) {
                    case AND:
                        return " AND ";
                    case OR:
                        return " OR ";
                    case EQ:
                        return " = ";
                    case GE:
                        return " >= ";
                    case GT:
                        return " > ";
                    case LE:
                        return " <= ";
                    case LT:
                        return " < ";
                }
                return null;
            }
        }

        public ExprPart(String e) {
            _e = e;
        }

        public BoolExpr and(ExprPart other) { return new BoolExpr(this, Op.AND, other); }
        public BoolExpr or(ExprPart other) { return new BoolExpr(this, Op.OR, other); }
        public BoolExpr eq(ExprPart other) { return new BoolExpr(this, Op.EQ, other); }
        public BoolExpr ge(ExprPart other) { return new BoolExpr(this, Op.GE, other); }
        public BoolExpr gt(ExprPart other) { return new BoolExpr(this, Op.GT, other); }
        public BoolExpr le(ExprPart other) { return new BoolExpr(this, Op.LE, other); }
        public BoolExpr lt(ExprPart other) { return new BoolExpr(this, Op.LT, other); }

        public BoolExpr and(Object other) {
            return new BoolExpr(this, Op.AND, new ExprPart(String.valueOf(other))); }
        public BoolExpr or(Object other) {
            return new BoolExpr(this, Op.OR, new ExprPart(String.valueOf(other))); }
        public BoolExpr eq(Object other) {
            return new BoolExpr(this, Op.EQ, new ExprPart(String.valueOf(other))); }
        public BoolExpr ge(Object other) {
            return new BoolExpr(this, Op.GE, new ExprPart(String.valueOf(other))); }
        public BoolExpr gt(Object other) {
            return new BoolExpr(this, Op.GT, new ExprPart(String.valueOf(other))); }
        public BoolExpr le(Object other) {
            return new BoolExpr(this, Op.LE, new ExprPart(String.valueOf(other))); }
        public BoolExpr lt(Object other) {
            return new BoolExpr(this, Op.LT, new ExprPart(String.valueOf(other))); }

        String toSQL() { return _e; }

        private final String _e;
    }

    public static class BoolExpr extends ExprPart {

        BoolExpr(ExprPart left, Op op, ExprPart right) {
            super(null);
            _left = left;
            _op = op;
            _right = right;
        }

        @Override
        String toSQL() {
            return String.format("(%s) %s (%s)", _left.toSQL(), _op, _right.toSQL());
        }

        private final ExprPart _left;
        private final Op _op;
        private final ExprPart _right;
    }

    /**
     * Represents database column definition. Declare NON-FINAL PUBLIC FIELDS WITHOUT of this type
     * and annotate them with {@link Column} and other annotations from above to declare a column.
     * Instance value will be assigned upon {@link TableDef} table construction.
     */
    public static final class ColumnDef implements Comparable<ColumnDef> {

        public static final class ForeignKey {

            public ForeignKey(String table, String column, ForeignKeyAction onDelete,
                    ForeignKeyAction onUpdate) {
                this.table = table;
                this.column = column;
                this.onDelete = onDelete;
                this.onUpdate = onUpdate;
            }

            public final String table;
            public final String column;
            public final ForeignKeyAction onDelete;
            public final ForeignKeyAction onUpdate;


            public String toString() {
                String result = String.format("REFERENCES %s(%s)", table, column);
                if (onDelete != null)
                    result = result + " ON DELETE " + onDelete;
                if (onUpdate != null)
                    result = result + " ON UPDATE " + onUpdate;
                return result;
            }
        }

        public ColumnDef(ColumnDef template, TableDef parentTable) {
            this(parentTable, template.index, template.name, template.type, template.indexName,
                    template.foreignKey);
        }

        public ColumnDef(int index, String name, String type, String indexName, ForeignKey
                foreignKey) {
            this(null, index, name, type, indexName, foreignKey);
        }

        public ColumnDef(TableDef parentTable, int index, String name, String type, String
                indexName, ForeignKey foreignKey) {
            this.parentTable = parentTable;
            this.index = index;
            this.name = name;
            this.type = type;
            this.indexName =
                    (parentTable != null && !parentTable.uniqueSchema && indexName != null) ?
                            parentTable.tableName + "_" + indexName :
                            indexName;
            this.foreignKey = foreignKey;
            this.qName = parentTable + "." + name;
        }

        @Override
        public int compareTo(final ColumnDef o) {
            return qName.compareTo(o.qName);
        }

        public final TableDef parentTable;
        public final int index;
        public final String name;
        public final String qName;
        public final String type;
        public final String indexName;
        public final ForeignKey foreignKey;

        public ExprPart expr() {
            return new ExprPart(this.qName);
        }

        public SelectResult select() {
            final ArrayList<ColumnDef> cs = new ArrayList<>(1);
            cs.add(this);
            return new SelectResult(cs);
        }

        public JoinResult join(ColumnDef other) {
            return join(other, JoinResult.Kind.INNER);
        }

        public JoinResult join(ColumnDef other, JoinResult.Kind kind) {
            return parentTable.join(other.parentTable, kind).on(expr().eq(other.expr()));
        }

        @Override
        public String toString() { return qName; }

        String toDefString() {
            return String.format("%s %s %s", name, type, foreignKey != null ? foreignKey : "");
        }

        static String toDefsString(Collection<ColumnDef> defs) {
            List<String> strDefs = new ArrayList<>(defs.size());
            for (ColumnDef def : defs) {
                strDefs.add(def.toDefString());
            }
            return joinStrings(", ", strDefs);
        }
    }

    public abstract static class TableDef extends ColumnSource {
        public TableDef(String name) {
            this(name, true);
        }

        /**
         * @param unique Flag, allowing shorter column index names. For non-unique tables index
         *               names are prepended with table names.
         */
        public TableDef(String name, boolean unique) {
            uniqueSchema = unique;
            this.tableName = name;

            final Map<String, ColumnDef> columnFields =
                    TableDef.collectEffectiveColumns(getClass());
            final Class<? extends TableDef> ownClass = getClass();

            columns = new LinkedHashMap<>(columnFields.size());
            for (Map.Entry<String, ColumnDef> entry : columnFields.entrySet()) {
                final ColumnDef boundColumn = new ColumnDef(entry.getValue(), this);
                columns.put(boundColumn.name, boundColumn);
                try {
                    final Field colField = ownClass.getField(entry.getKey());
                    colField.set(this, boundColumn);
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        public final boolean uniqueSchema;
        public final String tableName;
        // Collected preserving declared index order (ascending).
        public final Map<String, ColumnDef> columns;

        @Override
        public String toString() { return tableName; }

        // ColumnsSource
        @Override
        public String toSQL() {
            return tableName;
        }

        public String getCreateExpr() {
            return String.format("CREATE TABLE %s (%s);", tableName,
                    SchemaDecl.ColumnDef.toDefsString(columns.values()));
        }

        private static class ColumnAnnotations {
            ColumnAnnotations(Column col, Index index, ForeignKey key) {
                this.column = col;
                this.index = index;
                this.key = key;
            }

            final Column column;
            final Index index;
            final ForeignKey key;
        }

        private static Comparator<Pair<String, ColumnAnnotations>> columnAnnosComparator =
                new Comparator<Pair<String, ColumnAnnotations>>() {
            @Override
            public int compare(
                    final Pair<String, ColumnAnnotations> o1,
                    final Pair<String, ColumnAnnotations> o2) {
                return o1.second.column.index() - o2.second.column.index();
            }
        };

        public static Map<String, ColumnDef> collectNewColumns(
                final Class oldTableClass,
                final Class<? extends TableDef> newTableClass) {
            final Map<String, ColumnDef> oldColumns = collectEffectiveColumns(oldTableClass);
            final Map<String, ColumnDef> newColumns = collectEffectiveColumns(newTableClass);
            newColumns.keySet().removeAll(oldColumns.keySet());
            return newColumns;
        }

        /**
         * Collects declared columns for the whole table version hierarchy.
         * @param tableClass table class to inspect.
         * @return see collectDeclaredColumns |result| argument.
         */
        private static Map<String, ColumnDef> collectEffectiveColumns(Class tableClass) {
            Map<String, ColumnDef> result = new LinkedHashMap<>();
            while (!TableDef.class.equals(tableClass) &&
                    TableDef.class.isAssignableFrom(tableClass)) {
                collectDeclaredColumns(tableClass, result);
                tableClass = tableClass.getSuperclass();
            }
            return result;
        }

        /**
         * Collects fields annotated as database columns. See {@link Column} column annotation for
         * more info.
         * @param schemaClass table class to inspect.
         * @param result Mapping from field name to column definition, receiving declarations found.
         *               Columns are inserted in ascending order of their declared indices.
         *               NOTE: Existing entries are NOT overwritten.
         */
        private static void collectDeclaredColumns(Class schemaClass,
                final Map<String, ColumnDef> result) {
            // Collect all declared columns.
            ArrayList<Pair<String, ColumnAnnotations>> columnFields = new ArrayList<>();
            for (Field field : schemaClass.getDeclaredFields()) {
                final int fieldMods = field.getModifiers();
                if (Modifier.isStatic(fieldMods) || !Modifier.isPublic(fieldMods) ||
                        !ColumnDef.class.isAssignableFrom(field.getType()))
                    continue;
                final Column columnAnno = field.getAnnotation(Column.class);
                if (columnAnno == null || result.containsKey(field.getName()))
                    continue;

                columnFields.add(new Pair<>(field.getName(),
                        new ColumnAnnotations(
                                columnAnno,
                                field.getAnnotation(Index.class),
                                field.getAnnotation(ForeignKey.class)
                        )
                ));
            }

            // Sort them to restore declared order.
            Collections.sort(columnFields, columnAnnosComparator);

            // Add to resulting map.
            for (Pair<String, ColumnAnnotations> entry : columnFields) {
                final String fieldName = entry.first;
                final ColumnAnnotations annos = entry.second;
                String columnName = annos.column.name();
                if (columnName.isEmpty())
                    columnName = fieldName;
                final String indexName = annos.index != null ? annos.index.name() : null;
                final ForeignKey keyAnno = annos.key;
                final ColumnDef.ForeignKey keyDef = keyAnno == null ? null :
                        new ColumnDef.ForeignKey(keyAnno.table(), keyAnno.column(),
                                keyAnno.onDelete(), keyAnno.onUpdate());
                final ColumnDef column = new ColumnDef(
                        annos.column.index(),
                        columnName,
                        annos.column.type(),
                        indexName,
                        keyDef);
                result.put(fieldName, column);
            }
        }
    }

    /**
     * Helper class to declare a version of database schema. Use inheritance to create updated
     * versions.
     * Once a new subclass is defined and published it's supposed to be frozen (in terms of DB
     * schema) in order to guarantee, that at any time the latest version of the app knows how to
     * upgrade from all previous versions.
     * It is also crucial for automated testing of database upgrade process.
     * Tables are declared as public static fields of {@link TableDef} table type (or subclass).
     * All such fields are automatically collected through reflection and used to created needed
     * tables.
     */
    public static class DatabaseSchema {
        public static Map<String, TableDef> collectNewTables(
                final Class<? extends DatabaseSchema> oldSchemaClass,
                final Class<? extends DatabaseSchema> newSchemaClass)
                throws Exception {
            final Map<String, TableDef> oldTables = collectEffectiveTables(oldSchemaClass);
            final Map<String, TableDef> newTables = collectEffectiveTables(newSchemaClass);
            newTables.keySet().removeAll(oldTables.keySet());
            return newTables;
        }

        public static Map<String, TableDef> collectEffectiveTables(Class schemaClass)
                throws Exception {
            Map<String, TableDef> result = new LinkedHashMap<>();
            while (!DatabaseSchema.class.equals(schemaClass) &&
                    DatabaseSchema.class.isAssignableFrom(schemaClass)) {
                final Map<String, TableDef> schemaOwnTables = collectDeclaredTables(schemaClass);
                // Remove older versions of tables.
                schemaOwnTables.keySet().removeAll(result.keySet());
                result.putAll(schemaOwnTables);
                schemaClass = schemaClass.getSuperclass();
            }
            return result;
        }

        // NOTE: Declared tables are returned out of any order!
        private static Map<String, TableDef> collectDeclaredTables(Class schemaClass)
                throws Exception {
            Map<String, TableDef> result = new LinkedHashMap<>();
            for (Field field : schemaClass.getDeclaredFields()) {
                final int fieldMods = field.getModifiers();
                if (!Modifier.isStatic(fieldMods) || !Modifier.isPublic(fieldMods) ||
                        !TableDef.class.isAssignableFrom(field.getType()))
                    continue;

                final TableDef declaredTable = (TableDef) field.get(null);
                if (!result.containsKey(declaredTable.tableName))
                    result.put(declaredTable.tableName, declaredTable);
            }
            return result;
        }
    }
}

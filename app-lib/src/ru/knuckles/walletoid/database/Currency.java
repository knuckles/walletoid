/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.ContentValues;

import java.util.Locale;

import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_CURRENCIES;

public class Currency {
    // Use this ID for new currencies if they're not yet inserted to a database.
    public final static long UNKNOWN_ID = -1;

    public Currency() {
        this("", "", 1.0);
    }

    public Currency(String name, String symbol, double value) {
        this(UNKNOWN_ID, name, symbol, value);
    }

    public Currency(long id, String name, String symbol, double value) {
        if (name == null) throw new AssertionError();
        if (symbol == null) throw new AssertionError();
        _id = id;
        _name = name;
        _symbol = symbol;
        _value = value;
    }

    public Currency(Currency from, long id) {
        this(id, from._name, from._symbol, from._value);
    }

    @Override
    public String toString() {
        return String.format(Locale.ROOT, "%s (%s)", _name, _symbol);
    }

    public long getId() { return _id; }

    public String getName() { return _name; }

    public String getSymbol() { return _symbol; }

    public double getValue() { return _value; }

    public double setValue(double value) { return _value = value; }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(TBL_CURRENCIES.curr_name.name, _name);
        values.put(TBL_CURRENCIES.curr_symbol.name, _symbol);
        values.put(TBL_CURRENCIES.value.name, _value);
        return values;
    }

    public boolean similarTo(Currency other) {
        return
                _name.equals(other._name) &&
                _symbol.equals(other._symbol) &&
                _value == other._value;
    }

    private final long _id;
    private final String _name;
    private final String _symbol;

    private double _value;
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import ru.knuckles.walletoid.Utils;
import ru.knuckles.walletoid.database.SchemaDecl.ColumnDef;

import java.util.*;

import static ru.knuckles.walletoid.Utils.joinStrings;
import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_CATEGORIES;
import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_RECORDS;
import static ru.knuckles.walletoid.database.WalletSchema_v7.TBL_LINKS;

// TODO: use android.database.sqlite.SQLiteQueryBuilder instead of some of these.
class SqlTemplates {

    static String INSERT_INTO(String table, Collection<String> columns, String[] values) {
        return INSERT_INTO(table, columns, values, false);
    }

    static String INSERT_INTO(String table, Collection<String> columns, String[] values,
            boolean allowReplace) {
        List<String> ins_list = values != null ? Arrays.asList(values) : new ArrayList<>();
        while (ins_list.size() < columns.size()) {
            ins_list.add("?");
        }
        if (ins_list.size() > columns.size()) {
            ins_list = ins_list.subList(0, columns.size());
        }
        return String.format("INSERT %s INTO %s (%s) VALUES (%s);",
                allowReplace ? "OR REPLACE" : "",
                table,
                ru.knuckles.walletoid.Utils.joinStrings(columns),
                Utils.joinStrings(ins_list));
    }

    static String INSERT_SELECT(String in_table, Collection<String> columns, String from_table, String where) {
        return INSERT_SELECT(in_table, columns, from_table, columns, where);
    }

    static String INSERT_SELECT(
            String in_table, Collection<String> in_columns,
            String from_table, Collection<String> from_columns,
            String where) {
        return String.format(
                "INSERT INTO %s(%s) SELECT %s FROM %s %s",
                in_table,
                joinStrings(in_columns),
                joinStrings(from_columns),
                from_table,
                where != null ? where : "");
    }

    static String CREATE_INDEX(String name, String table, String column) {
        return String.format("CREATE INDEX %s ON %s (%s)", name, table, column);
    }

    // TODO: Get rid in favor of SQLiteQueryBuilder.buildQueryString
    static String SELECT_FROM_WHERE(String[] columns, String table, List<String> where) {
        return SELECT_FROM_WHERE(columns, table, joinStrings(" AND ", where));
    }

    static String SELECT_FROM_WHERE(String[] columns, String table, String whereExpr) {
        String q = String.format(
            "SELECT %s FROM %s",
            columns == null ? "*" : Utils.joinStrings(columns),
            table);
        if (whereExpr != null && !whereExpr.isEmpty())
            q += " WHERE " + whereExpr;
        return q;
    }

    static String INNER_JOIN(ColumnDef column1, ColumnDef column2) {
        return INNER_JOIN(
                column1.parentTable.tableName, column1.qName,
                column2.parentTable.tableName, column2.qName
        );
    }

    static String INNER_JOIN(String table1, String column1, String table2, String column2) {
        return String.format("(%1$s INNER JOIN %2$s %3$s)", table1, table2, ON(column1, column2));
    }

    static String ON(String column1, String column2) {
        return " ON " + column1 + "=" + column2;
    }

    static String BIGGEST_ROWID(String table, String column) {
        return String.format("SELECT %s FROM %s ORDER BY %s DESC limit 1", column, table, column);
    }

    static String WHERE_RAW(Collection<String> tokens) {
        return tokens.isEmpty() ? "" : joinStrings(" AND ", tokens);
    }

    static String FORMAT_TIMESTAMP(String timestampExpr, String format) {
        return String.format("strftime('%s', %s, 'unixepoch')", format, timestampExpr);
    }

    static String BRACE(String subQ) {
        return "(" + subQ + ")";
    }

    static String AS(String subQ, String alias) {
        return subQ + " as " + alias;
    }

    static String LIMIT_OFFSET(int limit, int offset) {
        if (limit <= 0 && offset <= 0)
            return "";
        return String.format(Locale.ROOT, "%d, %d", offset, limit);
    }

    static List<String> makeRecordsFilter(WalletFilter filter) {
        // TODO: Optimize by returning null in case of absence of interesting filters.
        List<String> tokens = new ArrayList<>();
        if (filter == null)
            return tokens;
        if (filter.getDateRangeStart() != null)
            tokens.add(TBL_RECORDS.date_time.qName + ">=" + filter.getDateRangeStart().getTimeInMillis());
        if (filter.getDateRangeEnd() != null)
            tokens.add(TBL_RECORDS.date_time.qName + "<" + filter.getDateRangeEnd().getTimeInMillis());
        if (filter.getCategoryID() != Category.UNKNOWN_ID)
            tokens.add(TBL_RECORDS.cat_id.qName + "=" + filter.getCategoryID());
        if (filter.getRecordId() != Record.UNKNOWN_ID)
            tokens.add(TBL_RECORDS.rec_id.qName + "=" + filter.getRecordId());
        return tokens;
    }

    static String UPDATE_STMT(ColumnDef[] columns) {
        if (columns == null || columns.length < 1)
            throw new IllegalArgumentException();
        final SchemaDecl.TableDef table = columns[0].parentTable;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(table.tableName).append(" SET ");
        boolean firstCol = true;
        for (ColumnDef cd : columns) {
            if (firstCol)
                firstCol = false;
            else
                sb.append(", ");
            sb.append(cd.name).append(" = ?");
        }

        return sb.toString();
    }

    static List<String> makeCatKindFilter(WalletFilter filter) {
        if (filter == null || filter.getCategoriesKind() == Category.Kind.ALL)
            return null;
        final List<String> result = new ArrayList<>();
        final boolean income = filter.getCategoriesKind() == Category.Kind.INCOME;
        result.add(String.format(Locale.ROOT, "%s = %d",
                TBL_CATEGORIES.is_income.name, income ? 1 : 0));
        return result;
    }

    static List<String> makeLinksFilter(WalletFilter filter) {
        if (filter == null)
            return null;
        return makeLinksFilter(filter.getMasterID());
    }

    static List<String> makeLinksFilter(long masterId) {
        if (masterId == Record.UNKNOWN_ID)
            return null;
        final List<String> result = new ArrayList<>();
        result.add(TBL_LINKS.master_id.name + "=" + masterId);
        return result;
    }
}

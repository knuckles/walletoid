/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import ru.knuckles.walletoid.BuildConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ru.knuckles.walletoid.database.Utils.createTable;
import static ru.knuckles.walletoid.database.Utils.dumpMasterDefs;

class WalletDBHelper extends SQLiteOpenHelper {

    /**
     * This class is a base for all ever existing and future upcoming versions of database schema
     * backing the Wallet.
     * Every subclass must have only a default constructor, as it's the only on used for
     * instantiation.
     */
    static abstract class WalletDBSchema extends SchemaDecl.DatabaseSchema {
        public abstract int getVersion();

        /**
         * Called when database is first created or needs schema upgrade.
         * Override, if special handling (like table re-creation) is required.
         * Don't create normal tables or indices here, declare them as public static fields instead.
         * The first thing you MUST do here is to call superclass! Otherwise upgrade sequence
         * will be broken.
         * DO NOT INSERT DATA INTO ANY TABLES AT THIS POINT. USE {@link #populate} INSTEAD.
         */
        public void setup(Context context, SQLiteDatabase db, int oldVersion) {}

        // TODO: Each schema should run setup/populate separately from its newer versions.
        // Currently, older versions |populate| implicitly depend on all future modifications of
        // tables it writes to.
        /**
         * Insert any initial data here only. The reason is to have full schema initialization
         * before insertion to allow triggers and indexes to come into effect.
         * @param allowExtraData Whether any additional data, apart from critical, is allowed.
         */
        public void populate(Context context, SQLiteDatabase db, int oldVersion,
                boolean allowExtraData) {}

        /**
         * Called right after database is open. Setup things that are not persistent (views, temp
         * indices, etc).
         */
        public void onOpen(SQLiteDatabase db) {}
    }

    WalletDBHelper(Context context, String DBName, boolean prepopulate) {
        this(context, DBName, SCHEMAS.size(), prepopulate);
    }

    WalletDBHelper(Context context, String DBName, int version, boolean prepopulate) {
        super(context, DBName, null, version);
        _context = context;
        _schema = getSchema(version);
        if (_schema == null || _schema.getVersion() != version)
            throw new IllegalStateException();
        _prepopulate = prepopulate;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        printTableDefs(db);
        _schema.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Map<String, SchemaDecl.TableDef> tables = null;
        try {
            tables = SchemaDecl.DatabaseSchema.collectEffectiveTables(_schema.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

        createSchema(db, tables, 0);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion != _schema.getVersion())
            throw new AssertionError();
        Map<String, SchemaDecl.TableDef> newTables = null;
        try {
            newTables = SchemaDecl.DatabaseSchema.collectNewTables(SCHEMAS.get(oldVersion - 1),
                    _schema.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

        createSchema(db, newTables, oldVersion);
    }

    private void createSchema(SQLiteDatabase db, Map<String, SchemaDecl.TableDef> tables,
            int oldVersion) {
        maybeCreateTables(db, tables);
        _schema.setup(_context, db, oldVersion);
        _schema.populate(_context, db, oldVersion, _prepopulate);
    }

    private static void maybeCreateTables(SQLiteDatabase db,
            Map<String, SchemaDecl.TableDef> tables) {
        if (tables == null)
            return;

        for (SchemaDecl.TableDef tableDef : tables.values()) {
            createTable(db, tableDef);
        }
    }

    public static int schemasCount() {
        return SCHEMAS.size();
    }

    private static WalletDBSchema getSchema(int version) {
        final Class<? extends WalletDBSchema> schemaClass = SCHEMAS.get(version - 1);
        try {
            return schemaClass.getConstructor().newInstance();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // A list of registered Wallet schemas. Must be sorted by version in ascending order!
    private final static List<Class<? extends WalletDBSchema>> SCHEMAS =
            new ArrayList<Class<? extends WalletDBSchema>>() {{
        add(WalletSchema_v1.class);
        add(WalletSchema_v2.class);
        add(WalletSchema_v3.class);
        add(WalletSchema_v4.class);
        add(WalletSchema_v5.class);
        add(WalletSchema_v6.class);
        add(WalletSchema_v7.class);
    }};

    private void printTableDefs(final SQLiteDatabase db) {
        if (!BuildConfig.DEBUG)
            return;
        for (String line : dumpMasterDefs(db)) {
            Log.v(getClass().getSimpleName(), "DB defs: " + line);
        }
    }

    private final WalletDBSchema _schema;
    private final Context _context;
    private final Boolean _prepopulate;
}

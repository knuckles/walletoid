/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import ru.knuckles.walletoid.CalendarUtils;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class WalletFilter {
    public interface FilterObserver {
        void onFilterChanged(WalletFilter filter);
    }

    public WalletFilter() {
        this(null, null);
    }

    public WalletFilter(long recordId) {
        this();
        _recId = recordId;
    }

    // TODO: Use timestamps instead of Calendars.
    public WalletFilter(Calendar startDate, Calendar endDate) {
        this(startDate, endDate, Category.Kind.ALL, Category.UNKNOWN_ID);
    }

    public WalletFilter(Calendar startDate, Calendar endDate, Category.Kind categories) {
        this(startDate, endDate, categories, Category.UNKNOWN_ID);
    }

    public WalletFilter(Calendar startDate, Calendar endDate,
            Category.Kind categories, long categoryID) {
        _startDate = startDate;
        _endDate = endDate;
        _categories = categories;
        _categoryID = categoryID;
    }

    public WalletFilter(WalletFilter other) {
        this(other.getDateRangeStart(), other.getDateRangeEnd(), other._categories, other._categoryID);
        _revSorting = other._revSorting;
        _masterID = other._masterID;
        _limit = other._limit;
        _offset = other._offset;
        _query = other._query;
    }

    public void addObserver(FilterObserver observer) {
        _observers.add(observer);
    }

    // TODO: Update clients to unsubscribe.
    public void removeObserver(FilterObserver observer) {
        _observers.remove(observer);
    }

    public void setFilters(long startDateMillis, long endDateMillis, long categoryID) {
        _startDate = maybeCreateCalendar(startDateMillis);
        _endDate = maybeCreateCalendar(endDateMillis);
        _categoryID = categoryID;

        notifyObservers();
    }

    public void setRecordId(long recId) {
        if (_recId == recId)
            return;
        _recId = recId;
        notifyObservers();
    }

    public long getRecordId() { return _recId; }

    public void setDateRange(long startDateMillis, long endDateMillis) {
        setFilters(startDateMillis, endDateMillis, _categoryID);
    }

    public void setCategory(Category.Kind kind, long catID) {
        if (_categories == kind && _categoryID == catID)
            return;
        _categories = kind;
        _categoryID = catID;
        notifyObservers();
    }

    public void setMasterRecordID(long masterRecordID) {
        if (_masterID == masterRecordID)
            return;
        _masterID = masterRecordID;
        notifyObservers();
    }

    /**
     * @param offset Number of records to skip.
     * @param limit Upper limit of records to sum.
     */
    public void setLimitOffset(int limit, int offset) {
        if (_limit == limit && _offset == offset)
            return;
        _limit = limit;
        _offset = offset;
        notifyObservers();
    }

    public void resetLimitOffset() {
        setLimitOffset(-1, 0);
    }

    public void setQuery(String query) {
        _query = query == null || query.isEmpty() ? null : query;
        notifyObservers();
    }

    public Calendar getDateRangeStart() {
        return maybeCloneCalendar(_startDate);
    }

    public Calendar getDateRangeEnd() {
        return maybeCloneCalendar(_endDate);
    }

    public Category.Kind getCategoriesKind() {
        return _categories;
    }

    public long getCategoryID() {
        return _categoryID;
    }

    public long getMasterID() {
        return _masterID;
    }

    public int getOffset() {
        return _offset;
    }

    public int getLimit() {
        return _limit;
    }

    public String getQuery() { return _query; }

    public boolean isRevSorting() { return _revSorting; }

    public void setRevSorting(boolean revSorting) {
        if (_revSorting == revSorting)
            return;
        _revSorting = revSorting;
        notifyObservers();
    }

    private void notifyObservers() {
        for (FilterObserver observer : _observers) {
            observer.onFilterChanged(this);
        }
    }

    private static Calendar maybeCloneCalendar(Calendar calendar) {
        return calendar != null ? (Calendar) calendar.clone() : null;
    }

    private static Calendar maybeCreateCalendar(long timestamp) {
        if (timestamp == 0)
            return null;
        return CalendarUtils.Cal(timestamp);
    }

    private Calendar _startDate;
    private Calendar _endDate;
    private Category.Kind _categories = Category.Kind.ALL;
    private long _categoryID = Category.UNKNOWN_ID;
    private long _recId = Record.UNKNOWN_ID;
    private long _masterID = Record.UNKNOWN_ID;
    private int _offset = 0;
    private int _limit = -1;
    private String _query = null;
    private boolean _revSorting = false;

    private final Set<FilterObserver> _observers = new TreeSet<>(new Comparator<FilterObserver>() {
        @Override
        public int compare(FilterObserver lhs, FilterObserver rhs) {
            return lhs.hashCode() - rhs.hashCode();
        }
    });
}

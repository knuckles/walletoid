/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Pair;
import ru.knuckles.walletoid.R;

import java.util.ArrayList;
import java.util.List;

import static ru.knuckles.walletoid.database.SchemaDecl.ColumnDef;
import static ru.knuckles.walletoid.database.SchemaDecl.TableDef;
import static ru.knuckles.walletoid.database.SqlTemplates.INSERT_INTO;

public class WalletSchema_v1 extends WalletDBHelper.WalletDBSchema {
    @Override
    public int getVersion() {
        return VERSION;
    }

    /**
     * This table holds description of record categories.
     * A category has a name, a kind (income or expense) and a color. Plus its ID of course.
     */
    public static class CategoriesTable extends TableDef {
        CategoriesTable() {
            super("categories");
        }

        // NOTE: All NOT NULLs were added some time later.

        @SchemaDecl.Column(index = 0,  type = "INTEGER PRIMARY KEY")
        public ColumnDef cat_id;

        @SchemaDecl.Column(index = 1, type = "SMALLINT NOT NULL")
        public ColumnDef is_income;

        @SchemaDecl.Column(index = 2, type = "CHARACTER", name = "name")
        public ColumnDef cat_name;

        @SchemaDecl.Column(index = 3, type = "INTEGER NOT NULL")
        public ColumnDef color;

    }
    public static final CategoriesTable TBL_CATEGORIES = new CategoriesTable();

    /**
     * This table holds the most valuable data for Walletoid - records.
     * Every record goes to some category (described below) and has (besides its ID) a timestamp, a sum,
     * and a textual description.
     */
    public static class RecordsTable extends TableDef {
        RecordsTable() {
            super("records");
        }

        // NOTE: All NOT NULLs were added some time later.

        @SchemaDecl.Column(index = 0, type = "INTEGER PRIMARY KEY")
        public ColumnDef rec_id;

        @SchemaDecl.Column(index = 1, type = "DATETIME DEFAULT CURRENT_TIMESTAMP")
        public ColumnDef date_time;

        @SchemaDecl.Column(index = 2, type = "INTEGER NOT NULL")
        public ColumnDef cat_id;

        @SchemaDecl.Column(index = 3, type = "DECIMAL(20, 4) NOT NULL")
        public ColumnDef sum;

        @SchemaDecl.Column(index = 4, type = "CHARACTER")
        public ColumnDef descr;
    }
    public static final RecordsTable TBL_RECORDS = new RecordsTable();

    @Override
    public void populate(Context context, SQLiteDatabase db, int oldVersion,
            boolean allowExtraData) {
        if (oldVersion >= VERSION || !allowExtraData)
            return;

        // Insert predefined categories.
        final Resources ctxResources = context.getResources();
        SQLiteStatement insStatement = db.compileStatement(
            INSERT_INTO(
                TBL_CATEGORIES.tableName,
                new ArrayList<String>() {{
                    add(TBL_CATEGORIES.is_income.name);
                    add(TBL_CATEGORIES.cat_name.name);
                    add(TBL_CATEGORIES.color.name);
                }},
                null));

        for (Pair<Integer, Integer> incCatResPair : INCOME_CATS) {
            insStatement.bindLong(1, RAW_INCOME);
            insStatement.bindString(2, ctxResources.getString(incCatResPair.first));
            insStatement.bindLong(3, ctxResources.getColor(incCatResPair.second));
            insStatement.executeInsert();
        }

        for (Pair<Integer, Integer> incCatResPair : EXPENSE_CATS) {
            insStatement.bindLong(1, RAW_EXPENSE);
            insStatement.bindString(2, ctxResources.getString(incCatResPair.first));
            insStatement.bindLong(3, ctxResources.getColor(incCatResPair.second));
            insStatement.executeInsert();
        }
    }

    protected boolean freshDB(int oldVersion) { return oldVersion == 0; }
    protected boolean upgrading(int oldVersion) { return !freshDB(oldVersion); }

    protected final static long RAW_EXPENSE = 0L;
    protected final static long RAW_INCOME = 1L;

    protected final static List<Pair<Integer, Integer>> INCOME_CATS =
            new ArrayList<Pair<Integer, Integer>>() {{
        add(new Pair<>(R.string.cats_salary, R.color.cats_salary));
        add(new Pair<>(R.string.cats_interest, R.color.cats_interest));
    }};

    protected final static List<Pair<Integer, Integer>> EXPENSE_CATS =
            new ArrayList<Pair<Integer, Integer>>() {{
        add(new Pair<>(R.string.cats_food, R.color.cats_food));
        add(new Pair<>(R.string.cats_clothes, R.color.cats_clothes));
        add(new Pair<>(R.string.cats_e8ment, R.color.cats_e8ment));
        add(new Pair<>(R.string.cats_transport, R.color.cats_transport));
        add(new Pair<>(R.string.cats_household, R.color.cats_household));
        add(new Pair<>(R.string.cats_health, R.color.cats_health));
        add(new Pair<>(R.string.cats_education, R.color.cats_education));
        add(new Pair<>(R.string.cats_communications, R.color.cats_communications));
    }};

    private static final int VERSION = 1;
}

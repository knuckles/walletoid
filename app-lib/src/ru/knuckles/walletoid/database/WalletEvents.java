/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class WalletEvents {
    /**
     * Subclass to handle notifications about {@link Wallet} updates.
     * NOTE: All methods are called on UI thread.
     * NOTE: No add/update notifications are fired when data is batch copied from another wallet or
     * changed due to database automatic constraints.
     */
    public static class RecordObserver extends Observer {
        public void onRecordAdded(long id) {}
        // NOTE: This also fires when linking to or unlinking from a master record!
        public void onRecordUpdated(Record oldValue) {}
        public void onRecordRemoved(Record oldValue) {}
    }

    public static class CategoryObserver extends Observer {
        public void onCategoryAdded(long id) {}
        public void onCategoryUpdated(long id) {}
        public void onCategoryRemoved(long removedCatId, long acceptorCatId) {}
    }

    public static class CurrencyObserver extends Observer {
        public void onCurrencyAdded(long id) {}
        public void onCurrencyUpdated(long id) {}
        public void onPrimaryCurrencySelected(long id) {}
        public void onCurrencyRemoved(long removedCurId, long acceptorCurId) {}
    }

    public static final int WE_RECORD_ADDED      = 1 << 0;
    public static final int WE_RECORD_UPDATED    = 1 << 1;
    public static final int WE_RECORD_REMOVED    = 1 << 2;
    public static final int WE_ALL_RECORD_EVT    =
            WE_RECORD_ADDED | WE_RECORD_UPDATED | WE_RECORD_REMOVED;

    public static final int WE_CATEGORY_ADDED    = 1 << 3;
    public static final int WE_CATEGORY_UPDATED  = 1 << 4;
    public static final int WE_CATEGORY_REMOVED  = 1 << 5;
    public static final int WE_ALL_CATEGORY_EVT  =
            WE_CATEGORY_ADDED | WE_CATEGORY_UPDATED | WE_CATEGORY_REMOVED;

    public static final int WE_CURRENCY_ADDED    = 1 << 6;
    public static final int WE_CURRENCY_UPDATED  = 1 << 7;
    public static final int WE_CURRENCY_REMOVED  = 1 << 8;
    public static final int WE_CURRENCY_PRIMARY  = 1 << 9;
    public static final int WE_ALL_CURRENCY_EVT  =
            WE_CURRENCY_ADDED | WE_CURRENCY_UPDATED | WE_CURRENCY_REMOVED | WE_CURRENCY_PRIMARY;

    public static final int WE_BLK_REC_CAT_UPD   = 1 << 10;
    public static final int WE_BLK_REC_CUR_UPD   = 1 << 11;
    public static final int WE_BLK_REC_REMOVAL   = 1 << 12;
    // NOTE: Fired on primary currency change, as it requires relative values recalculation.
    public static final int WE_BLK_CURRENCY_UPD  = 1 << 13;
    public static final int WE_BLK_COPY_COMPLETE = 1 << 14;
    public static final int WE_ALL_BULK_EVT =
            WE_BLK_REC_CAT_UPD | WE_BLK_REC_CUR_UPD | WE_BLK_REC_REMOVAL | WE_BLK_CURRENCY_UPD |
            WE_BLK_COPY_COMPLETE;

    public static final int WE_ALL_EVENTS =
            WE_ALL_RECORD_EVT | WE_ALL_CATEGORY_EVT | WE_ALL_CURRENCY_EVT | WE_ALL_BULK_EVT;

    public static abstract class AnyEventObserver {
        final long listenedEventFlags;

        protected AnyEventObserver(long listenedEventFlags) {
            this.listenedEventFlags = listenedEventFlags;
        }

        protected abstract void onWalletChanged(final long eventFlag);
    }

    public static class BulkObserver extends Observer {
        // At most this number of records will be copied and passed to bulk change observers.
        public static final int MAX_BACKED_UP_RECORDS = 100;

        /**
         * Fired after performing a bulk record update. See {@link Wallet#updateRecords}.
         * @param records List of removed or updated (state before update) records.
         *                Capped to {@link #MAX_BACKED_UP_RECORDS} items.
         *                If it's unclear how many records will be touched, or their number
         *                exceeds  maximum, this argument is null.
         */
        public void onBulkCategorySet(List<Record> records, long newCatId) {}
        public void onBulkCurrencySet(List<Record> records, long newCurrencyId) {}
        public void onBulkRecordsRemoval(List<Record> records) {}

        /**
         * Fired after performing a bulk currencies update. See
         * {@link Wallet#setReferenceCurrencyId}.
         */
        public void onCurrenciesUpdated() {}

        /**
         * Fired after a wallet content was copied using {@link Wallet#copyFrom}
         */
        public void onCopyCompleted() {}
    }

    WalletEvents() {
    }

    public void clear() {
        _recObservers.clear();
        _catObservers.clear();
        _curObservers.clear();
        _blkObservers.clear();
        _anyObservers.clear();
    }

    public void addRecordObserver(RecordObserver observer) {
        _recObservers.add(observer);
    }

    public void removeRecordObserver(RecordObserver observer) {
        _recObservers.remove(observer);
    }

    public void addCategoryObserver(CategoryObserver observer) {
        _catObservers.add(observer);
    }

    public void removeCategoryObserver(CategoryObserver observer) {
        _catObservers.remove(observer);
    }

    public void addCurrencyObserver(CurrencyObserver observer) {
        _curObservers.add(observer);
    }

    public void removeCurrencyObserver(CurrencyObserver observer) {
        _curObservers.remove(observer);
    }

    public void addBulkObserver(BulkObserver observer) {
        _blkObservers.add(observer);
    }

    public void removeBulkObserver(BulkObserver observer) {
        _blkObservers.remove(observer);
    }

    public void addAnyEventObserver(AnyEventObserver observer) {
        _anyObservers.add(new Pair<>(observer, observer.listenedEventFlags));
    }

    public void removeAnyEventObserver(AnyEventObserver observer) {
        _anyObservers.remove(new Pair<>(observer, observer.listenedEventFlags));
    }

    private abstract static class Observer {}

    abstract class Notifier <O extends Observer, S, E> {
        protected Notifier(long eventFlag) {
            _eventFlag = eventFlag;
            _handler = new Handler(Looper.getMainLooper());
        }

        boolean hasObservers() {
            return !getObservers().isEmpty();
        }

        void notifyObservers(S subject, E extra) {
            _handler.post(new Runnable() {
                @Override
                public void run() {
                    notifyObserversOnUIThread(subject, extra);
                }
            });
        }

        private void notifyObserversOnUIThread(S subject, E extra) {
            for (O observer : getObservers()) {
                try {
                    doNotify(observer, subject, extra);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            for (Pair<AnyEventObserver, Long> observer_flags : _anyObservers) {
                if ((_eventFlag & observer_flags.second) == 0)
                    continue;
                try {
                    observer_flags.first.onWalletChanged(_eventFlag);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        protected abstract Set<O> getObservers();
        protected abstract void doNotify(O observer, S subject, E extra);

        protected final long _eventFlag;
        private final Handler _handler;
    }

    abstract class NoArgNotifier<O extends Observer> extends Notifier<O, Object, Object> {
        NoArgNotifier(long eventFlag) {
            super(eventFlag);
        }

        public void notifyObservers() {
            notifyObservers(null, null);
        }

        protected void doNotify(O observer, Object subject, Object extra) {
            doNotify0(observer);
        }

        protected abstract void doNotify0(O observer);
    }

    abstract class OneArgNotifier<O extends Observer, S> extends Notifier<O, S, Object> {
        OneArgNotifier(long eventFlag) {
            super(eventFlag);
        }

        protected void doNotify(O observer, S subject, Object extra) {
            doNotify1(observer, subject);
        }

        protected abstract void doNotify1(O observer, S subject);
    }

    abstract class TwoArgNotifier<O extends Observer, S, E> extends Notifier<O, S, E> {
        TwoArgNotifier(long eventFlag) {
            super(eventFlag);
        }

        protected void doNotify(O observer, S subject, E extra) {
            doNotify2(observer, subject, extra);
        }

        protected abstract void doNotify2(O observer, S subject, E extra);
    }

    private abstract class RecordNotifier<T> extends OneArgNotifier<RecordObserver, T> {
        RecordNotifier(long eventFlag) {
            super(eventFlag);
        }

        @Override
        protected Set<RecordObserver> getObservers() {
            return _recObservers;
        }
    }

    final OneArgNotifier<RecordObserver, Long> recAddNotifier =
            new RecordNotifier<Long>(WE_RECORD_ADDED) {
        @Override
        protected void doNotify1(RecordObserver observer, Long subject) {
            observer.onRecordAdded(subject);
        }
    };

    final OneArgNotifier<RecordObserver, Record> recUpdNotifier =
            new RecordNotifier<Record>(WE_RECORD_UPDATED) {
        @Override
        protected void doNotify1(RecordObserver observer, Record subject) {
            observer.onRecordUpdated(subject);
        }
    };

    final OneArgNotifier<RecordObserver, Record> recRemNotifier =
            new RecordNotifier<Record>(WE_RECORD_REMOVED) {
        @Override
        protected void doNotify1(RecordObserver observer, Record subject) {
            observer.onRecordRemoved(subject);
        }
    };

    private abstract class CategoryNotifier<T> extends OneArgNotifier<CategoryObserver, T> {
        CategoryNotifier(long eventFlag) {
            super(eventFlag);
        }

        @Override
        protected Set<CategoryObserver> getObservers() {
            return _catObservers;
        }
    }

    final OneArgNotifier<CategoryObserver, Long> catAddNotifier =
            new CategoryNotifier<Long>(WE_CATEGORY_ADDED) {
        @Override
        protected void doNotify1(CategoryObserver observer, Long id) {
            observer.onCategoryAdded(id);
        }
    };

    final OneArgNotifier<CategoryObserver, Long> catUpdNotifier =
            new CategoryNotifier<Long>(WE_CATEGORY_UPDATED) {
        @Override
        protected void doNotify1(CategoryObserver observer, Long id) {
            observer.onCategoryUpdated(id);
        }
    };

    final Notifier<CategoryObserver, Long, Long> catRemNotifier =
            new TwoArgNotifier<CategoryObserver, Long, Long>(WE_CATEGORY_REMOVED) {
        @Override
        protected Set<CategoryObserver> getObservers() {
            return _catObservers;
        }

        @Override
        protected void doNotify2(CategoryObserver observer, Long id, Long extra) {
            observer.onCategoryRemoved(id, extra);
        }
    };

    private abstract class CurrencyNotifier<T> extends OneArgNotifier<CurrencyObserver, T> {
        CurrencyNotifier(long eventFlag) {
            super(eventFlag);
        }

        @Override
        protected Set<CurrencyObserver> getObservers() {
            return _curObservers;
        }
    }

    final OneArgNotifier<CurrencyObserver, Long> currAddNotifier =
            new CurrencyNotifier<Long>(WE_CURRENCY_ADDED) {
        @Override
        protected void doNotify1(CurrencyObserver observer, Long id) {
            observer.onCurrencyAdded(id);
        }
    };

    final OneArgNotifier<CurrencyObserver, Long> currUpdNotifier =
            new CurrencyNotifier<Long>(WE_CURRENCY_UPDATED) {
        @Override
        protected void doNotify1(CurrencyObserver observer, Long id) {
            observer.onCurrencyUpdated(id);
        }
    };

    final OneArgNotifier<CurrencyObserver, Long> currPrimarySelectNotifier =
            new CurrencyNotifier<Long>(WE_CURRENCY_PRIMARY) {
        @Override
        protected void doNotify1(CurrencyObserver observer, Long id) {
            observer.onPrimaryCurrencySelected(id);
        }
    };

    final TwoArgNotifier<CurrencyObserver, Long, Long> currRemNotifier =
            new TwoArgNotifier<CurrencyObserver, Long, Long>(WE_CURRENCY_REMOVED) {
        @Override
        protected Set<CurrencyObserver> getObservers() {
            return _curObservers;
        }

        @Override
        protected void doNotify2(CurrencyObserver observer, Long id, Long extra) {
            observer.onCurrencyRemoved(id, extra);
        }
    };

    final NoArgNotifier<BulkObserver> currBulkUpdateNotifier =
            new NoArgNotifier<BulkObserver>(WE_BLK_CURRENCY_UPD) {
        @Override
        protected Set<BulkObserver> getObservers() {
            return _blkObservers;
        }

        @Override
        protected void doNotify0(BulkObserver observer) {
            observer.onCurrenciesUpdated();
        }
    };

    final TwoArgNotifier<BulkObserver, List<Record>, Long> recBulkCatSetNotifier =
            new TwoArgNotifier<BulkObserver, List<Record>, Long>(WE_BLK_REC_CAT_UPD) {
        @Override
        protected Set<BulkObserver> getObservers() {
            return _blkObservers;
        }

        @Override
        protected void doNotify2(BulkObserver observer, List<Record> records, Long newCatId) {
            observer.onBulkCategorySet(records, newCatId);
        }
    };

    final TwoArgNotifier<BulkObserver, List<Record>, Long> recBulkCurSetNotifier =
            new TwoArgNotifier<BulkObserver, List<Record>, Long>(WE_BLK_REC_CUR_UPD) {
        @Override
        protected Set<BulkObserver> getObservers() {
            return _blkObservers;
        }

        @Override
        protected void doNotify2(BulkObserver observer, List<Record> records, Long newCurrId) {
            observer.onBulkCurrencySet(records, newCurrId);
        }
    };

    final OneArgNotifier<BulkObserver, List<Record>> recBulkRemovalNotifier =
            new OneArgNotifier<BulkObserver, List<Record>>(WE_BLK_REC_REMOVAL) {
        @Override
        protected Set<BulkObserver> getObservers() {
            return _blkObservers;
        }

        @Override
        protected void doNotify1(BulkObserver observer, List<Record> records) {
            observer.onBulkRecordsRemoval(records);
        }
    };

    final NoArgNotifier<BulkObserver> copyNotifier = new NoArgNotifier<BulkObserver>(WE_BLK_COPY_COMPLETE) {
        @Override
        protected Set<BulkObserver> getObservers() {
            return _blkObservers;
        }

        @Override
        protected void doNotify0(BulkObserver observer) {
            observer.onCopyCompleted();
        }
    };

    private final static Comparator<Object> _hashComparator = new Comparator<Object>() {
        @Override
        public int compare(Object lhs, Object rhs) {
            return lhs.hashCode() - rhs.hashCode();
        }
    };

    private final Set<RecordObserver> _recObservers = new TreeSet<>(_hashComparator);
    private final Set<CategoryObserver> _catObservers = new TreeSet<>(_hashComparator);
    private final Set<CurrencyObserver> _curObservers = new TreeSet<>(_hashComparator);
    private final Set<BulkObserver> _blkObservers = new TreeSet<>(_hashComparator);
    private final Set<Pair<AnyEventObserver, Long>> _anyObservers = new TreeSet<>(_hashComparator);
}

/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import ru.knuckles.walletoid.FileUtils;
import ru.knuckles.walletoid.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class WalletManager {
    public interface WalletFactory {
        Wallet createWallet(Context context, String name, SQLiteDatabase db);
    }

    // TODO: Try to make private.
    public static final String MAIN_DB_NAME = "WalletDB";

    public WalletManager(Context context) {
        assert context != null;
        _context = context;
        _wallets = new HashMap<>();
    }

    public Context getContext() {
        return _context;
    }

    public Wallet getMainWallet(boolean prepopulate) {
        return getWallet(MAIN_DB_NAME, prepopulate, true);
    }

    public void releaseMainWallet() {
        releaseWallet(MAIN_DB_NAME);
    }

    public boolean walletExists(String walletName) {
        return getWalletPath(walletName).exists();
    }

    /**
     * Yields a Wallet instance, that will use database file named as walletName.
     * @param walletName name of the Wallet (and underlying file base part).
     *                   NOTE: There's a special name, {@link WalletManager#MAIN_DB_NAME},
     *                   that corresponds to main program database, that you'll want to use most of
     *                   the time.
     * @param prepopulate tells whether to insert some predefined data to a newly created database.
     *                    Should be true only when using (or emulating) main database.
     *                    NOTE: Database is not guaranteed to be completely empty if |false| is
     *                    passed here. Logically critical data will be inserted anyway.
     * @param withDictionary whether to initialize a {@link RecordDictionary} dictionary for this
     *                       Wallet.
     * @return Wallet. NOTE: instances with same names are shared, so 2 calls to getWallet with the
     * same name will return the same object.
     */
    public Wallet getWallet(String walletName, boolean prepopulate, boolean withDictionary) {
        if (_wallets.containsKey(walletName))
            return _wallets.get(walletName).addRef().wallet();

        final WalletDBHelper dbHelper = new WalletDBHelper(_context, walletName, prepopulate);
        final Wallet newWallet = _factory.createWallet(this.getContext(), walletName,
                dbHelper.getWritableDatabase());
        final RecordDictionary dict = withDictionary ?
                                      new RecordDictionary(newWallet, getDictPath(walletName)) :
                                      null;
        _wallets.put(walletName, new WalletCell(newWallet, dict));
        return newWallet;
    }

    /**
     * Decrease Wallet usage counter. When it reaches zero, the Wallet is closed, and noone must use
     * it anymore.
     * It's recommended to drop all references to the instance before releasing it.
     * @param walletName Name that you passed to {@link #getWallet}. You did passed it exactly this
     *                   name, right?
     * @return whether Wallet is now free: has no more (reported) references.
     */
    public boolean releaseWallet(String walletName) {
        if (!_wallets.containsKey(walletName))
            throw new IllegalArgumentException(ERR_NOT_OPEN + walletName);
        WalletCell cell = _wallets.get(walletName).release();
        if (cell.free())
            _wallets.remove(walletName);
        return cell.free();
    }

    /**
     * Same as {@link #releaseWallet}, but will also delete the underlying database file.
     * It's HIGHLY recommended to drop all references to the instance before closing it.
     */
    public void releaseWalletAndDeleteFile(String walletName) {
        if (!releaseWallet(walletName))
            throw new IllegalStateException(String.format("Wallet %s is still free!", walletName));
        deleteWalletData(walletName);
    }

    public void deleteWalletData(String walletName) {
        getWalletPath(walletName).delete();
        getDictPath(walletName).delete();
    }

    /**
     * Call {@link #releaseWalletAndDeleteFile} on each open Wallet, EXCEPT the main one.
     */
    void closeAndDeleteAll() {
        for (String walletName : _wallets.keySet()) {
            if (walletName.equals(MAIN_DB_NAME))
                continue;
            releaseWalletAndDeleteFile(walletName);
        }
    }

    /**
     * Gets you a {@link RecordDictionary} dictionary associated with a given wallet, if one was
     * created.
     */
    public RecordDictionary getWalletDictionary(String walletName) {
        if (!_wallets.containsKey(walletName))
            throw new IllegalArgumentException(ERR_NOT_OPEN + walletName);
        RecordDictionary dict = _wallets.get(walletName).dict();
        if (dict == null)
            throw new IllegalStateException(
                    String.format("Dictionary was not created for Wallet '%s'", walletName));
        return dict;
    }

    public boolean backupFileExists() {
        return getBackupFile().exists();
    }

    public boolean backupMainWallet() throws IOException {
        final File backupFile = getBackupFile();
        final File outDir = backupFile.getParentFile();
        outDir.mkdirs();
        if (!outDir.isDirectory())
            throw new FileNotFoundException(
                    "Could not create directory: " + outDir.getAbsolutePath());
        if (backupFile.exists())
            backupFile.delete();
        FileUtils.copyFile(getWalletPath(MAIN_DB_NAME), backupFile);
        return true;
    }

    public boolean restoreMainWallet() throws Exception {
        if (_wallets.containsKey(MAIN_DB_NAME))
            throw new Exception("Wallet is in use!");
        final File backupFile = getBackupFile();
        if (!backupFile.exists())
            return false;
        FileUtils.copyFile(backupFile, getWalletPath(MAIN_DB_NAME));
        if (!getDictPath(MAIN_DB_NAME).delete()) {
            Log.e(getClass().getSimpleName(),
                    String.format(Locale.ROOT,
                            "Could not delete dictionary file for Wallet '%s'", MAIN_DB_NAME));
        }
        return true;
    }

    public List<String> checkRefCounts() {
        List<String> leakedWalletNames = new ArrayList<>();
        for (WalletCell cell : _wallets.values()) {
            if (!cell.free())
                leakedWalletNames.add(String.format(Locale.ROOT,
                        "%s (%d)" , cell.wallet().getName(), cell.refCounter));
        }
        return leakedWalletNames;
    }

    public File getDictPath(String walletName) {
        return _context.getFileStreamPath(walletName + "-dict.bin");
    }

    private File getWalletPath(String walletName) {
        return _context.getDatabasePath(walletName);
    }

    private File getBackupFile() {
        return new File(Environment.getExternalStorageDirectory(),
            String.format("%1$s/%1$s.bak", _context.getString(R.string.app_id)));
    }

    private static class WalletCell {
        WalletCell(Wallet wallet, RecordDictionary dict) {
            if (wallet == null)
                throw new IllegalArgumentException();
            this._wallet = wallet;
            this._dict = dict;
        }

        Wallet wallet() { return _wallet; }
        RecordDictionary dict() { return _dict; }
        boolean free() { return refCounter < 1; }

        WalletCell addRef() {
            refCounter++;
            return this;
        }

        WalletCell release() {
            refCounter--;
            if (free())
                close();
            return this;
        }

        private void close() {
            if (_wallet != null) {
                _wallet.close();
                _wallet = null;
            }
            if (_dict != null) {
                _dict.close();
                _dict = null;
            }
        }

        private Wallet _wallet;
        private RecordDictionary _dict;
        long refCounter = 1;
    }

    private static final String ERR_NOT_OPEN = "Wallet not yet open: ";

    private final Map<String, WalletCell> _wallets;
    private final Context _context;
    private WalletFactory _factory = new WalletFactory() {
        @Override
        public Wallet createWallet(final Context context, final String name,
                final SQLiteDatabase db) {
            return new Wallet(context, name, db);
        }
    };
}

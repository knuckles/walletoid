/**
 * Copyright (c) 2015, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import ru.knuckles.walletoid.R;

/**
 * This schema adds 'misc' record category.
 */
public class WalletSchema_v2 extends WalletSchema_v1 {
    public static final long CAT_ID_MISC_EXPENSE = Long.MAX_VALUE;

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void populate(final Context context, final SQLiteDatabase db, final int oldVersion,
            final boolean allowExtraData) {
        if (oldVersion >= VERSION)
            return;
        super.populate(context, db, oldVersion, allowExtraData);

        // Insert 'misc' expense category.
        Resources ctxResources = context.getResources();
        SQLiteStatement insWIdStatement = db.compileStatement(
            SqlTemplates.INSERT_INTO(
                TBL_CATEGORIES.tableName,
                TBL_CATEGORIES.columns.keySet(),
                null));

        insWIdStatement.bindLong(1, CAT_ID_MISC_EXPENSE);
        insWIdStatement.bindLong(2, RAW_EXPENSE);
        insWIdStatement.bindString(3, ctxResources.getString(R.string.cats_misc_expense));
        insWIdStatement.bindLong(4, ctxResources.getColor(R.color.cats_misc_expense));
        insWIdStatement.executeInsert();
    }

    private static final int VERSION = 2;
}

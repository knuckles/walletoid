/**
* Copyright (c) 2014, The walletoid authors.
* All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

package ru.knuckles.walletoid.arithmetic_evaluator;

import ru.knuckles.walletoid.Utils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class Expression {
    public abstract BigDecimal getValue() throws Exception;

    public static class Constant extends Expression {
        public BigDecimal getValue() {
            return _value;
        }

        @Override
        public String toString() {
            return _value.toString();
        }

        Constant(BigDecimal value) {
            this._value = value != null ? value : BigDecimal.ZERO;
        }

        private final BigDecimal _value;
    }

    public static class CompoundExpression extends Expression {
        public BigDecimal getValue() throws Exception {
            if (operator == null)
                throw new IllegalStateException("No operator specified");
            if (operands.size() == 0)
                throw new IllegalStateException("No operands specified");
            return operator.Apply(operands);
        }

        @Override
        public String toString() {
            String operatorStr = operator != null ? operator.toString() : "?";
            Character[] brackets = explicit ? _explicitBrackets : _implicitBrackets;
            return brackets[0] + Utils.joinStrings(operatorStr, operands) + brackets[1];
        }

        CompoundExpression() {}

        List<Expression> operands = new ArrayList<Expression>();
        Operator operator;
        boolean explicit = false;

        private static Character[] _explicitBrackets = {'(', ')'};
        private static Character[] _implicitBrackets = {'‹', '›'};
    }

    private static class DivideOperator extends Operator {
        DivideOperator(int priority, String shortName) throws Exception {
            super(priority, shortName);
            _method = BigDecimal.class.getDeclaredMethod("divide", BigDecimal.class, Integer.TYPE, RoundingMode.class);
        }

        @Override
        protected BigDecimal accumulate(BigDecimal accumulator, BigDecimal value)
                throws InvocationTargetException, IllegalAccessException {
            return (BigDecimal) _method.invoke(accumulator, value, 4, RoundingMode.HALF_UP);
        }
    }

    private static Operator PLUS;
    private static Operator MINUS;
    private static Operator MULTIPLY;
    private static Operator DIVIDE;
    static {
        try {
            PLUS        = new Operator(1, "add",        "+");
            MINUS       = new Operator(1, "subtract",   "-");
            MULTIPLY    = new Operator(2, "multiply",   "*");
            DIVIDE      = new DivideOperator(2,         "/");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static final Map<String, Operator> OPERATOR_MAP = new TreeMap<String, Operator>() {{
        put("+", PLUS);
        put("-", MINUS);
        put("*", MULTIPLY);
        put("/", DIVIDE);
    }};
}

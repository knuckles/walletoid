/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.arithmetic_evaluator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

class Scanner {
    static class Lexemme {
        public enum Type {
            NUMBER,
            OPERATOR,
            PAREN
        }

        Lexemme(Type type, String text, int position) {
            this.type = type;
            this.text = text;
            this.position = position;
        }

        @Override
        public String toString() {
            return type.toString() + ":" + text;
        }

        @Override
        public boolean equals(Object other) {
            return
                this == other ||
                this.getClass().isAssignableFrom(other.getClass()) &&
                this.equals((Lexemme) other);
        }

        public boolean equals(Lexemme other) {
            return
                this == other ||
                other != null &&
                type == other.type &&
                position == other.position &&
                text.equals(other.text);
        }

        final Type type;
        final String text;
        final int position;
    }

    private static enum State {
        INITIAL,
        WAIT_DIGIT,
    }

    public static List<Lexemme> scan(String string) {
        List<Lexemme> result = new ArrayList<Lexemme>();
        StringBuilder buffer = new StringBuilder();
        int bufferLexemmePosition = -1;
        State state = State.INITIAL;
        for (int i = 0; i < string.length(); i++) {
            Character character = string.charAt(i);
            CharKind charKind = ClassifyCharacter(character);
            if (charKind == CharKind.SPACE)
                continue;
            Lexemme.Type lexemmeType = MapCharKindToLexemmeType(charKind);
            if (lexemmeType == Lexemme.Type.NUMBER) {
                buffer.append(character);
                if (bufferLexemmePosition == -1)
                    bufferLexemmePosition = i;
                state = State.WAIT_DIGIT;
            }
            else {
                state = flushDigitBuffer(result, buffer, bufferLexemmePosition, state);
                bufferLexemmePosition = -1;
                result.add(new Lexemme(lexemmeType, character.toString(), i));
            }
        }
        flushDigitBuffer(result, buffer, bufferLexemmePosition, state);
        return result;
    }

    private static State flushDigitBuffer(List<Lexemme> result, StringBuilder buffer, int lexemmePosition, State state) {
        if (state == State.WAIT_DIGIT && buffer.length() > 0) {
            result.add(new Lexemme(Lexemme.Type.NUMBER, buffer.toString(), lexemmePosition));
            buffer.setLength(0);
            return State.INITIAL;
        }
        return state;
    }

    private static Lexemme.Type MapCharKindToLexemmeType(CharKind charKind) {
        switch (charKind) {
            case DIGIT:
            case DOT:
                return Lexemme.Type.NUMBER;
            case OPERATOR:
                return Lexemme.Type.OPERATOR;
            case PAREN:
                return Lexemme.Type.PAREN;
            default:
                throw new IllegalArgumentException();
        }
    }

    private enum CharKind {
        SPACE,
        DIGIT,
        DOT,
        OPERATOR,
        PAREN,
        UNKNOWN,
    }

    private static CharKind ClassifyCharacter(Character character) {
        String charString = character.toString();
        for (Map.Entry<CharKind, Pattern> classificationEntry : _charClassification.entrySet()) {
            if (classificationEntry.getValue().matcher(charString).matches())
                return classificationEntry.getKey();
        }
        return CharKind.UNKNOWN;
    }

    private static Map<CharKind, Pattern> _charClassification = new TreeMap<CharKind, Pattern>() {{
        put(CharKind.SPACE, Pattern.compile("\\s"));
        put(CharKind.DIGIT, Pattern.compile("[0-9]"));
        put(CharKind.DOT, Pattern.compile("\\."));
        put(CharKind.OPERATOR, Pattern.compile("[-+*/]"));
        put(CharKind.PAREN, Pattern.compile("[()]"));
    }};
}

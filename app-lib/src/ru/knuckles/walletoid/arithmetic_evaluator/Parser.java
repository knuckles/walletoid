/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.arithmetic_evaluator;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Stack;

class Parser {
    public Expression parse(List<Scanner.Lexemme> lexemmes) throws ParseException {
        _expressionStack = new Stack<Expression.CompoundExpression>();
        _expressionStack.push(new Expression.CompoundExpression());
        for (Scanner.Lexemme lexemme : lexemmes) {
            Expression.CompoundExpression topExpression = _expressionStack.peek();
            switch (lexemme.type) {
                case NUMBER: {
                    topExpression.operands.add(new Expression.Constant(new BigDecimal(lexemme.text)));
                    break;
                }
                case OPERATOR: {
                    insertOperator(Expression.OPERATOR_MAP.get(lexemme.text));
                    break;
                }
                case PAREN: {
                    if (lexemme.text.equals("(")) {
                        // TODO: assert current expression completeness.
                        Expression.CompoundExpression subexpression = new Expression.CompoundExpression();
                        subexpression.explicit = true;
                        topExpression.operands.add(subexpression);
                        _expressionStack.push(subexpression);
                    } else {
                        completeImplicitExpressions();
                        if (!_expressionStack.peek().explicit)
                            throw new ParseException("Unexpected closing paren", lexemme.position);
                        _expressionStack.pop();
                    }
                    break;
                }
            }
        }
        completeImplicitExpressions();
        // Return just a single Constant expression, if that's all we've parsed.
        if (_expressionStack.peek().operands.size() == 1)
            return _expressionStack.peek().operands.get(0);
        return _expressionStack.pop();
    }

    private void insertOperator(Operator operator) {
        Expression.CompoundExpression topExpression = _expressionStack.peek();
        if (topExpression.operator == null) {
            topExpression.operator = operator;
            return;
        }
        if (topExpression.operator.equals(operator))
            return;

        if (operator.priority > topExpression.operator.priority) {
            // We replace top-level expression last operand with a new expression and push it on the
            // stack. And it must be popped after it's complete (see else clause).
            replaceTopExpressionLastOperand(operator);
        } else {
            if (topExpression.explicit || _expressionStack.size() == 1) {
                // We can't pop and aggregate explicitly parenthetical expression,
                // because current operator should be INSIDE those parenthesis.
                aggregateOperandsOfExpression(topExpression, operator);
            } else {
                // Complete implicit subexpression.
                _expressionStack.pop();
                // This operator goes into parent expression.
                insertOperator(operator);
            }
        }
    }

    private void completeImplicitExpressions() {
        // Previously started subexpressions are over.
        while (_expressionStack.size() > 1 && !_expressionStack.peek().explicit)
            _expressionStack.pop();
    }

    private void replaceTopExpressionLastOperand(Operator operator) {
        Expression.CompoundExpression topExpression = _expressionStack.peek();
        int lastOperandIndex = topExpression.operands.size() - 1;
        Expression.CompoundExpression subexpression = new Expression.CompoundExpression();
        subexpression.operator = operator;
        subexpression.operands.add(topExpression.operands.get(lastOperandIndex));
        topExpression.operands.remove(lastOperandIndex);
        topExpression.operands.add(subexpression);
        _expressionStack.push(subexpression);
    }

    private void aggregateOperandsOfExpression(Expression.CompoundExpression expression, Operator operator) {
        // "Clone" expression.
        Expression.CompoundExpression subexpression = new Expression.CompoundExpression();
        List<Expression> newOperands = subexpression.operands;
        subexpression.operator = expression.operator;
        subexpression.operands = expression.operands;
        // Use clone as expression operand
        newOperands.add(subexpression);
        expression.operands = newOperands;
        expression.operator = operator;
    }

    private Stack<Expression.CompoundExpression> _expressionStack;
}

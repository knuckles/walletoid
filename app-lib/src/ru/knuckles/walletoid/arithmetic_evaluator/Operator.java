/**
* Copyright (c) 2014, The walletoid authors.
* All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

package ru.knuckles.walletoid.arithmetic_evaluator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;

class Operator {
    public Operator(int priority, String methodName, String shortName) throws Exception {
        this(priority, shortName);
        _method = BigDecimal.class.getDeclaredMethod(methodName, BigDecimal.class);
    }

    protected Operator(int priority, String shortName) {
        this.priority = priority;
        _shortName = shortName;
    }

    public BigDecimal Apply(List<Expression> operands) throws Exception {
        BigDecimal accumulator = null;
        for (Expression expression : operands) {
            BigDecimal expressionValue = expression.getValue();
            if (accumulator == null)
                accumulator = expressionValue;
            else
                accumulator = accumulate(accumulator, expressionValue);
        }
        return accumulator;
    }

    @Override
    public String toString() {
        return _shortName;
    }

    @Override
    public boolean equals(Object other) {
        return
            this == other ||
            this.getClass().isAssignableFrom(other.getClass()) &&
            this.equals((Operator) other);
    }

    public boolean equals(Operator other) {
        return
            this == other ||
            other != null &&
            this._method.equals(other._method);
    }

    protected BigDecimal accumulate(BigDecimal accumulator, BigDecimal value)
            throws InvocationTargetException, IllegalAccessException {
        return (BigDecimal) _method.invoke(accumulator, value);
    }

    public final int priority;

    protected final String _shortName;
    protected Method _method = null;
}

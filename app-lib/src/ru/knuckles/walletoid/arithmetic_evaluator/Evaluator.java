/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.arithmetic_evaluator;
import java.math.BigDecimal;
import java.text.ParseException;

public class Evaluator {
    public static BigDecimal evaluateString(String string) throws Exception {
        return parser().parse(Scanner.scan(string)).getValue();
    }

    public static Expression parseString(String string) throws ParseException {
        return parser().parse(Scanner.scan(string));
    }

    private static Parser parser() {
        if (_parser == null)
            _parser = new Parser();
        return _parser;
    }

    private static Parser _parser = null;
}

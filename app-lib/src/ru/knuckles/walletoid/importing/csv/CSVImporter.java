/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.importing.csv;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import ru.knuckles.walletoid.R;
import ru.knuckles.walletoid.Walletoid;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletManager;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class CSVImporter extends CSVImportTask.Delegate {
    public interface Delegate {
        /**
         * Notifies {@link Delegate} that the import process finished.
         * @param walletName contains imported data. Will be null, if an error occurred.
         */
        void onImportFinished(String walletName);
    }

    public CSVImporter(Context context, InputStreamReader input, Delegate delegate, long defaultCurrencyId)
            throws IOException, ParseException, Exception {
        assert context != null;
        assert input != null;
        assert delegate != null;

        _context = context;
        _delegate = delegate;

        _dialog = new ProgressDialog(context);
        _dialog.setIndeterminate(true);
        _dialog.setMessage(_context.getString(R.string.csv_read_progress, 0));
        _dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                _importTask.cancel(false);
            }
        });

        _wm = Walletoid.getInstance().getWalletManager();
        _wm.deleteWalletData(WALLET_NAME);
        _tempWallet = _wm.getWallet(WALLET_NAME, false, false);
        try {
            _importTask = new CSVImportTask(this, input, _tempWallet, defaultCurrencyId);
            _importTask.execute();

            _dialog.show();
        }
        catch (Exception e) {
            cleanup();
            throw e;
        }
    }

    // CSVImportTask.Delegate
    @Override
    void onProgressUpdate(CSVImportTask task, int value) {
        _dialog.setMessage(_context.getString(R.string.csv_read_progress, value));
    }

    @Override
    void onFinished(CSVImportTask task) {
        _dialog.dismiss();
        _delegate.onImportFinished(_tempWallet.getName());
        _wm.releaseWallet(_tempWallet.getName());
    }

    @Override
    void onError(CSVImportTask task, CSVImportTask.ImportResult error) {
        _dialog.dismiss();
        cleanup();

        if (error == CSVImportTask.ImportResult.ERR_CANCELLED)
            return;

        Exception e = task.getException();
        if (e != null)
            e.printStackTrace();

        int detailStringResID;
        switch (error) {
            case ERR_IO:
                detailStringResID = R.string.csv_io_error;
                break;
            case ERR_DB:
                detailStringResID = R.string.csv_db_error;
                break;
            case ERR_FORMAT:
                detailStringResID = R.string.csv_parse_error;
                break;
            default:
                detailStringResID = R.string.csv_unexpected_error;
                break;
        }
        String errorMesasge = _context.getString(R.string.csv_read_error, _context.getString(detailStringResID));
        Toast.makeText(_context, errorMesasge, Toast.LENGTH_LONG).show();

        _delegate.onImportFinished(null);
    }

    // PRIVATE

    private void cleanup() {
        if (_tempWallet != null)
            _wm.releaseWalletAndDeleteFile(WALLET_NAME);
    }

    private static final String WALLET_NAME = "csvimport";
    private final Delegate _delegate;
    private final Context _context;
    private final ProgressDialog _dialog;
    private final WalletManager _wm;
    private final Wallet _tempWallet;
    private final CSVImportTask _importTask;
}

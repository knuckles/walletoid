/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.importing.csv;

import android.database.SQLException;
import android.os.AsyncTask;
import au.com.bytecode.opencsv.CSVReader;
import ru.knuckles.walletoid.database.Wallet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class CSVImportTask extends AsyncTask<Void, Integer, CSVImportTask.ImportResult> {
    public enum ImportResult {
        ERR_SUCCESS,
        ERR_CANCELLED,
        ERR_IO,
        ERR_DB,
        ERR_FORMAT,
        ERR_UNEXPECTED
    }

    public abstract static class Delegate {
        /**
         * Called when new record is processed.
         * @param value is the number of {@link ru.knuckles.walletoid.database.Record records} processed so far.
         */
        abstract void onProgressUpdate(CSVImportTask task, int value);

        /**
         * Called when all input is processed successfully.
         */
        abstract void onFinished(CSVImportTask task);

        /**
         * Called when this task stops because of an error or cancellation.
         * @param error code.
         */
        abstract void onError(CSVImportTask task, ImportResult error);
    }

    private final Delegate _delegate;
    private final CSVReader _reader;
    private final Wallet _destWallet;
    private final long _defaultCurrencyId;
    private Exception _exception;
    private StringParser _parser;

    public CSVImportTask(Delegate delegate, InputStreamReader streamReader, Wallet destWallet, long defaultCurrencyId) {
        assert destWallet != null;
        _delegate = delegate;
        _reader = new CSVReader(streamReader);
        _destWallet = destWallet;
        _defaultCurrencyId = defaultCurrencyId;
    }

    @SuppressWarnings("unused")
    public Exception getException() {
        return _exception;
    }

    @Override
    protected ImportResult doInBackground(Void... params) {
        try {
            return importWalletData();
        }
        catch (ParseException e) {
            _exception = e;
            return ImportResult.ERR_FORMAT;
        }
        catch (SQLException e) {
            _exception = e;
            return ImportResult.ERR_DB;
        }
        catch (IOException e) {
            _exception = e;
            return ImportResult.ERR_IO;
        }
        catch (Exception e) {
            this._exception = e;
            return ImportResult.ERR_UNEXPECTED;
        }
    }

    private ImportResult importWalletData() throws IOException, ParseException {
        // Set up parser.
        String[] firstLine = _reader.readNext();
        _parser = StringParser.guessFormat(firstLine, new StringParser.SimpleDelegate(_destWallet), _defaultCurrencyId);

        // Read the rest of CSV file.
        return processInput(firstLine);
    }

    private ImportResult processInput(String[] firstLine) throws IOException, ParseException {
        int lines = 0;
        String[] values = firstLine;
        while (values != null) {
            if (isCancelled())
                return ImportResult.ERR_CANCELLED;
            _destWallet.addRecord(_parser.convertFromStrings(values));
            publishProgress(++lines);
            values = _reader.readNext();
        }

        return ImportResult.ERR_SUCCESS;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (_delegate != null) {
            _delegate.onProgressUpdate(this, values[0]);
        }
    }

    @Override
    protected void onPostExecute(ImportResult result) {
        try {
            _reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (result == ImportResult.ERR_SUCCESS)
            onSuccess();
        else
            onFailed(result);
    }

    @Override
    protected void onCancelled() {
        onFailed(ImportResult.ERR_CANCELLED);
    }

    private void onFailed(ImportResult errorCode) {
        if (_delegate != null)
            _delegate.onError(this, errorCode);
    }

    private void onSuccess() {
        if (_delegate != null)
            _delegate.onFinished(this);
    }
}

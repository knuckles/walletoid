/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.importing.csv;

import android.util.Pair;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class StringParser {
    /**
     * Enumeration of {@link Record} fields.
     * NOTE: Enumeration order determines in which order fields are guessed and then parsed.
     * Fields go from more to less complex/strict format. Also, parsing category type (income/expense) depends on sum.
     */
    public enum FieldType {
        FIELD_DATE,
        FIELD_SUM,
        FIELD_CATEGORY,
        FIELD_DESCRIPTION
    }

    /**
     * An interface for a delegate that will help the parser with some things,
     * that he can't take care of itself.
     */
    public interface Delegate {
        /**
         * Handles record {@link Category categories}.
         * @param name of the category to handle.
         * @param sumIsNegative whether current {@link Record} has negative sum.
         * @return ID for a category.
         */
        long getCategoryID(String name, boolean sumIsNegative);
    }

    /**
     * This is a simple implementation of {@link Delegate} for {@link StringParser}. It takes a {@link Wallet
     * wallet} and adds new categories when encountered. Thus it will return actual category IDs assigned by the
     * {@link Wallet}.
     */
    public static class SimpleDelegate implements Delegate {
        private final Wallet _wallet;
        private final Map<String, Category> _categoriesMap = new TreeMap<String, Category>();

        SimpleDelegate(Wallet wallet) {
            assert wallet != null;
            _wallet = wallet;
        }

        /**
         * When processing strings in {@link #getCategoryID}, this delegate collects categories.
         * Each new name is assigned and ID. This is done by {@link Wallet}, passed to constructor.
         * @return information about categories encountered during previous calls to {@link #getCategoryID}.
         */
        public Map<String, Category> getGatheredCategories() {
            return new TreeMap<String, Category>(_categoriesMap);
        }

        @Override
        public long getCategoryID(String name, boolean sumIsNegative) {
            Category recCategory;
            if (_categoriesMap.containsKey(name)) {
                recCategory = _categoriesMap.get(name);
            }
            else {
                Category.Kind kind = sumIsNegative ? Category.Kind.EXPENSE : Category.Kind.INCOME;
                long newCatID = _wallet.addCategory(new Category(name, 0, kind));
                recCategory = new Category(newCatID, name, 0, kind);
                _categoriesMap.put(name, recCategory);
            }
            return recCategory.getId();
        }
    }

    private class ParsingRecord extends Record {
        private final Delegate _delegate;

        public ParsingRecord(Delegate delegate) {
            super(Category.UNKNOWN_ID, System.currentTimeMillis(), new Record.Sum(BigDecimal.ZERO, _defaultCurrencyId));
            _delegate = delegate;
        }

        boolean hasNegativeSum = false;
    }

    private abstract static class RecordFieldParser {
        // Parse a string and assign value to a given record.
        public abstract void setRecordField(ParsingRecord record, String string) throws ParseException;

        public abstract FieldType getFieldType();

        // This should be "overridden" in subclasses.
        @SuppressWarnings("unused")
        public static RecordFieldParser tryString(String string) {
            return null;
        }
    }

    protected static class DateParser extends RecordFieldParser {
        private DateFormat _dateFormat;

        private DateParser(DateFormat dateFormat) {
            assert dateFormat != null;
            _dateFormat = dateFormat;
        }

        @Override
        public void setRecordField(ParsingRecord record, String string) throws ParseException {
            record.setTimestamp(_dateFormat.parse(string).getTime());
        }

        // Map of regular expressions to SimpleDateFormat patterns.
        // I actually stole the initial version from StackOverflow
        // http://stackoverflow.com/questions/3389348/parse-any-date-in-java
        private static final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {{
            put("^\\d{1,2}\\.\\d{1,2}\\.\\d{2}$", "dd.MM.yy");
            put("^\\d{8}$", "yyyyMMdd");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
            put("^\\d{12}$", "yyyyMMddHHmm");
            put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
            put("^\\d{14}$", "yyyyMMddHHmmss");
            put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
        }};

        /**
         * Determine DateFormat pattern matching with the given date string. Returns null if
         * format is unknown. You can simply extend DateUtil with more formats if needed.
         *
         * @param dateString The date string to determine the SimpleDateFormat pattern for.
         * @return The matching DateFormat, or null if format is unknown.
         * @see java.text.SimpleDateFormat
         */
        public static DateFormat determineDateFormat(String dateString) {
            for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
                try {
                    if (dateString.toLowerCase().matches(regexp)) {
                        DateFormat format = new SimpleDateFormat(DATE_FORMAT_REGEXPS.get(regexp));
                        format.parse(dateString);
                        return format;
                    }
                }
                catch (ParseException e) {
                    // Ignore exception and try next format if any.
                }
            }
            return null; // Unknown format.
        }

        public static RecordFieldParser tryString(String string) {
            DateFormat format = determineDateFormat(string);
            if (format == null)
                return null;
            return new DateParser(format);
        }

        @Override
        public FieldType getFieldType() {
            return FieldType.FIELD_DATE;
        }
    }

    protected static class CategoryParser extends RecordFieldParser {
        @Override
        public void setRecordField(ParsingRecord record, String string) {
            record.setCatID(record._delegate.getCategoryID(string, record.hasNegativeSum));
        }

        public static RecordFieldParser tryString(String string) {
            return (string == null) || string.isEmpty() ?
                null :
                new CategoryParser();
        }

        @Override
        public FieldType getFieldType() {
            return FieldType.FIELD_CATEGORY;
        }
    }

    protected static class SumParser extends RecordFieldParser {
        @Override
        public void setRecordField(ParsingRecord record, String string) {
            BigDecimal sumValue = new BigDecimal(string);
            record.hasNegativeSum = sumValue.compareTo(BigDecimal.ZERO) < 0;
            record.setSum(new Record.Sum(sumValue.abs(), record.getCurrencyID()));
        }

        public static RecordFieldParser tryString(String string) {
            try {
                new BigDecimal(string);
                return new SumParser();
            }
            catch (Exception e) {
            }
            return null;
        }

        @Override
        public FieldType getFieldType() {
            return FieldType.FIELD_SUM;
        }
    }

    protected static class DescriptionParser extends RecordFieldParser {
        @Override
        public void setRecordField(ParsingRecord record, String string) {
            record.setDescr(string);
        }

        public static RecordFieldParser tryString(String string) {
            return new DescriptionParser();
        }

        @Override
        public FieldType getFieldType() {
            return FieldType.FIELD_DESCRIPTION;
        }
    }

    private final Map<FieldType, Pair<RecordFieldParser, Integer>> _fieldMapping;
    private final Delegate _delegate;
    private final long _defaultCurrencyId;

    private StringParser(Map<FieldType, Pair<RecordFieldParser, Integer>> fieldMapping, Delegate delegate,
                         long defaultCurrencyId) {
        assert fieldMapping != null;
        assert delegate != null;
        _fieldMapping = fieldMapping;
        _delegate = delegate;
        _defaultCurrencyId = defaultCurrencyId;
    }

    /**
     * Gives access to information about the guessed strings format, which is the result of {@link #guessFormat}.
     * @return Map from {@link FieldType} to an index in a string array that you should pass to
     * {@link #convertFromStrings} to get a {@link Record}.
     */
    public Map<FieldType, Integer> getFieldMapping() {
        TreeMap<FieldType, Integer> result = new TreeMap<FieldType, Integer>();
        for (Map.Entry<FieldType, Pair<RecordFieldParser, Integer>> entry : _fieldMapping.entrySet()) {
            result.put(entry.getKey(), entry.getValue().second);
        }
        return result;
    }

    /**
     * Will try to parse given strings as {@link Record} fields as was previously guessed by {@link #guessFormat}.
     * @param strings to parse.
     * @return Parsed record.
     * @throws ParseException
     */
    public Record convertFromStrings(String[] strings) throws ParseException {
        ParsingRecord record = new ParsingRecord(_delegate);
        for (Map.Entry<FieldType, Pair<RecordFieldParser, Integer>> entry : _fieldMapping.entrySet()) {
            RecordFieldParser parser = entry.getValue().first;
            int fieldIndex = entry.getValue().second;
            parser.setRecordField(record, strings[fieldIndex]);
        }
        return record;
    }

    static private final Map<FieldType, Class<? extends RecordFieldParser>> _parserClasses =
            new TreeMap<FieldType, Class<? extends RecordFieldParser>>() {{
        put(FieldType.FIELD_DATE, DateParser.class);
        put(FieldType.FIELD_SUM, SumParser.class);
        put(FieldType.FIELD_CATEGORY, CategoryParser.class);
        put(FieldType.FIELD_DESCRIPTION, DescriptionParser.class);
    }};

    static private Pair<RecordFieldParser, Integer>
            findField(Class<? extends RecordFieldParser> parserClass, String[] strings) {

        try {
            Method tryMethod = parserClass.getDeclaredMethod("tryString", String.class);
            RecordFieldParser parser = null;
            for (int i = 0; i < strings.length; i++) {
                if (strings[i] == null)
                    continue;
                parser = (RecordFieldParser) tryMethod.invoke(null, strings[i]);
                if (parser != null)
                    return new Pair<RecordFieldParser, Integer>(parser, i);
            }
        }
        catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Try to guess {@link Record} fields in the given {@param strings}.
     * Fields are guessed in the following order (from more to less complex/strict format):
     * 1. Date. Several formats are tried.
     * 2. Sum. Positive or negative numbers.
     * 3. Category. Non-empty strings.
     * 4. Description. Matches anything.
     * If any of the fields mentioned above isn't recognized among available strings, {@link ParseException} is thrown.
     * @param strings representing fields of a {@link Record}.
     * @return StringParser instance, set up to parse {@link Record records} with the same fields order.
     * @throws ParseException
     */
    static public StringParser guessFormat(String[] strings, Delegate delegate, long defaultCurrencyId)
            throws ParseException {
        strings = strings.clone();
        Map<FieldType, Pair<RecordFieldParser, Integer>> guessedMapping =
            new TreeMap<FieldType, Pair<RecordFieldParser, Integer>>();
        for (FieldType fieldType: FieldType.values()) {
            Class<? extends RecordFieldParser> parserClass = _parserClasses.get(fieldType);
            Pair<RecordFieldParser, Integer> parserAndField = findField(parserClass, strings);
            if (parserAndField == null)
                throw new ParseException("Could not find field of type: " + parserClass.getSimpleName(), 0);
            guessedMapping.put(parserAndField.first.getFieldType(), parserAndField);
            strings[parserAndField.second] = null;  // remove this field from guessing.

        }
        return new StringParser(guessedMapping, delegate, defaultCurrencyId);
    }
}

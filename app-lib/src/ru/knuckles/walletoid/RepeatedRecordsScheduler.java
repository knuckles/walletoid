/*
 * Copyright (c) 2017, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import ru.knuckles.walletoid.CalendarUtils.TimeInterval;
import ru.knuckles.walletoid.activities.IntentExtras;
import ru.knuckles.walletoid.activities.RecordEditActivity;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.database.Wallet;
import ru.knuckles.walletoid.database.WalletManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class manages OS-level details of scheduling and repeating {@link Record} records. It
 * provides no guarantees beyond those of Android's NotificationManager. Abnormal/manual system
 * clock changes are not handled in any special way. In particular: skipped records won't be
 * replayed.
 */
// TODO: RepeatedRecordsScheduler assumes Wallet is always WalletManager's main wallet.
public class RepeatedRecordsScheduler extends BroadcastReceiver {
    // BroadcastReceiver

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action == null)
            return;

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // Reacting to this event is not really needed since application sets alarms itself.
            return;
        }

        final Pair<String, Long> inAppAction;
        try {
            inAppAction = parseInAppAction(context, action);
        }
        catch (Exception e) {
            Log.w(RepeatedRecordsScheduler.class.getSimpleName(),
                    String.format("Could not parse in-app action from string: '%s'", action));
            return;
        }
        if (inAppAction == null)
            return;

        try {
            switch (inAppAction.first) {
                case REPEAT_REC: {
                    onTimeToRepeatRecord(context, inAppAction.second, intent.getExtras());
                    break;
                }
            }
        }
        catch (Exception e) {
            Log.w(RepeatedRecordsScheduler.class.getSimpleName(),
                    String.format("Failed inside in-app action '%s': %s", action, e));
        }
    }

    // PUBLIC

    public static boolean recordIsScheduled(Context context, long recordId) {
        return findBroadcast(context, recordId) != null;
    }

    public static void scheduleRecord(Context context, Record record, TimeInterval interval,
            boolean allowTriggerInPast) {
        final Bundle extras = new Bundle();
        final Calendar intendedTriggerDate = CalendarUtils.getNextRepetitionDate(
                record.getCalendar(), interval, allowTriggerInPast);
        final long triggerDateMillis = intendedTriggerDate.getTimeInMillis();
        extras.putLong(INTENDED_REPEAT_DATE, triggerDateMillis);

        Log.i(RepeatedRecordsScheduler.class.getSimpleName(),
                String.format("Setting alarm. Record: %s; planned date: %s",
                        record.toString(), _dateFormat.format(triggerDateMillis)));

        // Android will notify us when specified date/time is due. If time is changed manually to
        // the future, we will also get a notification, but only once (not several times for each
        // "passed" interval).
        final AlarmManager alarmMgr =
                (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.set(AlarmManager.RTC, triggerDateMillis,
                createBroadcast(context, record.getId(), extras));
    }

    public static void unscheduleRecord(Context context, long recordId) {
        final PendingIntent existingBroadcast = findBroadcast(context, recordId);
        if (existingBroadcast != null) {
            Log.i(RepeatedRecordsScheduler.class.getSimpleName(),
                String.format("Removing alarm for record: %d", recordId));
            final AlarmManager alarmMgr =
                    (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmMgr.cancel(existingBroadcast);
        }
        else {
            Log.w(RepeatedRecordsScheduler.class.getSimpleName(),
                String.format("No alarm found for record: %d", recordId));
        }
    }

    public static void setAlarms(Context context) {
        Log.i(RepeatedRecordsScheduler.class.getSimpleName(), "Setting record alarms");
        final WalletManager wm = Walletoid.getInstance().getWalletManager();
        if (!wm.walletExists(WalletManager.MAIN_DB_NAME))
            return;  // Don't want to trigger Wallet database creation (without prepopulating it).

        // We don't want to trigger Wallet data population or creation of dictionary.
        final Wallet wallet = wm.getWallet(WalletManager.MAIN_DB_NAME, false, false);
        try {
            for (final Map.Entry<Long, TimeInterval> idAndInterval :
                    wallet.getScheduledRecordsIdMap().entrySet()) {
                // Check record existence.
                final Record templateRec = wallet.getRecord(idAndInterval.getKey());
                final List<Record> reps = wallet.findRepetitionsInCurrentPeriod(
                        templateRec.getId(), idAndInterval.getValue());
                // Set alarm.
                scheduleRecord(context, templateRec, idAndInterval.getValue(), reps.isEmpty());
            }
        } finally {
            wm.releaseMainWallet();
        }
    }

    public static void removeAlarms(Context context) {
        Log.i(RepeatedRecordsScheduler.class.getSimpleName(), "Removing all record alarms");
        Map<Long, TimeInterval> recMap = getRepeatedRecords(context);
        if (recMap == null)
            return;
        for (long recId : recMap.keySet()) {
            unscheduleRecord(context, recId);
        }
    }

    // PRIVATE

    private static Map<Long, TimeInterval> getRepeatedRecords(Context context) {
        final WalletManager wm = Walletoid.getInstance().getWalletManager();
        if (!wm.walletExists(WalletManager.MAIN_DB_NAME))
            return null;  // Don't want to trigger Wallet database creation.

        // We don't want to trigger Wallet data population or creation of dictionary.
        final Wallet wallet = wm.getWallet(WalletManager.MAIN_DB_NAME, false, false);
        try {
            return wallet.getScheduledRecordsIdMap();
        } finally {
            wm.releaseMainWallet();
        }
    }

    private static PendingIntent createBroadcast(Context context, long recordId, Bundle extras) {
        return maybeGetBroadcast(context, recordId, true, extras);
    }

    private static PendingIntent findBroadcast(Context context, long recordId) {
        return maybeGetBroadcast(context, recordId, false, null);
    }

    private static PendingIntent maybeGetBroadcast(Context context, long recordId,
            boolean allowCreate, Bundle extras) {
        final Intent intent = new Intent(context, RepeatedRecordsScheduler.class);
        intent.setAction(buildInAppAction(context, REPEAT_REC, recordId));
        if (allowCreate && extras != null)
            intent.putExtras(extras);
        return PendingIntent.getBroadcast(context, 0, intent,
                allowCreate ? PendingIntent.FLAG_CANCEL_CURRENT : PendingIntent.FLAG_NO_CREATE);
    }

    private void onTimeToRepeatRecord(Context context, Long recordId, Bundle extras) {
        long intendedMillis = extras.getLong(INTENDED_REPEAT_DATE, System.currentTimeMillis());

        final WalletManager wm = Walletoid.getInstance().getWalletManager();
        final Wallet wallet = wm.getMainWallet(false);
        try {
            final Record record;
            try {
                record = wallet.getRecord(recordId);
            }
            catch (Exception e) {
                // This should not happen, but let's handle it nicely.
                // Record doesn't exist anymore. Remove alarm.
                unscheduleRecord(context, recordId);
                return;
            }

            Log.i(RepeatedRecordsScheduler.class.getSimpleName(),
                String.format("Time to repeat record: %s; planned date: %s",
                        record.toString(), _dateFormat.format(intendedMillis)));

            notifyOnRecordRepeat(context, recordId, intendedMillis);
            // Set next alarm. Don't allow triggering in the past, as this spawns an endless loop!
            scheduleRecord(context, record, wallet.getRepetitionInterval(recordId), false);
        }
        finally {
            wm.releaseMainWallet();
        }
    }

    public static void notifyOnRecordRepeat(Context context, long recordId, long newRecTimestamp) {
        final WalletManager wm = Walletoid.getInstance().getWalletManager();
        final Wallet wallet = wm.getMainWallet(false);
        try {
            final Intent recEditIntent = new Intent(context, RecordEditActivity.class);
            recEditIntent.setAction(buildInAppAction(context, EDIT_REC, recordId));
            recEditIntent.putExtra(IntentExtras.NOTIFICATION_ID, _notificationId);
            recEditIntent.putExtra(RecordEditActivity.EXTRA_REPEAT_RECORD_ID, recordId);
            recEditIntent.putExtra(RecordEditActivity.EXTRA_REPEAT_RECORD_TS, newRecTimestamp);
            final PendingIntent intent = PendingIntent.getActivity(context, 1, recEditIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            final Record record = wallet.getRecord(recordId);
            final String contentText = context.getString(R.string.record_summary_template,
                record.getDescr(),
                wallet.getCurrency(record.getSum().currencyId).getSymbol(),
                record.getSum().amount.floatValue());

            final Notification.Builder builder =
                    new Notification.Builder(context.getApplicationContext());
            builder.setContentTitle(context.getString(R.string.repeated_record_pending));
            builder.setContentText(contentText);
            builder.setSmallIcon(R.drawable.walletoid_logo);
            builder.setWhen(newRecTimestamp);
            builder.setContentIntent(intent);
            final Notification notification =
                    Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN ?
                            extractNotificationPreJB(builder) :
                            extractNotificationJBPlus(builder);

            final NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(_notificationId, notification);
            _notificationId++;
        }
        finally {
            wm.releaseMainWallet();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static Notification extractNotificationPreJB(Notification.Builder builder) {
        return builder.getNotification();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static Notification extractNotificationJBPlus(Notification.Builder builder) {
        return builder.build();
    }

    private static String buildInAppAction(Context context, String action, long recordId) {
        // Purpose is to make it unique enough for Intent.filterEquals.
        return context.getPackageName() + SEPARATOR + action + SEPARATOR + String.valueOf(recordId);
    }

    private static Pair<String, Long> parseInAppAction(Context context, String action) {
        if (!action.startsWith(context.getPackageName() + SEPARATOR))
            return null;
        final int prefixLen = context.getPackageName().length() + 1;
        final int separatorIdx = action.indexOf(SEPARATOR, prefixLen);
        final String actionId = action.substring(prefixLen, separatorIdx);
        final String recIdString = action.substring(separatorIdx + 1, action.length());
        final long recordId = Long.decode(recIdString);
        return new Pair<>(actionId, recordId);
    }

    private static final DateFormat _dateFormat =
            new SimpleDateFormat("dd-MM-yy HH:mm", Locale.getDefault());
    private static final String REPEAT_REC = "repeat_rec";
    private static final String EDIT_REC = "edit_rec";
    private static final Character SEPARATOR = ':';
    private static final String INTENDED_REPEAT_DATE = "intended_repeat_date";
    private static int _notificationId = 0;
}

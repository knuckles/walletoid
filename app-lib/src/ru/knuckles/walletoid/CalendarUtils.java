/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import java.util.*;

public class CalendarUtils {
    public enum TimeInterval {
        YEAR,
        MONTH,
        WEEK,
        DAY;

        public static TimeInterval fromOrdinal(int ord) {
            return values()[ord];
        }

        public int asCalendarField() {
            switch (this) {
                case YEAR:
                    return Calendar.YEAR;
                case MONTH:
                    return Calendar.MONTH;
                case WEEK:
                    return Calendar.WEEK_OF_YEAR;
                case DAY:
                    return Calendar.DAY_OF_YEAR;
            }
            throw new IllegalArgumentException();
        }
    }

    public static class DateRange {
        public interface Observer {
            void onPeriodChanged(Calendar start, Calendar end);
        }

        public DateRange(TimeInterval interval) {
            _interval = interval;
            updateRange();
        }

        public DateRange(DateRange copyFrom) {
            _interval = copyFrom._interval;
            _rangeStart.setTimeInMillis(copyFrom._rangeStart.getTimeInMillis());
            _rangeEnd.setTimeInMillis(copyFrom._rangeEnd.getTimeInMillis());
        }

        public void addObserver(Observer observer) { _observers.add(observer); }

        public void removeObserver(Observer observer) { _observers.remove(observer); }

        public Calendar getRangeStart() { return (Calendar) _rangeStart.clone(); }

        public Calendar getRangeEnd() { return (Calendar) _rangeEnd.clone(); }

        public TimeInterval getInterval() { return _interval; }

        public void setInterval(TimeInterval interval) {
            if (interval == _interval)
                return;

            _interval = interval;
            updateRange();
        }

        public void setStart(int year, int month, int day) {
            _rangeStart.set(year, month, day);
            updateRange();
        }

        public void setRange(Calendar start, TimeInterval interval) {
            if (start == null)
                throw new AssertionError();
            _interval = interval;
            _rangeStart.setTimeInMillis(start.getTimeInMillis());
            updateRange();
        }

        public void inc(int amount) {
            _rangeStart.add(_interval.asCalendarField(), amount);
            updateRange();
        }

        private void updateRange() {
            calendarToIntervalBeginning(_rangeStart, _interval);
            _rangeEnd.setTimeInMillis(_rangeStart.getTimeInMillis());
            _rangeEnd.add(_interval.asCalendarField(), 1);

            notifyObservers();
        }

        private void notifyObservers() {
            for (Observer observer : _observers) {
                try {
                    observer.onPeriodChanged(getRangeStart(), getRangeEnd());
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private TimeInterval _interval;
        private Calendar _rangeStart = Calendar.getInstance();
        private Calendar _rangeEnd = Calendar.getInstance();

        private Set<Observer> _observers = new TreeSet<>(new Comparator<Observer>() {
            @Override
            public int compare(final Observer o1, final Observer o2) {
                return o1.hashCode() - o2.hashCode();
            }
        });
    }

    static public long TS(int year, int month, int day, int hour, int minute) {
        truncateCalendarFieldsUntil(_temp, Calendar.MINUTE);
        _temp.set(year, month, day, hour, minute);
        return _temp.getTimeInMillis();
    }

    static public long TS(int year, int month, int day) {
        clearCalendarTime(_temp);
        _temp.set(year, month, day);
        return _temp.getTimeInMillis();
    }

    static public Calendar Cal(long timestamp) {
        final Calendar result = Calendar.getInstance();
        if (timestamp >= 0)
            result.setTimeInMillis(timestamp);
        return result;
    }

    static public int weeksBetween(Calendar a, Calendar b) {
        return daysBetween(a, b) / 7;
    }

    public static int daysBetween(Calendar a, Calendar b) {
        if (b.before(a)) {
            return -daysBetween(b, a);
        }
        int daysDiff = b.get(Calendar.DAY_OF_YEAR) - a.get(Calendar.DAY_OF_YEAR);
        final int aYear = a.get(Calendar.YEAR);
        final int bYear = b.get(Calendar.YEAR);
        if (aYear != bYear) {
            for (int i = aYear; i < bYear; i ++) {
                daysDiff += a.getActualMaximum(Calendar.DAY_OF_YEAR);
            }
        }
        return daysDiff;
    }

    static public void clearCalendarTime(Calendar calendar) {
        truncateCalendarFieldsUntil(calendar, Calendar.DAY_OF_WEEK);
    }

    static public void setCalendarToMonthStart(Calendar calendar) {
        truncateCalendarFieldsUntil(calendar, Calendar.MONTH);
    }

    static public void setCalendarToYearStart(Calendar calendar) {
        truncateCalendarFieldsUntil(calendar, Calendar.YEAR);
    }

    /**
     * Set all |calendar| fields, smaller than |field|, to their minimum possible value.
     */
    static public void truncateCalendarFieldsUntil(Calendar calendar, int firstUntouchedField) {
        // Sorted descending.
        final int[] FIELDS_TO_RESET = {
            Calendar.MILLISECOND,
            Calendar.SECOND,
            Calendar.MINUTE,
            Calendar.HOUR_OF_DAY,
            Calendar.DAY_OF_WEEK,
            Calendar.DAY_OF_MONTH,
            Calendar.MONTH
        };

        for (int field : FIELDS_TO_RESET) {
            if (field <= firstUntouchedField)
                break;
            int val = (field == Calendar.DAY_OF_WEEK) ?
                    calendar.getFirstDayOfWeek() :
                    calendar.getActualMinimum(field);
            calendar.set(field, val);
        }
    }

    static public void calendarToIntervalBeginning(Calendar calendar, TimeInterval interval) {
        int calFieldToTruncateTo;
        switch (interval) {
            case YEAR:
                calFieldToTruncateTo = Calendar.YEAR;
                break;
            case MONTH:
                calFieldToTruncateTo = Calendar.MONTH;
                break;
            case WEEK:
                calFieldToTruncateTo = Calendar.DAY_OF_YEAR;  // 1 less than DAY_OF_WEEK, which will be reset.
                break;
            case DAY:
                calFieldToTruncateTo = Calendar.HOUR;  // 1 less than HOUR_OF_DAY, which will be reset.
                break;
            default:
                throw new IllegalArgumentException();
        }
        truncateCalendarFieldsUntil(calendar, calFieldToTruncateTo);
    }

    static public Calendar getMonthStart(Calendar calendar) {
        Calendar monthStart = (Calendar) calendar.clone();
        setCalendarToMonthStart(monthStart);
        return monthStart;
    }

    static public Calendar getYearStart(Calendar calendar) {
        Calendar yearStart = (Calendar) calendar.clone();
        setCalendarToYearStart(yearStart);
        return yearStart;
    }

    static public Calendar getCurrentMonthStart() {
        return getMonthStart(Calendar.getInstance());
    }

    static public Calendar getCurrentYearStart() {
        Calendar yearStart = Calendar.getInstance();
        setCalendarToYearStart(yearStart);
        return yearStart;
    }

    static public boolean isSameMonth(Calendar calendar1, Calendar calendar2) {
        return calendar1.get(Calendar.ERA) == calendar2.get(Calendar.ERA) &&
               calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
               calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);
    }

    static public boolean isSameDay(Calendar calendar1, Calendar calendar2) {
        return calendar1.get(Calendar.ERA) == calendar2.get(Calendar.ERA) &&
               calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
               calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }

    static public Calendar getNextRepetitionDate(final Calendar originalDate,
            final TimeInterval interval, final boolean allowPastDate) {
        final Calendar earliestRepetitionDate = Calendar.getInstance();
        if (allowPastDate)
            calendarToIntervalBeginning(earliestRepetitionDate, interval);
        final Calendar after =
                originalDate.before(earliestRepetitionDate) ? earliestRepetitionDate : originalDate;
        return getNextRepetitionDate(originalDate, after, interval);
    }

    static public Calendar getNextRepetitionDate(final Calendar originalDate, final Calendar after,
            final TimeInterval interval) {
        final Calendar result = (Calendar) after.clone();
        truncateCalendarFieldsUntil(result, Calendar.MINUTE);  // Clear seconds and their fractions.
        copyFieldValue(Calendar.MINUTE, originalDate, result);
        copyFieldValue(Calendar.HOUR_OF_DAY, originalDate, result);

        switch (interval) {
            case YEAR:
                copyFieldValue(Calendar.MONTH, originalDate, result);
                // Intentionally no break here. Yearly records should happen on the same day as well.
            case MONTH:
                int dayOfMonth = originalDate.get(Calendar.DAY_OF_MONTH);
                dayOfMonth = Math.min(dayOfMonth, after.getActualMaximum(Calendar.DAY_OF_MONTH));
                result.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                break;
            case WEEK:
                copyFieldValue(Calendar.DAY_OF_WEEK, originalDate, result);
        }
        if (result.before(after) || result.equals(after)) {
            // Calendar.add automatically adjusts date of month to smaller value, if needed.
            result.add(interval.asCalendarField(), 1);
        }
        return result;
    }

    public static void copyFieldValue(int field, Calendar from, Calendar to) {
        to.set(field, from.get(field));
    }

    // Buffer instance for operations on timestamps.
    static final private Calendar _temp = Calendar.getInstance();
}

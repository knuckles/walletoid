/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.content.ContentValues;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Holds a template string containing placeholders which can be later {@link #instantiate} replaced
 * with arbitrary values. Placeholders are declared using ${placeholderName} syntax. The name
 * "placeholderName" is expected to be present among provided {@link ContentValues} values during
 * this template instantiation.
 */
public class TemplateString {
    public TemplateString(CharSequence template) {
        _template = template;
    }

    /**
     * Replaces all placeholders with their values and returns the resulting string.
     * @param values Placeholder values.
     */
    public String instantiate(ContentValues values) {
        StringBuffer buffer = new StringBuffer();
        final Matcher matcher = _PLACEHOLDER_EXPR.matcher(_template);
        while (matcher.find()) {
            final String phName = matcher.group(1);
            String replacement = values.getAsString(phName);
            if (replacement == null)
                throw new IllegalArgumentException(
                        "Missing value for placeholder: " + phName);
            matcher.appendReplacement(buffer, replacement);
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    private final static Pattern _PLACEHOLDER_EXPR =
            Pattern.compile("\\$\\{(\\w+)\\}", Pattern.MULTILINE);
    private final CharSequence _template;
}

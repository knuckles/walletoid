/**
 * Copyright (c) 2013, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.TreeSet;

public class AssetContentProvider extends ContentProvider {
    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings2, String s2) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    @Override
    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        String fileName = uri.getLastPathSegment();

        if (fileName == null || !EXPORTED_RESOURCES.contains(fileName))
            throw new FileNotFoundException(fileName);

        final Resources resources = getContext().getResources();
        // Assets are not allowed in library projects, so we store those kind of things as raw resources.
        final int resId = resources.getIdentifier(fileName, "raw", getContext().getPackageName());
        return resources.openRawResourceFd(resId);
    }

    private final static Set<String> EXPORTED_RESOURCES = new TreeSet<String>() {{
        add("apache_20");
    }};
}

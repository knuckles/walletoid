/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.content.ContentValues;
import android.widget.Spinner;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.*;

public class Utils {
    private final static String DEFAULT_DELIM = ", ";

    public static String joinStrings(Object[] objects) {
        return joinStrings(DEFAULT_DELIM, objects);
    }

    public static String joinStrings(String delimiter, Object[] objects) {
        StringBuilder builder = new StringBuilder();
        boolean firstObject = true;
        for (Object object : objects) {
            if (firstObject)
                firstObject = false;
            else
                builder.append(delimiter);
            builder.append(object != null ? object.toString() : NULL);
        }
        return builder.toString();
    }

    public static String joinStrings(Iterable objects) {
        return joinStrings(DEFAULT_DELIM, objects);
    }

    public static String joinStrings(String delimiter, Iterable objects) {
        StringBuilder builder = new StringBuilder();
        boolean firstObject = true;
        for (Object object : objects) {
            if (firstObject)
                firstObject = false;
            else
                builder.append(delimiter);
            builder.append(object != null ? object.toString() : NULL);
        }
        return builder.toString();
    }

    public static String joinStrings(ContentValues values) {
        return joinStrings(DEFAULT_DELIM, values);
    }

    public static String joinStrings(String delimiter, ContentValues values) {
        StringBuilder builder = new StringBuilder();
        boolean firstObject = true;
        for (String k : values.keySet()) {
            if (firstObject)
                firstObject = false;
            else
                builder.append(delimiter);
            builder.append(k);
            builder.append("=");
            builder.append(values.getAsString(k));
        }
        return builder.toString();
    }

    public static <T> T[] concatArrays(T[]... args) {
        if (args.length < 1)
            return null;

        int totalLen = 0;
        for (T[] a : args) {
            totalLen += a.length;
        }

        @SuppressWarnings("unchecked")
        T[] result = (T[]) Array.newInstance(args[0].getClass().getComponentType(), totalLen);
        int filled = 0;
        for (T[] a : args) {
            System.arraycopy(a, 0, result, filled, a.length);
            filled += a.length;
        }

        return result;
    }

    public static <K, V> Map<K, V> mergeMaps(Map<K, V> m1, Map<K, V> m2) {
        Map<K, V> result = new TreeMap<>(m1);
        result.putAll(m2);
        return result;
    }

    public static <V> V head(Collection<V> c) {
        for (V v : c) {
            return v;
        }
        return null;
    }

    public static boolean setSpinnerSelectionToItemWithId(Spinner spinner, long id) {
        int idPos = 0;
        final int maxIndex = spinner.getCount() - 1;
        for (; idPos <= maxIndex; idPos++) {
            if (spinner.getItemIdAtPosition(idPos) == id)
                break;
        }

        if (idPos > maxIndex)
            return false;

        spinner.setSelection(idPos);
        return true;
    }

    public static int clampInt(int val, int min, int max) {
        return Math.max(min, Math.min(max, val));
    }

    public static float clampFloat(float val, float min, float max) {
        return Math.max(min, Math.min(max, val));
    }

    public static int valueOrder(double value) {
        return valueOrder(value, false);
    }

    public static int valueOrder(double value, boolean round) {
        value = Math.abs(value);
        if (value == 0.0f)
            return 0;
        final double logValue = Math.log10(value);
        final double result = round ? Math.round(logValue) : Math.floor(logValue);
        return (int) result;
    }

    public static byte[] uuidToBytes(final UUID uuid) {
        final ByteBuffer bb = ByteBuffer.allocate(16);
        bb.clear();
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        bb.flip();
        return bb.array().clone();  // ByteBuffer is shared across calls!
    }

    public static UUID bytesToUUID(final byte[] bytes) {
        if (bytes == null)
            return null;
        if (bytes.length != 16)
            throw new IllegalArgumentException();
        final ByteBuffer bb = ByteBuffer.allocate(16);
        bb.clear();
        bb.put(bytes);
        bb.flip();
        return new UUID(bb.getLong(), bb.getLong());
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String hexEncode(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static class ApproxValue {
        @Override
        public String toString() {
            return String.format(Locale.ROOT, "ApproxValue: %,.3f^%d (%f)", value, power, originalValue);
        }

        public ApproxValue(double origValue) {
            this(new BigDecimal(origValue));
        }

        public ApproxValue(BigDecimal origValue) {
            originalValue = origValue.floatValue();
            final int order = valueOrder(originalValue);
            final Map.Entry<Integer, String> orderEntry = valuePowers.lowerEntry(order + 1);
            if (orderEntry == null || orderEntry.getKey() == 1) {
                value = originalValue;
                power = 1;
                suffix = "";
                return;
            }
            power = orderEntry.getKey();
            suffix = orderEntry.getValue();
            value = origValue.scaleByPowerOfTen(-1 * power).floatValue();
        }

        public double magnifier() { return Math.pow(10, power); }

        public final float originalValue;
        public final float value;
        public final int power;
        public final String suffix;

        private static final TreeMap<Integer, String> valuePowers = new TreeMap<Integer, String>() {{
            put(0, "");
            put(3, "K");
            put(6, "M");
            put(9, "G");
        }};
    }

    private static final String NULL = "[null]";
}

/**
 * Copyright (c) 2016, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;
import ru.knuckles.walletoid.database.RecordDictionary;
import ru.knuckles.walletoid.database.WalletManager;

import java.io.File;
import java.lang.ref.WeakReference;

public class Walletoid extends Application {

    public static Walletoid getInstance() {
        return _instance;
    }

    public WalletManager getWalletManager() {
        return _walletManager;
    }

    // Application

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        _walletManager = new WalletManager(this);

        checkVersionUpgrade();

        try {
            RepeatedRecordsScheduler.setAlarms(this);
        }
        catch (Exception e) {
            Log.e(getClass().getSimpleName(),
                String.format("Error while rescheduling repeated records: %s", e.toString()));
            e.printStackTrace();
        }
    }

    // PRIVATE

    private void checkVersionUpgrade() {
        int currentVersionCode = INVALID_VERSION_CODE;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        final SharedPreferences prefs = getSharedPreferences(getString(R.string.app_id), MODE_PRIVATE);
        final int prevVersionCode = prefs.getInt(PREF_LAST_RUN_VERSION_CODE, INVALID_VERSION_CODE);

        try {
            if (currentVersionCode > prevVersionCode)
                upgradeFrom(prevVersionCode, currentVersionCode);
        }
        catch (Exception e) {
            Log.e(getClass().getSimpleName(),
                String.format("Error while running application upgrade from v. %d to v. %d: %s",
                    prevVersionCode, currentVersionCode, e.toString()));
            e.printStackTrace();
        }

        final SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putInt(PREF_LAST_RUN_VERSION_CODE, currentVersionCode);
        prefEditor.apply();
    }

    private void upgradeFrom(int prevVersionCode, int currentVersionCode) {
        if (prevVersionCode == 6) {
            // Rename record dictionary file to match main Wallet name.
            final File oldFile = getFileStreamPath("walletoid_dict.bin");
            if (!oldFile.exists())
                return;
            final File newFile = _walletManager.getDictPath(WalletManager.MAIN_DB_NAME);
            if (!oldFile.renameTo(newFile)) {
                Log.w(getClass().getSimpleName(), "Could not rename dictionary file");
            }
        }
        if (prevVersionCode >= 8 && prevVersionCode <= 19 &&
                _walletManager.walletExists(WalletManager.MAIN_DB_NAME)) {
            // Rev 468:60b33ee6b introduced the bug #72: dictionary might have been left unsaved.
            _walletManager.getWallet(WalletManager.MAIN_DB_NAME, false, true);
            final RecordDictionary dict =
                    _walletManager.getWalletDictionary(WalletManager.MAIN_DB_NAME);
            _dictObserver = new WeakReference<>(new RecordDictionary.Observer() {
                @Override
                public void onReadyStateChanged(final boolean ready) {
                    if (!_taskStarted) {
                        _taskStarted = true;
                        return;
                    }
                    _dictObserver.clear();
                    _walletManager.releaseWallet(WalletManager.MAIN_DB_NAME);
                }
                protected boolean _taskStarted = false;
            });
            dict.addObserver(_dictObserver);
            dict.rebuildFromWallet();
        }
    }

    private WeakReference<RecordDictionary.Observer> _dictObserver;

    private static Walletoid _instance;
    private static final int INVALID_VERSION_CODE = -1;
    private static final String PREF_LAST_RUN_VERSION_CODE = "app.last_version";

    private WalletManager _walletManager;
}

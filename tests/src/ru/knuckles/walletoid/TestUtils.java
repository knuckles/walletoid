/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import android.util.Log;
import ru.knuckles.walletoid.database.*;

import java.math.BigDecimal;

public class TestUtils {
    public static void dumpWallet(Wallet wallet) {
        for (Currency c : wallet.getCurrenciesList()) {
            Log.i("Wallet-" + wallet.getName(), c.toString());
        }

        for (Category c : wallet.getCategoriesList()) {
            Log.i("Wallet-" + wallet.getName(), c.toString());
        }

        for (Record r : wallet.getRecordsList(null, null)) {
            Log.i("Wallet-" + wallet.getName(), r.toString());
        }
    }

    public Currency mkRandomCurrency() {
        _curCntr++;
        return new Currency("currency_" + _curCntr, "S" + _curCntr, _curCntr);
    }

    public Currency mutateCurrency(Currency currency) {
        _curCntr++;
        return new Currency(
                currency.getId(),
                currency.getName() + "_mut_" + _curCntr,
                "S" + _curCntr,
                currency.getValue() + 1);
    }

    public Currency addRandomCurrency(Wallet wallet) {
        final Currency added = wallet.getCurrency(wallet.addCurrency(mkRandomCurrency()));
        return added;
    }

    public Category mkRandomCategory(boolean income) {
        _catCntr++;
        return new Category("category_" + _catCntr, _catCntr,
                income ? Category.Kind.INCOME : Category.Kind.EXPENSE);
    }

    public Category mutateCategory(Category category) {
        _catCntr++;
        return new Category(
                category.getId(),
                category.getName() + "_mut_" + _catCntr,
                _catCntr,
                category.getKind());
    }

    public Category addRandomCategory(boolean income, Wallet wallet) {
        final Category added = wallet.getCategory(wallet.addCategory(mkRandomCategory(income)));
        return added;
    }

    public Record mkRandomRecord(Category cat, Currency cur, float sum, Wallet wallet) {
        if (cat == null)
            cat = wallet.getCategory(WalletSchema_v7.CAT_ID_MISC_EXPENSE);
        if (cur == null)
            cur = wallet.getCurrency(WalletSchema_v7.DEF_CURRENCY_ID);
        return new Record(cat.getId(), System.currentTimeMillis(), new BigDecimal(sum), cur
                .getId(), "record_" + _recCntr++);
    }

    public Record addRandomRecord(Category cat, Currency cur, float sum, Wallet wallet) {
        final Record added =
                wallet.getRecord(wallet.addRecord(mkRandomRecord(cat, cur, sum, wallet)));
        Log.d(getClass().getSimpleName(), wallet.getName() + ": Added Record: " + added);
        return added;
    }

    private int _curCntr = 0;
    private int _catCntr = 0;
    private int _recCntr = 0;
}

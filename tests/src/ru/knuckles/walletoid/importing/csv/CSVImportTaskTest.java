/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.importing.csv;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.util.Pair;
import ru.knuckles.walletoid.database.*;
import ru.knuckles.walletoid.tests.R;

import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CSVImportTaskTest extends InstrumentationTestCase {
    private static final String WALLET_NAME = "testdb";
    private WalletManager _wm;
    private Wallet _importedWallet;
    private Exception _importError;

    public void setUp() throws Exception {
        super.setUp();
        _wm = new WalletManager(getInstrumentation().getTargetContext());
        _wm.deleteWalletData(WALLET_NAME);
        _importedWallet = _wm.getWallet(WALLET_NAME, false, false);
        _importedWallet.addCurrency(new Currency("Dollar", "$", 1));
    }

    public void tearDown() throws Exception {
        _wm.releaseWalletAndDeleteFile(WALLET_NAME);
        super.tearDown();
    }

    abstract class TestingTaskDelegate extends CSVImportTask.Delegate {
        protected int _value = 0;

        public int getLastProgressValue() {
            return _value;
        }
    }

    public void testBackgroundImport() throws Throwable {
        Context testContext = getInstrumentation().getContext();

        final CountDownLatch latch = new CountDownLatch(1);
        TestingTaskDelegate _taskDelegate = new TestingTaskDelegate() {
            @Override
            void onProgressUpdate(CSVImportTask task, int value) {
                _value = value;
            }

            @Override
            void onFinished(CSVImportTask task) {
                latch.countDown();
            }

            @Override
            void onError(CSVImportTask task, CSVImportTask.ImportResult errorCode) {
                latch.countDown();
                _importError = task.getException();
            }
        };

        final int NUM_RECORDS = 66;
        final Record.Sum TOTAL_EXPENSES = new Record.Sum(92188, WalletSchema_v4.DEF_CURRENCY_ID);
        final CSVImportTask _task = new CSVImportTask(
            _taskDelegate,
            new InputStreamReader(testContext.getResources().openRawResource(R.raw.test1)),
            _importedWallet,
            WalletSchema_v4.DEF_CURRENCY_ID);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                _task.execute();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

        // Test that the task basically finished successfully.
        if (_importError != null) {
            throw _importError;
        }

        // Imported data tests.

        List<Category> categories = _importedWallet.getCategoriesList(Category.Kind.ALL);
        assertNotNull(categories);
        // TODO: Map 'misc' category onto a predefined one.
        assertEquals(10, categories.size());  // 2 predefined + 8 imported.

        assertEquals(NUM_RECORDS, _taskDelegate.getLastProgressValue());

        List<Record> allRecords = _importedWallet.getRecordsList(null, null);
        assertEquals(NUM_RECORDS, allRecords.size());

        final Pair<Record.Sum, Record.Sum> totalSums = _importedWallet.getTotalSums(
            new WalletFilter(null, null, Category.Kind.EXPENSE, Category.UNKNOWN_ID));
        assertEquals(TOTAL_EXPENSES, totalSums.second);
    }
}

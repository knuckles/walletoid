/**
 * Copyright (c) 2015, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import junit.framework.TestCase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static java.util.Calendar.*;
import static ru.knuckles.walletoid.CalendarUtils.*;

public class CalendarUtilsTest extends TestCase {

    public void testTruncateCalendarFieldsUpTo() throws Exception {
        Calendar origin = Calendar.getInstance();
        Calendar calendar = (Calendar) origin.clone();
        truncateCalendarFieldsUntil(calendar, Calendar.YEAR);
        assertEquals(origin.get(Calendar.YEAR), calendar.get(Calendar.YEAR));
        assertEquals(0, calendar.get(Calendar.MONTH));
        assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(0, calendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(0, calendar.get(Calendar.MINUTE));
        assertEquals(0, calendar.get(Calendar.SECOND));
        assertEquals(0, calendar.get(Calendar.MILLISECOND));
    }


    private void checkFieldEquals(int field, Calendar cal1, Calendar cal2) {
        assertEquals(cal1.get(field), cal2.get(field));
    }
    private void checkSameDay(Calendar cal1, Calendar cal2) {
        checkFieldEquals(Calendar.YEAR, cal1, cal2);
        checkFieldEquals(Calendar.MONTH, cal1, cal2);
        checkFieldEquals(Calendar.DAY_OF_YEAR, cal1, cal2);
        checkFieldEquals(Calendar.DAY_OF_WEEK, cal1, cal2);
    }
    public void testClearTime() {
        final Calendar now = Calendar.getInstance();
        final Calendar test = (Calendar) now.clone();

        clearCalendarTime(test);
        checkSameDay(now, test);

        // Add some days to get away from potential week start, where days would match after truncating to week start.
        now.add(Calendar.DAY_OF_YEAR, 3);
        test.add(Calendar.DAY_OF_YEAR, 3);
        clearCalendarTime(test);
        checkSameDay(now, test);
    }

    private final static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm", Locale.getDefault());
    private void assertDatesEqual(long expectedTS, long actualTS) {
        if (actualTS != expectedTS)
            fail(String.format(Locale.ROOT, "Case failed. Expected: %s; actual: %s",
                    dateFormat.format(expectedTS), dateFormat.format(actualTS)));
    }

    private void checkRepetitionCase(long origTS, long afterTS, TimeInterval interval, long resultTS) {
        final Calendar actual = getNextRepetitionDate(Cal(origTS), Cal(afterTS), interval);
        assertDatesEqual(resultTS, actual.getTimeInMillis());
    }
    public void testRepetitionDates() {
        // Daily cases.
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 5), TimeInterval.DAY, TS(2017, JANUARY, 6));
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 30), TimeInterval.DAY, TS(2017, JANUARY, 31));
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 31), TimeInterval.DAY, TS(2017, FEBRUARY, 1));

        // Weekly cases.
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 5), TimeInterval.WEEK, TS(2017, JANUARY, 12));
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 12), TimeInterval.WEEK, TS(2017, JANUARY, 19));
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 19), TimeInterval.WEEK, TS(2017, JANUARY, 26));
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 26), TimeInterval.WEEK, TS(2017, FEBRUARY, 2));

        // Monthly cases
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 5), TimeInterval.MONTH, TS(2017, FEBRUARY, 5));
        checkRepetitionCase(TS(2017, JANUARY, 10), TS(2017, FEBRUARY, 15), TimeInterval.MONTH, TS(2017, MARCH, 10));
        checkRepetitionCase(TS(2016, DECEMBER, 30), TS(2017, JANUARY, 31), TimeInterval.MONTH, TS(2017, FEBRUARY, 28));
        checkRepetitionCase(TS(2017, JANUARY, 28), TS(2017, FEBRUARY, 10), TimeInterval.MONTH, TS(2017, FEBRUARY, 28));
        checkRepetitionCase(TS(2017, JANUARY, 29), TS(2017, FEBRUARY, 10), TimeInterval.MONTH, TS(2017, FEBRUARY, 28));
        checkRepetitionCase(TS(2017, JANUARY, 31), TS(2017, FEBRUARY, 10), TimeInterval.MONTH, TS(2017, FEBRUARY, 28));
        checkRepetitionCase(TS(2016, JANUARY, 30), TS(2016, FEBRUARY, 10), TimeInterval.MONTH, TS(2016, FEBRUARY, 29));
        checkRepetitionCase(TS(2016, JANUARY, 29), TS(2016, FEBRUARY, 10), TimeInterval.MONTH, TS(2016, FEBRUARY, 29));
        checkRepetitionCase(TS(2016, JANUARY, 28), TS(2016, FEBRUARY, 10), TimeInterval.MONTH, TS(2016, FEBRUARY, 28));

        // Yearly cases.
        checkRepetitionCase(TS(2017, JANUARY, 5), TS(2017, JANUARY, 5), TimeInterval.YEAR, TS(2018, JANUARY, 5));
        // Leap year
        checkRepetitionCase(TS(2016, FEBRUARY, 29), TS(2017, FEBRUARY, 5), TimeInterval.YEAR, TS(2017, FEBRUARY, 28));
        // Leap year
        checkRepetitionCase(TS(2016, FEBRUARY, 29), TS(2017, FEBRUARY, 28), TimeInterval.YEAR, TS(2018, FEBRUARY, 28));
    }

    private void checkIntervalBeginningCase(long testDate, TimeInterval interval, long resultTS) {
        final Calendar actual = Cal(testDate);
        calendarToIntervalBeginning(actual, interval);
        assertDatesEqual(resultTS, actual.getTimeInMillis());
    }
    public void testIntervalBeginnings() {
        checkIntervalBeginningCase(TS(2017, JULY, 15), TimeInterval.YEAR, TS(2017, JANUARY, 1));

        checkIntervalBeginningCase(TS(2017, JULY, 15), TimeInterval.MONTH, TS(2017, JULY, 1));
        checkIntervalBeginningCase(TS(2017, JULY, 1), TimeInterval.MONTH, TS(2017, JULY, 1));
        checkIntervalBeginningCase(TS(2017, FEBRUARY, 3), TimeInterval.MONTH, TS(2017, FEBRUARY, 1));

        // NOTE: Tests crafted for US locale!
        checkIntervalBeginningCase(TS(2017, JULY, 15), TimeInterval.WEEK, TS(2017, JULY, 9));
        checkIntervalBeginningCase(TS(2017, JULY, 9), TimeInterval.WEEK, TS(2017, JULY, 9));
        checkIntervalBeginningCase(TS(2017, JULY, 16), TimeInterval.WEEK, TS(2017, JULY, 16));
        checkIntervalBeginningCase(TS(2017, JULY, 17), TimeInterval.WEEK, TS(2017, JULY, 16));
        checkIntervalBeginningCase(TS(2017, JULY, 22), TimeInterval.WEEK, TS(2017, JULY, 16));
    }

    public void testDaysBetween() {
        assertEquals(0, daysBetween(Cal(TS(2017, FEBRUARY, 8, 11, 10)), Cal(TS(2017, FEBRUARY, 8, 23, 50))));
        assertEquals(1, daysBetween(Cal(TS(2017, FEBRUARY, 6, 11, 10)), Cal(TS(2017, FEBRUARY, 7, 23, 50))));
        assertEquals(1, daysBetween(Cal(TS(2017, FEBRUARY, 6, 23, 50)), Cal(TS(2017, FEBRUARY, 7, 1, 5))));
        assertEquals(-1, daysBetween(Cal(TS(2017, FEBRUARY, 7, 1, 5)), Cal(TS(2017, FEBRUARY, 6, 23, 50))));
        assertEquals(364, daysBetween(Cal(TS(2015, JANUARY, 1, 0, 0)), Cal(TS(2015, DECEMBER, 31, 23, 59))));
        assertEquals(365, daysBetween(Cal(TS(2016, JANUARY, 1, 0, 0)), Cal(TS(2016, DECEMBER, 31, 23, 59))));
        assertEquals(366, daysBetween(Cal(TS(2016, JANUARY, 1, 12, 0)), Cal(TS(2017, JANUARY, 1, 12, 0))));
    }
}

/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.database.sqlite.SQLiteConstraintException;
import android.test.InstrumentationTestCase;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class WalletSchemaTest extends InstrumentationTestCase {
    @Override
    public void setUp() throws Exception {
        super.setUp();
        final WalletManager wm = new WalletManager(getInstrumentation().getTargetContext());
        wm.deleteWalletData(getName());
        _wallet = wm.getWallet(getName(), true, false);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    static void doAllTests(Wallet wallet) {
        doTestCategories(wallet);
        doTestCurrencies(wallet);
        doTestConstraints(wallet);
    }

    public void testVersions() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
        InstantiationException {
        final Class[] schemas = {
            WalletSchema_v1.class,
            WalletSchema_v2.class,
            WalletSchema_v3.class,
            WalletSchema_v4.class,
            WalletSchema_v5.class,
            WalletSchema_v6.class,
            WalletSchema_v7.class,
        };

        int version = 1;
        for (Class schemaClass : schemas) {
            WalletDBHelper.WalletDBSchema schema =
                (WalletDBHelper.WalletDBSchema) schemaClass.getDeclaredConstructor().newInstance();
            assertEquals(version, schema.getVersion());
            version++;
        }
    }

    public void testCategories() throws Exception {
        doTestCategories(_wallet);
    }

    static void doTestCategories(Wallet wallet) {
        // We expect some predefined categories.
        final Map<Long, Category> incCategories = wallet.getCategoriesMap(Category.Kind.INCOME);
        assertEquals(3, incCategories.size());

        final Map<Long, Category> expCategories = wallet.getCategoriesMap(Category.Kind.EXPENSE);
        assertEquals(9, expCategories.size());

        Category miscExpense = wallet.getCategory(WalletSchema_v7.CAT_ID_MISC_EXPENSE);
        assertNotNull(miscExpense);
        assertTrue(miscExpense.getName().length() > 0);

        Category miscIncome = wallet.getCategory(WalletSchema_v7.CAT_ID_MISC_INCOME);
        assertNotNull(miscIncome);
        assertTrue(miscIncome.getName().length() > 0);
    }

    public void testCurrencies() throws Exception {
        doTestCurrencies(_wallet);
    }

    static void doTestCurrencies(Wallet wallet) {
        // We expect some predefined currencies.
        List<Currency> currencies = wallet.getCurrenciesList();
        assertTrue("We expect some predefined currencies", currencies.size() > 0);
        assertEquals(WalletSchema_v7.DEF_CURRENCY_ID, currencies.get(0).getId());
    }

    public void testConstraints() throws Exception {
        doTestConstraints(_wallet);
    }

    static void doTestConstraints(Wallet wallet) {
        // Built-in categories can't be removed.
        Exception dbError = null;
        try {
            wallet.deleteCategory(WalletSchema_v7.CAT_ID_MISC_EXPENSE);
        }
        catch (SQLiteConstraintException e) {
            dbError = e;
        }
        assertNotNull("We should have get an exception: can't delete \"misc\" category", dbError);

        dbError = null;
        try {
            wallet.deleteCategory(WalletSchema_v7.CAT_ID_MISC_INCOME);
        }
        catch (SQLiteConstraintException e) {
            dbError = e;
        }
        assertNotNull("We should have get an exception: can't delete \"misc\" category", dbError);


        // Now add a record to a category, we will try to delete later.
        Currency currency = wallet.getCurrenciesList().get(0);
        final long cat1Id = wallet.addCategory(new Category(0, Category.Kind.EXPENSE));
        long r1 = wallet.addRecord(
            new Record(cat1Id, System.currentTimeMillis(), new BigDecimal(100), currency.getId(), "descr"));

        dbError = null;
        try {
            wallet.deleteCategory(cat1Id);
        }
        catch (SQLiteConstraintException e) {
            dbError = e;
        }
        // We should have get an exception: can't delete a category in use.
        assertNotNull(dbError);

        dbError = null;
        try {
            wallet.deleteCurrency(currency.getId());
        }
        catch (SQLiteConstraintException e) {
            dbError = e;
        }
        // We should have get an exception: can't delete a currency in use.
        assertNotNull(dbError);
    }

    private Wallet _wallet;
}

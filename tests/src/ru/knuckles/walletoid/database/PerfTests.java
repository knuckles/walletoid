package ru.knuckles.walletoid.database;

import android.test.InstrumentationTestCase;
import android.util.Log;
import android.util.Pair;
import ru.knuckles.walletoid.CalendarUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class PerfTests extends InstrumentationTestCase {
    @Override
    public void setUp() throws Exception {
        super.setUp();
        _walletManager = new WalletManager(getInstrumentation().getTargetContext());
    }

    @Override
    public void tearDown() throws Exception {
        _walletManager.closeAndDeleteAll();
        super.tearDown();
    }

    // Note: ZZZ in the name here is used to make this test appear last in the run queue.
    public void testZZZPerf() throws Exception {
        final int fillMonths = 6;
        final long maxMedianTimeMs = 30;

        _walletManager.deleteWalletData(getName());
        final Wallet wallet = _walletManager.getWallet(getName(), true, false);
        randomFillWallet(wallet, fillMonths);

        Calendar begin = CalendarUtils.getCurrentMonthStart();
        begin.add(Calendar.MONTH, -fillMonths);
        Calendar currMonth = (Calendar) begin.clone();

        long times[] = new long[fillMonths];
        for (int mi = 0; mi < fillMonths; ++mi) {
            Calendar endMonth = (Calendar) currMonth.clone();
            endMonth.add(Calendar.MONTH, 1);

            long startTime = System.nanoTime();
            final Pair<Record.Sum, Record.Sum> sums = wallet.getTotalSums(new WalletFilter(currMonth, endMonth));
            long durationMs = (System.nanoTime() - startTime) / 1000000;
            times[mi] = durationMs;
            currMonth = endMonth;
        }

        Arrays.sort(times);
        final int l = times.length;
        final long medianTime = l % 2 == 1 ?
                                times[(l - 1) / 2] :
                                (times[l / 2] + times[l / 2 - 1]) / 2;
        assertTrue(String.format("Median time exceeds %dms, it's %dms", maxMedianTimeMs, medianTime),
            medianTime < maxMedianTimeMs);
        Log.d(this.getClass().getSimpleName(), String.format("Wallet.getTotalSums median time: %dms", medianTime));
    }

    private void randomFillWallet(Wallet wallet, int fillMonths) {
        final int incomeCats = 5;
        final int expenseCats = 5;
        final int recsPerMonth = 2000;
        final int linksPerMonth = 100;
        final int minSum = 1;
        final int maxSum = 10000;

        List<Long> incomeCatIds = new ArrayList<>();
        for (int ci = 0; ci < incomeCats; ++ci) {
            incomeCatIds.add(wallet.addCategory(
                new Category(String.format("Income %d", ci), 0, Category.Kind.INCOME)));
        }

        List<Long> expenseCatIds = new ArrayList<>();
        for (int ci = 0; ci < expenseCats; ++ci) {
            expenseCatIds.add(wallet.addCategory(
                new Category(String.format("Expense %d", ci), 0, Category.Kind.EXPENSE)));
        }

        Calendar begin = CalendarUtils.getCurrentMonthStart();
        begin.add(Calendar.MONTH, -fillMonths);
        Calendar currMonth = (Calendar) begin.clone();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        final long defaultCurrencyId = wallet.getCurrenciesList().get(0).getId();

        wallet.beginTransaction();
        try {
            // Loop through months
            for (int mi = 0; mi < fillMonths; ++mi) {
                final long minTs = currMonth.getTimeInMillis();
                currMonth.add(Calendar.MONTH, 1);
                final long maxTs = currMonth.getTimeInMillis();
                Log.d(this.getClass().getSimpleName(), String.format("Filling month %d", mi + 1));
                List<Long> incRecIds = new ArrayList<>();
                List<Long> expRecIds = new ArrayList<>();

                // Add records to curernt month.
                for (int ri = 0; ri < recsPerMonth; ++ri) {
                    // Add either income or expense record.
                    final boolean isIncome = ri % 2 == 0;
                    List<Long> catIds = isIncome ? incomeCatIds : expenseCatIds;
                    List<Long> destIdsList = isIncome ? incRecIds : expRecIds;
                    final long lastAddedId = wallet.addRecord(new Record(
                        catIds.get(random.nextInt(0, catIds.size())),
                        random.nextLong(minTs, maxTs),
                        new Record.Sum(random.nextDouble(minSum, maxSum), defaultCurrencyId)));
                    destIdsList.add(lastAddedId);

                    // Link added record to random existing record.
                    final boolean mkLink = linksPerMonth > 0 && ri > 0 && ri % (recsPerMonth / linksPerMonth) == 0;
                    if (mkLink) {
                        final List<Long> masterIdsList = isIncome ? expRecIds : incRecIds;
                        wallet.linkRecords(lastAddedId, masterIdsList.get(random.nextInt(0, masterIdsList.size())));
                    }
                }
            }
            wallet.transactionSucceeded();
        }
        finally {
            wallet.endTransaction();
        }
    }

    private WalletManager _walletManager;
}

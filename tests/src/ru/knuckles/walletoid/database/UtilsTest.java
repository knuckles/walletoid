package ru.knuckles.walletoid.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.test.InstrumentationTestCase;

import java.io.File;
import java.util.TreeMap;

public class UtilsTest extends InstrumentationTestCase {
    private SQLiteDatabase _db;
    private static String tableName = "test";

    @Override
    public void setUp() throws Exception {
        super.setUp();
        File dbFile = getInstrumentation().getTargetContext().getDatabasePath("testdb");
        dbFile.getParentFile().mkdirs();
        if (dbFile.exists())
            dbFile.delete();
        _db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
        try {
            _db.execSQL("CREATE TABLE " + tableName + " (name TEXT);");
            SQLiteStatement s = _db.compileStatement("INSERT INTO " + tableName + " (name) VALUES (?);");

            s.bindString(1, "joe");
            s.executeInsert();
            s.bindString(1, "donny");
            s.executeInsert();
            s.bindString(1, "claire");
            s.executeInsert();
            s.bindString(1, "bill");
            s.executeInsert();
            s.bindString(1, "harry");
            s.executeInsert();
        }
        catch (Exception e) {
            _db.close();
            throw e;
        }
    }

    @Override
    public void tearDown() throws Exception {
        closeAndDeleteDb();
        super.tearDown();
    }

    private void closeAndDeleteDb() {
        if (_db != null) {
            _db.close();
            (new File(_db.getPath())).delete();
        }
    }

    public void testGetTableBiggestRowId() throws Exception {
        assertEquals(5, Utils.getTableBiggestRowId(_db, tableName, "rowid"));
    }

    public void testShiftRowIds() throws Exception {
        Utils.shiftRowIds(_db, tableName, "rowid", 3);
        assertEquals(8, Utils.getTableBiggestRowId(_db, tableName, "rowid"));

        Utils.shiftRowIds(_db, tableName, "rowid", 30);
        assertEquals(38, Utils.getTableBiggestRowId(_db, tableName, "rowid"));
    }

    public void testRemapColumnValues() throws Exception {
        Utils.remapColumnValues(_db, tableName, "name", new TreeMap<String, String>() {{
            put("donny", "sunny");
            put("harry", "ben");
        }});
        assertEquals("sunny", getName(2));
        assertEquals("ben", getName(5));
    }

    private String getName(long id) {
        if (_getStmt == null) {
            _getStmt = _db.compileStatement("SELECT name FROM " + tableName + " WHERE rowid = ?");
        }
        _getStmt.bindLong(1, id);
        return _getStmt.simpleQueryForString();
    }
    private SQLiteStatement _getStmt = null;
}

/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.InstrumentationTestCase;

import java.io.File;

public class WalletDBHelperTest extends InstrumentationTestCase {
    private final String dbName = "testdb";

    public void setUp() throws Exception {
        super.setUp();
        deleteDBFile();
    }

    public void tearDown() throws Exception {
        deleteDBFile();
        super.tearDown();
    }

    private void deleteDBFile() {
        final File dbFile = getInstrumentation().getTargetContext().getDatabasePath(dbName);
        if (dbFile.exists() && !dbFile.delete())
            throw new AssertionError();
    }

    public void testUpgrades() {
        final Context context = getInstrumentation().getTargetContext();
        final int latestVersion = WalletDBHelper.schemasCount();
        for (int version = 1; version <= latestVersion; version++) {
            final WalletDBHelper helper = new WalletDBHelper(context, dbName, version, true);
            final SQLiteDatabase db = helper.getReadableDatabase();
            try {
                if (version == latestVersion) {
                    // Do some basic sanity checks.
                    WalletSchemaTest.doAllTests(new Wallet(context, dbName, db));
                }
            }
            finally {
                helper.close();
            }
        }
    }
}

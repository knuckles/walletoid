/*
 * Copyright (c) 2017, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.database;

import android.graphics.Color;
import android.os.Looper;
import android.os.MessageQueue;
import android.test.InstrumentationTestCase;
import android.util.Pair;
import ru.knuckles.walletoid.TestUtils;

import java.math.BigDecimal;
import java.util.*;

import static ru.knuckles.walletoid.CalendarUtils.*;

public class WalletTest extends InstrumentationTestCase {
    @Override
    public void setUp() throws Exception {
        super.setUp();
        _walletManager = new WalletManager(getInstrumentation().getTargetContext());
        _bulkCatSetEvents.clear();
        _bulkCurSetEvents.clear();
        _bulkRecRemovalEvents.clear();
        _walletManager.closeAndDeleteAll();
        // In case something's left from previous crashed run.
        _walletManager.deleteWalletData(getName());
    }

    @Override
    public void tearDown() throws Exception {
        _walletManager.closeAndDeleteAll();
        super.tearDown();
    }

    public void testCategories() throws Exception {
        Wallet wallet = _walletManager.getWallet(getName(), false, false);

        String testName1 = "cat1";
        assertFalse(wallet.hasCategoryNamed(testName1));
        wallet.addCategory(new Category(testName1, 0, Category.Kind.EXPENSE));
        assertTrue(wallet.hasCategoryNamed(testName1));
        Category cat1 = wallet.findCategoryByName(testName1);
        assertNotNull(cat1);
        assertEquals(testName1, cat1.getName());
        assertFalse(cat1.getKind().isIncome());
        assertEquals(0, cat1.getColor());

        assertEquals(cat1, wallet.getCategory(cat1.getId()));

        String testName2 = "cat2";
        wallet.addCategory(new Category(testName2, 1, Category.Kind.INCOME));
        Category cat2 = wallet.findCategoryByName(testName2);
        assertNotNull(cat2);
        assertNotNull(cat2);
        assertEquals(testName2, cat2.getName());
        assertTrue(cat2.getKind().isIncome());
        assertEquals(1, cat2.getColor());

        assertEquals(cat2, wallet.getCategory(cat2.getId()));

        assertFalse(cat1.equals(cat2));

        List<Category> list = wallet.getCategoriesList(Category.Kind.ALL);
        assertEquals(4, list.size());  // 2 'misc' categories + 2 manually added.
        assertTrue(list.indexOf(cat1) > -1);
        assertTrue(list.indexOf(cat2) > -1);

        Category randCat = _tu.addRandomCategory(false, wallet);
        final Category randCatUpd = new Category(_tu.mkRandomCategory(false), randCat.getId());
        wallet.updateCategory(randCatUpd);
        randCat = wallet.getCategory(randCatUpd.getId());
        assertTrue(randCat.similarTo(randCatUpd));  // Test internal cache was updated.
    }

    public void testCurrencies() throws Exception {
        Wallet wallet = _walletManager.getWallet(getName(), false, false);

        Currency randCurr = _tu.addRandomCurrency(wallet);

        final Currency randCurrUpd = new Currency(_tu.mkRandomCurrency(), randCurr.getId());
        wallet.updateCurrency(randCurrUpd);
        randCurr = wallet.getCurrency(randCurrUpd.getId());
        assertTrue(randCurr.similarTo(randCurrUpd));  // Test internal cache was updated.
    }

    public void testRecords() throws Exception {
        final Wallet wallet = _walletManager.getWallet(getName(), true, true);
        final RecordDictionary dict = _walletManager.getWalletDictionary(wallet.getName());

        final long btcid = wallet.addCurrency(new Currency("Bitcoin", "B", 500));
        Currency bitcoin = wallet.getCurrency(btcid);
        final Currency shitcoin = wallet.getCurrency(wallet.addCurrency(
                new Currency("Shitcoin", "S", 0.1)));

        final Pair<Record.Sum, Record.Sum> totalsBefore = wallet.getTotalSums(null);
        assertEquals(0, totalsBefore.first.amount.intValueExact());
        assertEquals(0, totalsBefore.second.amount.intValueExact());

        List<Record> initialRecs =
            wallet.getRecordsList(null, null);
        assertEquals(0, initialRecs.size());
        assertNull(wallet.getMostRecentRecord());

        // Prepare some records.
        Record r1t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            System.currentTimeMillis(),
            new Record.Sum(100, WalletSchema_v4.DEF_CURRENCY_ID),
            "foo");
        Record r2t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            System.currentTimeMillis(),
            new Record.Sum(2.5, bitcoin.getId()),
            "bar");
        r2t.getCalendar().add(Calendar.DAY_OF_MONTH, 1);
        Record r3t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            System.currentTimeMillis(),
            new Record.Sum(3000, shitcoin.getId()),
            "baz");
        r3t.getCalendar().add(Calendar.DAY_OF_MONTH, 2);

        // Make sure the dictionary is empty.
        assertEquals(
                Record.UNKNOWN_ID,
                dict.guessCategory(Category.Kind.EXPENSE, r1t.getDescr()));
        assertEquals(
                Record.UNKNOWN_ID,
                dict.guessCategory(Category.Kind.EXPENSE, r2t.getDescr()));
        assertEquals(
                Record.UNKNOWN_ID,
                dict.guessCategory(Category.Kind.EXPENSE, r3t.getDescr()));

        // Insert them.
        long r1id = wallet.addRecord(r1t);
        long r2id = wallet.addRecord(r2t);
        long r3id = wallet.addRecord(r3t);
        assertTrue(r2id != r1id);
        assertTrue(r3id != r2id && r3id != r1id);

        // Check record category guessing.
        waitForMainThreadMessages();
        assertEquals(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                dict.guessCategory(Category.Kind.EXPENSE, r1t.getDescr()));
        assertEquals(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                dict.guessCategory(Category.Kind.EXPENSE, r2t.getDescr()));
        assertEquals(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                dict.guessCategory(Category.Kind.EXPENSE, r3t.getDescr()));

        final WalletFilter miscExpensesFilter = new WalletFilter(
                null,
                null,
                Category.Kind.EXPENSE,
                WalletSchema_v2.CAT_ID_MISC_EXPENSE);
        // List test.
        List<Record> recs = wallet.getRecordsList(miscExpensesFilter, null);
        assertEquals(3, recs.size());
        assertEquals(0, recs.indexOf(wallet.getRecord(r1id)));
        assertEquals(1, recs.indexOf(wallet.getRecord(r2id)));
        assertEquals(2, recs.indexOf(wallet.getRecord(r3id)));

        // Same but in different currency.
        final float EPSILON = 0.001f;
        recs = wallet.getRecordsList(miscExpensesFilter, shitcoin);
        assertEquals(3, recs.size());
        assertEquals(
                r1t.getSum().amount.floatValue() / shitcoin.getValue(),
                recs.get(0).getSum().amount.floatValue(),
                EPSILON);
        assertEquals(
                r2t.getSum().amount.floatValue() * bitcoin.getValue() / shitcoin.getValue(),
                recs.get(1).getSum().amount.floatValue(),
                EPSILON);
        assertEquals(
                r3t.getSum().amount.floatValue(),
                recs.get(2).getSum().amount.floatValue(),
                EPSILON);

        // Full-text search test.
        final WalletFilter ftsFilter = new WalletFilter(miscExpensesFilter);
        ftsFilter.setQuery("ba*");
        recs = wallet.getRecordsList(ftsFilter, null);
        assertEquals(2, recs.size());
        assertTrue(r2t.similarTo(recs.get(0)));
        assertTrue(r3t.similarTo(recs.get(1)));

        // Update test.
        Record r1 = wallet.getRecord(r1id);
        assertTrue(r1t.similarTo(r1));
        r1.setSum(new Record.Sum(400, WalletSchema_v4.DEF_CURRENCY_ID));
        wallet.updateRecord(r1);
        recs = wallet.getRecordsList(miscExpensesFilter, null);
        assertEquals(3, recs.size());
        assertEquals(r1, wallet.getRecord(r1id));

        assertTrue(r2t.similarTo(wallet.getRecord(r2id)));
        assertTrue(r3t.similarTo(wallet.getRecord(r3id)));

        // Sum test.
        final Pair<Record.Sum, Record.Sum> totalsAfter = wallet.getTotalSums(null);
        assertEquals(new Record.Sum(0, WalletSchema_v4.DEF_CURRENCY_ID), totalsAfter.first);
        assertEquals(new Record.Sum(400 + 1250 + 300, WalletSchema_v4.DEF_CURRENCY_ID),
                totalsAfter.second);

        // Recency test.
        assertEquals(wallet.getRecord(r3id), wallet.getMostRecentRecord());

        // Duplication test.
        final Record r1dup = wallet.getRecord(wallet.addRecord(r1.makeRepeated(0)));
        assertTrue(r1.visAttributesMatch(r1dup));
        recs = wallet.getRecordsList(miscExpensesFilter, null);
        assertEquals(4, recs.size());

        // Repetition search test.
        final List<Record> similar =
                wallet.findRepetitionsInCurrentPeriod(r1.getId(), TimeInterval.MONTH);
        assertEquals(1, similar.size());
        assertTrue(r1.visAttributesMatch(similar.get(0)));
    }

    public void testLinks() throws Exception {
        final Wallet wallet = _walletManager.getWallet(getName(), true, false);

        // Just one income category.
        final long cat1id = wallet.addCategory(new Category(0, Category.Kind.INCOME));

        long bitcoinId = wallet.addCurrency(new Currency("Bitcoin", "B", 500));
        long shitcoinId = wallet.addCurrency(new Currency("Shitcoin", "S", 0.1));

        // Prepare some records.
        Record r1t = new Record(
            cat1id,
            System.currentTimeMillis(),
            new BigDecimal(100),
            WalletSchema_v4.DEF_CURRENCY_ID,
            "descr1");
        Calendar plus2Months = (Calendar) r1t.getCalendar().clone();
        plus2Months.add(Calendar.MONTH, 2);
        Record r2t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            plus2Months.getTimeInMillis(),
            new BigDecimal(2),
            bitcoinId,
            "descr2");
        Record r3t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            plus2Months.getTimeInMillis(),
            new BigDecimal(3000),
            shitcoinId,
            "descr3");

        // Now add records
        long r1 = wallet.addRecord(r1t);
        long r2 = wallet.addRecord(r2t);
        long r3 = wallet.addRecord(r3t);
        // ... and link them together
        wallet.linkRecords(r2, r1);
        wallet.linkRecords(r3, r1);

        // Linked records keep their original currencies.
        assertEquals(WalletSchema_v4.DEF_CURRENCY_ID, wallet.getRecord(r1).getCurrencyID());
        assertEquals(bitcoinId, wallet.getRecord(r2).getCurrencyID());
        assertEquals(shitcoinId, wallet.getRecord(r3).getCurrencyID());

        // Check that links are visible.
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r2));
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r3));
        assertTrue(wallet.hasSlaveRecords(r1));
        List<Record> slaves = wallet.findSlaveRecords(r1);
        assertNotNull(slaves);
        assertEquals(2, slaves.size());
        assertEquals(wallet.getRecord(r2), slaves.get(0));
        assertEquals(wallet.getRecord(r3), slaves.get(1));
        assertEquals(new Record.Sum(1300, WalletSchema_v4.DEF_CURRENCY_ID),
                     wallet.getRecord(r1).getSlavesSum());
        assertEquals(new Record.Sum(1400, WalletSchema_v4.DEF_CURRENCY_ID),
            wallet.getRecord(r1).getEffectiveSum());

        // Summing master records only must also include sum of their slaves.
        Calendar monthStart = (Calendar) r1t.getCalendar().clone();
        setCalendarToMonthStart(monthStart);
        Calendar monthEnd = (Calendar) monthStart.clone();
        monthEnd.add(Calendar.MONTH, 1);
        final Pair<Record.Sum, Record.Sum> totalSums = wallet.getTotalSums(
            new WalletFilter(monthStart, monthEnd, Category.Kind.ALL, Category.UNKNOWN_ID));
        assertEquals(new Record.Sum(1400, WalletSchema_v4.DEF_CURRENCY_ID), totalSums.first);

        // Check that updating the master record won't break the link (as REPLACE would do).
        Record rec1 = wallet.getRecord(r1);
        rec1.setSum(new Record.Sum(400, WalletSchema_v4.DEF_CURRENCY_ID));
        wallet.updateRecord(rec1);
        assertEquals(rec1, wallet.findMasterRecord(r2));
        assertEquals(rec1, wallet.findMasterRecord(r3));

        // Unlinking.
        assertTrue(wallet.unlinkRecord(r2));
        assertNull(wallet.findMasterRecord(r2));
        assertTrue(wallet.hasSlaveRecords(r1));
        assertEquals(1, wallet.findSlaveRecords(r1).size());
        assertNotNull(wallet.findMasterRecord(r3));

        // After removing master record all its links are destroyed.
        wallet.removeRecord(r1);
        assertNull(wallet.findMasterRecord(r2));
        assertNull(wallet.findMasterRecord(r3));
        assertFalse(wallet.hasSlaveRecords(r1));
        assertEquals(0, wallet.findSlaveRecords(r1).size());

        // Repeat previous case, but remove one slave record and leave the master.
        r1 = wallet.addRecord(r1t);
        wallet.linkRecords(r2, r1);
        wallet.linkRecords(r3, r1);
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r2));
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r3));
        wallet.removeRecord(r2);
        assertNull(wallet.findMasterRecord(r2));
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r3));
        assertTrue(wallet.hasSlaveRecords(r1));
        slaves = wallet.findSlaveRecords(r1);
        assertNotNull(slaves);
        assertEquals(1, slaves.size());
        assertEquals(wallet.getRecord(r3), slaves.get(0));

        // Relinking
        r2 = wallet.addRecord(r2t);
        wallet.linkRecords(r2, r1);
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r2));
        wallet.linkRecords(r2, r1);  // Existing link, should be no problem.
        assertEquals(wallet.getRecord(r1), wallet.findMasterRecord(r2));
        long r4 = wallet.addRecord(r1t);
        wallet.linkRecords(r2, r4);
        assertEquals(wallet.getRecord(r4), wallet.findMasterRecord(r2));
    }

    // TODO: Move to SyncTest
    public void testCopyFrom() throws Exception {
        final String testWalletName = "test_copy_from";
        final String tempWalletName = "test_copy_from_temporary";
        _walletManager.deleteWalletData(testWalletName);
        _walletManager.deleteWalletData(tempWalletName);
        Wallet testWallet = _walletManager.getWallet(testWalletName, false, false);
        try {
            final Category coolCat = testWallet.getCategory(testWallet.addCategory(
                    new Category("cool", 0, Category.Kind.EXPENSE)));
            final long dollar = testWallet.addCurrency(new Currency(1, "USD", "$", 1));

            Wallet tempWallet = _walletManager.getWallet(tempWalletName, false, false);
            try {
                final Category cat1 = tempWallet.getCategory(tempWallet.addCategory(
                        new Category("other cat 1", 0, Category.Kind.EXPENSE)));
                final Category cat2 = tempWallet.getCategory(tempWallet.addCategory(
                        new Category("other cat 2", 0, Category.Kind.INCOME)));
                final long curr1 = tempWallet.addCurrency(new Currency(33, "taller", "T", 1));

                final long r1 = tempWallet.addRecord(new Record(
                    cat1.getId(),
                    TS(2012, 3, 6),
                    new BigDecimal(2000),
                    curr1,
                    "expense X"
                ));
                final long r2 = tempWallet.addRecord(new Record(
                    cat2.getId(),
                    TS(2012, 3, 7),
                    new BigDecimal(4000),
                    curr1,
                    "income Y"
                ));
                tempWallet.linkRecords(r2, r1);

                Map<Long, Long> catMapping = new TreeMap<Long, Long>() {{
                    // 'cat2' records will be assigned to 'coolCat'.
                    put(cat2.getId(), coolCat.getId());
                }};

                Map<Long, Long> currMapping = new TreeMap<Long, Long>() {{
                    // Map the only new currency to something by default.
                    put(curr1, dollar);
                }};

                testWallet.copyFrom(tempWallet, catMapping, currMapping);

                List<Category> allCats = testWallet.getCategoriesList(Category.Kind.ALL);
                assertEquals(allCats.toString(), 5, allCats.size());
                assertTrue(testWallet.hasCategoryNamed(coolCat.getName()));
                assertTrue(testWallet.hasCategoryNamed(cat1.getName()));
                assertTrue(testWallet.hasCategoryNamed(cat2.getName()));

                List<Record> allRecs = testWallet.getRecordsList(null, null);
                assertEquals(2, allRecs.size());
                // This record category was mapped to 'coolCat'.
                assertEquals(coolCat.getId(), allRecs.get(1).getCatID());

                // Check that records are linked.
                assertFalse(allRecs.get(0).getSlavesSum().amount.equals(BigDecimal.ZERO));
                assertEquals(allRecs.get(1).getSum(), allRecs.get(0).getSlavesSum());
            }
            finally {
                _walletManager.releaseWalletAndDeleteFile(tempWalletName);
            }
        }
        finally {
            _walletManager.releaseWalletAndDeleteFile(testWalletName);
        }
    }

    public void testGrouping() throws Exception {
        final double SIGMA = 0.0001;
        final Wallet wallet = _walletManager.getWallet(getName(), true, false);
        Currency defCurr = wallet.getCurrency(WalletSchema_v4.DEF_CURRENCY_ID);
        assertTrue(defCurr.getValue() > 0);
        final Currency bitcoin = wallet.getCurrency(wallet.addCurrency(
                new Currency("Bitcoin", "B", 500)));
        final long crapCat = wallet.addCategory(
                new Category("crap", Color.BLUE, Category.Kind.EXPENSE));

        // Prepare 3 groups of records, dated with an interval of 1 month.
        // Month 1. Same category but different currencies.
        Record t11 = new Record(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                getCurrentYearStart().getTimeInMillis(),
                new BigDecimal(100),
                WalletSchema_v4.DEF_CURRENCY_ID,
                "misc1");
        Record t12 = new Record(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                t11.getTimestamp(),
                new BigDecimal(2),
                bitcoin.getId(),
                "misc2");

        // Month 2. Different categories and different currencies.
        Calendar plusMonth = t12.getCalendar();
        plusMonth.add(Calendar.MONTH, 1);
        Record t21 = new Record(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                plusMonth.getTimeInMillis(),
                new BigDecimal(200),
                WalletSchema_v4.DEF_CURRENCY_ID,
                "misc3");
        Record t22 = new Record(
                crapCat,
                t21.getTimestamp(),
                new BigDecimal(1),
                bitcoin.getId(),
                "crap1");

        // Month 3. Same category and same currency. Will be used twice.
        Calendar plusAnotherMonth = (Calendar) plusMonth.clone();
        plusAnotherMonth.add(Calendar.MONTH, 1);
        Record t31 = new Record(
                crapCat,
                plusAnotherMonth.getTimeInMillis(),
                new BigDecimal(3),
                bitcoin.getId(),
                "crap2");


        // Insert them.
        wallet.addRecord(t11);
        wallet.addRecord(t12);

        wallet.addRecord(t21);
        wallet.addRecord(t22);

        wallet.addRecord(t31);
        wallet.addRecord(t31);

        // Prepare filter, which spans whole year and should cover all added records.
        Calendar yearStart = (Calendar) t11.getCalendar().clone();
        setCalendarToYearStart(yearStart);
        Calendar yearEnd = (Calendar) yearStart.clone();
        yearEnd.add(Calendar.YEAR, 1);
        WalletFilter filter = new WalletFilter(yearStart, yearEnd, Category.Kind.EXPENSE);

        List<Record> all = wallet.getRecordsList(new WalletFilter(yearStart, yearEnd), null);

        // Get records grouped by month and category.
        final List<Record> recs =
                wallet.groupByDateAndCategory(filter, TimeInterval.MONTH, bitcoin);
        // 3 different months touched, but second month has 2 categories.
        assertEquals(4, recs.size());

        // Check group sums and currencies.

        // Month 1.
        final Record g0 = recs.get(0);
        assertEquals(WalletSchema_v2.CAT_ID_MISC_EXPENSE, g0.getCatID());
        assertEquals(100.0 / 500.0 + 2, g0.getSum().amount.floatValue(), SIGMA);
        assertEquals(bitcoin.getId(), g0.getSum().currencyId);

        // Month 2.
        final Record g1 = recs.get(1);
        assertEquals(crapCat, g1.getCatID());
        assertEquals(1.0, g1.getSum().amount.floatValue(), SIGMA);
        assertEquals(bitcoin.getId(), g1.getSum().currencyId);

        final Record g2 = recs.get(2);
        assertEquals(WalletSchema_v2.CAT_ID_MISC_EXPENSE, g2.getCatID());
        assertEquals(200.0 / 500.0, g2.getSum().amount.floatValue(), SIGMA);
        assertEquals(bitcoin.getId(), g2.getSum().currencyId);

        // Month 3.
        final Record g3 = recs.get(3);
        assertEquals(crapCat, g3.getCatID());
        assertEquals(3 + 3, g3.getSum().amount.floatValue(), SIGMA);
        assertEquals(bitcoin.getId(), g3.getSum().currencyId);
    }

    public void testRanges() throws Exception {
        final Wallet wallet = _walletManager.getWallet(getName(), true, false);
        wallet.events.addBulkObserver(_blkObserver);

        final long incomeCat = wallet.addCategory(new Category(0, Category.Kind.INCOME));

        Calendar date15 = Calendar.getInstance();
        date15.set(Calendar.DAY_OF_MONTH, 15);
        Calendar date10 = (Calendar) date15.clone();
        date10.set(Calendar.DAY_OF_MONTH, 10);

        // First record.
        final Record r1t = new Record(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                date15.getTimeInMillis(),
                new Record.Sum(100, WalletSchema_v4.DEF_CURRENCY_ID),
                "foo");
        long r1id = wallet.addRecord(r1t);

        // Added later, but appears earlier if sorted by date.
        final Record r2t = new Record(
                WalletSchema_v2.CAT_ID_MISC_EXPENSE,
                date10.getTimeInMillis(),
                new Record.Sum(100, WalletSchema_v4.DEF_CURRENCY_ID),
                "bar");
        long r2id = wallet.addRecord(r2t);

        // Income record.
        long r3id = wallet.addRecord(new Record(
                incomeCat,
                date10.getTimeInMillis(),
                new Record.Sum(1000, WalletSchema_v4.DEF_CURRENCY_ID),
                "baz"));

        // Create a filter, that covers whole month containing all records.
        Calendar monthStart = getMonthStart(date10);
        Calendar monthEnd = (Calendar) monthStart.clone();
        monthEnd.add(Calendar.MONTH, 1);
        WalletFilter filter = new WalletFilter(monthStart, monthEnd);

        Pair<Record.Sum, Record.Sum> totals = wallet.getTotalSums(filter);
        assertEquals(1000, totals.first.amount.intValueExact());
        assertEquals(200, totals.second.amount.intValueExact());

        // This should include record |r1| only, as it comes third with current |filter|.
        filter.setLimitOffset(1, 2);

        // Test updating.
        long updated = wallet.updateRecords(filter, incomeCat, WalletSchema_v4.DEF_CURRENCY_ID);
        waitForMainThreadMessages();

        assertEquals(1, updated);
        final Record r1u = wallet.getRecord(r1id);
        assertNotNull(r1u);
        assertEquals(incomeCat, r1u.getCatID());
        assertEquals(WalletSchema_v4.DEF_CURRENCY_ID, r1u.getCurrencyID());
        assertEquals(1, _bulkCatSetEvents.size());
        assertEquals(1, _bulkCatSetEvents.get(0).first.size());
        assertTrue(r1t.similarTo(_bulkCatSetEvents.get(0).first.get(0)));
        assertEquals(incomeCat, _bulkCatSetEvents.get(0).second.longValue());

        assertEquals(1, _bulkCurSetEvents.size());
        assertEquals(1, _bulkCurSetEvents.get(0).first.size());
        assertTrue(r1t.similarTo(_bulkCurSetEvents.get(0).first.get(0)));
        assertEquals(WalletSchema_v4.DEF_CURRENCY_ID, _bulkCurSetEvents.get(0).second.longValue());

        // Test updating when filtering by description.
        filter.setQuery("ba*");
        filter.setLimitOffset(1, 1);  // Will touch record r3.
        updated = wallet.updateRecords(filter,
                WalletSchema_v7.CAT_ID_MISC_EXPENSE,
                WalletSchema_v4.DEF_CURRENCY_ID);
        waitForMainThreadMessages();

        assertEquals(1, updated);
        assertEquals(2, _bulkCatSetEvents.size());
        assertEquals(1, _bulkCatSetEvents.get(1).first.size());
        assertEquals(r3id, (long) _bulkCatSetEvents.get(1).first.get(0).getId());
        final Record r3u = wallet.getRecord(r3id);
        assertNotNull(r3u);
        assertEquals(WalletSchema_v7.CAT_ID_MISC_EXPENSE, r3u.getCatID());


        // Test removal.
        filter.setQuery(null);
        filter.setLimitOffset(1, 2);
        final Record r1 = wallet.getRecord(r1id);
        final long removed = wallet.removeRecords(filter);

        assertEquals(1, removed);
        assertNull(wallet.findRecord(r1id));
        assertNotNull(wallet.findRecord(r2id));
        assertEquals(1, _bulkRecRemovalEvents.size());
        assertTrue(r1.similarTo(_bulkRecRemovalEvents.get(0).get(0)));
    }

    public void testCurrencyValues() {
        final double EPSILON = 0.0001;
        final Wallet wallet = _walletManager.getWallet(getName(), true, false);

        // Just one income category.
        final long cat1id = wallet.addCategory(new Category(0, Category.Kind.INCOME));

        // Prepare currencies.
        Currency bitcoins = wallet.getCurrency(wallet.addCurrency(
                new Currency("Bitcoin", "B", 500)));
        Currency shitcoins = wallet.getCurrency(wallet.addCurrency(
                new Currency("Shitcoin", "S", 0.01)));

        // Prepare some records.

        // Expense record #1.
        final Calendar cal = Calendar.getInstance();
        final Record r1t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            cal.getTimeInMillis(),
            new Record.Sum(100, WalletSchema_v4.DEF_CURRENCY_ID),
            "r1");

        // Income record.
        cal.add(Calendar.DAY_OF_MONTH, 1);
        final Record r2t = new Record(
            cat1id,
            cal.getTimeInMillis(),
            new Record.Sum(2, bitcoins.getId()),
            "r2");

        // Expense record #2.
        cal.add(Calendar.DAY_OF_MONTH, 1);
        final Record r3t = new Record(
            WalletSchema_v2.CAT_ID_MISC_EXPENSE,
            cal.getTimeInMillis(),
            new Record.Sum(3000, shitcoins.getId()),
            "r3");
        assertTrue(r3t.getCalendar().getTimeInMillis() > r1t.getCalendar().getTimeInMillis());

        // Insert them.
        long r1id = wallet.addRecord(r1t);
        long r2id = wallet.addRecord(r2t);
        long r3id = wallet.addRecord(r3t);
        // And link together.
        wallet.linkRecords(r2id, r1id);
        wallet.linkRecords(r3id, r2id);

        // Expected aggregated sums expressed in default currency value.
        final double incomesInDefaultCurrency =
                r2t.getSum().amount.floatValue() * bitcoins.getValue() +  // Own r2 sum.
                r3t.getSum().amount.floatValue() * shitcoins.getValue();  // Sum of its slave record
        final double expensesInDefaultCurrency =
                r1t.getSum().amount.floatValue() +  // Own r1 sum.
                incomesInDefaultCurrency +  // Sum of its slave records recursively (r2 and r3).
                r3t.getSum().amount.floatValue() * shitcoins.getValue();  // Sum of r3.

        // Empty filter.
        final WalletFilter filter1 = new WalletFilter();
        Pair<Record.Sum, Record.Sum> inDefCurr = wallet.getTotalSums(filter1);
        assertEquals(WalletSchema_v4.DEF_CURRENCY_ID, inDefCurr.first.currencyId);
        assertEquals(WalletSchema_v4.DEF_CURRENCY_ID, inDefCurr.second.currencyId);
        assertEquals(incomesInDefaultCurrency, inDefCurr.first.amount.floatValue(), EPSILON);
        assertEquals(expensesInDefaultCurrency, inDefCurr.second.amount.floatValue(), EPSILON);

        // Filter with time range covering all records.
        final WalletFilter filter2 = new WalletFilter(filter1);
        filter2.setDateRange(
                r1t.getCalendar().getTimeInMillis() - 1,
                r3t.getCalendar().getTimeInMillis() + 1);
        Pair<Record.Sum, Record.Sum> inBtc = wallet.getTotalSums(filter2, bitcoins);
        assertEquals(bitcoins.getId(), inBtc.first.currencyId);
        assertEquals(bitcoins.getId(), inBtc.second.currencyId);
        assertEquals(incomesInDefaultCurrency / bitcoins.getValue(),
                inBtc.first.amount.floatValue(), EPSILON);
        assertEquals(expensesInDefaultCurrency / bitcoins.getValue(),
                inBtc.second.amount.floatValue(), EPSILON);

        // Same, but skipping the first record.
        final WalletFilter filter3 = new WalletFilter(filter2);
        filter3.setLimitOffset(2, 1);  // Skip first record.
        Pair<Record.Sum, Record.Sum> inShc = wallet.getTotalSums(filter3, shitcoins);
        assertEquals(shitcoins.getId(), inShc.first.currencyId);
        assertEquals(shitcoins.getId(), inShc.second.currencyId);
        assertEquals(incomesInDefaultCurrency / shitcoins.getValue(),
                inShc.first.amount.floatValue(), EPSILON);
        assertEquals(r3t.getSum().amount.floatValue(), inShc.second.amount.floatValue(), EPSILON);

        // Same, but restricting category to "MISC".
        final WalletFilter filter4 = new WalletFilter(filter3);
        filter4.setCategory(Category.Kind.ALL, WalletSchema_v2.CAT_ID_MISC_EXPENSE);
        Pair<Record.Sum, Record.Sum> inBtc2 = wallet.getTotalSums(filter4, bitcoins);
        assertEquals(bitcoins.getId(), inBtc2.first.currencyId);
        assertEquals(bitcoins.getId(), inBtc2.second.currencyId);
        assertEquals(0, inBtc2.first.amount.floatValue(), EPSILON);
        assertEquals(
                r3t.getSum().amount.floatValue() * shitcoins.getValue() / bitcoins.getValue(),
                inBtc2.second.amount.floatValue(), EPSILON);
    }

    // Wait for Wallet event notifications to reach us.
    private void waitForMainThreadMessages() throws InterruptedException {
        final MessageQueue mainQ = Looper.getMainLooper().getQueue();
        while (!mainQ.isIdle()) {
            Thread.sleep(50);
        }
    }

    private final List<Pair<List<Record>, Long>> _bulkCatSetEvents = new ArrayList<>();
    private final List<Pair<List<Record>, Long>> _bulkCurSetEvents = new ArrayList<>();
    private final List<List<Record>> _bulkRecRemovalEvents = new ArrayList<>();

    private WalletEvents.BulkObserver _blkObserver = new WalletEvents.BulkObserver() {
        public void onBulkCategorySet(List<Record> records, long newCatId) {
            _bulkCatSetEvents.add(new Pair<>(records, newCatId));
        }
        public void onBulkCurrencySet(List<Record> records, long newCurrencyId) {
            _bulkCurSetEvents.add(new Pair<>(records, newCurrencyId));
        }
        public void onBulkRecordsRemoval(List<Record> records) {
            _bulkRecRemovalEvents.add(records);
        }
        public void onCurrenciesUpdated() {}
        public void onCopyCompleted() {}
    };

    private TestUtils _tu = new TestUtils();
    private WalletManager _walletManager;
}

/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import junit.framework.TestCase;
import ru.knuckles.walletoid.database.Record;

import java.math.BigDecimal;

import static ru.knuckles.walletoid.CalendarUtils.*;

public class RecordTest extends TestCase {
    public void testProperties() throws Exception {
        Record rec = new Record(42, 8, TS(2012, 4, 6), new BigDecimal(300), 3, "Test");
        assertEquals(42, rec.getId());
        assertEquals(8, rec.getCatID());
        assertEquals(3, rec.getCurrencyID());
        assertEquals(new BigDecimal(300), rec.getSum().amount);
        assertEquals("Test", rec.getDescr());
    }

    public void testEquals() throws Exception {
        Record rec1 = new Record(42, 8, TS(2012, 4, 6), new BigDecimal(300), 3, "Test");
        Record rec2 = new Record(42, 8, TS(2012, 4, 6), new BigDecimal(300), 3, "Test");
        Record rec3 = new Record(84, 8, TS(2012, 4, 6), new BigDecimal(300), 3, "Test");
        assertEquals(rec1, rec2);
        assertFalse(rec1.equals(rec3));
        assertTrue(rec1.similarTo(rec3));
        assertTrue(rec3.similarTo(rec2));

        Object o1 = rec1;
        Object o2 = rec2;
        assertTrue(o1.equals(o2));
        assertTrue(rec1.equals(o2));
    }
}

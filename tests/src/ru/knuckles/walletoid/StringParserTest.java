/**
 * Copyright (c) 2013, The Walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid;

import junit.framework.TestCase;
import ru.knuckles.walletoid.database.Category;
import ru.knuckles.walletoid.database.Record;
import ru.knuckles.walletoid.importing.csv.StringParser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import static ru.knuckles.walletoid.importing.csv.StringParser.*;

public class StringParserTest extends TestCase {

    private class FakeParserDelegate implements Delegate {
        private long _nextCatId = 0;
        private Map<String, Category> _categoriesMap = new TreeMap<String, Category>();

        @Override
        public long getCategoryID(String name, boolean sumIsNegative) {
            if (_categoriesMap.containsKey(name)) {
                return _categoriesMap.get(name).getId();
            }

            Category.Kind kind = sumIsNegative ? Category.Kind.EXPENSE : Category.Kind.INCOME;
            Category recCategory = new Category(_nextCatId, name, 0, kind);
            _categoriesMap.put(name, recCategory);
            _nextCatId++;
            return recCategory.getId();
        }

        public Map<String, Category> getGatheredCategories() {
            return new TreeMap<String, Category>(_categoriesMap);
        }
    }

    public void testGuessFormat() throws Exception {
        String[] testCSV = {"10.09.12", "test category", "-3500", "test descr"};
        String[] testCSVCopy = testCSV.clone();
        StringParser converter = guessFormat(testCSV, new FakeParserDelegate(), 1);
        assertEquals(
            new TreeMap<FieldType, Integer>() {{
                put(FieldType.FIELD_DATE, 0);
                put(FieldType.FIELD_CATEGORY, 1);
                put(FieldType.FIELD_SUM, 2);
                put(FieldType.FIELD_DESCRIPTION, 3);
            }},
            converter.getFieldMapping());

        // StringParser.guessFormat must not modify original strings.
        assertTrue(Arrays.deepEquals(testCSV, testCSVCopy));
    }

    public void testConversion() throws Exception {
        String[][] testData = {
            {"02.10.12", "misc", "-55", "Batteries"},
            {"03.10.12", "misc", "-46", "Batteries"},
            {"03.10.12", "food", "-133", "Lemons, bread, tea"},
            {"04.10.12", "entertainment", "-1700", "Bar"},
            {"05.10.12", "salary", "54000", "For September"}
        };

        long[] dates = new long[5];
        // NOTE: Calendar counts months starting from 0! So, 9 is October.
        dates[0] = CalendarUtils.TS(2012, 9, 2);
        dates[1] = CalendarUtils.TS(2012, 9, 3);
        dates[2] = CalendarUtils.TS(2012, 9, 3);
        dates[3] = CalendarUtils.TS(2012, 9, 4);
        dates[4] = CalendarUtils.TS(2012, 9, 5);

        final long currencyId = 3;
        Record[] referenceRecords = {
            new Record(0, dates[0], new BigDecimal("55"), currencyId, "Batteries"),
            new Record(0, dates[1], new BigDecimal("46"), currencyId, "Batteries"),
            new Record(1, dates[2], new BigDecimal("133"), currencyId, "Lemons, bread, tea"),
            new Record(2, dates[3], new BigDecimal("1700"), currencyId, "Bar"),
            new Record(3, dates[4], new BigDecimal("54000"), currencyId, "For September")
        };

        FakeParserDelegate parserDelegate = new FakeParserDelegate();
        StringParser converter = guessFormat(testData[0], parserDelegate, currencyId);
        for (int i = 0; i < testData.length; i++) {
            Record record = converter.convertFromStrings(testData[i]);
            assertEquals(referenceRecords[i], record);
        }

        TreeMap<String, Category> referenceCategories = new TreeMap<String, Category> () {{
            put("misc", new Category(0L, "misc", 0, Category.Kind.EXPENSE));
            put("food", new Category(1L, "food", 0, Category.Kind.EXPENSE));
            put("entertainment", new Category(2L, "entertainment", 0, Category.Kind.EXPENSE));
            put("salary", new Category(3L, "salary", 0, Category.Kind.INCOME));
        }};
        Map<String, Category> gatheredCategories = parserDelegate.getGatheredCategories();
        assertEquals(referenceCategories, gatheredCategories);
    }

    public void testParseErrors() throws Exception {
        String[][] testData = {
            {"02.10.12", "misc", "-55 not a number", "Batteries"},
            {"03.10.12", "misc", "-46", null},
            {"03.10.12", "", "-133", ""},
            {"04.10.", "entertainment", "-1700", "Bar"},
            {"", "", "", ""}
        };

        for (String[] datum : testData) {
            try {
                guessFormat(datum, new FakeParserDelegate(), 1);
                assertTrue("Must throw", false);
            }
            catch (Exception e) {
                assertEquals(ParseException.class, e.getClass());
            }
        }
    }
}

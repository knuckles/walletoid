package ru.knuckles.walletoid.arithmetic_evaluator;

import junit.framework.TestCase;

public class EvaluatorTest extends TestCase {
    private static double sigma = 0.0001;

    public void testSimpleOperators() throws Exception {
        assertEquals(5, Evaluator.evaluateString("3+2").intValueExact(), sigma);
        assertEquals(1, Evaluator.evaluateString("3-2").intValueExact(), sigma);
        assertEquals(6, Evaluator.evaluateString("3*2").intValueExact(), sigma);
        assertEquals(1.5, Evaluator.evaluateString("3/2").floatValue(), sigma);
    }

    public void testCompoundExpression() throws Exception {
        assertEquals(1000, Evaluator.evaluateString("200 + 400*2").intValueExact());
    }

    public void testLongExpression() throws Exception {
        assertEquals(3.555, Evaluator.evaluateString(ScannerTest.expression).floatValue(), sigma);
    }

    public void testMoreExotics() throws Exception {
        assertEquals(5000, Evaluator.evaluateString("100 * 50 / 2 * 3 / 6 * 4").floatValue(), sigma);
    }

    public void testSingleConstant() throws Exception {
        Expression e = Evaluator.parseString("100");
        assertNotNull(e);
        assertEquals(100, e.getValue().intValueExact());
        assertSame(Expression.Constant.class, e.getClass());
    }
}
/**
 * Copyright (c) 2014, The walletoid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package ru.knuckles.walletoid.arithmetic_evaluator;

import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;

public class ScannerTest extends TestCase {

    public void testScan() throws Exception {
        List<Scanner.Lexemme> lexemmes = Scanner.scan(expression);
        assertNotNull(lexemmes);
        assertEquals(expectedLexemmes, lexemmes);
    }

    static final String expression = "((10 + 2* 3-.5) /4) - 0.32";

    static List<Scanner.Lexemme> expectedLexemmes = new LinkedList<Scanner.Lexemme>() {{
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.PAREN,     "(",    0));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.PAREN,     "(",    1));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    "10",   2));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.OPERATOR,  "+",    5));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    "2",    7));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.OPERATOR,  "*",    8));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    "3",    10));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.OPERATOR,  "-",    11));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    ".5",   12));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.PAREN,     ")",    14));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.OPERATOR,  "/",    16));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    "4",    17));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.PAREN,     ")",    18));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.OPERATOR,  "-",    20));
        add(new Scanner.Lexemme(Scanner.Lexemme.Type.NUMBER,    "0.32", 22));
    }};
}
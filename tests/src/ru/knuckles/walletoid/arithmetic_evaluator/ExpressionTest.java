package ru.knuckles.walletoid.arithmetic_evaluator;

import junit.framework.TestCase;

import java.math.BigDecimal;

public class ExpressionTest extends TestCase {
    public void testConstant() throws Exception {
        BigDecimal testValue = new BigDecimal(3.57889011);
        Expression constant = new Expression.Constant(testValue);
        assertSame(testValue, constant.getValue());
    }

    public void testCompoundBasics() {
        Expression.CompoundExpression e = new Expression.CompoundExpression();
        assertNull(e.operator);
        assertNotNull(e.operands);
        assertEquals(0, e.operands.size());
    }

    static class TestCase {
        public final double result;
        public final String op;
        public final double[] operands;

        TestCase(double operands[], String op, double result) {
            this.result = result;
            this.op = op;
            this.operands = operands;
        }
    }

    static double numbers[] = new double[]{1.53, 2.47, 0.5};
    static TestCase cases[] = {
        new TestCase(numbers, "+", 4.5),
        new TestCase(numbers, "-", -1.44),
        new TestCase(numbers, "*", 1.88955),
        new TestCase(numbers, "/", 1.23886639676),
    };

    public void testPlus() throws Exception {
        testOperator(cases[0]);
    }

    public void testMinus() throws Exception {
        testOperator(cases[1]);
    }

    public void testMultiplication() throws Exception {
        testOperator(cases[2]);
    }

    public void testDivision() throws Exception {
        testOperator(cases[3]);
    }

    private void testOperator(TestCase testCase) throws Exception {
        Expression.CompoundExpression expr = new Expression.CompoundExpression();
        expr.operator = Expression.OPERATOR_MAP.get(testCase.op);
        for (double operand : testCase.operands)
            expr.operands.add(new Expression.Constant(new BigDecimal(operand)));
        assertEquals(testCase.result, expr.getValue().floatValue(), 0.0001);
    }
}